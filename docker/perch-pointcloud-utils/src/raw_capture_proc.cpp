#include "boost/filesystem.hpp"
#include <fstream>
#include <pcl/conversions.h>
#include <pcl/io/auto_io.h>
#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>
#include <pcl/surface/organized_fast_mesh.h>

#include <pcl/features/normal_3d.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/point_types.h>
#include <pcl/surface/gp3.h>

#include <cmath>
#include <string.h>

pcl::PolygonMesh pcd2mesh(pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud) {

  // Normal estimation*
  pcl::NormalEstimation<pcl::PointXYZRGB, pcl::Normal> n;
  pcl::PointCloud<pcl::Normal>::Ptr normals(new pcl::PointCloud<pcl::Normal>);
  pcl::search::KdTree<pcl::PointXYZRGB>::Ptr tree(
      new pcl::search::KdTree<pcl::PointXYZRGB>);
  tree->setInputCloud(cloud);
  n.setInputCloud(cloud);
  n.setSearchMethod(tree);
  n.setKSearch(20);
  n.compute(*normals);
  //* normals should not contain the point normals + surface curvatures

  // Concatenate the XYZ and normal fields*
  pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr cloud_with_normals(
      new pcl::PointCloud<pcl::PointXYZRGBNormal>);
  pcl::concatenateFields(*cloud, *normals, *cloud_with_normals);
  //* cloud_with_normals = cloud + normals

  // Create search tree*
  pcl::search::KdTree<pcl::PointXYZRGBNormal>::Ptr tree2(
      new pcl::search::KdTree<pcl::PointXYZRGBNormal>);
  tree2->setInputCloud(cloud_with_normals);

  // Initialize objects
  pcl::GreedyProjectionTriangulation<pcl::PointXYZRGBNormal> gp3;
  pcl::PolygonMesh triangles;

  // Set the maximum distance between connected points (maximum edge length)
  gp3.setSearchRadius(1);

  // Set typical values for the parameters
  gp3.setMu(2.5);
  gp3.setMaximumNearestNeighbors(100);
  gp3.setMaximumSurfaceAngle(M_PI / 4); // 45 degrees
  gp3.setMinimumAngle(M_PI / 18);       // 10 degrees
  gp3.setMaximumAngle(2 * M_PI / 3);    // 120 degrees
  gp3.setNormalConsistency(false);

  // Get result
  gp3.setInputCloud(cloud_with_normals);
  gp3.setSearchMethod(tree2);
  gp3.reconstruct(triangles);

  // Additional vertex information
  std::vector<int> parts = gp3.getPartIDs();
  std::vector<int> states = gp3.getPointStates();

  // save file

  // Finish return triangles;
  return triangles;
}

int main(int argc, char **argv) {
  std::cout << "Hello" << std::endl;
  std::string log_folder;
  std::string output_folder;
  if (argc < 5) {
    std::cout << "Usage raw_capture_proc <log_folder> <output_folder> "
                 "<success-thresh> <center-thresh-m> overwrite (y|n)>"
              << std::endl;
    return -1;
  }
  log_folder = std::string(argv[1]);
  output_folder = std::string(argv[2]);
  double success_thresh = strtof(argv[3], NULL);
  double center_thresh_m = strtof(argv[4], NULL);

  bool overwrite;
  if (*argv[5] == 'y' || *argv[5] == 'Y') {
    overwrite = true;
  } else if (*argv[5] == 'n' || *argv[5] == 'N') {
    overwrite = false;
  } else {
    std::cout << "Overwrite has to be y|n, you have put: '" << argv[5] << "'"
              << std::endl;
    return -1;
  }

  boost::filesystem::path log_folder_path = boost::filesystem::path(log_folder);
  std::string grasp_success = "";
  bool skip_note_check = false;
  if (log_folder_path.string().find("ng_bad") != std::string::npos) {
    skip_note_check = true;
    grasp_success = "fail";
  }
  if (log_folder_path.string().find("ng_good") != std::string::npos) {
    skip_note_check = true;
    grasp_success = "success";
  }

  /**
   * 1. Find the notes file and determine if this was a successful grasp
   * location.
   */
  if (!skip_note_check) {
    boost::filesystem::recursive_directory_iterator end;
    for (boost::filesystem::recursive_directory_iterator it(log_folder);
         it != end; ++it) {
      if (it->path().extension() == ".txt") {
        // Is there a slash in the file?
        bool found_slash = false;
        {
          std::ifstream file_notes(it->path().string());
          if (file_notes.is_open()) {
            std::string line;
            while (std::getline(file_notes, line)) {
              if (line.find("/") != std::string::npos) {
                found_slash = true;
                break;
              }
            }
          } else {
            std::cout << "Error opening notes file!" << std::endl;
            return -1;
          }
        }

        // look for success or otherwise in the first line.
        std::ifstream file_notes(it->path().string());
        if (file_notes.is_open()) {
          std::string line;
          std::getline(file_notes, line);
          std::size_t found = line.find("success");
          if (found == 0) {
            grasp_success = "success";
          } else {
            found = line.find("fail");
            if (found == 0) {
              grasp_success = "fail";
            } else {
              grasp_success = "undefined";
            }
          }
        }

        /*
         *
         * only success binary success/fail in the first line, in case there's
        need to use actual success float, uncoment this block
         *
         *
        std::cout << "found slash" << std::endl;
        // Rewind the file and determine success.
        std::ifstream file_notes(it->path().string());
        if (file_notes.is_open()) {
          std::string line;
          while (std::getline(file_notes, line)) {
            std::size_t found = line.find("/");
            if (found != std::string::npos) {
              // Convert left and right into number.
              int success_count = atoi(&(line[0]));
              int total_count = atoi(&(line[2]));
              double success_rate = success_count / (total_count + 0.0);
              std::cout << "success_rate: " << success_rate << std::endl;
              std::cout << "success thresh: " << success_thresh << std::endl;
              if (success_rate > success_thresh) {
                grasp_success = "success";
              } else if (success_rate < success_thresh) {
                grasp_success = "fail";
              } else {
                grasp_success = "undefined";
              }
            }
          }
        }
        */
      }
    }
  }

  /**
   * 2. Loop through all pcds and create the meshes.
   */

  boost::filesystem::recursive_directory_iterator end;
  for (boost::filesystem::recursive_directory_iterator it(log_folder);
       it != end; ++it) {
    if (it->path().extension() == ".pcd") {
      std::string pcd_filename = it->path().stem().string();

      // Create new folder
      std::string out_path_folder = output_folder + "/" +
                                    log_folder_path.leaf().string() + "/" +
                                    pcd_filename;

      if (boost::filesystem::exists(out_path_folder + "/annotated.ply")) {
        if (overwrite) {
          boost::filesystem::remove(out_path_folder + "/*");
          std::cout << "Overwritting " << out_path_folder << "." << std::endl;
        } else {
          std::cout << "Skipping " << out_path_folder << ", already exists."
                    << std::endl;
          continue;
        }
      } else {
        std::cout << "Processing " << out_path_folder << "." << std::endl;
      }

      boost::filesystem::create_directories(
          boost::filesystem::path(out_path_folder));

      // Copy the pcd to the new folder
      // std::string orig_pcd = out_path_folder + "/original.pcd";
      // boost::filesystem::copy_file(it->path(),
      // boost::filesystem::path(orig_pcd));

      // Copy notes.
      if (!skip_note_check) {
        std::string out_notes = out_path_folder + "/notes.txt";
        boost::filesystem::copy_file(
            boost::filesystem::path(log_folder + "/notes.txt"),
            boost::filesystem::path(out_path_folder + "/notes.txt"),
            boost::filesystem::copy_option::overwrite_if_exists);
      }

      pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_in(
          new pcl::PointCloud<pcl::PointXYZRGB>);
      if (pcl::io::loadPCDFile(it->path().string(), *cloud_in) == -1) {
        PCL_ERROR("Couldn't read pcd file! \n");
        return (-1);
      }

      if (!cloud_in->isOrganized()) {

        pcl::PolygonMesh mesh = pcd2mesh(cloud_in);
        // Save mesh to see what it looks like.
        pcl::io::savePLYFileBinary(out_path_folder + "/original.ply", mesh);

        // Corlor all vertices within 2cm of grasp point red or green.
        // Take a MxN patch at the center.

        for (int i = 0; i < cloud_in->points.size(); i++) {
          pcl::PointXYZRGB *point = &cloud_in->points[i];
          float x = point->x;
          float y = point->y;
          float z = point->z;

          if (isnan(point->z)) {
            continue;
          }

          float dist = std::pow(x, 2) + std::pow(y, 2);
          dist = std::sqrt(dist);
          if (dist < center_thresh_m) {
            if (dist > center_thresh_m * 0.8) {
              if (grasp_success == "success") {
                point->r = 0;
                point->g = 255;
                point->b = 0;
              } else if (grasp_success == "fail") {
                point->r = 255;
                point->g = 0;
                point->b = 0;
              } else {
                point->r = 0;
                point->g = 0;
                point->b = 255;
              }
            }
          }
        }

        // std::string annotated_pcd = out_path_folder + "/annotated.pcd";
        // pcl::io::savePCDFileBinary(annotated_pcd, *cloud_in;

        mesh = pcd2mesh(cloud_in);
        // Save mesh to see what it looks like.
        pcl::io::savePLYFileBinary(out_path_folder + "/annotated.ply", mesh);
      } else {

        // Create a mesh.
        pcl::OrganizedFastMesh<pcl::PointXYZRGB> ofm;

        ofm.setInputCloud(cloud_in);
        ofm.setMaxEdgeLength(1.0);
        ofm.setTrianglePixelSize(1);
        ofm.setTriangulationType(
            pcl::OrganizedFastMesh<pcl::PointXYZRGB>::TRIANGLE_ADAPTIVE_CUT);
        pcl::PolygonMesh mesh;
        ofm.reconstruct(mesh);

        pcl::PointCloud<pcl::PointXYZRGB> cloud_result;
        pcl::fromPCLPointCloud2(mesh.cloud, cloud_result);

        uint32_t center_row = cloud_result.height / 2;
        uint32_t center_col = cloud_result.width / 2;

        // Save mesh to see what it looks like.
        pcl::io::savePLYFileBinary(out_path_folder + "/original.ply", mesh);

        pcl::PointXYZRGB center_point = cloud_result.at(center_col, center_row);

        // Corlor all vertices within 2cm of grasp point red or green.
        uint32_t row_samples =
            center_thresh_m * 1e4; // Not sure what the scaling is here....
                                   // But for 2cm, 200 seemed good.
        uint32_t col_samples = center_thresh_m * 1e4;
        uint32_t nan_count = 0;
        // Take a MxN patch at the center.
        for (uint32_t i = center_row - (row_samples / 2);
             i < center_row + (row_samples / 2); i++) {
          for (uint32_t j = center_col - (col_samples / 2);
               j < center_col + (col_samples / 2); j++) {
            pcl::PointXYZRGB &point = cloud_result.at(j, i);
            double diff_x = point.x - center_point.x;
            double diff_y = point.y - center_point.y;
            double diff_z = point.z - center_point.z;
            if (isnan(point.z)) {
              continue;
            }

            // This is in meters.
            float dist = std::pow(diff_x, 2) + std::pow(diff_y, 2);
            dist = std::sqrt(dist);
            if (dist < center_thresh_m) {
              if (dist > center_thresh_m * 0.8) {
                if (grasp_success == "success") {
                  point.r = 0;
                  point.g = 255;
                  point.b = 0;
                } else if (grasp_success == "fail") {
                  point.r = 255;
                  point.g = 0;
                  point.b = 0;
                } else {
                  point.r = 0;
                  point.g = 0;
                  point.b = 255;
                }
              }
            }
          }
        }

        // Set cloud to colored.
        pcl::toPCLPointCloud2(cloud_result, mesh.cloud);

        // Save mesh to see what it looks like.
        pcl::io::savePLYFileBinary(out_path_folder + "/annotated.ply", mesh);
      }
    }
  }
}
