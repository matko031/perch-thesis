#include "boost/filesystem.hpp"
#include <fstream>
#include <pcl/conversions.h>
#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>
#include <pcl/io/auto_io.h>
#include <pcl/surface/organized_fast_mesh.h>

#include <pcl/point_types.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/features/normal_3d.h>
#include <pcl/surface/gp3.h>

#include <string.h>
#include <cmath>


pcl::PolygonMesh pcd2mesh (pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud )
{

    // Normal estimation*
    pcl::NormalEstimation<pcl::PointXYZRGB, pcl::Normal> n;
    pcl::PointCloud<pcl::Normal>::Ptr normals (new pcl::PointCloud<pcl::Normal>);
    pcl::search::KdTree<pcl::PointXYZRGB>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZRGB>);
    tree->setInputCloud (cloud);
    n.setInputCloud (cloud);
    n.setSearchMethod (tree);
    n.setKSearch (20);
    n.compute (*normals);
    //* normals should not contain the point normals + surface curvatures

    // Concatenate the XYZ and normal fields*
    pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr cloud_with_normals (new pcl::PointCloud<pcl::PointXYZRGBNormal>);
    pcl::concatenateFields (*cloud, *normals, *cloud_with_normals);
    //* cloud_with_normals = cloud + normals

    // Create search tree*
    pcl::search::KdTree<pcl::PointXYZRGBNormal>::Ptr tree2 (new pcl::search::KdTree<pcl::PointXYZRGBNormal>);
    tree2->setInputCloud (cloud_with_normals);

    // Initialize objects
    pcl::GreedyProjectionTriangulation<pcl::PointXYZRGBNormal> gp3;
    pcl::PolygonMesh triangles;

    // Set the maximum distance between connected points (maximum edge length)
    gp3.setSearchRadius (0.025);

    // Set typical values for the parameters
    gp3.setMu (2.5);
    gp3.setMaximumNearestNeighbors (100);
    gp3.setMaximumSurfaceAngle(M_PI/4); // 45 degrees
    gp3.setMinimumAngle(M_PI/18); // 10 degrees
    gp3.setMaximumAngle(2*M_PI/3); // 120 degrees
    gp3.setNormalConsistency(false);

    // Get result
    gp3.setInputCloud (cloud_with_normals);
    gp3.setSearchMethod (tree2);
    gp3.reconstruct (triangles);

    // Additional vertex information
    std::vector<int> parts = gp3.getPartIDs();
    std::vector<int> states = gp3.getPointStates();

    // save file

    // Finish
    return triangles;
}


int main(int argc, char **argv) {
    std::string log_folder;
    std::string output_folder;
    if (argc < 5) {
        std::cout << "Usage raw_capture_proc <log_folder> <output_folder> "
                  "<success-thresh> <center-thresh-m>"
                  << std::endl;
        return -1;
    }

    log_folder = std::string(argv[1]);
    output_folder = std::string(argv[2]);
    double success_thresh = strtof(argv[3], NULL);
    double center_thresh_m = strtof(argv[4], NULL);

    boost::filesystem::path log_folder_path = boost::filesystem::path(log_folder);
    bool was_successful_grasp = false;
    bool skip_note_check = false;
    if (log_folder_path.string().find("ng_bad") != std::string::npos) {
        skip_note_check = true;
    }
    if (log_folder_path.string().find("ng_good") != std::string::npos) {
        skip_note_check = true;
        was_successful_grasp = true;
    }

    /**
     * 1. Find the notes file and determine if this was a successful grasp
     * location.
     */
    if (!skip_note_check) {
        boost::filesystem::recursive_directory_iterator end;
        for (boost::filesystem::recursive_directory_iterator it(log_folder);
                it != end; ++it) {
            if (it->path().extension() == ".txt") {
                std::cout << "hello!" << std::endl;
                // Is there a slash in the file?
                bool found_slash = false;
                {
                    std::ifstream file_notes(it->path().string());
                    if (file_notes.is_open()) {
                        std::string line;
                        while (std::getline(file_notes, line)) {
                            if (line.find("/") != std::string::npos) {
                                found_slash = true;
                                break;
                            }
                        }
                    } else {
                        std::cout << "Error opening notes file!" << std::endl;
                        return -1;
                    }
                }
                if (found_slash) {
                    // Rewind the file and determine success.
                    std::ifstream file_notes(it->path().string());
                    if (file_notes.is_open()) {
                        std::string line;
                        while (std::getline(file_notes, line)) {
                            std::size_t found = line.find("/");
                            if (found != std::string::npos) {
                                // Convert left and right into number.
                                int success_count = atoi(&(line[0]));
                                int total_count = atoi(&(line[2]));
                                double success_rate = success_count / (total_count + 0.0);
                                was_successful_grasp = (success_rate > success_thresh);
                            }
                        }
                    }
                } else {
                    // look for success or otherwise in the first line.
                    std::ifstream file_notes(it->path().string());
                    if (file_notes.is_open()) {
                        std::string line;
                        std::getline(file_notes, line);
                        std::size_t found = line.find("success");
                        if (found == 0) {
                            was_successful_grasp = true;
                        }
                    }
                }
            }
        }
    }

    /**
     * 2. Loop through all pcds and create the meshes.
     */

    boost::filesystem::recursive_directory_iterator end;
    for (boost::filesystem::recursive_directory_iterator it(log_folder);
            it != end; ++it) {
        if (it->path().extension() == ".pcd") {
            std::string pcd_filename = it->path().stem().string();

            // Create new folder
            std::string out_path_folder = output_folder + "/" +
                                          log_folder_path.leaf().string() + "/" +
                                          pcd_filename;

            boost::filesystem::create_directories(
                boost::filesystem::path(out_path_folder));

            // Copy the pcd to the new folder
            std::string orig_pcd = out_path_folder + "/original.pcd";
            boost::filesystem::copy_file(it->path(),
                                         boost::filesystem::path(orig_pcd));

            // Copy notes.
            if (!skip_note_check) {
                boost::filesystem::copy_file(
                    boost::filesystem::path(log_folder + "/notes.txt"),
                    boost::filesystem::path(out_path_folder + "/notes.txt"));
            }

            pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_in(
                new pcl::PointCloud<pcl::PointXYZRGB>);
            if (pcl::io::loadPCDFile(orig_pcd, *cloud_in) == -1) {
                PCL_ERROR("Couldn't read pcd file! \n");
                return (-1);
            }


            pcl::PolygonMesh mesh = pcd2mesh(cloud_in);
            // Save mesh to see what it looks like.
            pcl::io::savePLYFileBinary(out_path_folder + "/original.ply", mesh);

            // Corlor all vertices within 2cm of grasp point red or green.
            // Take a MxN patch at the center.

            for (int i = 0; i < cloud_in->points.size(); i++) {
                pcl::PointXYZRGB* point = &cloud_in->points[i];
                float x = point->x;
                float y = point->y;
                float z = point->z;

                if (isnan(point->z)) {
                    continue;
                }

                float dist = std::pow(x,2) + std::pow(y,2);
                dist = std::sqrt(dist);
                if (dist < center_thresh_m) {
                    if (was_successful_grasp) {
                        point->r = 0;
                        point->g = 255;
                        point->b = 0;
                    } else {
                        point->r = 255;
                        point->g = 0;
                        point->b = 0;
                    }
                }
            }

            std::string annotated_pcd = out_path_folder + "/annotated.pcd";
            pcl::io::savePCDFileASCII (annotated_pcd, *cloud_in);

            mesh = pcd2mesh(cloud_in);
            // Save mesh to see what it looks like.
            pcl::io::savePLYFileBinary(out_path_folder + "/annotated.ply", mesh);

        }
    }
}


