#!/bin/bash

if [ -z "$5" ]
  then
      echo "Usage: process_all.sh <pisgah-data-directory> <output-proc-directory> <success-thresh> <center-thresh-m> overwrite(y|n)"
    exit
fi
echo "HI there"
find $1 -mindepth 1 \( -iname "caltech*" -o -iname "pisgah*" -o -iname "jpl*" -o -iname "test*" \) | parallel "ppu-raw-capture-proc {} $2 $3 $4 $5"

#find $1 -mindepth 1 \( -iname "caltech*" -o -iname "pisgah*" -o -iname "jpl*" -o -iname "test*" \) -exec ppu-raw-capture-proc {} $2 $3 $4 $5 \;

exit 0

