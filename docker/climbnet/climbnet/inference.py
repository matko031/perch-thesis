import pickle
from pathlib import Path

from .model import FootholdModel
from .Config import make_config
from .split import DataSplit

def evaluate(model_path, val_data_path, balance_classes=True, seed=None):
   
    f = open( str(Path(model_path) / "config.pickle"), "rb")
    config = pickle.load(f)
    f.close()

    model = FootholdModel(user_config=config, model_path=model_path)
    model.load_weights()

    data = DataSplit()
    data.validation.read(
        path=val_data_path,
        balance_classes=balance_classes,
        update_labels_if_insufficient_surface_support=config[
            "update_labels_if_insufficient_surface_support"
        ],
        config=config,
        seed=seed
    )

    data.uint_range_to_float_range()

    score = model.keras_model.evaluate(
        model.get_inputs(data.validation), data.validation.data["y"], verbose=0
    )

    return dict(zip(model.keras_model.metrics_names, score))

    #print("Test loss (last/max): %f / %f", (score[0], best_score[0]))
    #print("Test accuracy (last/max): %f / %f:", (score[1], best_score[1]))



def predict(model_path, val_data_path):
   
    f = open( str(Path(model_path) / "config.pickle"), "rb")
    config = pickle.load(f)
    f.close()

    model = FootholdModel(user_config=config, model_path=model_path)
    model.load_weights()

    data = DataSplit()
    data.validation.read(
        path=val_data_path,
        balance_classes=False,
        update_labels_if_insufficient_surface_support=config[
            "update_labels_if_insufficient_surface_support"
        ],
        config=config,
    )

    data.uint_range_to_float_range()

    predictions = model.keras_model.predict( model.get_inputs(data.validation) )
    res = {"correct": data.validation.data["y"],  "predicted": predictions, }
    if "g" in data.validation.data:
        res["grasps"] = data.validation.data["g"]

    return res










