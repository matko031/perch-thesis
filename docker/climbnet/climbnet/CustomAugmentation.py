import random

import numpy as np
from skimage import exposure


class CustomAugmentation:
    def __init__(
        self,
        alpha=False,
        beta=False,
        histogram_equalization=False,
        rot90=False,
        preprocessor_=False,
    ):
        self.alpha = alpha
        self.beta = beta
        self.histogram_equalization = histogram_equalization
        self.rot90 = rot90
        self.preprocessor = preprocessor_

    def __call__(self, x):
        x = x.astype(np.float32)
        if self.alpha:
            x *= self.alpha
        if self.beta:
            x += self.beta
        if self.histogram_equalization:
            if np.random.random() < 0.5:  # pylint: disable=no-member
                x = exposure.equalize_hist(x)
        if self.rot90:
            num_rotations = random.choice([0, 1, 2, 3])
            x = np.rot90(x, num_rotations)
        if self.preprocessor:
            x = self.preprocessor(x)
        return x
