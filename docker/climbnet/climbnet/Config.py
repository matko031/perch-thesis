import copy
import logging
import numpy as np

default_conf = {
    # Data
    "depth_img_rows": 32,
    "depth_img_cols": 32,
    "color_img_rows": 66,
    "color_img_cols": 66,
    "input": "depth", # choices: "depth", "grey", "color', "depth-grey-stack", "depth-color-stack", "depth-grey", "depth-color"
    "num_classes": 2,
    "std_axes": "", #choices=: "", "0", "012", "12"

    # Reading
    "update_labels_if_insufficient_surface_support": False,
    "balance_classes": True,

    # Training
    "batch_size": 128,
    "epochs": 50,  
    "steps_per_epoch_ratio": 1,
    "validation_steps_ratio": 1,
    "validation_split_ratio": 0.3,
    "augment": False,
    "verbose": 1,
    "architecture": "mnist_less",

    # Misc
    "verbosity": {"logging_level": logging.DEBUG, "choices": True, "config": True},
}


def make_config(provided_conf=None):
    if provided_conf is None:
        provided_conf = {}

    c = copy.deepcopy(default_conf)
    c.update(provided_conf)

    logging.basicConfig(level=default_conf["verbosity"]["logging_level"])

    it = c["input"]
    c["use_depth"] = "depth" in it
    c["use_color"] = "color" in it or "grey" in it
    c["cat_color_and_depth"] = "stack" in it
    c["n_color_channels"] = 3 if "color" in it else 1

    if c["cat_color_and_depth"]:
        c["depth_img_rows"] = c["color_img_rows"]
        c["depth_img_cols"] = c["color_img_cols"]

    c["depth_input_shape"] = (c["depth_img_rows"], c["depth_img_cols"], 1)
    c["color_input_shape"] = (
        c["color_img_rows"],
        c["color_img_cols"],
        c["n_color_channels"],
    )

    c["color_depth_input_shape"] = (
        c["color_img_rows"],
        c["color_img_cols"],
        c["n_color_channels"] * c["use_color"] + c["use_depth"],
    )
    return c


def print_dict_aux(stats, indent, subconfig):
    inc = "  "
    for k, v in sorted(stats.items()):
        if isinstance(v, dict):
            if subconfig is None:
                print(f"{indent}{k}:")
                print_dict_aux(v, indent=indent + inc, subconfig=subconfig)
            else:
                print_dict_aux(v, indent=f"{indent}:{k}", subconfig=subconfig[k])
        elif isinstance(v, list) and all(map(lambda x: isinstance(x, float), v)):
            a = np.array(v)
            print(
                f"{indent}{k}:\n"
                f"{indent}{inc}mean: {a.mean()}\n"
                f"{indent}{inc}{inc}in: [{a.min()}, {a.max()}], len: {len(a)}"
            )
        else:
            if subconfig is None:
                print(f"{indent}{k}: {v}")
            else:
                if k in subconfig and isinstance(subconfig[k], Choices):
                    print(f"{indent}:{k}: {v}")


def print_dict(stats, indent="", only_config_choices=False):
    subconfig = None
    if only_config_choices:
        subconfig = default_conf
    print_dict_aux(stats, indent, subconfig)


def flatten_config_aux(d, parent_key="", sep=":", subconfig=None):
    assert isinstance(d, dict)
    if subconfig is not None:
        assert isinstance(subconfig, dict)

    items = []
    for k, v in d.items():
        new_key = parent_key + sep + k if parent_key else k
        if isinstance(v, dict):
            sub = None
            if subconfig is not None:
                assert isinstance(subconfig[k], dict)
                sub = subconfig[k]
            items.extend(flatten_config_aux(v, new_key, sep=sep, subconfig=sub).items())
        else:
            if subconfig is None or (
                k in subconfig and isinstance(subconfig[k], Choices)
            ):
                items.append((new_key, v))

    return dict(items)


def flatten_config(config, choices_only=False, choices_config=None):
    subconfig = None
    if choices_only:
        if choices_config is not None:
            subconfig = choices_config
        else:
            subconfig = default_conf
    return flatten_config_aux(config, subconfig=subconfig)
