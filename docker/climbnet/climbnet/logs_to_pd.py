from collections import defaultdict

import pandas
import numpy as np

from .Config import flatten_config, print_dict


def logs_to_pd(logs, sort_key, sort_key_only=False, verbose=False):
    if sort_key is None:
        results = logs["results"]
    else:
        results = sorted(logs["results"], key=lambda x: np.mean(x[sort_key]))

    csv = defaultdict(list)

    for r in results:
        for k, v in flatten_config(
            r["config"], choices_only=True, choices_config=logs["default_conf"]
        ).items():
            csv[k].append(v)
        if verbose:
            print(f"Results for config:")
            print_dict(r["config"], indent="    ", only_config_choices=True)
            print()
        if sort_key_only:
            display_dict = {k: v for k, v in r.items() if k == sort_key}
        else:
            display_dict = {k: v for k, v in r.items() if k != "config"}
        if verbose:
            print_dict(display_dict, indent="    ")
            print("\n")
        for k, v in display_dict.items():
            if "acc" in k:

                def mod(x):
                    return int(x * 100)

            elif "time" in k:

                def mod(x):
                    return int(x / 60)

            else:

                def mod(x):
                    return x

            if isinstance(v, list):
                if "acc" in k:
                    csv[k + "_mean"].append(mod(np.mean(v)))
                    csv[k + "_std"].append(int(np.std(v) * 10000) / 100)
                    # csv[k].append(v)
                    # csv[k+"_min"].append(mod(np.min(v)))
                    # csv[k+"_max"].append(mod(np.max(v)))
            else:
                csv[k].append(mod(np.mean(v)))

    df = pandas.DataFrame.from_dict(csv)
    return df
