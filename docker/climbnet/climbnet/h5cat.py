import argparse
import numpy
import h5py
import sklearn


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("input", nargs="+")
    parser.add_argument("-o", "--output", default=None)
    parser.add_argument("-s", "--shuffle", action="store_true")
    parser.add_argument("-S", "--sample", default=None)
    args = parser.parse_args()

    datasets = dict()
    for fi, fname in enumerate(args.input):
        print("Adding {}".format(fname), end="", flush=True)
        f = h5py.File(fname, "r")
        l = -1
        for name, dataset in f.items():
            print(" {}".format(name), end="", flush=True)
            if not name in datasets.keys():
                datasets[name] = dataset[:]
            else:
                datasets[name] = numpy.concatenate((datasets[name], dataset))
            if l == -1:
                l = len(datasets[name])
            else:
                assert l == len(datasets[name])
        f.close()
        print(f" length: {l} file {fi}/{len(args.input)}")

    if args.shuffle:
        print("Shuffling", end="", flush=True)
        # Do not use random.shuffle on numpy arrays:
        # https://www.reddit.com/r/Python/comments/42uqqo/a_nasty_little_bug_involving_random_and_numpy/
        # sigh
        # import random
        # for name, dataset in datasets.items():
        #     print(" {}".format(name), end='', flush=True)
        #     rgen = random.Random()
        #     rgen.seed(0)
        #     rgen.shuffle(dataset)
        all_keys = list(datasets.keys())
        datasets = dict(
            zip(all_keys, sklearn.utils.shuffle(*[datasets[k] for k in all_keys]))
        )
        print()

    if args.sample is not None:
        assert args.shuffle, "Are you sure you intent to sample without shuffle?"
        print("Trimming", end="", flush=True)
        for k, v in datasets.items():
            print(" {}".format(k), end="", flush=True)
            datasets[k] = v[: args.sample]
        print()

    print("Writing to disk")
    output = h5py.File(args.output, "w")
    for name, dataset in datasets.items():
        output.create_dataset(name, data=dataset)
    output.close()
