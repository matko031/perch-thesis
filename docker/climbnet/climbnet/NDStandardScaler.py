import random
import numpy as np
from sklearn.base import TransformerMixin
from sklearn.preprocessing import StandardScaler


class NDStandardScaler(TransformerMixin):
    def __init__(self, axes=None, **kwargs):
        # copy=True was in the args of StandardScaler in the original version
        # of this class, probably because we're copying the data around anyway.
        assert "copy" not in kwargs.keys()
        self._scaler = StandardScaler(copy=True, **kwargs)
        self._orig_shape = None
        # self._sample_axes: ids of axes along which we compute statistics
        if axes is None:
            self._sample_axes = []
        else:
            self._sample_axes = axes
        # self._sample_axes: ids of axes along which stats are computed
        # independently.
        self._indep_axes = None
        self._moveaxes_order = None

    def fit(self, X, **kwargs):
        if len(self._sample_axes) == 0:
            return self
        X = np.array(X)
        # Save the original shape to reshape the flattened X later
        # back to its original shape
        if len(X.shape) > 1:
            self._orig_shape = np.array(X.shape)
            assert max(self._sample_axes) < len(X.shape)
            self._indep_axes = [
                x for x, _ in enumerate(X.shape) if x not in self._sample_axes
            ]
            self._moveaxes_order = self._sample_axes + self._indep_axes

        X = self._flatten(X)
        self._scaler.fit(X, **kwargs)
        return self

    def transform(self, X, **kwargs):
        if len(self._sample_axes) == 0:
            return X
        X = np.array(X)
        X = self._flatten(X)
        X = self._scaler.transform(X, **kwargs)
        X = self._reshape(X)
        return X

    def _flatten(self, X):
        X = np.moveaxis(X, self._moveaxes_order, list(range(len(X.shape))))
        n_std_dims = -1
        n_indep_dims = -1
        if 0 in self._sample_axes:
            n_indep_dims = np.prod(self._orig_shape[self._indep_axes])
        else:
            assert 0 in self._indep_axes
            n_std_dims = np.prod(self._orig_shape[self._sample_axes])
        X = X.reshape(n_std_dims, n_indep_dims)
        return X

    def _reshape(self, X):
        shape = [
            *self._orig_shape[self._sample_axes],
            *self._orig_shape[self._indep_axes],
        ]
        shape[self._moveaxes_order.index(0)] = -1
        X = X.reshape(shape)
        X = np.moveaxis(X, list(range(len(X.shape))), self._moveaxes_order)
        return X

    def _test(self):
        data = np.random.rand(100, 40, 50, 3)
        test = np.random.rand(150, 40, 50, 3)
        # Ideally, data would come from a more complex distribution:
        # filename = 'file.h5'
        # import h5py
        # file_data = np.array(h5py.File(filename, 'r')['C'])
        # data = file_data[0:100]
        # test = file_data[100:250]

        for _ in range(10):
            axes = random.sample(
                list(range(len(data.shape))), k=random.randint(1, len(data.shape))
            )
            ss = NDStandardScaler(axes=axes, with_mean=True, with_std=True)
            data_fit_transform = ss.fit_transform(data)
            data_mean = np.mean(data, axis=tuple(axes), keepdims=True)
            data_centered = data - data_mean
            data_stdev = np.std(data_centered, axis=tuple(axes), keepdims=True)
            data_centered_scaled = np.divide(data_centered, data_stdev)
            if not np.allclose(data_centered_scaled, data_fit_transform):
                descrep = np.logical_not(
                    np.isclose(data_centered_scaled, data_fit_transform)
                )
                assert np.allclose(data_fit_transform[descrep], 0)

            if 0 in axes:
                test_std = ss.transform(test)
                test_centered = test - data_mean
                test_centered_scaled = np.divide(test_centered, data_stdev)
                if not np.allclose(test_centered_scaled, test_std):
                    descrep = np.logical_not(np.isclose(test_centered_scaled, test_std))
                    assert np.allclose(data_fit_transform[descrep], 0)
