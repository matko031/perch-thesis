from IPython.core.debugger import Pdb
from .Config import make_config
from .train import train_model
from .inference import evaluate, predict


name = "climbnet"


def debug(self):
    Pdb().set_trace()
