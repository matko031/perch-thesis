import argparse
import gc
import logging
import pickle
import random
import sys
import time
import tensorflow 
import json

import numpy as np
from IPython.core import ultratb
from tqdm import tqdm

from .Config import make_config, default_conf, print_dict
from .model import FootholdModel
from .split import DataSplit, split


def train_model(
    train_data_path,
    user_config=None,
    val_data_path=None,
    model_path=None,
    logs=None,
    seed=None
):

    config = make_config(provided_conf=user_config)

    if seed is None:
        print("Using hardware random seed.")
        random.seed()
    else:
        print("Setting random seed to %d." % seed)
        random.seed(seed)
        np.random.seed(seed)
        tensorflow.random.set_seed(seed)
        # tf.compat.v1set_random_seed(1) # useless unless forcing CPU & single thread

    data = DataSplit()

    data.train.read(
        train_data_path,
        balance_classes=config["balance_classes"],
        update_labels_if_insufficient_surface_support=config[
            "update_labels_if_insufficient_surface_support"
        ],
        config=config,
    )
    logging.info("Progress: Read training set.")

    if val_data_path == "-":
        data = split(data, config["validation_split_ratio"])
        logging.info("Progress: split data.")
    elif val_data_path:
        print(
            "WARNING: loading validation set with def parameters. This may "
            "balance classes, or re-label based on support."
        )
        data.validation.read(
            val_data_path,
            balance_classes=config["balance_classes"],
            update_labels_if_insufficient_surface_support=config[
                "update_labels_if_insufficient_surface_support"
            ],
            config=config,
        )
        logging.info("Progress: Read validation set.")
    else:
        Exception("No validation dataset provided")

    # scalers = data.train.standardize(axes=[int(a) for a in config["std_axes"]])
    # if "0" in config["std_axes"]:
    #     data.validation.standardize(scalers=scalers)

    print("Scaling and centering...")
    data.uint_range_to_float_range()
    print("Scaling and centering... done.")

    # print("Data length: " + str(len(data.train.data["i"])))
    # print("Data hash: %s" % data.hash(True)[0:5])

    fhmodel = FootholdModel(user_config=user_config, model_path=model_path)
    fhmodel.build_model()
    model, result = fhmodel.fit(data)

    return (model, result)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-t", "--training-set", default=None, nargs="1", help="Traning set"
    )

    parser.add_argument(
        "-f", "--config-file", default=None, nargs="1", help="Config file in json format"
    )

    parser.add_argument(
        "-c", "--config-string", default=None, nargs="1", help="Config string in json format"
    )


    parser.add_argument(
        "-v",
        "--validation-set",
        default="-",
        help="Validation set. If set to -, validation set taken from training set",
    )
    parser.add_argument("-m", "--model", default=None, help="Model output")
    parser.add_argument("-l", "--log-dir", default=None, help="Log")
    args = parser.parse_args()

    if args.config_file:
        user_config = json.load(args.config_file)
    elif args.config_string:
        user_config = json.loads(args.config_string)
    else:
        user_config = None

    print("User config:")
    print(user_config)

    train_model(
        train_data_path=args.train_data_path,
        user_config=user_config,
        val_data_path=args.validation_set,
        model_path=args.model,
        logs=args.log_dir,
    )

