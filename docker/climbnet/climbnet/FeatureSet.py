import hashlib
import random
import logging

import h5py
import numpy as np
import tables
import cv2
import sklearn

from .NDStandardScaler import NDStandardScaler


def hexhash(a):
    if a is None:
        return str()
    # the hash() function is only consistent within a process.
    # hash("foo") will return different values across processes.
    return hashlib.md5(a.tobytes()).hexdigest()


class FeatureSet:
    def __init__(self, verbose=False):
        self.data = dict()
        self.source_data_size = None
        self.verbose = False

    def empty(self):
        return len(self.data.keys()) == 0

    def pixels_are_float(self):
        pixels_are_float = self.data["x"].dtype != "uint8"

        if "c" in self.data.keys():
            assert self.data["c"].dtype == self.data["x"].dtype

        return pixels_are_float

    def read(
        self,
        path,
        balance_classes,
        update_labels_if_insufficient_surface_support,
        fields=None,
        config=None,
        slice_id=None,
        seed=None
    ):
        if seed is not None:
            np.random.seed(seed)
            random.seed(seed)
        if not fields:
            fields = ["x", "c", "y", "p", "i", "g"]

        # Read uint8 data from h5:

        #data_file = tables.open_file(path, mode="r")
        data_file = h5py.File(path, "r")
        assert self.empty()
        for f in fields:
            #if hasattr(data_file.root, f.upper()):
            if f.upper() in data_file.keys():
                if slice_id is not None: 
                    self.data[f] = np.array(data_file[f.upper()][
                        10000 * slice_id : 10000 * (slice_id + 1)
                    ])
                    if len(self.data[f]) == 0:
                        return False
                else:
                    self.data[f] = np.array(data_file[f.upper()][:])

        data_file.close()
        assert self.data["x"].dtype == "uint8"

        # If needed, convert color images to greyscale:
        if config["use_color"]:
            assert "c" in self.data.keys()
            assert self.data["c"].dtype == "uint8"
            if ( config["use_color"] and self.data["c"].shape[3] != config["n_color_channels"]):
                if self.data["c"].shape[3] == 3 and config["n_color_channels"] == 1:
                    self.data["c"] = np.mean(self.data["c"], axis=3, keepdims=True)
                elif self.data["c"].shape[3] == 1 and config["n_color_channels"] == 3:
                    raise Exception("Model wants color input; grayscale provided.")
                else:
                    raise Exception(
                        "Unsupported case: %i input channels, %i channels requested."
                        % (self.data["c"].shape[3], config["n_color_channels"])
                    )


        # Re-scale depth images:
        if config["use_depth"]:
            if self.data["x"].shape[1:] != config["depth_input_shape"]:
                if self.verbose:
                    logging.info(
                        "Resizing depth images from %s to %s.",
                        self.data["x"].shape[1:],
                        config["depth_input_shape"],
                    )
                x = np.zeros(
                    (len(self.data["x"]),) + config["depth_input_shape"], dtype=np.float32
                )
                for idx in range(len(self.data["x"])):
                    img = cv2.resize(  # pylint: disable=no-member
                        self.data["x"][idx, :, :, 0].astype(np.float32),
                        (config["depth_input_shape"][0], config["depth_input_shape"][1]),
                        interpolation=cv2.INTER_LINEAR,  # pylint: disable=no-member
                    )
                    x[idx, :, :, 0] = img
                self.data["x"] = x

        # Re-scale color images:
        if config["use_color"]:
            if self.data["c"].shape[1:] != config["color_input_shape"]:
                if self.verbose:
                    logging.info(
                        "Resizing color images from %s to %s.",
                        self.data["c"].shape[1:],
                        config["color_input_shape"],
                    )
                c = np.zeros(
                    (len(self.data["c"]),) + config["color_input_shape"], dtype=np.float32
                )
                for idc in range(len(self.data["c"])):
                    img = cv2.resize(  # pylint: disable=no-member
                        self.data["c"][idc, :, :, :].astype(np.float32),
                        (config["color_input_shape"][0], config["color_input_shape"][1]),
                        interpolation=cv2.INTER_LINEAR,  # pylint: disable=no-member
                    )
                    if len(img.shape) == 2:
                        c[idc, :, :, 0] = img
                    else:
                        c[idc, :, :] = img
                self.data["c"] = c

                # Re-scale color images:
            if config["cat_color_and_depth"]:
                self.data["cx"] = np.append(self.data["c"], self.data["x"], axis=3)
                if self.data["cx"].shape[1:] != config["color_depth_input_shape"]:
                    if self.verbose:
                        logging.info(
                            "Resizing color_depth images from %s to %s.",
                            self.data["cx"].shape[1:],
                            config["color_depth_input_shape"],
                        )
                    cx = np.zeros(
                        (len(self.data["cx"]),) + config["color_depth_input_shape"], dtype=np.float32
                    )
                    for idcx in range(len(self.data["cx"])):
                        img = cv2.resize(  # pylint: disable=no-member
                            self.data["cx"][idcx, :, :, :].astype(np.float32),
                            (config["color_depth_input_shape"][0], config["color_depth_input_shape"][1]),
                            interpolation=cv2.INTER_LINEAR,  # pylint: disable=no-member
                        )
                        if len(img.shape) == 2:
                            cx[idcx, :, :, 0] = img
                        else:
                            cx[idcx, :, :] = img
                    self.data["cx"] = cx


        # Update labels and balance classes:
        if update_labels_if_insufficient_surface_support:
            self.update_labels_if_insufficient_surface_support()
        if balance_classes:
            self.balance_classes()
        if self.verbose:
            print(f"Read {len(self.data['x'])} elements")
        return True

    def uint_range_to_float_range(self):
        if "x" in self.data.keys():
            self.data["x"] = 1.0 / 128.0 * self.data["x"].astype(np.float32) - 1.0
        if "c" in self.data.keys():
            self.data["c"] = 1.0 / 128.0 * self.data["c"].astype(np.float32) - 1.0

    def standardize(self, scalers=None, axes=None):
        # Argument against per-image standardization (axes==[1,2]):
        # - Making each image zero-mean could prevent the network from learning
        #   to ignore pixels for which there are no measurements (color and depth)
        #   fixme: instead, how about making zero mean but without condisering or
        #   affecting areas with no measurements?
        # - Making each image unit-std: the scale of an image is a relevant
        #   feature, both for color and depth.
        # Argument against per-pixel standardization (axes==[0]):
        # - Could damage some of the regularity of image features?
        # - Could make it harder to learn to identify pixels for which there are
        #   no measurements.

        if scalers is None:
            scalers = dict()
            for key in ["x", "c", "cx"]:
                if key in self.data.keys():
                    scalers[key] = NDStandardScaler(axes=axes)
                    self.data[key] = scalers[key].fit_transform(self.data[key])
        else:
            assert axes is None
            for key, scaler in scalers.items():
                assert key in self.data.keys()
                self.data[key] = scaler.transform(self.data[key])
        return scalers

    def hash(self, on_index_only=False):
        s = str()
        for k, v in self.data.items():
            if on_index_only and k != "i":
                continue
            s += hexhash(v)
        return hashlib.md5(s.encode("utf-8")).hexdigest()

    def update_labels_if_insufficient_surface_support(self):
        # bar = Bar('Re-labeling', max=self.data['x'].shape[0])
        sums = (self.data["x"] != 255).sum(
            axis=(1, 2), dtype=np.float32
        ).squeeze() / self.data["x"][0].size
        # indices = (sums<.75)
        a = 0.65
        b = 0.85
        sums = (sums - a) / (b - a)
        indices = np.random.rand(*sums.shape) > sums
        if self.verbose:
            print(
                "Forcing class of %d class-1 (success) points (out of %d) to 0"
                % (indices.sum(), self.data["x"].shape[0])
            )
        self.data["y"][indices, :] = [1, 0]

        # see https://stackoverflow.com/questions/36383107/how-to-evaluate-the-sum-of-values-within-array-blocks
        # https://stats.stackexchange.com/questions/17109/measuring-entropy-information-patterns-of-a-2d-binary-matrix
        # from matplotlib import pyplot as plt
        # import cv2
        # for c in range(self.data['x'].shape[1]):
        #     img = (self.data['x'][c].squeeze() == 255).astype(np.uint8)
        #     plt.imshow(self.data['x'][c].squeeze(), cmap='gray')
        #     plt.show()
        #     print(img)
        #     #np.expand_dims(a, axis=0)
        #     plt.imshow(img*128, cmap = 'gray')
        #     plt.show()
        #     sobelx = cv2.Sobel(img,cv2.CV_16S,1,0,ksize=1)
        #     sobely = cv2.Sobel(img,cv2.CV_16S,0,1,ksize=1)
        #     plt.imshow(sobelx*128, cmap = 'gray')
        #     plt.show()
        #     plt.imshow(sobely*128, cmap = 'gray')
        #     plt.show()

    def append(self, fs):
        if len(self.data) == 0:
            for k, v in fs.data.items():
                self.data[k] = v[:]
        elif len(fs.data) == 0:
            pass
        elif self.data.keys() == fs.data.keys():
            for k, v in fs.data.items():
                self.data[k] = np.append(self.data[k], v, axis=0)
        else:
            raise Exception(
                "Keys do not match: self: ["
                + " ".join(self.data.keys())
                + "], fs: ["
                + " ".join(fs.data.keys())
                + "]"
            )

    def concatenate(self, *fs):
        # This function should be merged with append.
        assert len(self.data) == 0
        assert len(fs) > 0
        assert all(len(f.data) != 0 for f in fs)
        keys = fs[0].data.keys()
        assert all(set(keys) == set(f.data.keys()) for f in fs)
        for k in keys:
            self.data[k] = np.concatenate([f.data[k] for f in fs], axis=0)

    def balance_classes(self ):
        m = np.min(np.sum(self.data["y"], 0))
        s = np.array([], dtype=int)
        for c in range(self.data["y"].shape[1]):
            y = self.data["y"][:, c]
            trim = np.nonzero(y)[0]
            if self.verbose:
                print("Found %d points in class %d." % (len(trim), c))
            np.random.shuffle(trim)
            if len(trim) > m:
                if self.verbose:
                    print(" Trimming class %d from %d to %d elements" % (c, len(trim), m))
                trim = trim[0:m]
            s = np.append(s, trim)
        s = sorted(s)
        for k, v in self.data.items():
            self.data[k] = v[s]
        if self.verbose:
            print("Trimming done.")

    def shuffle(self):
        # Do not use random.shuffle on numpy arrays:
        # https://www.reddit.com/r/Python/comments/42uqqo/a_nasty_little_bug_involving_random_and_numpy/
        # sigh
        # seed = random.randrange(2147483648)
        # for v in self.data.values():
        #     rgen = random.Random()
        #     rgen.seed(seed)
        #     rgen.shuffle(self.data[k])

        all_keys = list(self.data.keys())
        for k, v in zip(
            all_keys, sklearn.utils.shuffle(*[self.data[k] for k in all_keys])
        ):
            self.data[k] = v

        # permutation = np.random.permutation(self.data['x'].shape[0])
        # for k, v in self.data.items():
        #     self.data[k] = v[permutation]
