import random
import math
import hashlib
from scipy import spatial
import numpy as np
from .FeatureSet import FeatureSet
from .predict import write_scores


class DataSplit:
    def __init__(self):
        self.train = FeatureSet()
        self.validation = FeatureSet()
        self.test = FeatureSet()

    def uint_range_to_float_range(self):
        for attr in ["train", "validation", "test"]:
            getattr(self, attr).uint_range_to_float_range()

    def add(self, datasplit):
        self.train.append(datasplit.train)
        self.validation.append(datasplit.validation)
        self.test.append(datasplit.test)

    def hash(self, on_index_only=False):
        s = str()
        for attr in ["train", "validation", "test"]:
            s += getattr(self, attr).hash(on_index_only)
        return hashlib.md5(s.encode("utf-8")).hexdigest()

    def shuffle(self):
        for attr in ["train", "validation", "test"]:
            getattr(self, attr).shuffle()


def split_by_class(feature_set):
    s = dict()
    for y in [0, 1]:
        s[y] = FeatureSet()
        indices = np.array(feature_set.data["y"][:, y], dtype=bool)
        for k, v in feature_set.data.items():
            s[y].data[k] = v[indices]
    return s


def split_by_index(feature_set, indices):
    mask = np.zeros(feature_set.data["i"].shape, dtype=bool)
    mask[indices] = True
    # assert "i" in feature_set.data.keys()
    # write_scores(f"/tmp/i{random.randint(0, 1000)}", np.array(list(zip(feature_set.data["i"], mask))), 1202152)
    train_validation = DataSplit()
    for k, v in feature_set.data.items():
        train_validation.train.data[k] = v[~mask]
        train_validation.validation.data[k] = v[mask]
    return train_validation


def split_spatial_scipy(feature_set, validation_split_ratio):
    # Find _clusters_ nonintersecting balls of _n_val_per_cluster_ elements.
    # _n_val_per_cluster_ is one 1/_clusters_ of the total number of validation
    # elements. Therefore, all the balls together form a validation set that
    # isn't directly meshed with the train set, but still spans several
    # locations throughout the wall.
    tree = spatial.cKDTree(feature_set.data["p"])  # pylint: disable=no-member
    clusters = 3
    print(f"USING {clusters} CLUSTERS for train-val split")
    n_val_per_cluster = int(
        math.floor(int(validation_split_ratio * len(feature_set.data["p"])) / clusters)
    )
    indices = np.array([], dtype=int)
    cluster_count = clusters
    while cluster_count != 0:
        point = feature_set.data["p"][random.randrange(len(feature_set.data["p"]))]
        # point = feature_set.data['p'][np.random.randint(len(feature_set.data['p']))]
        # print("Centering validation set around (%f, %f, %f)" % (point[0], point[1], point[2]))
        _, i = tree.query(point, n_val_per_cluster)
        if np.intersect1d(indices, i).size > 0:
            continue
        else:
            cluster_count -= 1
            indices = np.union1d(indices, i)
    assert indices.size == clusters * n_val_per_cluster
    data = split_by_index(feature_set, indices)
    return data


def split_spatial(feature_set, validation_split_ratio):
    return split_spatial_scipy(feature_set, validation_split_ratio)


# Assume all data is in train_val_test.train
# Populate train_val_test.validation with regional data taken separately from each class
# in the train set
def split(train, validation_split_ratio):
    print("Creating train/validation splits from training set...")
    # labels = np.full((22749, 1), -1)
    train_validation = DataSplit()
    train_validation.train.source_data_size = train.train.source_data_size
    train_validation.validation.source_data_size = train.train.source_data_size
    feature_set_by_class = split_by_class(train.train)
    for _, same_class_feature_set in feature_set_by_class.items():
        same_class_train_validation = split_spatial(
            same_class_feature_set, validation_split_ratio
        )
        train_validation.add(same_class_train_validation)
    print(
        " Train size: %d / Validation size: %d"
        % (
            len(train_validation.train.data["x"]),
            len(train_validation.validation.data["x"]),
        )
    )
    return train_validation
