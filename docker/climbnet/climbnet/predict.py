import argparse
import os

from tensorflow.keras.preprocessing.image import ImageDataGenerator

from .Config import make_config
from .CustomAugmentation import CustomAugmentation
from .FeatureSet import FeatureSet
from .model import FootholdModel


def write_scores(output_path, scores, n_samples=None):
    if not n_samples:
        with open(output_path, "w") as f:
            for i in scores:
                f.write("%.6f\n" % i[1])
    else:
        d = dict(scores)
        with open(output_path, "w") as f:
            for i in range(0, n_samples):
                if i in d:
                    f.write("%.6f\n" % d[i])
                else:
                    f.write("-1\n")


def test(test_data_path, model_input, scores_path, config):
    fhmodel = FootholdModel(config, model_path=model_input)
    fhmodel.load_weights()
    n_samples = None

    # fixme: think about standardization of data in train.py. Does a similar
    # transform need to be applied to the data before predict?
    assert config["std_axes"] == ""

    if os.path.isdir(test_data_path):
        test_preprocessor = CustomAugmentation(alpha=1.0 / 128.0, beta=-1.0)
        test_datagen = ImageDataGenerator(preprocessing_function=test_preprocessor)

        test_generator = test_datagen.flow_from_directory(
            test_data_path,
            target_size=(config["depth_img_rows"], config["depth_img_cols"]),
            color_mode="grayscale",
            batch_size=config["batch_size"],
            class_mode="categorical",
            shuffle=False,
        )

        scores = fhmodel.predict_generator(test_generator, test_data_path)
    else:
        scores = {}
        slice_id = 0
        while True:
            feature_set = FeatureSet()
            if not feature_set.read(
                test_data_path,
                balance_classes=False,
                update_labels_if_insufficient_surface_support=False,
                config=config,
                slice_id=slice_id,
            ):
                break
            print(f"Processing slice {slice_id}...")
            feature_set.uint_range_to_float_range()

            scores = {**scores, **dict(fhmodel.predict(feature_set))}
            n_samples = feature_set.source_data_size
            slice_id += 1

        print("n_samples:", n_samples)

    if scores_path is not None:
        write_scores(scores_path, scores, n_samples)

    # https://github.com/tensorflow/tensorflow/issues/3388
    # K.clear_session()


def main():

    parser = argparse.ArgumentParser()
    parser.add_argument("-t", "--testing-set", required=True, help="Testing set")
    parser.add_argument("-m", "--model", required=True, help="Model input")
    parser.add_argument("-s", "--scores", default=None, help="Scores output file")
    args = parser.parse_args()

    config = make_config()

    if config["update_labels_if_insufficient_surface_support"]:
        print("Warning: labels not updated in predict mode.")
    if config["balance_classes"]:
        print("Warning: classes are not balanced in predict mode.")

    test(
        test_data_path=args.testing_set,
        model_input=args.model,
        scores_path=args.scores,
        config=config,
    )
