import argparse
import os
import logging
import gc
import pickle
from pathlib import Path

import tensorflow as tf
import tensorflow.compat.v1 as tfv1
from tensorflow.keras.layers import (
    Concatenate,
    Conv2D,
    Dense,
    Dropout,
    Flatten,
    Input,
    MaxPooling2D,
    BatchNormalization,
)
from tensorflow.keras.regularizers import l2
from tensorflow.keras.models import Model, Sequential
from tensorflow.keras.utils import plot_model
import numpy as np

from .Config import make_config
from . import analyse


class FootholdModel:
    def __init__(self, user_config=None, model_path=None):
        tfv1.logging.set_verbosity(tfv1.logging.ERROR)
        assert tf.keras.backend.image_data_format() != "channels_first"
        config = make_config(provided_conf=user_config)

        self.config = config
        self.keras_model = None
        self.callbacks = None
        self.model_path = model_path
        self.history = None

    def load_weights(self, model_path=None):
        if model_path:
            self.keras_model = tf.keras.models.load_model(model_path)
        else:
            assert self.model_path
            self.keras_model = tf.keras.models.load_model(self.model_path)

    def assemble_callbacks(self, model_path=None):
        callbacks = []
        if self.model_path:
            checkpointer = tf.keras.callbacks.ModelCheckpoint(
                filepath=self.model_path, monitor="val_accuracy", verbose=1, save_best_only=True
            )
            callbacks.append(checkpointer)

        class LossHistory(tf.keras.callbacks.Callback):
            def __init__(self):
                tf.keras.callbacks.Callback.__init__(self)
                self.losses = []

            def on_train_begin(self, logs=None):
                self.losses = []

            def on_batch_end(self, batch, logs=None):
                self.losses.append(logs.get("loss"))

        self.history = LossHistory()
        callbacks.append(self.history)

        self.callbacks = callbacks


    def build_model_functional(self):
        def color_feature_layers(x):
            if self.config["architecture"] == "mnist_less":
                x = Conv2D(8, (3, 3), activation="relu")(x)
                x = MaxPooling2D(pool_size=(2, 2))(x)
                x = Conv2D(16, (3, 3), activation="relu")(x)
                x = MaxPooling2D(pool_size=(2, 2))(x)
            elif self.config["architecture"] == "mnist":
                x = Conv2D(32, (3, 3), activation="relu")(x)
                x = MaxPooling2D(pool_size=(2, 2))(x)
                x = Conv2D(64, (3, 3), activation="relu")(x)
                x = MaxPooling2D(pool_size=(2, 2))(x)
                x = Dropout(0.5)(x)
            else:
                raise Exception(
                    'Architecture "{}" does not exist.'.format(
                        self.config["architecture"]
                    )
                )
            return x

        def depth_feature_layers(x):
            if self.config["architecture"] == "mnist_less":
                x = Conv2D(
                    8,
                    (3, 3),
                    activation="relu",
                    # kernel_regularizer=l2(0.01)
                )(x)
                x = MaxPooling2D(pool_size=(2, 2))(x)

            else:
                raise Exception(
                    'Architecture "{}" does not exist.'.format(
                        self.config["architecture"]
                    )
                )
            return x

        def head_layers(x):
            if self.config["architecture"] == "mnist_less":
                # x = BatchNormalization()(x)
                # x = Dropout(0.5)(x)
                # x = MaxPooling2D(pool_size=(2, 2))(x)
                x = Conv2D(16, (3, 3), activation="relu", kernel_regularizer=l2(0.01))(
                    x
                )
                # x = BatchNormalization()(x)
                # x = Dropout(0.5)(x)
                x = MaxPooling2D(pool_size=(2, 2))(x)
                x = Flatten()(x)
                x = Dense(64, activation="relu", kernel_regularizer=l2(0.01))(x)
                # x = Dropout(0.5)(x)
                x = Dense(
                    self.config["num_classes"],
                    activation="softmax",
                    kernel_regularizer=l2(0.01),
                )(x)
            else:
                raise Exception(
                    'Architecture "{}" does not exist.'.format(
                        self.config["architecture"]
                    )
                )
            return x

        if self.config["cat_color_and_depth"]:
            inputs = Input(shape=self.config["color_depth_input_shape"])
            x = color_feature_layers(inputs)
            x = head_layers(x)
            self.keras_model = Model(inputs=inputs, outputs=x)
        else:
            color_inputs = Input(shape=self.config["color_input_shape"])
            depth_inputs = Input(shape=self.config["depth_input_shape"])

            if self.config["use_color"] and self.config["use_depth"]:
                color_layers = color_feature_layers(color_inputs)
                depth_layers = depth_feature_layers(depth_inputs)

                x = Concatenate()(
                    [
                        Dense(64)(color_layers),
                        Dense(64)(depth_layers)
                    ]
                )
                inputs = [color_inputs, depth_inputs]
            elif self.config["use_color"] and not self.config["use_depth"]:
                x = color_feature_layers(color_inputs)
                inputs = color_inputs
            elif not self.config["use_color"] and self.config["use_depth"]:
                x = depth_feature_layers(depth_inputs)
                inputs = depth_inputs
            else:
                raise Exception("Model needs to use at least one input.")

            x = head_layers(x)
            self.keras_model = Model(inputs=inputs, outputs=x)

    def build_model(self, model_api="functional"):
        if model_api == "functional":
            self.build_model_functional()
        else:
            raise Exception('Model API "{}" does not exist.'.format(model_api))

        self.keras_model.compile(
            loss=tf.keras.losses.categorical_crossentropy,
            optimizer=tf.keras.optimizers.Adam(),
            metrics=["accuracy"],
        )

    def get_inputs(self, feature_set):
        if self.config["cat_color_and_depth"]:
            inputs = feature_set.data["cx"]
        else:
            inputs = []
            if self.config["use_color"]:
                inputs.append(feature_set.data["c"])
            if self.config["use_depth"]:
                inputs.append(feature_set.data["x"])
        if (len(inputs)) == 1:
            return inputs[0]
        return inputs


    def fit(self, data):
        if not self.keras_model:
            self.build_model()

        if not self.callbacks:
            self.assemble_callbacks()

        assert not data.validation.empty()
        line_delimiter="##############################"
        print(f"\n\n{line_delimiter}\n{self.config['input']}\n{line_delimiter}\n\n")
        gc.collect()
        history = self.keras_model.fit(
            self.get_inputs(data.train),
            data.train.data["y"],
            batch_size=self.config["batch_size"],
            epochs=self.config["epochs"],
            shuffle=True,  
            verbose=self.config["verbose"],
            validation_data=(
                self.get_inputs(data.validation),
                data.validation.data["y"],
            ),
            callbacks=self.callbacks,
        )

        self.keras_model.save(self.model_path)

        if self.model_path:
            self.load_weights()
        score = self.keras_model.evaluate(
            self.get_inputs(data.validation), data.validation.data["y"], verbose=0
        )
        best_score = score
        
        f = open( str(Path(self.model_path) / "config.pickle"), "wb")
        pickle.dump(self.config, f)
        f.close()

        f = open( str(Path(self.model_path) / "history.pickle"), "wb")
        pickle.dump(history, f)
        f.close()

        print("Test loss (last/max): %f / %f", (score[0], best_score[0]))
        print("Test accuracy (last/max): %f / %f:", (score[1], best_score[1]))
        return (self.keras_model, best_score)

    def predict(self, feature_set):
        prob = self.keras_model.predict(
            self.get_inputs(feature_set), verbose=self.config["verbose"]
        )
        assert len(prob) == len(feature_set.data["x"])
        y_success = prob[:, 1]
        if "i" in feature_set.data.keys():
            return list(zip(feature_set.data["i"], y_success))
        return list(zip(range(len(y_success)), y_success))

    def predict_generator(self, test_generator, data_dir):

        assert os.path.isdir(data_dir)
        classes = sorted(os.walk(data_dir).__next__()[1])
        ids = []
        for c in classes:
            c_dir = os.path.join(data_dir, c)
            c_ids = []
            walk = os.walk(c_dir).__next__()
            flag = "-depth.png"
            for sample in walk[2]:
                if flag in sample:
                    c_ids.append(int(sample.strip(flag)))
            ids.extend(sorted(c_ids))
        assert len(ids) % self.config["batch_size"] == 0
        steps = len(ids) // self.config["batch_size"]
        prob = self.keras_model.predict_generator(
            test_generator, verbose=self.config["verbose"], steps=steps
        )
        assert len(ids) == len(prob)
        y_success = prob[:, 1]
        return zip(ids, y_success)

    def plot(self, to_file):
        print("to_file:", to_file)
        plot_model(self.keras_model, to_file, show_names=False, show_shapes=True, show_layer_names=True)

    def summary(self):
        self.keras_model.summary()


def plot(model_path, output_path, print_summary=False):

    config = analyse.get_config(model_path)

    if config is None:
        raise Exception('Missing config.pickle in model directory')

    fhmodel = FootholdModel(model_path=model_path, user_config=config)
    fhmodel.load_weights()
    fhmodel.plot(output_path)
    if print_summary:
        fhmodel.summary()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-m", "--model", default=None, help="Model input")
    parser.add_argument("-o", "--output", default=None, help="Image output")
    args = parser.parse_args()
    plot(args.model, args.output)
