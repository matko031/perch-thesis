from pathlib import Path
from sklearn.metrics import roc_curve
import climbnet
#from tensorflow import keras
import pickle
from matplotlib import pyplot as plt
import re
import sys


def get_models(config, models_dir, required=False, unique=False):
    models_dir = Path(models_dir)
    model_paths = []
    for model in models_dir.iterdir():
        model_fits = True
        model_config = get_config_from_name(model.name)
        for k, v in config.items():
            if str(k) not in model_config or str(v) != str(model_config[str(k)]):
                model_fits = False
                break
        if model_fits:
            model_paths.append(str(model.resolve()))


    if unique and len(model_paths) > 1:
        raise Exception("Multiple models found: " + str(model_paths))

    if required and len(model_paths) < 1:
        raise Exception("No model found: " + str(config))

    if unique:
        return model_paths[0]

    return model_paths


def get_config(model_path):
    config_path = Path(model_path) / "config.pickle"
    if config_path.is_file():
        with open(config_path, "rb") as f:
            return pickle.load(f)

    return None

def get_history(model_path):
    history_path = Path(model_path) / "history.pickle"
    if history_path.is_file():
        with open(history_path, "rb") as f:
            return pickle.load(f).history

    return None


def plot_history(model_path, metrics):
    if not isinstance(metrics, list) and not isinstance(metrics, tuple):
        metrics = [metrics]

    history = get_history(model_path)
    for metric in metrics:
        plt.plot(history[metric])


def get_sources(sources_string):
    sources = sources_string.split("_")
    regex = "^([a-zA-Z]*)(\d)"
    training_source, training_nb = re.findall(regex, sources[0])[0]
    valid_source, valid_nb = re.findall(regex, sources[1])[0]
    return {
        "training": {
            "source": training_source,
            "number_descriptors": training_nb
        },
        "validation": {
            "source": valid_source,
            "number_descriptors": valid_nb
        }
    }

def get_config_from_name(model_path, oldname=False):
    name = Path(model_path).name

    input_regex ="-?(i_[a-zA-Z]+(?:-[a-zA-Z]+)*?)-([a-zA-Z]+_)"
    match = re.search(input_regex, name)
    start, end = match.span()
    if start == 0:
        name = match.group(2) + name[end:]
    else:
        name = name[:start] + "-" +  match.group(2) + name[end:]


    config = {}
    config["i"] = match.group(1)[2:]

    params = name.split("-") 
    if oldname:
        for param in params:
            if param.startswith("res"):
                config["res"] = int(param[3:])

            elif param.startswith("size"):
                config["size"] = int(param[4:])

            elif param.startswith("train"):
                config["train"] = param[5:]

            elif param.startswith("valid"):
                config["valid"] = param[5:]

            elif param.startswith("d"):
                config["d"] = param[1:]


            elif param.startswith("b"):
                config["b"] = int(param[1:])

            elif param.startswith("e"):
                config["e"] = int(param[1:])

            else:
                Exception("Unrecognized parameter: " + param)
    else:
        for param in params:
            k, v = param.split("_")       
            config[k] = v

    return config


def get_name_from_config(config):
    name = ""
    for k,v in config.items():
        name += f"{k}_{v}-"
    
    if len(name) > 0:
        return name[:-1]


def get_label(user_config, model_path, delim=", "):
    label_config = {}
    model_config = get_config_from_name(model_path)
    for k,v in model_config.items():
        if k not in user_config:
            label_config[k]=v

    label = ""

    for k, v in label_config.items():
        label += f"{str(k)}: {str(v)}{delim}"

    return label



def plot_last_metric(user_config, metric):
    models = get_models(user_config, "models")
    values = []
    labels = []

    for model in models:
        label = get_label(user_config, model, "\n")
        value = get_history(model)[metric][-1]

        labels.append(label)
        values.append(value)

    plt.bar(labels, values)
        


def print_metrics(user_config, metrics1):
    models = get_models(user_config, "models")
    if not isinstance(metrics, list) and not isinstance(metrics, tuple):
        metrics = [metrics]

    for model in models:
        history = get_history(model)
        metric_string = ""
        for metric in metrics:
            metric_string += f"{metric} {history[metric][-1]} - "

        metric_string = metric_string[:-1]

        print(f"{Path(model).name}: {metric_string}")



def plot_roc_curve(model_path, valid_data_path):
    predictions = climbnet.predict(model_path, valid_data_path).ravel()

    data = DataSplit()
    data.validation.read(
        path=val_data_path,
        balance_classes=False,
        update_labels_if_insufficient_surface_support=config[
            "update_labels_if_insufficient_surface_support"
        ],
        config=config,
    )

    data.uint_range_to_float_range()

    score = model.keras_model.evaluate(
        model.get_inputs(data.validation), data.validation.data["y"], verbose=0
        )



    #fpr_keras, tpr_keras, thresholds_keras = roc_curve(y_test, y_pred_keras)


if __name__ == "__main__":
    valid_data_path = "res1000-size96/rs/n1/combined.h5"

    {"res": 1000, "size": 96, "e": 100, }

    config = {"res": 1000, "size": 200, "train": "jpl10", "valid": "rs1", "e": 50, "b": 64}
    plt.figure(1)
    plot_last_metric(config, "accuracy")
    plt.ylim([0,1])
    plt.title("Training accuracy")

    plt.figure(2)
    plot_last_metric(config, "val_accuracy")
    plt.ylim([0,1])
    plt.title("Validation accuracy")


    #plt.show()
