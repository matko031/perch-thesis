import pickle
import argparse

from .logs_to_pd import logs_to_pd


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("logs")
    parser.add_argument("csv")
    args = parser.parse_args()

    with open(args.logs, "rb") as f:
        logs = pickle.load(f)
    df = logs_to_pd(logs, sort_key=None)
    df.to_csv(args.csv)
