import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()


def generate_install_requires():
    req_struct = [
        "matplotlib",
        "tqdm",
        "tensorflow",
        "ptvsd",
        "tables",
        "ipython",
        "scikit-image",
        "scikit-learn",
        "opencv-python",
    ]
    req_list = []
    for r in req_struct:
        if isinstance(r, str):
            r_string = r
        elif isinstance(r, dict):
            assert r["format"] == "github", "Only supporting GitHub format for now."
            r_string = "%s @ %s/tarball/%s" % (r["name"], r["url"], r["commit"])
            if "args" in r.keys():  # pylint: disable=no-member
                r_string += "#" + "&".join([f"{k}={v}" for (k, v) in r["args"].items()])
        req_list.append(r_string)
    return req_list


setuptools.setup(
    name="climbnet-detry",
    version="0.0.1",
    author="Renaud Detry",
    author_email="detry@jpl.nasa.gov",
    description="climbnet",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="",
    entry_points={
        "console_scripts": [
            "climbnet-train=climbnet.train:main",
            "climbnet-logs=climbnet.logs:main",
            "climbnet-predict=climbnet.predict:main",
            "climbnet-plot=climbnet.model:plot",
            "climbnet-h5cat=climbnet.h5cat:main",
        ],
    },
    packages=setuptools.find_packages(),
    install_requires=generate_install_requires(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],
    include_package_data=True,
)
