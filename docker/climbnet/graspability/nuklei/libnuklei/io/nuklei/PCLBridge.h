// (C) Copyright Renaud Detry   2007-2015.
// Distributed under the GNU General Public License and under the
// BSD 3-Clause License (See accompanying file LICENSE.txt).

/** @file */

#ifndef NUKLEI_PCL_BRIDGE_H
#define NUKLEI_PCL_BRIDGE_H

#ifdef NUKLEI_USE_PCL

#include <nuklei/KernelCollection.h>
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <boost/math/special_functions/fpclassify.hpp>

// Forward declaration of PCL types
//namespace pcl {
//  template <typename PointT>
//  class PointCloud;
//  
//  struct PointXYZ;
//  //struct PointXYZI;
//  //struct PointXYZRGBA;
//  struct PointXYZRGB;
//  struct PointNormal;
//  struct PointXYZRGBNormal;
//  //struct PointXYZINormal;
//}

namespace nuklei {
  
  inline bool valid(const pcl::PointXYZ& p)
  {
    float t = p.x + p.y + p.z;
    return !(boost::math::isnan)(t);
  }

  inline bool valid(const pcl::PointNormal& p)
  {
    float t = p.x + p.y + p.z + p.normal_x + p.normal_y + p.normal_z;
    return !(boost::math::isnan)(t);
  }

  inline bool valid(const pcl::PointXYZRGB& p)
  {
    float t = p.x + p.y + p.z; // colors may be nan
    return !(boost::math::isnan)(t);
  }

  inline bool valid(const pcl::PointXYZRGBNormal& p)
  {
    float t = p.x + p.y + p.z + p.normal_x + p.normal_y + p.normal_z; // colors may be nan
    return !(boost::math::isnan)(t);
  }

  
  inline pcl::PointXYZ pclPoint(const pcl::PointNormal& pn)
  {
    return pcl::PointXYZ(pn.x, pn.y, pn.z);
  }
  
  inline pcl::Normal pclNormal(const pcl::PointNormal& pn)
  {
    return pcl::Normal(pn.normal_x, pn.normal_y, pn.normal_z);
  }
  
  inline pcl::PointNormal pclPointNormal(const pcl::PointXYZ& p, const pcl::Normal& n)
  {
    pcl::PointNormal pn;
    pn.x = p.x;
    pn.y = p.y;
    pn.z = p.z;
    pn.normal_x = n.normal_x;
    pn.normal_y = n.normal_y;
    pn.normal_z = n.normal_z;
    return pn;
  }

  
  
  template<typename T>
  Vector3 wmCopy(const Eigen::Matrix<T, 3, 1>& e)
  {
    return Vector3(e.x(), e.y(), e.z());
  }
  
  template<typename T>
  Quaternion wmCopy(const Eigen::Quaternion<T>& e)
  {
    return Quaternion(e.w(), e.x(), e.y(), e.z());
  }
  
  template<typename T>
  Eigen::Matrix<T, 3, 1> eigenCopy(const Vector3& n)
  {
    return Eigen::Matrix<T, 3, 1>(n.X(), n.Y(), n.Z());
  }
  
  template<typename T>
  Eigen::Quaternion<T> eigenCopy(const Quaternion& n)
  {
    return Eigen::Quaternion<T>(n.W(), n.X(), n.Y(), n.Z());
  }

  
  
  inline Vector3 wmCopy(const pcl::PointXYZ& p)
  {
    return Vector3(p.x, p.y, p.z);
  }
  
  inline pcl::PointXYZ pclCopy(const Vector3& n)
  {
    return pcl::PointXYZ(n.X(), n.Y(), n.Z());
  }
  
  inline Vector3 wmNormalCopy(const pcl::Normal& p)
  {
    return Vector3(p.normal_x, p.normal_y, p.normal_z);
  }
  
  inline pcl::Normal pclNormalCopy(const Vector3& n)
  {
    return pcl::Normal(n.X(), n.Y(), n.Z());
  }
  
  template<typename PCLPoint>
  inline void copyXYZ(Vector3& v, const PCLPoint& p)
  {
    v.X() = p.x;
    v.Y() = p.y;
    v.Z() = p.z;
  }

  template<typename PCLPoint>
  inline void copyXYZ(PCLPoint& p, const Vector3& v)
  {
    p.x = v.X();
    p.y = v.Y();
    p.z = v.Z();
  }

  template<typename PCLPoint>
  inline void copyNormal(Vector3& v, const PCLPoint& p)
  {
    v.X() = p.normal_x;
    v.Y() = p.normal_y;
    v.Z() = p.normal_z;
  }
  
  template<typename PCLPoint>
  inline void copyNormal(PCLPoint& p, const Vector3& v)
  {
    p.normal_x = v.X();
    p.normal_y = v.Y();
    p.normal_z = v.Z();
  }

  
  template<typename T>
  Eigen::Matrix<T, 3, 1> eigenCopy(const pcl::PointXYZ& p)
  {
    return Eigen::Matrix<T, 3, 1>(p.x, p.y, p.z);
  }
  
  template<typename T>
  pcl::PointXYZ pclCopy(const Eigen::Matrix<T, 3, 1>& e)
  {
    return pcl::PointXYZ(e.x(), e.y(), e.z());
  }

  template<typename T>
  Eigen::Matrix<T, 3, 1> eigenNormalCopy(const pcl::Normal& p)
  {
    return Eigen::Matrix<T, 3, 1>(p.normal_x, p.normal_y, p.normal_z);
  }
  
  template<typename T>
  pcl::Normal pclNormalCopy(const Eigen::Matrix<T, 3, 1>& e)
  {
    return pcl::Normal(e.x(), e.y(), e.z());
  }

  
  kernel::r3 nukleiKernelFromPclPoint(const pcl::PointXYZ& p);

  kernel::r3 nukleiKernelFromPclPoint(const pcl::PointXYZRGB& p);
  
  /**
   *
   *
   * Warning: converts to axial orientation. 
   */
  kernel::r3xs2p nukleiKernelFromPclPoint(const pcl::PointNormal& p);
  
  /**
   *
   *
   * Warning: converts to axial orientation. 
   */
  kernel::r3xs2p nukleiKernelFromPclPoint(const pcl::PointXYZRGBNormal& p);

  

  void pclPointFromNukleiKernel(pcl::PointXYZ& p, const kernel::r3& k);

  void pclPointFromNukleiKernel(pcl::PointXYZ& p, const kernel::base& k);

  
  void pclPointFromNukleiKernel(pcl::PointXYZRGB& p, const kernel::r3& k);
  
  void pclPointFromNukleiKernel(pcl::PointXYZRGB& p, const kernel::base& k);

  
  /**
   *
   *
   * Warning: converts to axial orientation. 
   */
  void pclPointFromNukleiKernel(pcl::PointNormal& p, const kernel::r3xs2& k);
  void pclPointFromNukleiKernel(pcl::PointNormal& p, const kernel::r3xs2p& k);
  
  void pclPointFromNukleiKernel(pcl::PointNormal& p, const kernel::base& k);

  
  /**
   *
   *
   * Warning: converts to axial orientation. 
   */
  void pclPointFromNukleiKernel(pcl::PointXYZRGBNormal& p, const kernel::r3xs2& k);
  void pclPointFromNukleiKernel(pcl::PointXYZRGBNormal& p, const kernel::r3xs2p& k);
  
  void pclPointFromNukleiKernel(pcl::PointXYZRGBNormal& p, const kernel::base& k);

  
  template<typename PointT>
  KernelCollection nukleiFromPcl(const pcl::PointCloud<PointT>& cloud,
                                 const bool removeInvalidPoints = false)
  {
    KernelCollection kc;
    for (typename pcl::PointCloud<PointT>::const_iterator i = cloud.begin();
         i != cloud.end(); ++i)
    {
      if (valid(*i))
      {
        kc.add(nukleiKernelFromPclPoint(*i));
      }
      else
      {
        if (removeInvalidPoints) continue;
        kc.add(nukleiKernelFromPclPoint(*i));
        kc.back().setNaNFlag();
      }
    }
    if (!removeInvalidPoints && !cloud.is_dense)
    {
      kc.setGridSize(cloud.width, cloud.height);
    }
    return kc;
  }

  template<typename PointT>
  pcl::PointCloud<PointT> pclFromNuklei(const KernelCollection& kc)
  {
    pcl::PointCloud<PointT> cloud;

    for (KernelCollection::const_iterator i = kc.begin();
         i != kc.end(); ++i)
    {
      PointT point;
      pclPointFromNukleiKernel(point, *i);
      cloud.push_back(point);
    }

    if (kc.isOrganized())
    {
      KernelCollection::GridSize gs = kc.getGridSize();
      cloud.width    = gs.first;
      cloud.height   = gs.second;
      cloud.is_dense = false;
    }
    else
    {
      NUKLEI_ASSERT(cloud.width == kc.size() && cloud.height == 1 &&
                    cloud.is_dense == true);
    }

    return cloud;
  }

}

#endif


#endif
