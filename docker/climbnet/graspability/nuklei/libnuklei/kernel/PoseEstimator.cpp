// (C) Copyright Renaud Detry   2007-2015.
// Distributed under the GNU General Public License and under the
// BSD 3-Clause License (See accompanying file LICENSE.txt).

/** @file */

#include <nuklei/PoseEstimator.h>
#include <boost/bind.hpp>
#include <nuklei/parallelizer.h>
#include <nuklei/Plotter.h>

namespace nuklei
{
  Plotter p;
  int current_i = 0;
  
  PoseEstimator::PoseEstimator(const double locH,
                               const double oriH,
                               const int nChains,
                               const int n,
                               boost::shared_ptr<CustomIntegrandFactor> cif,
                               const bool partialview,
                               const bool progress) :
  objectCenter_(Vector3::ZERO), objectSize_(0), visu_(false),
  computeNormals_(true), light_(true), viewpoint_(Vector3::ZERO),
  evaluationStrategy_(KernelCollection::MAX_EVAL),
  loc_h_(locH), ori_h_(oriH),
  nChains_(nChains), chainLength_(-1), n_(n), tempInflection_(.2),
  independentProposalProbability_(.75),
  independentProposalPriorThreshold_(0),
  
  cif_(cif), partialview_(partialview), bidirectionalMatchingScore_(false),
  progress_(progress), meshTol_(4),
  sceneless_(false)
  {
    if (nChains_ <= 0) nChains_ = 8;
    parallel_ = typeFromName<parallelizer>(PARALLELIZATION);
  }
  
  double PoseEstimator::getLocH() const { return loc_h_; }
  void PoseEstimator::setLocH(const double locH)
  {
    loc_h_ = locH;
    sceneModel_.setKernelLocH(loc_h_);
    objectModel_.setKernelLocH(loc_h_);
  }
  
  double PoseEstimator::getOriH() const { return ori_h_; }
  void PoseEstimator::setOriH(const double oriH)
  {
    ori_h_ = oriH;
    sceneModel_.setKernelOriH(ori_h_);
    objectModel_.setKernelOriH(ori_h_);
  }
  
  
  kernel::se3
  PoseEstimator::modelToSceneTransformation()
  {
    NUKLEI_TRACE_BEGIN();
    KernelCollection kc = modelToSceneTransformations();
    NUKLEI_ASSERT(int(kc.size()) == 1);
    return kernel::se3(kc.front());
    NUKLEI_TRACE_END();
  }
  
  KernelCollection
  PoseEstimator::modelToSceneTransformations(const bool returnSample)
  {
    NUKLEI_TRACE_BEGIN();
    
    if (objectModel_.size() == 0 || (!sceneless_ && sceneModel_.size() == 0))
      NUKLEI_THROW("Empty input cloud.");
    
    if (objectModel_.front().polyType() == kernel::base::R3)
    {
      if (computeNormals_)
      {
        objectModel_.buildNeighborSearchTree();
        objectModel_.computeSurfaceNormals();
      }
    }
    
    if (!sceneless_)
      if (sceneModel_.front().polyType() == kernel::base::R3)
      {
        if (computeNormals_)
        {
          sceneModel_.buildNeighborSearchTree();
          sceneModel_.computeSurfaceNormals();
        }
      }
    
    if (objectModel_.front().polyType() != sceneModel_.front().polyType())
      NUKLEI_THROW("Input point clouds must be defined on the same domain.");
    
    if (light_ && sceneModel_.size() > 10000)
    {
      sceneModel_.computeKernelStatistics();
      KernelCollection tmp;
      KernelCollection::sample_iterator i =
      sceneModel_.sampleBegin(10000);
      for (; i != i.end(); i++)
      {
        tmp.add(*i);
      }
      sceneModel_ = tmp;
    }
    
    kernel::base::ptr moments = objectModel_.moments();
    objectCenter_ = moments->getLoc();
    objectSize_ = moments->getLocH();
    
    if (loc_h_ <= 0)
      loc_h_ = objectSize_ / 10;
    
    if (n_ <= 0)
    {
      n_ = objectModel_.size();
      if (n_ > 1000)
      {
        NUKLEI_WARN("Warning: Object model has more than 1000 points. "
                    "To keep computational cost low, only 1000 points will be "
                    "used at each inference loop. "
                    "Use -n to force a large number of model points.");
        n_ = 1000;
      }
    }
    
    if (chainLength_ <= 0) chainLength_ = 10*n_*(partialview_?4:1);
    
    sceneModel_.setKernelLocH(loc_h_);
    sceneModel_.setKernelOriH(ori_h_);
    objectModel_.setKernelLocH(loc_h_);
    objectModel_.setKernelOriH(ori_h_);
    
    objectModel_.computeKernelStatistics();
    sceneModel_.computeKernelStatistics();
    sceneModel_.buildKdTree();
    
    if (partialview_)
    {
      if (viewpoint_ == Vector3::ZERO)
        NUKLEI_WARN("Viewpoint at [0,0,0]");
      if (!meshFilename_.empty())
        objectModel_.readMeshFromOffFile(meshFilename_);
      else
      {
        NUKLEI_WARN("Building mesh for object model");
        objectModel_.buildMesh();
      }
      objectModel_.buildPartialViewCache(meshTol_, as_const(objectModel_).front().polyType() == kernel::base::R3XS2P);
    }
    
    checkDisplanarity();
    
    NUKLEI_LOG(
               "Parameters:\n" <<
               "  " << "Object model size: " << objectModel_.size() << "\n" <<
               "  " << "Scene model size: " << sceneModel_.size() << "\n" <<
               "  " << NUKLEI_NVP(n_) << "\n" <<
               "  " << NUKLEI_NVP(nChains_) << "\n" <<
               "  " << NUKLEI_NVP(chainLength_) << "\n" <<
               "  " << NUKLEI_NVP(loc_h_) << "\n" <<
               "  " << NUKLEI_NVP(ori_h_) << "\n" <<
               "  " << NUKLEI_NVP(tempInflection_) << "\n" <<
               "  " << NUKLEI_NVP(independentProposalProbability_) << "\n" <<
               "  " << NUKLEI_NVP(independentProposalPriorThreshold_) << "\n" <<
               "  " << "IP prior size: " << ipPrior_.size() << "\n" <<
               "  " << NUKLEI_NVP(partialview_) << "\n" <<
               "  " << NUKLEI_NVP(meshTol_) << "\n"
               );
    
    p.disable();
    
    KernelCollection poses, poseSample;
    if (!hasOpenMP())
    {
      NUKLEI_WARN("OpenMP not configured. Running on one core.");
    }
    
    if (progress_)
    {
      pi_.reset(new ProgressIndicator(0, chainLength_*nChains_ / 10, "Estimating pose ", 0));
      //pi_->initialize(0, chainLength_*nChains_ / 10, "Estimating pose ", 0);
    }
    
    NUKLEI_ASSERT(parallel_ == parallelizer::OPENMP || parallel_ == parallelizer::SINGLE);
#ifdef _OPENMP
#pragma omp parallel for
#endif
    for (int i = 0; i < nChains_; ++i)
    {
      kernel::se3 tmp;
      if (returnSample)
        tmp = mcmc(n_, poseSample);
      else
        tmp = mcmc(n_);
#ifdef _OPENMP
#pragma omp critical(nuklei_pe_data)
#endif
      {
        poses.add(tmp);
      }
    }
    
    if (progress_)
      pi_->forceEnd();
    
    if (returnSample)
    {
      for (KernelCollection::iterator i = poseSample.begin();
           i != poseSample.end(); i++)
        i->setWeight(findMatchingScore(dynamic_cast<const kernel::se3&>(*i)));
      NUKLEI_LOG("Pose sample size: " << poseSample.size());
      p.write_r("/shared/tmp/d.r");
      return poseSample;
    }
    else
    {
      kernel::se3 pose(*poses.sortBegin(1));
      pose.setWeight(findMatchingScore(pose));
      KernelCollection kc;
      kc.add(pose);
      return kc;
    }
    
    NUKLEI_TRACE_END();
  }
  
  double
  PoseEstimator::findMatchingScore(const kernel::se3& pose, bool bidirectional) const
  {
    NUKLEI_TRACE_BEGIN();
    kernel::se3 t = pose;
    
    if (!partialview_)
    {
      double w1 = 0;
      double w2 = 0;
      
      KernelCollection tmp = objectModel_;
      if (bidirectional)
      {
        tmp.transformWith(t);
        tmp.computeKernelStatistics();
        tmp.buildKdTree();
      }
      if (sceneless_)
        t.setWeight((cif_?cif_->factor(pose):1.));
      else
      {
        for (KernelCollection::const_iterator i = objectModel_.begin();
             i != objectModel_.end(); ++i)
        {
          w1 += sceneModel_.evaluationAt(*i->polyTransformedWith(t),
                                         evaluationStrategy_);
        }
        
        if (bidirectional)
        {
          for (KernelCollection::const_iterator i = sceneModel_.begin();
               i != sceneModel_.end(); ++i)
          {
            w2 += tmp.evaluationAt(*i, evaluationStrategy_);
          }
        }
        
        if (bidirectional)
          t.setWeight(std::sqrt(w1/objectModel_.size()*w2/objectModel_.size()) * (cif_?cif_->factor(pose):1.));
        else
          t.setWeight(w1/objectModel_.size() * (cif_?cif_->factor(pose):1.));
      }
      //fixme: shouldn't there be a multiplication of weight by cif_->test(pose)?
    }
    else
    {
      // Use a mesh to compute the partial view of the model and compute the
      // matching score from that.

      if (cif_ && !cif_->test(t))
        t.setWeight(0);
      else
      {
        KernelCollection::const_partialview_iterator viewIterator =
        objectModel_.partialViewBegin(viewpointInFrame(t), meshTol_, false, true);
        if (sceneless_)
          t.setWeight((cif_?cif_->factor(pose):1.));
        else
        {
          for (KernelCollection::const_partialview_iterator i = viewIterator;
               i != i.end(); ++i)
          {
            weight_t w = sceneModel_.evaluationAt(*i->polyTransformedWith(t),
                                                  evaluationStrategy_);
            t.setWeight(t.getWeight() + w);
          }
          t.setWeight(t.getWeight()/std::pow(std::distance(viewIterator, viewIterator.end()), .7) * (cif_?cif_->factor(pose):1.));
        }        
      }
    }
    return t.getWeight();
    NUKLEI_TRACE_END();
  }
  
  
  void PoseEstimator::loadObject(const std::string& objectFilename,
                                 const std::string& meshfile)
  {
    NUKLEI_TRACE_BEGIN();
    KernelCollection objectModel;
    readObservations(objectFilename, objectModel);
    setObjectModel(objectModel);
    setMeshFilename(meshfile);
    NUKLEI_TRACE_END();
  }
  
  void PoseEstimator::loadScene(const std::string& sceneFilename,
                                const std::string& viewpointfile,
                                const bool light)
  {
    NUKLEI_TRACE_BEGIN();
    KernelCollection sceneModel;
    if (!sceneless_)
      readObservations(sceneFilename, sceneModel);
    setSceneModel(sceneModel);
    setViewpoint(kernel::se3(*readSingleObservation(viewpointfile)).getLoc());
    setLight(light);
    NUKLEI_TRACE_END();
  }
  
  
  void PoseEstimator::load(const std::string& objectFilename,
                           const std::string& sceneFilename,
                           const std::string& meshfile,
                           const std::string& viewpointfile,
                           const bool light,
                           const bool computeNormals)
  {
    NUKLEI_TRACE_BEGIN();
    KernelCollection objectModel, sceneModel;
    readObservations(objectFilename, objectModel);
    if (!sceneless_)
      readObservations(sceneFilename, sceneModel);
    Vector3 viewpoint(0, 0, 0);
    if (partialview_)
    {
      NUKLEI_ASSERT(!viewpointfile.empty());
      viewpoint = kernel::se3(*readSingleObservation(viewpointfile)).getLoc();
    }
    load(objectModel, sceneModel, meshfile, viewpoint,
         light, computeNormals);
    NUKLEI_TRACE_END();
  }
  
  
  void PoseEstimator::load(const KernelCollection& objectModel,
                           const KernelCollection& sceneModel,
                           const std::string& meshfile,
                           const Vector3& viewpoint,
                           const bool light,
                           const bool computeNormals)
  {
    NUKLEI_TRACE_BEGIN();
    setObjectModel(objectModel);
    setSceneModel(sceneModel);
    setMeshFilename(meshfile);
    setViewpoint(viewpoint);
    setLight(light);
    setComputeNormals(computeNormals);
    NUKLEI_TRACE_END();
  }
  
  
  // Temperature function (cooling factor)
  double PoseEstimator::Ti(const unsigned i, const unsigned F)
  {
    {
      const double T0 = .5;
      const double TF = .05;
      
      return std::max(T0 * std::pow(TF/T0, double(i)/F), TF);
    }
  }
  
  bool PoseEstimator::recomputeIndices(std::vector<int>& indices,
                                       const kernel::se3& nextPose,
                                       const int n) const
  {
    Vector3 mean = objectModel_.mean()->getLoc();
    Vector3 v = la::normalized(viewpointInFrame(nextPose) - mean);
    std::vector<int> pindices = objectModel_.partialView(v, meshTol_, true, true);
    
    if (pindices.size() < 20) return false;
    indices = pindices;
    
    //      indices = objectModel_.partialView(viewpointInFrame(nextPose),
    //                                         meshTol_);
    std::random_shuffle(indices.begin(), indices.end(), Random::uniformInt);
    if (indices.size() > n)
      indices.resize(n);
    return true;
  }
  
  /**
   * This function implements the algorithm of Fig. 2: Simulated annealing
   * algorithm of the ACCV paper.
   * - T_j is given by @c temperature
   * - u is given by Random::uniform()
   * - w_j is @c currentPose
   */
  bool
  PoseEstimator::metropolisHastings(kernel::se3& currentPose,
                                    weight_t &currentWeight,
                                    const weight_t temperature,
                                    const bool firstRun,
                                    const int n) const
  {
    NUKLEI_TRACE_BEGIN();
    
    // Whether we go for a local or independent proposal
    bool independentProposal = Random::uniform() < independentProposalProbability_ || firstRun || currentWeight < FLOATTOL;
    
    // Most of the time, independent proposals won't be accepted anymore once the temperature has reached .05.
    if (independentProposal && temperature <= .05)
    {
      independentProposal = false;
    }
    
    if (independentProposalProbability_ <= 0)
      independentProposal = false;
    
    
    if (sceneless_) NUKLEI_ASSERT(independentProposal == false);
    
    // Randomly select particles from the object model
    std::vector<int> indices;
    for (KernelCollection::const_sample_iterator
         i = objectModel_.sampleBegin(n);
         i != i.end();
         i++)
    {
      indices.push_back(i.index());
    }
    std::random_shuffle(indices.begin(), indices.end(), Random::uniformInt);
    
    // Next chain state
    kernel::se3 nextPose;
    
    if (independentProposal)
    {
      // Go for independent proposal
      
      p.push("independent", current_i);
      
      for (int count = 0; ; ++count)
      {
        if (count == 100) return false;
        const kernel::base& randomModelPoint = objectModel_.at(indices.at(Random::uniformInt(indices.size())));
        kernel::se3::ptr k2 = randomModelPoint.polySe3Proj();
        kernel::se3::ptr k1;
        if (independentProposalIndices_.size() > 0)
          k1 = sceneModel_.at(independentProposalIndices_.at(Random::uniformInt(independentProposalIndices_.size()))).polySe3Proj();
        else
          k1 = sceneModel_.at(Random::uniformInt(sceneModel_.size())).polySe3Proj();
        
        nextPose = k1->transformationFrom(*k2);
        
        if (cif_ && !cif_->test(nextPose)) continue;
        
        if (partialview_)
        {
          bool visible = false;
          
          if (randomModelPoint.polyType() == kernel::base::R3XS2P)
            visible = objectModel_.isVisibleFrom(kernel::r3xs2p(randomModelPoint),
                                                 viewpointInFrame(nextPose),
                                                 meshTol_);
          else
            visible = objectModel_.isVisibleFrom(randomModelPoint.getLoc(),
                                                 viewpointInFrame(nextPose),
                                                 meshTol_);
          if (!visible) continue;
          
          if (! recomputeIndices(indices, nextPose, n))
            continue;
        }
        
        break;
      }
    }
    else
    {
      // Go for local proposal
      
      p.push("local", current_i);
      NUKLEI_DEBUG_ASSERT(currentPose.loc_h_ > 0 && currentPose.ori_h_ > 0);
      for (int count = 0; ; ++count)
      {
        if (count == 100) return false;
        nextPose = currentPose.sample();
        if (cif_ && !cif_->test(nextPose)) continue;
        if (partialview_ && ! recomputeIndices(indices, nextPose, n))
          continue;
        break;
      }
    }
    
    weight_t weight = 0;
    
    double threshold = Random::uniform();
    if (independentProposalProbability_ < 0)
      threshold = 1;
    
    double factor = (cif_?cif_->factor(nextPose):1.);
    
    // Go through the points of the model
    for (unsigned pi = 0; pi < indices.size(); ++pi)
    {
      const kernel::base& objectPoint =
      objectModel_.at(indices.at(pi));
      
      kernel::base::ptr test = objectPoint.polyTransformedWith(nextPose);
      
      weight_t w = 0;
      if (sceneless_) w = 1;
      else
      {
        if (WEIGHTED_SUM_EVIDENCE_EVAL)
        {
          w = (sceneModel_.evaluationAt(*test,
                                        KernelCollection::WEIGHTED_SUM_EVAL) +
               WHITE_NOISE_POWER/sceneModel_.size() );
        }
        else
        {
          w = (sceneModel_.evaluationAt(*test, KernelCollection::MAX_EVAL) +
               WHITE_NOISE_POWER );
        }
      }
      w *= factor;
      
      weight += w;
      
      // At least consider sqrt(size(model)) points
      if (pi < std::sqrt(indices.size())) continue;
      
      
      weight_t nextWeight = weight/(pi+1.);
      if (partialview_) nextWeight = weight/std::sqrt(pi+1.);
      // For the first run, consider all the points of the model
      if (firstRun)
      {
        if (pi == indices.size()-1)
        {
          currentPose = nextPose;
          currentWeight = nextWeight;
          return true;
        }
        else continue;
      }
      
      weight_t dec = std::pow(nextWeight/currentWeight, 1./temperature);
      if (independentProposal) dec *= currentWeight/nextWeight;
      if (currentWeight < FLOATTOL)
        dec = 1;

      // Early abort
      if (dec < .6*threshold)
      {
        p.push(independentProposal?"independent_f":"local_f", current_i);
        return false;
      }
      
      // MH decision
      if (pi == indices.size()-1)
      {
        if (dec > threshold)
        {
          p.push(independentProposal?"independent_s":"local_s", current_i);
          if (independentProposalProbability_ < 0)
            NUKLEI_ASSERT(nextWeight >= currentWeight);
          currentPose = nextPose;
          currentWeight = nextWeight;
          return true;
        }
        else
        {
          p.push(independentProposal?"independent_f":"local_f", current_i);
          return false;
        }
      }
    }
    NUKLEI_THROW("Reached forbidden state.");
    NUKLEI_TRACE_END();
  }
  
  kernel::se3
  PoseEstimator::mcmc(const int n, boost::optional<KernelCollection&> poseSample)
  {
    NUKLEI_TRACE_BEGIN();
    
    // begin and end bandwidths for the local proposal
    const coord_t bLocH = objectSize_/10;
    const coord_t eLocH = objectSize_/40;
    const coord_t bOriH = .1;
    const coord_t eOriH = .02;
    const unsigned e = chainLength_-1;
    
    kernel::se3 currentPose, bestPose;
    weight_t currentWeight = 0;
    currentPose.setLocH(bLocH);
    currentPose.setOriH(bOriH);
    bestPose.setWeight(currentWeight);
    metropolisHastings(currentPose, currentWeight, 1, true, n);
    int sinceLastDisplay = 0;
    
    
    for (int i = 0; i < chainLength_; i++)
    {
      current_i = i;
      {
        currentPose.setLocH(double(e-i)/e * bLocH +
                            double(i)/e * eLocH);
        currentPose.setOriH(double(e-i)/e * bOriH +
                            double(i)/e * eOriH);
        if (currentPose.loc_h_ <= 0)
        {
          NUKLEI_THROW("Unexpected value for currentPose.loc_h_.");
        }
        if (progress_ && i%10 == 0) pi_->mtInc();
      }
      sinceLastDisplay++;
      bool stepped = metropolisHastings(currentPose, currentWeight,
                                        Ti(i, chainLength_ * tempInflection_),
                                        false, n);
      if (stepped) p.push("weight", currentWeight);
      if (poseSample && stepped)
      {
#ifdef _OPENMP
#pragma omp critical(nuklei_pe_data)
#endif
        {
          poseSample->add(currentPose);
          poseSample->back().setWeight(currentWeight);
          if (grid_.isOrganized() && sinceLastDisplay > 900 && visu_)
          {
            grid_.computePdfImage(*poseSample);
            grid_.displayPdfImage();
            sinceLastDisplay = 0;
          }
        }
      }
      
      if (currentWeight > bestPose.getWeight())
      {
        bestPose = currentPose;
        bestPose.setWeight(currentWeight);
      }
    }
    
    return bestPose;
    NUKLEI_TRACE_END();
  }
  
  Vector3 PoseEstimator::viewpointInFrame(const kernel::se3& frame) const
  {
    kernel::r3 v;
    v.loc_ = viewpoint_;
    return v.transformedWith(frame.inverse()).getLoc();
  }
  
  void
  PoseEstimator::writeAlignedModel(const std::string& filename,
                                   const kernel::se3& t) const
  {
    NUKLEI_TRACE_BEGIN();
    KernelCollection objectModel = objectModel_;
    if (partialview_)
    {
      objectModel = KernelCollection();
      kernel::se3 tt = t;
      for (KernelCollection::const_iterator i = objectModel_.begin();
           i != objectModel_.end(); ++i)
      {
        objectModel.add(*i);
        bool visible = false;
        const kernel::base& tmp = *i;
        if (tmp.polyType() == kernel::base::R3XS2P)
          visible = objectModel_.isVisibleFrom(kernel::r3xs2p(tmp),
                                               viewpointInFrame(tt),
                                               meshTol_);
        else
          visible = objectModel_.isVisibleFrom(tmp.getLoc(),
                                               viewpointInFrame(tt),
                                               meshTol_);
        if (visible)
        {
          RGBColor c(0, 0, 1);
          ColorDescriptor d;
          d.setColor(c);
          objectModel.back().setDescriptor(d);
        }
        else
        {
          RGBColor c(.5, .5, .5);
          ColorDescriptor d;
          d.setColor(c);
          objectModel.back().setDescriptor(d);
        }
      }
    }
    objectModel.transformWith(t);
    writeObservations(filename,
                      objectModel,
                      Observation::PCD);
    NUKLEI_TRACE_END();
  }
  
  void PoseEstimator::writePdfImage(const std::string& filename,
                                    const KernelCollection& poseSample)
  {
    grid_.computePdfImage(poseSample);
    grid_.writePdfImage(filename);
  }
  
  void PoseEstimator::setCustomIntegrandFactor(boost::shared_ptr<CustomIntegrandFactor> cif)
  {
    cif_ = cif;
  }
  
  boost::shared_ptr<CustomIntegrandFactor> PoseEstimator::getCustomIntegrandFactor() const
  {
    return cif_;
  }
  
  void PoseEstimator::checkDisplanarity()
  {
    NUKLEI_ASSERT(!sceneless_); // not updated for sceneless
    if (ipPrior_.size() != 0 && as_const(sceneModel_).size() != 0)
    {
      NUKLEI_ASSERT(ipPrior_.size() == as_const(sceneModel_).size());
      for (unsigned i = 0; i < ipPrior_.size(); ++i)
      {
        NUKLEI_ASSERT(Vector3(ipPrior_.at(i).getLoc()-as_const(sceneModel_).at(i).getLoc()).SquaredLength() < FLOATTOL);
      }
      
      independentProposalIndices_.clear();
      for (unsigned int i = 0; i < ipPrior_.size(); ++i)
      {
        if (ipPrior_.at(i).getWeight() > independentProposalPriorThreshold_)
          independentProposalIndices_.push_back(i);
      }
    }
  }
  
  void PoseEstimator::setPartialView(const bool p)
  {
    if (bidirectionalMatchingScore_)
      NUKLEI_THROW("Warning: bidirectionalMatchingScore_ and partialview_ shouldn't be set together");
    partialview_ = p;
  }

  void PoseEstimator::setBidirectionalMatchingScore(const bool p)
  {
    if (partialview_ && p)
      NUKLEI_THROW("Warning: setBidirectionalMatchingScore has no effect when partial view is enabled");
    bidirectionalMatchingScore_ = p;
  }

  
}
