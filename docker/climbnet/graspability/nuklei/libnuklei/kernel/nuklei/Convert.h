// (C) Copyright Renaud Detry   2007-2015.
// Distributed under the GNU General Public License and under the
// BSD 3-Clause License (See accompanying file LICENSE.txt).

/** @file */

#ifndef NUKLEI_CONVERT_H
#define NUKLEI_CONVERT_H

#include <nuklei/ObservationIO.h>
#include <nuklei/simple_map.h>
#include <nuklei/KernelCollection.h>

namespace nuklei {
  
  struct Converter
  {
    Converter() :
    writerType_(Observation::UNKNOWN), readerType_(Observation::UNKNOWN) {}
    
    int convert(int argc, char ** argv);
    int concatenate(int argc, char ** argv);
    
    void run(const std::string& scriptFilename);
    
    void convert(const std::vector<std::string>& files,
                 const kernel::se3* transfo = NULL,
                 const double scale = 0,
                 const bool normalizePose = false,
                 const std::string& normalizingTransfoFile = "",
                 const bool normalizeScale = false,
                 const std::string& normalizingScaleFile = "",
                 const bool uniformizeWeights = false,
                 const Observation::Type inType = Observation::UNKNOWN,
                 const Observation::Type outType = Observation::UNKNOWN,
                 boost::shared_ptr<RegionOfInterest> roi = boost::shared_ptr<RegionOfInterest>(),
                 const int nObs = -1,
                 const double minDist = 0,
                 const bool removePlane = false,
                 const bool removeBelowPlane = false,
                 const bool readPlane = false,
                 const int ransacIter = 100,
                 const double inlierThreshold = 8,
                 const std::string& fittedPlaneFile = "",
                 const kernel::base::Type newKernelType = kernel::base::UNKNOWN,
                 bool removeNormals = false,
                 const std::string &filterRGB = "",
                 const std::string &setRGB = "",
                 const Color::Type colorToLoc = Color::UNKNOWN,
                 const bool preserveGrid = false,
                 const bool normalizeWeightsToScale1 = false);
    
  protected:
    NUKLEI_UNIQUE_PTR<ObservationWriter> writer_;
    Observation::Type writerType_;
    Observation::Type readerType_;
    simple_map<std::string, KernelCollection> swap_;
  };
  
  inline int convert(int argc, char ** argv)
  {
    Converter c;
    return c.convert(argc, argv);
  }

  inline int concatenate(int argc, char ** argv)
  {
    Converter c;
    return c.concatenate(argc, argv);
  }

}

#endif
