// (C) Copyright Renaud Detry   2007-2015.
// Distributed under the GNU General Public License and under the
// BSD 3-Clause License (See accompanying file LICENSE.txt).

/** @file */

#include <wordexp.h>
#include <string>
#include <limits>
#include <sys/time.h>
#include <sys/resource.h>
#include <boost/tuple/tuple.hpp>
#include <boost/algorithm/string/trim.hpp>

#include <tclap/CmdLine.h>
#include <nuklei/tclap-wrappers.h>
#include <nuklei/CoViS3DObservationIO.h>
#include <nuklei/SerializedKernelObservationIO.h>
#include <nuklei/Serial.h>
#include <nuklei/nullable.h>
#include <nuklei/ProgressIndicator.h>
#include <nuklei/ObservationIO.h>
#include <nuklei/simple_map.h>
#include <nuklei/Convert.h>

namespace nuklei
{
  void Converter::convert(const std::vector<std::string>& files,
                          const kernel::se3* transfo,
                          const double scale,
                          const bool normalizePose,
                          const std::string& normalizingTransfoFile,
                          const bool normalizeScale,
                          const std::string& normalizingScaleFile,
                          const bool uniformizeWeights,
                          const Observation::Type inType,
                          const Observation::Type outType,
                          boost::shared_ptr<RegionOfInterest> roi,
                          const int nObs,
                          const double minDist,
                          const bool removePlane,
                          const bool removeBelowPlane,
                          const bool readPlane,
                          const int ransacIter,
                          const double inlierThreshold,
                          const std::string& fittedPlaneFile,
                          const kernel::base::Type newKernelType,
                          bool removeNormals,
                          const std::string &filterRGB,
                          const std::string &setRGB,
                          const Color::Type colorToLoc,
                          const bool preserveGrid,
                          const bool normalizeWeightsToScale1)
  {
    //  bool storeInKc = removePlane || !fittedPlaneFile.empty() ||
    //    nObs >= 0 || minDist > 0 || minDist == -1 ||
    //    normalizePose || !normalizingTransfoFile.empty() ||
    //    normalizeScale || !normalizingScaleFile.empty() ||
    //    newKernelType != kernel::base::UNKNOWN || removeNormals ||
    //    !filterRGB.empty() || !setRGB.empty() || colorToLoc != Color::UNKNOWN;
    
    writerType_ = outType;
    KernelCollection points;
    
    if (preserveGrid)
    {
      NUKLEI_ASSERT(files.size() == 2);
      NUKLEI_ASSERT(roi);
      
      NUKLEI_ASSERT(files.front() != "-" && files.back() != "-");
      
      readObservationsWithSpecificFormat(files.front(), points, Observation::PCDGRID);
      
      for (KernelCollection::iterator i = points.begin();
           i != points.end(); ++i)
      {
        if (!roi->contains(i->getLoc()))
        {
          i->setLoc(Vector3(std::numeric_limits<double>::quiet_NaN(),
                            std::numeric_limits<double>::quiet_NaN(),
                            std::numeric_limits<double>::quiet_NaN()));
          if (i->hasDescriptor())
          {
            RGBColor& c = dynamic_cast<RGBColor&>(dynamic_cast<ColorDescriptor&>(i->getDescriptor()).getColor());
            c.setRGB(Vector3::ZERO);
          }
        }
      }
      
      writerType_ = Observation::PCD;
    }
    else
      for (std::vector<std::string>::const_iterator file_i = files.begin();
           file_i != --files.end(); ++file_i)
      {
        NUKLEI_UNIQUE_PTR<ObservationReader> reader;
        
        if (file_i->front() == '[')
        {
          NUKLEI_ASSERT(swap_.has_key(*file_i));
          const KernelCollection& kc = swap_[*file_i];
          if (roi)
          {
            for (KernelCollection::const_iterator k_i = kc.begin(); k_i != kc.end(); ++k_i)
              if (roi->contains(k_i->getLoc()))
                points.add(*k_i);
          }
          else
            points.add(kc);
        }
        else
        {
          if (inType == Observation::UNKNOWN)
            reader = ObservationReader::createReader(*file_i);
          else
            reader = ObservationReader::createReader(*file_i, inType);
          
          reader->addRegionOfInterest(roi);
          readerType_ = reader->type();

          if (writerType_ == Observation::UNKNOWN)
            writerType_ = reader->type();

          points.add(*reader->readObservations());
        }
      }
    
    for (KernelCollection::iterator i = points.begin();
         i != points.end(); ++i)
    {
      if (transfo != NULL) i->polyMakeTransformWith(*transfo);
      if (scale > 0) i->setLoc(i->getLoc()*scale);
    }
    
    if (uniformizeWeights)
    {
      points.uniformizeWeights();
    }
    
    if (normalizeWeightsToScale1)
    {
      points.scaleWeightsToMax1();
    }
    
    if (!preserveGrid &&
        files.size() > 2 && !uniformizeWeights &&
        (readerType_ == Observation::NUKLEI || readerType_ == Observation::SERIAL))
    {
      std::cout << "Warning: concatenating several files. "
      "Keep in mind that weights may not be consistently mixed. "
      "Use --uniformize_weights if appropriate." << std::endl;
    }
    
    //if (storeInKc)
    {
      if (removePlane || removeBelowPlane || !fittedPlaneFile.empty())
      {
        kernel::se3 k;
        if (readPlane)
        {
          k = kernel::se3(*readSingleObservation(fittedPlaneFile));
        }
        else
        {
          k = points.ransacPlaneFit(inlierThreshold, ransacIter);
        }
        
        Vector3 planeNormal = la::matrixCopy(k.ori_).GetColumn(2);
        if (planeNormal.Z() < 0) planeNormal.Z() = -planeNormal.Z();
        
        Plane3 plane(planeNormal, k.loc_);
        
        KernelCollection tmp = points;
        points.clear();
        for (KernelCollection::const_iterator
             i = tmp.begin();
             i != tmp.end(); ++i)
        {
          Vector3 loc = i->getLoc();
          double distToPlane = plane.DistanceTo(loc);
          if (removePlane && std::fabs(distToPlane) < inlierThreshold)
            continue;
          if (removeBelowPlane && distToPlane < 0)
            continue;
          points.add(*i);
        }
        
        if (!fittedPlaneFile.empty() && !readPlane)
        {
          writeSingleObservation(fittedPlaneFile, k, Observation::SERIAL);
        }
      }
      
      if (!filterRGB.empty())
      {
        Vector3 colorVector = numify<Vector3>(filterRGB);
        RGBColor rgbColor(colorVector.X(), colorVector.Y(), colorVector.Z());
        HSVConeColor hsvColor(rgbColor);
        KernelCollection tmp = points;
        points.clear();
        for (KernelCollection::const_iterator
             i = tmp.begin();
             i != tmp.end(); ++i)
        {
          if (i->hasDescriptor())
          {
            const ColorDescriptor* cDesc = dynamic_cast<const ColorDescriptor*>(&i->getDescriptor());
            HSVConeColor c(cDesc->getColor());
            if (cDesc != NULL &&
                (c.distanceTo(hsvColor) < HSVConeColor().getMaxDist() / 2))
              points.add(*i);
          }
        }
      }
      
      if (removeNormals)
      {
        KernelCollection kc1;
        for (KernelCollection::const_iterator
             i = points.begin();
             i != points.end(); ++i)
        {
          kernel::r3 k;
          k.loc_ = i->getLoc();
          k.setWeight(i->getWeight());
          if (i->hasDescriptor()) k.setDescriptor(i->getDescriptor());
          kc1.add(k);
        }
        
        points = kc1;
      }
      
      if (newKernelType != kernel::base::UNKNOWN)
      {
        if (newKernelType == kernel::base::R3)
        {
          points.removeNormalsOrOrientations();
        }
        else if (newKernelType == kernel::base::R3XS2P ||
                 newKernelType == kernel::base::R3XS2)
        {
          points.buildNeighborSearchTree();
          points.computeSurfaceNormals(newKernelType);
        }
        else NUKLEI_THROW("Unsupported kernel type.");
      }
      
      if (!setRGB.empty())
      {
        if (setRGB == "-")
        {
          for (KernelCollection::iterator
               i = points.begin();
               i != points.end(); ++i)
          {
            if (i->hasDescriptor())
              i->clearDescriptor();
          }
        }
        else
        {
          RGBColor rgbColor;
          try {
            Vector3 colorVector = numify<Vector3>(setRGB);
            rgbColor.setRGB(colorVector);
          } catch (Error & e) {
            Serial::readObject(rgbColor, setRGB);
          }
          for (KernelCollection::iterator
               i = points.begin();
               i != points.end(); ++i)
          {
            if (i->hasDescriptor())
              dynamic_cast<VisualDescriptor&>(i->getDescriptor()).setColor(rgbColor);
            else
            {
              ColorDescriptor cd;
              cd.setColor(rgbColor);
              i->setDescriptor(cd);
            }
          }
        }
      }
      
      if (normalizePose || !normalizingTransfoFile.empty())
      {
        KernelCollection kc1 = points;
        kc1.uniformizeWeights();
        kernel::se3 p = kc1.linearLeastSquarePlaneFit();
        kernel::se3 transfo = p.inverse();
        
        if (!normalizingTransfoFile.empty())
        {
          writeSingleObservation(normalizingTransfoFile, transfo, Observation::TXT);
          //Serial::writeObject(transfo, normalizingTransfoFile);
        }
        
        if (normalizePose)
        {
          std::cout << "Normalizing translation: " << transfo.loc_ << "\n" <<
          "Normalizing quaternion: " << transfo.ori_ << std::endl;
          
          points.transformWith(transfo);
        }
      }
      
      if (normalizeScale || !normalizingScaleFile.empty())
      {
        KernelCollection kc1 = points;
        
        kc1.uniformizeWeights();
        coord_t stdev = kc1.moments()->getLocH();
        
        if (!normalizingScaleFile.empty())
        {
          std::ofstream ofs(normalizingScaleFile.c_str());
          ofs << 1./stdev << std::endl;
        }
        
        if (normalizeScale)
        {
          std::cout << "Normalizing scale: " << 1./stdev << std::endl;
          
          for (KernelCollection::iterator
               i = points.begin();
               i != points.end(); ++i)
          {
            i->setLoc(i->getLoc()/stdev);
          }
        }
      }
      
      if (colorToLoc != Color::UNKNOWN)
      {
        KernelCollection tmp = points;
        points.clear();
        for (KernelCollection::const_iterator
             i = tmp.begin();
             i != tmp.end(); ++i)
        {
          NUKLEI_ASSERT(i->hasDescriptor());
          
          const Color &icolor = dynamic_cast<const ColorDescriptor&>(i->getDescriptor()).getColor();
          NUKLEI_UNIQUE_PTR<Color> ocolor;
          switch (colorToLoc)
          {
            case Color::RGB:
              ocolor = NUKLEI_UNIQUE_PTR<Color>(new RGBColor(icolor));
              break;
            case Color::HSV:
              ocolor = NUKLEI_UNIQUE_PTR<Color>(new HSVColor(icolor));
              break;
            case Color::HSVCONE:
              ocolor = NUKLEI_UNIQUE_PTR<Color>(new HSVConeColor(icolor));
              break;
            default:
              NUKLEI_ASSERT(false);
              break;
          }
          
          GVector v = ocolor->getVector();
          kernel::r3 colorKernel;
          for (int d = 0; d < 3; ++d)
          {
            colorKernel.loc_[d] = v[d];
          }
          colorKernel.setDescriptor(i->getDescriptor());
          points.add(colorKernel);
        }
      }
      
      double minDist2 = minDist;
      
      if (minDist2 == -1)
      {
        std::vector<double> distances;
        points.computeKernelStatistics();
        points.buildKdTree();
        for (KernelCollection::const_iterator
             i = as_const(points).begin();
             i != as_const(points).end(); ++i)
        {
          auto nn = points.knnSearch(std::distance(as_const(points).begin(), i), 1);
          distances.push_back(nn.front().second);
        }
        minDist2 = std::sqrt(*max_element(distances.begin(), distances.end()));
        

        std::cout << "Max distance between two nearest neighbors: " << minDist2 << std::endl;
      }
      
      if (minDist2 > 0)
      {
#define NUKLEI_NEW_MINDIST2
#ifdef NUKLEI_NEW_MINDIST2
        points = points.minDistSample(minDist2);
#else
        KernelCollection tmp = points;
        points.clear();
        std::vector<Vector3> posVec;
        ProgressIndicator pi(tmp.size(), "Filtering points: ");
        for (KernelCollection::const_iterator
             i = tmp.begin();
             i != tmp.end(); ++i)
        {
          bool cnt = false;
          Vector3 iLoc = i->getLoc();
          for (std::vector<Vector3>::const_iterator
               j = posVec.begin();
               j != posVec.end(); ++j)
          {
            if ( (iLoc - *j).SquaredLength() < minDist2*minDist2 )
            {
              cnt = true;
              break;
            }
          }
          if (!cnt)
          {
            points.add(*i);
            posVec.push_back(iLoc);
          }
          pi.inc();
        }
#endif
      }
      
      if (nObs >= 0)
      {
        points.computeKernelStatistics();
        KernelCollection tmp;
        for (KernelCollection::const_sample_iterator i = points.sampleBegin(nObs);
             i != i.end(); ++i)
          tmp.add(*i);
        points = tmp;
      }
    }

    if (files.back().front() == '[')
    {
      if (swap_.has_key(files.back()))
        swap_.erase(files.back());
      swap_.insert(files.back(), points);
      std::cout << "caching " << files.back() << " " << swap_[files.back()].size() << std::endl;
    }
    else
    {
      writer_ = ObservationWriter::createWriter(files.back(), writerType_);
      writer_->writeObservations(points);
      writer_->writeBuffer();
    }
  }
  
  int Converter::convert(int argc, char ** argv)
  {
    NUKLEI_TRACE_BEGIN();
    
    /* Parse command line arguments */
    
    TCLAP::CmdLine cmd(INFOSTRING + "Convert App." );
    
    /* Standard arguments */
    
    TCLAP::ValueArg<int> niceArg
    ("", "nice",
     "Proccess priority.",
     false, NICEINC, "int", cmd);
    
    /* Custom arguments */
    
    TCLAP::UnlabeledMultiArg<std::string> fileListArg
    ("",
     "List of p files. The (p-1) first files are read as input, "
     "optionally transformed, "
     "concatenated, "
     "and written to the last file of the list.",
     true, "filename", cmd);
    
    TCLAP::ValueArg<std::string> transformationArg
    ("", "transformation",
     "File containing an nuklei::kernel::se3.",
     false, "", "string", cmd);
    
    TCLAP::ValueArg<std::string> invTransformationArg
    ("", "inv_transformation",
     "File containing an nuklei::kernel::se3.",
     false, "", "string", cmd);
    
    TCLAP::ValueArg<double> scaleArg
    ("", "scale",
     "Scale factor.",
     false, 0, "float", cmd);
    
    TCLAP::ValueArg<std::string> translationArg
    ("t", "translation",
     "Translation vector, as \"tx ty tz\".",
     false, "", "string", cmd);
    
    TCLAP::SwitchArg normalizePoseArg
    ("", "normalize_pose",
     "Make center of gravity 0, and normalize ori.", cmd);
    
    TCLAP::ValueArg<std::string> normalizingTransfoFileArg
    ("", "normalizing_transfo",
     "File to which the normalizing transformation will be written.",
     false, "", "string", cmd);
    
    TCLAP::SwitchArg normalizeScaleArg
    ("", "normalize_scale",
     "Make radius of bounding sphere .5.", cmd);
    
    TCLAP::ValueArg<std::string> normalizingScaleFileArg
    ("", "normalizing_scale",
     "File to which the normalizing scale will be written.",
     false, "", "string", cmd);
    
    TCLAP::SwitchArg uniformizeWeightsArg
    ("", "uniformize_weights",
     "Uniformize weights (1 or 1/size).", cmd);

    TCLAP::SwitchArg normalizeWeightsToScale1Arg
    ("", "max1_weights",
     "Normalize weights to max at 1.", cmd);

    TCLAP::ValueArg<std::string> quaternionRotationArg
    ("q", "quaternion_rotation",
     "Rotation quaternion, as \"w x y z\".",
     false, "", "string", cmd);
    
    TCLAP::ConstrainedValueArg<std::string> inTypeArg
    ("r", "in_type",
     "Specifies the input file type.",
     false, nameFromType<Observation>(Observation::UNKNOWN),
     listTypeNames<Observation>(), cmd);
    
    TCLAP::ConstrainedValueArg<std::string> outTypeArg
    ("w", "out_type",
     "Specifies the output file type.",
     false, nameFromType<Observation>(Observation::UNKNOWN),
     listTypeNames<Observation>(), cmd);
    
    TCLAP::ValueArg<int> nObsArg
    ("n", "num_obs",
     "Number of output observations.",
     false, -1, "int", cmd);
    
    TCLAP::ValueArg<double> minDistArg
    ("", "min_dist",
     "In the output set, points are at least separated by the value of this "
     "argument.",
     false, 0, "float", cmd);
    
    TCLAP::SwitchArg removeDominantPlaneArg
    ("", "remove_dominant_plane",
     "Remove dominant plane.", cmd);
    
    TCLAP::SwitchArg removeBelowDominantPlaneArg
    ("", "remove_below_dominant_plane",
     "Remove points below dominant plane.", cmd);
    
    TCLAP::SwitchArg readDominantPlaneArg
    ("", "read_dominant_plane",
     "Read --fitted_plane argument instead of fitting plane.", cmd);
    
    TCLAP::ValueArg<int> ransacIterArg
    ("", "ransac_iter",
     "Number of RANSAC iterations in plane fitting. "
     "A value of 100 is a good start.",
     false, 100, "int", cmd);
    
    TCLAP::ValueArg<std::string> fittedPlaneArg
    ("", "fitted_plane",
     "Fit plane to data. Write the plane params to the provided file. "
     "The plane is written as an SE(3) point. The Z axis of the ori is normal "
     "to the plane. The other two axes indicate its principal components.",
     false, "", "string", cmd);
    
    TCLAP::ValueArg<double> removePlaneInlierTArg
    ("", "inlier_threshold",
     "Inlier threshold for removing plane with RANSAC. "
     "A value of 8mm is a good start",
     false, 8, "float", cmd);
    
    TCLAP::SwitchArg makeR3xs2pArg
    ("", "make_r3xs2p",
     "Compute an axial surface normal at each point, using local location differentials.", cmd);
    
    TCLAP::SwitchArg computeNormalsArg
    ("", "compute_normals",
     "Compute an axial surface normal at each point, using local location differentials.", cmd);
    
    TCLAP::ConstrainedValueArg<std::string> convertKernelTypeArg
    ("k", "kernel_type",
     "Either compute surface normals or remove them.",
     false, nameFromType<kernel::base>(kernel::base::UNKNOWN),
     listTypeNames<kernel::base>(), cmd);
    
    TCLAP::SwitchArg removeNormalsArg
    ("", "remove_normals",
     "Remove surface normals.", cmd);
    
    TCLAP::ValueArg<std::string> filterRGBArg
    ("", "filter_rgb",
     "Keep only elements close to that color.",
     false, "", "RGB triplet", cmd);
    
    TCLAP::ValueArg<std::string> setRGBColorArg
    ("", "set_rgb",
     "Set all elements to that color.",
     false, "", "RGB triplet", cmd);
    
    TCLAP::MultiArg<std::string> sphereROIArg
    ("", "sphere_roi",
     "Specifies a sphere region of interest, through center-radius string.",
     false, "string", cmd);
    
    TCLAP::MultiArg<std::string> nsphereROIArg
    ("", "nsphere_roi",
     "Specifies a negated-sphere region of interest, through center-radius string.",
     false, "string", cmd);
    
    TCLAP::MultiArg<std::string> boxROIArg
    ("", "box_roi",
     "Specifies a box region of interest, in the form \"cx cy cz qw qx qy qz sx sy sz\" "
     "where c is the box center, q is the box quaternion orientation, and s is the box scale (edge sizes).",
     false, "string", cmd);
    
    TCLAP::ConstrainedValueArg<std::string> colorToLocArg
    ("", "color_to_loc",
     "Triggers replacement of all kernel location from their color, expressed in the supplied colorspace.",
     false, nameFromType<Color>(Color::UNKNOWN),
     listTypeNames<Color>(), cmd);
    
    TCLAP::SwitchArg preserveGridArg
    ("g", "preserve_grid",
     "Preserve grid format.", cmd);
    
    //  TCLAP::MultiArg<std::string> boxROIArg
    //    ("", "box_roi",
    //     "Specifies a box region of interest, in the form \"cx cy cz a1x a1y a1z a2x a2y a2z e3\" "
    //     "where a1 is the axis going from the center of the box to the center of "
    //     "gravity of one of its faces, a2 is the same for an adjacent face, and e3 "
    //     "is the half of the box depth. ",
    //     false, "string", cmd);
    
    cmd.parse( argc, argv );
    
    if (-20 <= niceArg.getValue() && niceArg.getValue() <= 20)
      NUKLEI_ASSERT(setpriority(PRIO_PROCESS, 0, niceArg.getValue()) == 0);
    
    NUKLEI_ASSERT(fileListArg.getValue().size() > 1);
    
    kernel::se3::ptr transfo;
    
    if (!transformationArg.getValue().empty())
    {
      if (!invTransformationArg.getValue().empty())
        NUKLEI_THROW("Please specify either " << transformationArg.getName() <<
                     " or " << invTransformationArg.getName() << ".");
      if (!translationArg.getValue().empty())
        NUKLEI_THROW("Please specify either " << transformationArg.getName() <<
                     " or " << translationArg.getName() << ".");
      if (!quaternionRotationArg.getValue().empty())
        NUKLEI_THROW("Please specify either " << transformationArg.getName() <<
                     " or " << quaternionRotationArg.getName() << ".");
      transfo.reset(new kernel::se3);
      
      try {
        *transfo = kernel::se3(*readSingleObservation(transformationArg.getValue()));
      } catch (std::exception& e)
      {
        Serial::readObject(*transfo, transformationArg.getValue());
      }
    }
    else if (!invTransformationArg.getValue().empty())
    {
      if (!translationArg.getValue().empty())
        NUKLEI_THROW("Please specify either " << invTransformationArg.getName() <<
                     " or " << translationArg.getName() << ".");
      if (!quaternionRotationArg.getValue().empty())
        NUKLEI_THROW("Please specify either " << invTransformationArg.getName() <<
                     " or " << quaternionRotationArg.getName() << ".");
      kernel::se3 rt;
      try {
        rt = kernel::se3(*readSingleObservation(invTransformationArg.getValue()));
      } catch (std::exception& e)
      {
        Serial::readObject(rt, invTransformationArg.getValue());
      }
      transfo.reset(new kernel::se3(rt.inverse()));
    }
    else if (!translationArg.getValue().empty() || !quaternionRotationArg.getValue().empty())
    {
      transfo.reset(new kernel::se3);
      if (!translationArg.getValue().empty())
        transfo->loc_ = numify<Vector3>(translationArg.getValue());
      if (!quaternionRotationArg.getValue().empty())
        transfo->ori_ = la::normalized(numify<Quaternion>(quaternionRotationArg.getValue()));
    }
    
    boost::shared_ptr<RegionOfInterest> roi;
    
    for (std::vector<std::string>::const_iterator i = sphereROIArg.getValue().begin();
         i != sphereROIArg.getValue().end(); i++)
    {
      boost::shared_ptr<RegionOfInterest> sphereROI
      (new SphereROI(*i));
      if (roi)
        roi->enqueue(sphereROI);
      else
        roi = sphereROI;
    }
    for (std::vector<std::string>::const_iterator i = nsphereROIArg.getValue().begin();
         i != nsphereROIArg.getValue().end(); i++)
    {
      boost::shared_ptr<RegionOfInterest> sphereROI
      (new SphereROI(*i));
      sphereROI->setSign(false);
      if (roi)
        roi->enqueue(sphereROI);
      else
        roi = sphereROI;
    }
    for (std::vector<std::string>::const_iterator i = boxROIArg.getValue().begin();
         i != boxROIArg.getValue().end(); i++)
    {
      boost::shared_ptr<RegionOfInterest> boxROI
      (new BoxROI(*i));
      if (roi)
        roi->enqueue(boxROI);
      else
        roi = boxROI;
    }
    
    kernel::base::Type convertKernelType = kernel::base::UNKNOWN;
    if (makeR3xs2pArg.getValue() || computeNormalsArg.getValue())
      convertKernelType = kernel::base::R3XS2P;
    
    kernel::base::Type convertKernelType2 = typeFromName<kernel::base>(convertKernelTypeArg.getValue());
    if (convertKernelType == kernel::base::UNKNOWN)
      convertKernelType = convertKernelType2;
    else if (convertKernelType2 != kernel::base::UNKNOWN)
      NUKLEI_THROW("Conflicting normals-related arguments provided.");
    
    convert(fileListArg.getValue(), transfo.get(), scaleArg.getValue(),
            normalizePoseArg.getValue(), normalizingTransfoFileArg.getValue(),
            normalizeScaleArg.getValue(), normalizingScaleFileArg.getValue(),
            uniformizeWeightsArg.getValue(),
            typeFromName<Observation>(inTypeArg.getValue()),
            typeFromName<Observation>(outTypeArg.getValue()),
            roi,
            nObsArg.getValue(), minDistArg.getValue(),
            removeDominantPlaneArg.getValue(),
            removeBelowDominantPlaneArg.getValue(),
            readDominantPlaneArg.getValue(),
            ransacIterArg.getValue(),
            removePlaneInlierTArg.getValue(),
            fittedPlaneArg.getValue(),
            convertKernelType,
            removeNormalsArg.getValue(),
            filterRGBArg.getValue(),
            setRGBColorArg.getValue(),
            typeFromName<Color>(colorToLocArg.getValue()),
            preserveGridArg.getValue(),
            normalizeWeightsToScale1Arg.getValue());
    
    return 0;
    
    NUKLEI_TRACE_END();
  }

  int Converter::concatenate(int argc, char ** argv)
  {
    NUKLEI_TRACE_BEGIN();
    
    /* Parse command line arguments */
    
    TCLAP::CmdLine cmd(INFOSTRING + "Concatenation App." );
    
    /* Standard arguments */
    
    TCLAP::ValueArg<int> niceArg
    ("", "nice",
     "Proccess priority.",
     false, NICEINC, "int", cmd);
    
    /* Custom arguments */
    
    TCLAP::UnlabeledMultiArg<std::string> fileListArg
    ("",
     "List of p files. The (p-1) first files are read as input, "
     "concatenated, "
     "and written to the last file of the list.",
     true, "filename", cmd);
    
    cmd.parse( argc, argv );
    
    if (-20 <= niceArg.getValue() && niceArg.getValue() <= 20)
      NUKLEI_ASSERT(setpriority(PRIO_PROCESS, 0, niceArg.getValue()) == 0);
    
    NUKLEI_ASSERT(fileListArg.getValue().size() > 1);
    
    convert(fileListArg.getValue());
    
    return 0;
    
    NUKLEI_TRACE_END();
  }
  
  void Converter::run(const std::string& scriptFilename)
  {
    std::ifstream ifs(scriptFilename.c_str());
    NUKLEI_ASSERT(ifs.good());
    std::string line;
    
    while (std::getline(ifs, line))
    {
      boost::algorithm::trim(line);
      if (line.empty())
        continue;
      if (line.front() == '#')
        continue;
      
      wordexp_t we;
      wordexp(line.c_str(), &we, 0);
      
      int r = this->convert(we.we_wordc, we.we_wordv);
      NUKLEI_ASSERT(r == 0);
      
      wordfree(&we);
    }
  }

  
}



