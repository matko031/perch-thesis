// (C) Copyright Renaud Detry   2007-2015.
// Distributed under the GNU General Public License and under the
// BSD 3-Clause License (See accompanying file LICENSE.txt).

/** @file */

#include "KernelCollectionTypes.h"

#include <nuklei/KernelCollection.h>
#include <nuklei/ObservationIO.h>
#include <nuklei/ProgressIndicator.h>

#include "nanoflann.hpp"

namespace nuklei {
  
  namespace nanoflann_types
  {
    
    struct PointCloud
    {
      typedef double T;
      struct Point
      {
        Point(T x, T y, T z) : x(x), y(y), z(z) {}
        T  x,y,z;
      };
      
      std::vector<Point>  pts;
      
      // Must return the number of data points
      inline size_t kdtree_get_point_count() const { return pts.size(); }
      
      // Returns the distance between the vector "p1[0:size-1]" and the data point with index "idx_p2" stored in the class:
      inline T kdtree_distance(const T *p1, const size_t idx_p2,size_t size) const
      {
        const T d0=p1[0]-pts[idx_p2].x;
        const T d1=p1[1]-pts[idx_p2].y;
        const T d2=p1[2]-pts[idx_p2].z;
        return d0*d0+d1*d1+d2*d2;
      }
      
      // Returns the dim'th component of the idx'th point in the class:
      // Since this is inlined and the "dim" argument is typically an immediate value, the
      //  "if/else's" are actually solved at compile time.
      inline T kdtree_get_pt(const size_t idx, int dim) const
      {
        if (dim==0) return pts[idx].x;
        else if (dim==1) return pts[idx].y;
        else return pts[idx].z;
      }
      
      // Optional bounding-box computation: return false to default to a standard bbox computation loop.
      //   Return true if the BBOX was already computed by the class and returned in "bb" so it can be avoided to redo it again.
      //   Look at bb.size() to find out the expected dimensionality (e.g. 2 or 3 for point clouds)
      template <class BBOX>
      bool kdtree_get_bbox(BBOX &bb) const { return false; }
      
    };
    
    using namespace nuklei_nanoflann;
    typedef KDTreeSingleIndexAdaptor<
		L2_Simple_Adaptor<double, PointCloud > ,
		PointCloud,
		3 /* dim */
		> KDTreeIndex;
    typedef std::pair<boost::shared_ptr<KDTreeIndex>, PointCloud> Tree;
  }
  
  void KernelCollection::buildNeighborSearchTree()
  {
    NUKLEI_TRACE_BEGIN();
#ifdef NUKLEI_USE_CGAL
    using namespace cgal_neighbor_search_types;
    
    boost::shared_ptr<Tree> tree(new Tree);
    for (const_iterator i = as_const(*this).begin(); i != as_const(*this).end(); ++i)
    {
      Vector3 loc = i->getLoc();
      tree->insert(Point_d(loc.X(), loc.Y(), loc.Z()));
    }
    if (deco_.has_key(NSTREE_KEY)) deco_.erase(NSTREE_KEY);
    deco_.insert(NSTREE_KEY, tree);
    
#else
    NUKLEI_THROW("This function requires CGAL. See http://nuklei.sourceforge.net/doxygen/group__install.html");
#endif
    NUKLEI_TRACE_END();
  }
  
  void KernelCollection::buildKdTree()
  {
    if (as_const(*this).size() == 0) return;
    
    if (KDTREE_NANOFLANN)
    {
      using namespace nanoflann_types;
      
      PointCloud pc;
      for (const_iterator i = as_const(*this).begin(); i != as_const(*this).end(); ++i)
      {
        Vector3 loc = i->getLoc();
        pc.pts.push_back(PointCloud::Point(loc.X(), loc.Y(), loc.Z()));
      }
            
      boost::shared_ptr<Tree> tree(new Tree(boost::shared_ptr<KDTreeIndex>(), pc));
      tree->first = boost::shared_ptr<KDTreeIndex>(new KDTreeIndex(3 /*dim*/, tree->second, KDTreeSingleIndexAdaptorParams(10 /* max leaf */) ));
      tree->first->buildIndex();
      
      if (deco_.has_key(KDTREE_KEY)) deco_.erase(KDTREE_KEY);
      deco_.insert(KDTREE_KEY, tree);
    }
    else
    {
      using namespace libkdtree_types;
      
      boost::shared_ptr<Tree> tree(new Tree);
      for (const_iterator i = as_const(*this).begin(); i != as_const(*this).end(); ++i)
      {
        Vector3 loc = i->getLoc();
        tree->insert(FlexiblePoint(loc.X(), loc.Y(), loc.Z(),
                                   std::distance(as_const(*this).begin(), i)));
      }
      tree->optimise();
      
      if (deco_.has_key(KDTREE_KEY)) deco_.erase(KDTREE_KEY);
      deco_.insert(KDTREE_KEY, tree);
    }
  }
  
  std::vector< std::pair<size_t,coord_t> >
  KernelCollection::radiusSearch(const Vector3& center, const double radius) const
  {
    return radiusSearch(-1, center, radius);
  }

  std::vector< std::pair<size_t,coord_t> >
  KernelCollection::radiusSearch(const size_t i, const double radius) const
  {
    return radiusSearch(i, at(i).getLoc(), radius);
  }

  std::vector< std::pair<size_t,coord_t> >
  KernelCollection::radiusSearch(const size_t index, const Vector3& center, const double radius) const
  {
    NUKLEI_TRACE_BEGIN();

    using namespace nanoflann_types;

    NUKLEI_ASSERT(KDTREE_NANOFLANN);
    if (!deco_.has_key(KDTREE_KEY))
      NUKLEI_THROW("Undefined kd-tree. Call buildKdTree() first.");

    boost::shared_ptr<Tree> tree(deco_.get< boost::shared_ptr<Tree> >(KDTREE_KEY));


    std::vector<std::pair<size_t,coord_t> > indices_dists, filtered_indices_dists;

    // nanoflann::findNeighbors and nanoflann::radiusSearch cost the same as long
    // as SearchParams::sorted is set to false for radiusSearch
    //#define NUKLEI_USE_NANOFLANN_FINDNEIGHBORS
#ifdef NUKLEI_USE_NANOFLANN_FINDNEIGHBORS
    RadiusResultSet<coord_t,size_t> resultSet(radius*radius,indices_dists);
    as_const(*tree).first->findNeighbors(resultSet, center, nuklei_nanoflann::SearchParams());
#else    
    SearchParams params;
    params.sorted = false;
    as_const(*tree).first->radiusSearch(center, radius*radius, indices_dists, params);
#endif

    if (index >= 0)
      for (const auto& id : indices_dists)
        {
          if (index >= 0 && id.first == index) continue;
          filtered_indices_dists.push_back(id);
        }
    
    return filtered_indices_dists;
    
    NUKLEI_TRACE_END();
  }

  std::vector< std::pair<size_t,coord_t> >
  KernelCollection::knnSearch(const Vector3& center, const int k) const
  {
    return knnSearch(-1, center, k);
  }
  
  std::vector< std::pair<size_t,coord_t> >
  KernelCollection::knnSearch(const size_t i, const int k) const
  {
    return knnSearch(i, at(i).getLoc(), k);
  }
  
  // center is required.
  // index is optional. If index >= 0, it's removed fron neighborhood.
  std::vector< std::pair<size_t,coord_t> >
  KernelCollection::knnSearch(const size_t index, const Vector3& center, const int k) const
  {
    NUKLEI_TRACE_BEGIN();
    
    using namespace nanoflann_types;
    
    NUKLEI_ASSERT(KDTREE_NANOFLANN);
    if (!deco_.has_key(KDTREE_KEY))
      NUKLEI_THROW("Undefined kd-tree. Call buildKdTree() first.");
    
    boost::shared_ptr<Tree> tree(deco_.get< boost::shared_ptr<Tree> >(KDTREE_KEY));
    
    size_t num_results = k;
    if (index >= 0) num_results++; // because we will ultimately remove point #index
    std::vector<size_t>   ret_index(num_results);
    std::vector<coord_t> out_dist_sqr(num_results);
    
    num_results = as_const(*tree).first->knnSearch(center, num_results, &ret_index[0], &out_dist_sqr[0]);
    ret_index.resize(num_results);
    out_dist_sqr.resize(num_results);
    
    std::vector< std::pair<size_t,coord_t> > indices_dists;
    for (unsigned i = 0; i < ret_index.size(); ++i)
    {
      if (index >= 0 && ret_index.at(i) == index) continue;
      indices_dists.push_back(std::pair<size_t,coord_t>(ret_index.at(i), out_dist_sqr.at(i)));
    }
    
    return indices_dists;
    
    NUKLEI_TRACE_END();
  }

  
  template<class KernelType>
  weight_t KernelCollection::staticEvaluationAt(const kernel::base &k,
                                                const EvaluationStrategy strategy) const
  {
    NUKLEI_TRACE_BEGIN();
    
    NUKLEI_ASSERT(size() > 0);
    
    NUKLEI_ASSERT(KernelType().type() == *kernelType_);
    NUKLEI_ASSERT(*kernelType_ == k.polyType());
    
    coord_t value = 0;
    if (KDTREE_DENSITY_EVAL && size() > 1000)
    {
#if NUKLEI_CHECK_KDTREE_COUNT
      int n_inside = 0;
      coord_t rng = maxLocCutPoint()*maxLocCutPoint();
      {
        const KernelType &evalPoint = static_cast<const KernelType&>(k);
        for (const_iterator i = begin(); i != end(); i++)
        {
          const KernelType &densityPoint = static_cast<const KernelType&>(*i);
          
          if ((densityPoint.loc_-evalPoint.loc_).SquaredLength() < rng)
            n_inside++;
        }
      }
#endif
      
      if (KDTREE_NANOFLANN)
      {
        using namespace nanoflann_types;
        if (!deco_.has_key(KDTREE_KEY))
          NUKLEI_THROW("Undefined kd-tree. Call buildKdTree() first.");
        
        const KernelType &evalPoint = static_cast<const KernelType&>(k);
        
        boost::shared_ptr<Tree> tree(deco_.get< boost::shared_ptr<Tree> >(KDTREE_KEY));
        //FlexiblePoint s(evalPoint.loc_.X(), evalPoint.loc_.Y(), evalPoint.loc_.Z(), -1);
        
        coord_t range = maxLocCutPoint();
        // nanoflann takes squared distances.
        range = range*range;
        
        std::vector<std::pair<size_t,coord_t> > indices_dists;
        RadiusResultSet<coord_t,size_t> resultSet(range,indices_dists);
        
        as_const(*tree).first->findNeighbors(resultSet, evalPoint.loc_, nuklei_nanoflann::SearchParams());
        
        for (std::vector<std::pair<size_t,coord_t> >::const_iterator i = indices_dists.begin(); i != indices_dists.end(); i++)
        {
          coord_t cvalue = 0;
          const KernelType &densityPoint = static_cast<const KernelType&>(at(i->first));
          cvalue = densityPoint.eval(evalPoint);
          if (strategy == MAX_EVAL) value = std::max(value, cvalue);
          else if (strategy == WEIGHTED_MAX_EVAL) value = std::max(value, cvalue * densityPoint.getWeight());
          else if (strategy == SUM_EVAL) value += cvalue;
          else if (strategy == WEIGHTED_SUM_EVAL) value += cvalue * densityPoint.getWeight();
          else NUKLEI_ASSERT(false);
        }
      }
      else
      {
        using namespace libkdtree_types;
        if (!deco_.has_key(KDTREE_KEY))
          NUKLEI_THROW("Undefined kd-tree. Call buildKdTree() first.");
        
        const KernelType &evalPoint = static_cast<const KernelType&>(k);
        std::vector<FlexiblePoint> in_range;
        
        
        boost::shared_ptr<Tree> tree(deco_.get< boost::shared_ptr<Tree> >(KDTREE_KEY));
        FlexiblePoint s(evalPoint.loc_.X(), evalPoint.loc_.Y(), evalPoint.loc_.Z(), -1);
        
        coord_t range = maxLocCutPoint();
        
        as_const(*tree).find_within_range(s, range, std::back_inserter(in_range));
        
        for (std::vector<FlexiblePoint>::const_iterator i = in_range.begin(); i != in_range.end(); i++)
        {
          coord_t cvalue = 0;
          const KernelType &densityPoint = static_cast<const KernelType&>(at(i->idx()));
          cvalue = densityPoint.eval(evalPoint);
          if (strategy == MAX_EVAL) value = std::max(value, cvalue);
          else if (strategy == WEIGHTED_MAX_EVAL) value = std::max(value, cvalue * densityPoint.getWeight());
          else if (strategy == SUM_EVAL) value += cvalue;
          else if (strategy == WEIGHTED_SUM_EVAL) value += cvalue * densityPoint.getWeight();
          else NUKLEI_ASSERT(false);
        }
      }
    }
    else
    {
      const KernelType &evalPoint = static_cast<const KernelType&>(k);
      for (const_iterator i = begin(); i != end(); i++)
      {
        coord_t cvalue = 0;
        const KernelType &densityPoint = static_cast<const KernelType&>(*i);
        cvalue = densityPoint.eval(evalPoint);
        if (strategy == MAX_EVAL) value = std::max(value, cvalue);
        else if (strategy == WEIGHTED_MAX_EVAL) value = std::max(value, cvalue * densityPoint.getWeight());
        else if (strategy == SUM_EVAL) value += cvalue;
        else if (strategy == WEIGHTED_SUM_EVAL) value += cvalue * densityPoint.getWeight();
        else NUKLEI_ASSERT(false);
      }
    }
    
    return value;
    NUKLEI_TRACE_END();
  }
  
  weight_t KernelCollection::evaluationAt(const kernel::base &k,
                                          const EvaluationStrategy strategy) const
  {
    NUKLEI_TRACE_BEGIN();
    if (empty()) return 0;
    NUKLEI_ASSERT(kernelType_ == k.polyType());
    
    coord_t value = 0;
    switch (*kernelType_)
    {
      case kernel::base::R3:
      {
        value = staticEvaluationAt<kernel::r3>(k, strategy);
        break;
      }
      case kernel::base::R3XS2:
      {
        value = staticEvaluationAt<kernel::r3xs2>(k, strategy);
        break;
      }
      case kernel::base::R3XS2P:
      {
        value = staticEvaluationAt<kernel::r3xs2p>(k, strategy);
        break;
      }
      case kernel::base::SE3:
      {
        value = staticEvaluationAt<kernel::se3>(k, strategy);
        break;
      }
      default:
      {
        // One could implement a fallback polymorphic eval here (polyEval), but
        // it's so slow that it's not really useful.
        NUKLEI_THROW("Unknow kernel type.");
        break;
      }
    }
    
    return value;
    NUKLEI_TRACE_END();
  }
  
  std::vector<double> KernelCollection::displanarityCoefficient() const
  {
    NUKLEI_TRACE_BEGIN();
    NUKLEI_ASSERT(KDTREE_NANOFLANN);
    if (!deco_.has_key(KDTREE_KEY))
      NUKLEI_THROW("Undefined kd-tree. Call buildKdTree() first.");

    KernelCollection grid;
    coord_t range = .1;
    int disc = 5;
    for (double x = -range; x < range; x += range/disc)
      for (double y = -range; y < range; y += range/disc)
      {
        kernel::r3 k;
        k.loc_ = Vector3(x, y, 0);
        if (k.loc_.Length() < range)
          grid.add(k);
      }
    
    if (0)
    {
      // Debug
      kernel::r3 test, test2;
      test.loc_ = Vector3::ZERO;
      test.loc_h_ = 1;
      test2 = test;
      test2.loc_.X() = test.loc_h_*2.1;
      NUKLEI_ASSERT(test.eval(test2) == 0);
      test2.loc_.X() = test.loc_h_*1.9;
      NUKLEI_ASSERT(test.eval(test2) > 0);
    }
    
    ProgressIndicator pi(size(), "Computing displanarity: ", Log::INFO);
    std::vector<double> v(size());
#ifdef _OPENMP
#pragma omp parallel for
#endif
    for (unsigned i = 0; i < size(); i++)
    {
      using namespace nanoflann_types;
      
      const kernel::base& evalPoint = at(i);
      
      boost::shared_ptr<Tree> tree(deco_.get< boost::shared_ptr<Tree> >(KDTREE_KEY));
      
      std::vector<std::pair<size_t,coord_t> > indices_dists;
      RadiusResultSet<coord_t,size_t> resultSet(range*range,indices_dists);
      
      as_const(*tree).first->findNeighbors(resultSet, evalPoint.getLoc(), nuklei_nanoflann::SearchParams());
      
      KernelCollection local;
      kernel::se3 frame;
      {
        const kernel::r3xs2* r3xs2kp = dynamic_cast<const kernel::r3xs2*>(&evalPoint);
        const kernel::r3xs2p* r3xs2pkp = dynamic_cast<const kernel::r3xs2p*>(&evalPoint);
        if (r3xs2kp != NULL)
        {
          frame.loc_ = r3xs2kp->loc_;
          frame.ori_ = la::so3FromS2(r3xs2kp->dir_, 2);
        }
        else if (r3xs2pkp != NULL)
        {
          frame.loc_ = r3xs2pkp->loc_;
          frame.ori_ = la::so3FromS2(r3xs2pkp->dir_, 2);
        }
        else NUKLEI_THROW("Incompatible point type");
      }
      Vector3 sum = Vector3::ZERO;
      double zdists = 0;

      KernelCollection kc;
      for (std::vector<std::pair<size_t,coord_t> >::const_iterator near = indices_dists.begin(); near != indices_dists.end(); near++)
      {
        kernel::r3 k;
        k.loc_ = la::project(frame.loc_, frame.ori_, at(near->first).getLoc());
        sum += k.loc_;
        zdists += std::fabs(k.loc_.Z());
        kc.add(k);
      }
      
      kc.computeKernelStatistics();
      kc.setKernelLocH(.02);
      if (kc.size() > 1000) kc.buildKdTree();
      double gridCoeff = 0;
      for (KernelCollection::const_iterator g_i = grid.begin(); g_i != grid.end(); ++g_i)
      {
        gridCoeff += kc.evaluationAt(*g_i, KernelCollection::MAX_EVAL) == 0;
      }
#if 0
      writeObservations("/tmp/kc.xml", kc);
      char ch = 0;
      do {
        std::cout << "Char: " << std::flush;
        std::cin >> ch;
      }
      while (ch != 'c');
#endif
#ifdef _OPENMP
#pragma omp critical(nuklei_displanarity)
#endif
      {
        //v.push_back(zdists);
        v.at(i) = gridCoeff;
        //v.push_back( (zdists+2*std::fabs(sum.X())+2*std::fabs(sum.Y())) / indices_dists.size()  );
        //v.push_back(zdists+std::fabs(sum.X())+std::fabs(sum.Y()));
        pi.inc();
      }
    }
    
    return v;
    NUKLEI_TRACE_END();
  }

  KernelCollection KernelCollection::minDistSample(const double d) const
  {
    NUKLEI_TRACE_BEGIN();
    using namespace libkdtree_types;

    ProgressIndicator pi(size(), "Computing min-dist sample: ", Log::INFO);
    KernelCollection sample;
    boost::shared_ptr<Tree> tree(new Tree);

    for (KernelCollection::const_iterator
         i = begin();
         i != end(); ++i)
    {
      const Vector3& iLoc = i->getLoc();
      FlexiblePoint s(iLoc.X(), iLoc.Y(), iLoc.Z(),
                      std::distance(as_const(*this).begin(), i));
      std::vector<FlexiblePoint> in_range;
      as_const(*tree).find_within_range(s, d, std::back_inserter(in_range));
      bool hasNeighbor = false;
      for (std::vector<FlexiblePoint>::const_iterator n_i = in_range.begin(); n_i != in_range.end(); n_i++)
      {
        double distance = FlexiblePoint::Distance().transformed_distance(*n_i, s);
        if (distance < d*d)
        {
          hasNeighbor = true;
          break;
        }
      }

      if (!hasNeighbor)
      {
        sample.add(*i);
        tree->insert(s);
        //tree->optimise();
      }
      
      pi.inc();
    }

    return sample;
    NUKLEI_TRACE_END();
  }


}

