// (C) Copyright Renaud Detry   2007-2015.
// Distributed under the GNU General Public License and under the
// BSD 3-Clause License (See accompanying file LICENSE.txt).

/** @file */

#include <boost/math/special_functions/fpclassify.hpp>
#include <nuklei/KernelCollection.h>
#include <nuklei/ObservationIO.h>
#include <nuklei/Image.h>

#ifdef NUKLEI_USE_OPENCV
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#endif

namespace nuklei {

#ifdef NUKLEI_USE_OPENCV
  const double cw = .1;
#endif
  
  void KernelCollection::computeColorImage()
  {
    NUKLEI_TRACE_BEGIN();
#ifdef NUKLEI_USE_OPENCV
    NUKLEI_ASSERT(isOrganized());
    NUKLEI_ASSERT(!deco_.has_key(COLORIMAGE_KEY));
    
    GridSize gs = getGridSize();
    
    cv::Mat image(gs.second, gs.first, CV_8UC3);
    
    for (int i = 0; i < gs.first; ++i)
      for (int j = 0; j < gs.second; ++j)
      {
        const RGBColor& c = dynamic_cast<const RGBColor&>(dynamic_cast<const ColorDescriptor&>(at(i,j).getDescriptor()).getColor());
        bgr_t& bgr = image.at<bgr_t>(j,i);
        bgr.r = c.R()*255;
        bgr.g = c.G()*255;
        bgr.b = c.B()*255;
        bgr.r = cw * bgr.r + (1-cw)*255;
        bgr.g = cw * bgr.g + (1-cw)*255;
        bgr.b = cw * bgr.b + (1-cw)*255;
      }
    
    if (deco_.has_key(COLORIMAGE_KEY)) deco_.erase(COLORIMAGE_KEY);
    deco_.insert(COLORIMAGE_KEY, image);
    
    //cv::namedWindow("Color Image", cv::WINDOW_AUTOSIZE);
    
#else
    NUKLEI_THROW("This function requires OpenCV.");
#endif
    NUKLEI_TRACE_END();
  }

  void KernelCollection::computePdfImage(const KernelCollection& density)
  {
    NUKLEI_TRACE_BEGIN();
#ifdef NUKLEI_USE_OPENCV
    
    KernelCollection d2;
    const KernelCollection& cloud = *this;
    
    for (KernelCollection::const_iterator i = density.begin();
         i != density.end(); ++i)
    {
      kernel::r3 kk;
      kk.loc_ = i->getLoc();//+Vector3(la::matrixCopy(kernel::se3(*i).ori_).GetColumn(2))*0.237617;
      kk.setWeight(i->getWeight());
      kk.loc_h_ = i->getLocH();
      d2.add(kk);
    }
    
    d2.setKernelLocH(.04);
    
    d2.normalizeWeights();
    d2.computeKernelStatistics();
    d2.buildKdTree();
    
    double maxw = 0;
    std::vector<double> weights;
    for (KernelCollection::const_iterator i = cloud.begin();
         i != cloud.end(); ++i)
    {
      if ((boost::math::isnan)(i->getLoc().Z())) continue;
      coord_t iv = as_const(d2).evaluationAt(*i, KernelCollection::WEIGHTED_MAX_EVAL);
      maxw = std::max(iv, maxw);
      weights.push_back(iv);
    }
    
    GridSize gs = getGridSize();
    
    if (!deco_.has_key(PDFIMAGE_KEY)) deco_.insert(PDFIMAGE_KEY, cv::Mat(gs.second, gs.first, CV_8UC3));
    cv::Mat& image = deco_.get<cv::Mat>(PDFIMAGE_KEY);
    cv::Mat& origImage = deco_.get<cv::Mat>(COLORIMAGE_KEY);

    for (int i = 0, j = 0; i < cloud.size(); ++i)
    {
      double w = 0;
      if (!(boost::math::isnan)(cloud.at(i).getLoc().Z()))
        w = weights.at(j++)/maxw;
      
      //if (w < 0) w = 0; if (w > 1) w = 1;
      bgr_t& origbgr = origImage.at<bgr_t>(i);
      bgr_t& bgr = image.at<bgr_t>(i);
      bgr.r = origbgr.r;
      bgr.g = (origbgr.g - (1-cw)*255) + (1-cw)*(1-w)*255;
      bgr.b = (origbgr.b - (1-cw)*255) + (1-cw)*(1-w)*255;
      /*
      bgr.r = c.R()*255;
      bgr.g = c.G()*255;
      bgr.b = c.B()*255;
      
      RGBColor o(dynamic_cast<const ColorDescriptor&>(cloud.at(i).getDescriptor()).getColor());
      RGBColor m(HSVColor(0./180.*M_PI, w, 1));
      RGBColor c;
      c.setVector(.1*o.getVector()+.9*m.getVector());
      bgr.r = c.R()*255;
      bgr.g = c.G()*255;
      bgr.b = c.B()*255;
       */
    }

#else
    NUKLEI_THROW("This function requires OpenCV.");
#endif
    NUKLEI_TRACE_END();
  }
  
  void KernelCollection::displayColorImage() const
  {
    NUKLEI_TRACE_BEGIN();
#ifdef NUKLEI_USE_OPENCV
    
    NUKLEI_ASSERT(isOrganized());
    NUKLEI_ASSERT(deco_.has_key(COLORIMAGE_KEY));

    const cv::Mat& image = deco_.get<cv::Mat>(COLORIMAGE_KEY);

    cv::imshow("Color Image", image);
    cv::waitKey(1);
    
#else
    NUKLEI_THROW("This function requires OpenCV.");
#endif
    NUKLEI_TRACE_END();
  }

  void KernelCollection::displayPdfImage() const
  {
    NUKLEI_TRACE_BEGIN();
#ifdef NUKLEI_USE_OPENCV
    
    NUKLEI_ASSERT(isOrganized());
    NUKLEI_ASSERT(deco_.has_key(PDFIMAGE_KEY));
    
    const cv::Mat& image = deco_.get<cv::Mat>(PDFIMAGE_KEY);
    
    cv::imshow("Color Image", image);
    cv::waitKey(1);
    
#else
    NUKLEI_THROW("This function requires OpenCV.");
#endif
    NUKLEI_TRACE_END();
  }

  void KernelCollection::writeColorImage(const std::string& filename) const
  {
    NUKLEI_TRACE_BEGIN();
#ifdef NUKLEI_USE_OPENCV
    
    NUKLEI_ASSERT(isOrganized());
    NUKLEI_ASSERT(deco_.has_key(COLORIMAGE_KEY));
    
    const cv::Mat& image = deco_.get<cv::Mat>(COLORIMAGE_KEY);
    
    cv::imwrite(filename, image);
    
#else
    NUKLEI_THROW("This function requires OpenCV.");
#endif
    NUKLEI_TRACE_END();
  }
  
  void KernelCollection::writePdfImage(const std::string& filename) const
  {
    NUKLEI_TRACE_BEGIN();
#ifdef NUKLEI_USE_OPENCV
    
    NUKLEI_ASSERT(isOrganized());
    NUKLEI_ASSERT(deco_.has_key(PDFIMAGE_KEY));
    
    const cv::Mat& image = deco_.get<cv::Mat>(PDFIMAGE_KEY);
    
    cv::imwrite(filename, image);
    
#else
    NUKLEI_THROW("This function requires OpenCV.");
#endif
    NUKLEI_TRACE_END();
  }

}

