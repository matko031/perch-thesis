// (C) Copyright Renaud Detry   2007-2015.
// Distributed under the GNU General Public License and under the
// BSD 3-Clause License (See accompanying file LICENSE.txt).

/** @file */

#ifdef NUKLEI_USE_OPENCV
#include <opencv2/core/core.hpp>
#endif
#ifdef NUKLEI_USE_YAMLCPP
#include <yaml-cpp/yaml.h>
#endif

#include <nuklei/Camera.h>


namespace nuklei
{
  
  Camera::Camera()
  {
    
  }
  
  std::vector<Vector2> Camera::project(const std::vector<Vector3> worldPoints) const
  {
    std::vector<Vector2> imagePoints;
    for (std::vector<Vector3>::const_iterator i = worldPoints.begin();
         i != worldPoints.end(); ++i)
    {
      imagePoints.push_back(project(*i));
    }
    return imagePoints;
  }
  
  Vector2 Camera::project(const Vector3& p) const
  {
    Vector4 proj = projection_ * Vector4(p.X(), p.Y(), p.Z(), 1);
    Vector2 pix_coord(proj.X()/proj.Z(), proj.Y()/proj.Z());
    return pix_coord;
  }
  
  void Camera::read(const std::string& opencvStereoCalibFile)
  {
    NUKLEI_TRACE_BEGIN();
#ifdef NUKLEI_USE_YAMLCPP
    YAML::Node calib = YAML::LoadFile(opencvStereoCalibFile);
    YAML::Node projMatrixNode = calib["projection_matrix"];
    int rows = projMatrixNode["rows"].as<int>();
    int cols = projMatrixNode["cols"].as<int>();
    NUKLEI_ASSERT(rows == 3 && cols == 4);
    YAML::Node projNode = projMatrixNode["data"];
    NUKLEI_ASSERT(projNode.IsSequence());
    YAML::const_iterator it = projNode.begin();
    for (int r = 0; r < 3; ++r)
      for (int c = 0; c < 4; ++c)
      {
        NUKLEI_ASSERT(it != projNode.end());
        projection_(r,c) = it->as<double>();
        it++;
      }
    projection_(3, 0) = projection_(3, 1) = projection_(3, 2) = 0;
    projection_(3, 3) = 1;
    NUKLEI_ASSERT(it == projNode.end());
#else
    NUKLEI_THROW("This function requires yaml-cpp");
#endif
    NUKLEI_TRACE_END();
  }
  
}
