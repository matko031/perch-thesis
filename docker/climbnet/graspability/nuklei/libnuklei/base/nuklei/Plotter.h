// (C) Copyright Renaud Detry   2007-2015.
// Distributed under the GNU General Public License and under the
// BSD 3-Clause License (See accompanying file LICENSE.txt).

/** @file */


#ifndef NUKLEI_PLOTTER_H
#define NUKLEI_PLOTTER_H

#include <map>
#include <set>
#include <vector>
#include <cassert>
#include <fstream>
#include <nuklei/Common.h>

namespace nuklei {
  
#define NUKLEI_PLOTTER_F(p, x, y) p.push(#y, x, y)
#define NUKLEI_PLOTTER_D(p, x) p.push(#x, x)
#define NUKLEI_PLOTTER_COMMENT(p, var) p.comment(#var, var)
  
  class Plotter
  {
  public:
    typedef double T;
    typedef std::string mapkey_t;
    typedef std::vector<std::string> comment_collection;
    typedef std::vector< std::pair<T,T> > function_t;
    typedef std::vector<T> density_t;
    typedef std::map< mapkey_t, function_t > function_map;
    typedef std::map< mapkey_t, density_t > density_map;
    
    Plotter() : on_(true) {}
    
    void enable() { on_ = true; }
    void disable() { on_ = false; }
    
    void push(mapkey_t key, T val)
    {
      if (!on_) return;
      densities_[key].push_back(val);
    }
    
    void push(mapkey_t key, T x, T y)
    {
      if (!on_) return;
      functions_[key].push_back(std::make_pair(x, y));
    }
    
    template<typename U>
    void comment(std::string key, U value)
    {
      if (!on_) return;
      comments_.push_back(key + " = " +  stringify(value));
    }
    
    void comment(std::string c)
    {
      if (!on_) return;
      comments_.push_back(c);
    }
    
    void write_r(const std::string& filename)
    {
      if (!on_) return;

      std::ofstream ofs(filename.c_str(), std::ios::out);
      
      for (std::vector<std::string>::const_iterator v_i = comments_.begin();
           v_i != comments_.end(); v_i++)
        ofs << "# " << *v_i << "\n";
      ofs << "\n";
      
      for (function_map::const_iterator fm_i = functions_.begin();
           fm_i != functions_.end(); fm_i++)
      {
        ofs << clean(fm_i->first) << "_x = c(";
        for (function_t::const_iterator f_i = fm_i->second.begin();
             f_i != fm_i->second.end(); f_i++)
        {
          ofs << f_i->first;
          if (f_i != --fm_i->second.end()) ofs << ", ";
        }
        ofs << ");\n";
        
        ofs << clean(fm_i->first) << "_y = c(";
        for (function_t::const_iterator f_i = fm_i->second.begin();
             f_i != fm_i->second.end(); f_i++)
        {
          ofs << f_i->second;
          if (f_i != --fm_i->second.end()) ofs << ", ";
        }
        ofs << ");\n";
      }
      
      for (density_map::const_iterator dm_i = densities_.begin();
           dm_i != densities_.end(); dm_i++)
      {
        ofs << clean(dm_i->first) << " = c( ";
        for (density_t::const_iterator d_i = dm_i->second.begin();
             d_i != dm_i->second.end(); d_i++)
        {
          ofs << *d_i;
          if (d_i != --dm_i->second.end()) ofs << ", ";
        }
        ofs << ");\n";
      }
      
      for (function_map::const_iterator fm_i = functions_.begin();
           fm_i != functions_.end(); fm_i++)
        ofs << "dev.new();\n plot(" << clean(fm_i->first) << "_x, "
        << clean(fm_i->first) << "_y"
        << ");\n";
      
      for (density_map::const_iterator dm_i = densities_.begin();
           dm_i != densities_.end(); dm_i++)
        ofs << "dev.new();\n plot(density(" << clean(dm_i->first)
        << "));\n";
      
      ofs << "message(\"Press Return To Continue\")\n"
      "      invisible(readLines(\"stdin\", n=1))" << std::endl;
      
    }
    
    void write_octave(const std::string& filename, bool matlab = false)
    {
      if (!on_) return;

      std::vector<std::string> legend;
      
      std::ofstream ofs(filename.c_str(), std::ios::out);
      
      for (std::vector<std::string>::const_iterator v_i = comments_.begin();
           v_i != comments_.end(); v_i++)
        ofs << "% " << *v_i << std::endl;
      ofs << std::endl;
      
      ofs << "figure; hold on;\n" << std::endl;
      
      for (function_map::const_iterator fm_i = functions_.begin();
           fm_i != functions_.end(); fm_i++)
      {
        ofs << clean(fm_i->first) << " = [ ";
        
        for (function_t::const_iterator f_i = fm_i->second.begin();
             f_i != fm_i->second.end(); f_i++)
        {
          ofs << f_i->first << " " << f_i->second;
          if (f_i != --fm_i->second.end()) ofs << " ; ";
        }
        ofs << "];\n" << std::endl;
      }
      
      
      for (function_map::const_iterator fm_i = functions_.begin();
           fm_i != functions_.end(); fm_i++)
      {
        legend.push_back(clean(fm_i->first));
        ofs << "plot(" << clean(fm_i->first) << "(:,1), "
        << clean(fm_i->first) << "(:,2)";
        if (matlab)
          ofs << ", '*');\n" << std::endl;
        else
          ofs << ", '-;" << fm_i->first << ";');\n" << std::endl;
      }
      
      if (matlab)
      {
        ofs << "legend(";
        for (std::vector<std::string>::const_iterator i = legend.begin();
             i != legend.end(); ++i)
        {
          ofs << "'" << *i << "'";
          if (i+1 != legend.end())
            ofs << ",";
        }
        ofs << ");\n";
      }
      
      if (!densities_.empty())
        ofs << "error(\"KDE not supported in octave.\")\n" << std::endl;
    }
    
    static int main( int argc, char ** argv )
    {
      Plotter p;
      
      p.comment("This is a test.");
      
      const double STEP = .1;
      
      for (double d = -3; d <= 3; d += STEP)
      {
        p.push("y", d, d*d);
        p.push("p", d*d);
      }
      
      p.comment("step", STEP);
      
      p.write_octave("/tmp/octave.m");
      p.write_r("/tmp/r.r");
      
      return 0;
    }
    
  private:
    function_map functions_;
    density_map densities_;
    comment_collection comments_;
    bool on_;
    
    std::string clean(const std::string &dirty)
    {
      std::string c = dirty;
      for (std::string::iterator s_i = c.begin(); s_i != c.end(); s_i++)
      {
        if (! (*s_i >= 'a' && *s_i <= 'z') &&
            ! (*s_i >= 'A' && *s_i <= 'z') &&
            ! (*s_i >= '0' && *s_i <= '9'))
          *s_i = '_';
      }
      return "a" + c;
    }
  };
  
  
}

#endif

