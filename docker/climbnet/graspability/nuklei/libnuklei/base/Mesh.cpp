// (C) Copyright Renaud Detry   2007-2015.
// Distributed under the GNU General Public License and under the
// BSD 3-Clause License (See accompanying file LICENSE.txt).

/** @file */

// CGAL_INTERSECTION_VERSION == 1: CGAL::Object, slower
// CGAL_INTERSECTION_VERSION == 2: boost::optional< boost::variant< T... > >
#ifndef CGAL_INTERSECTION_VERSION
#define CGAL_INTERSECTION_VERSION 1
#endif
#define NUKLEI_MESH_DEBUG

#ifdef NUKLEI_USE_CGAL37
#include <boost/foreach.hpp>
#include <CGAL/Simple_cartesian.h>
#include <CGAL/Surface_mesh.h>
typedef CGAL::Simple_cartesian<double> K;
typedef CGAL::Surface_mesh<K::Point_2> CMesh2;
typedef CGAL::Surface_mesh<K::Point_3> CMesh3;
typedef Mesh::Vertex_index vertex_descriptor;
typedef Mesh::Face_index face_descriptor;
#endif

#ifdef NUKLEI_USE_CGAL
// http://doc.cgal.org/4.3/Kernel_23/group__intersection__linear__grp.html
// http://doc.cgal.org/4.3/Kernel_23/Kernel_23_2intersections_8cpp-example.html
// http://doc.cgal.org/4.3/Kernel_23/group__intersection__grp.html
#include <boost/bind.hpp>
#include <boost/variant/apply_visitor.hpp>
#include <CGAL/Simple_cartesian.h>
#include <CGAL/intersections.h>
#include <CGAL/Exact_predicates_exact_constructions_kernel.h>
#include <CGAL/Cartesian_converter.h>
typedef CGAL::Simple_cartesian<double> IK;
typedef CGAL::Exact_predicates_exact_constructions_kernel EK;
typedef EK::Point_2                     EPoint;
typedef EK::Segment_2                   ESegment;
typedef EPoint Point;
typedef ESegment Segment;
typedef IK::Point_2                     IPoint;
typedef IK::Segment_2                   ISegment;
typedef CGAL::Cartesian_converter<IK,EK> IK_to_EK;
typedef CGAL::Cartesian_converter<EK,IK> EK_to_IK;

#if CGAL_INTERSECTION_VERSION == 1
#else
template<typename K>
struct Intersector
{
  typedef typename CGAL::cpp11::result_of<typename K::Intersect_2(typename K::Segment_2,typename K::Segment_2)>::type result_type;
  const typename K::Segment_2& s;
  typename K::Intersect_2 intersect;
  Intersector(const typename K::Segment_2& seg): s(seg) {}
  result_type
  operator() (const typename K::Segment_2& other) const
  {
    return intersect(s, other);
  }
};
#endif


#ifdef NUKLEI_MESH_TREE

#include <CGAL/AABB_tree.h>
#include <CGAL/AABB_traits.h>
#include <CGAL/AABB_segment_primitive.h>
typedef IK::FT FT;
typedef std::list<ISegment>::iterator Iterator;
typedef CGAL::AABB_segment_primitive<IK, Iterator> Primitive;
typedef CGAL::AABB_traits<IK, Primitive> Traits;
typedef CGAL::AABB_tree<Traits> Tree;
#endif

#endif

#ifdef NUKLEI_USE_PCL
#include <pcl/io/ply_io.h>
#include <pcl/conversions.h>
#include <pcl/point_types.h>
#endif

#include <nuklei/Mesh.h>
#include <nuklei/PCLBridge.h>
#include <nuklei/Image.h>



namespace nuklei
{
  
  template<> void
  Mesh<Vector3>::read(const std::string& filename)
  {
#ifdef NUKLEI_USE_PCL
#if PCL_MINOR_VERSION >= 8
    pcl::PolygonMesh pclMesh;
    pcl::PLYReader reader;
    reader.read(filename, pclMesh);
    //NUKLEI_ASSERT(reader.read(filename, pclMesh) > 0);
    pcl::PointCloud<pcl::PointXYZ> cloud;
    pcl::fromPCLPointCloud2(pclMesh.cloud, cloud);
    g_ = Graph(cloud.size());
    std::cout << "Read " << cloud.size() << " points" << std::endl;
    for (std::vector<pcl::Vertices>::const_iterator i = pclMesh.polygons.begin();
         i != pclMesh.polygons.end(); ++i)
    {
      if (i->vertices.size() == 3u)
      {
        size_t previous = i->vertices.back();
        for (std::vector<uint32_t>::const_iterator vi = i->vertices.begin(); vi != i->vertices.end(); ++vi)
        {
          g_[*vi] = wmCopy(cloud.at(*vi));
          NUKLEI_ASSERT(boost::add_edge(previous, *vi, g_).second);
          previous = *vi;
        }
      }
//      else if (i->vertices.size() == 4u)
//      {
//        Vertex v0 = boost::add_vertex(g_); g_[v0] = wmCopy(cloud.at(i->vertices.at(0)));
//        Vertex v1 = boost::add_vertex(g_); g_[v1] = wmCopy(cloud.at(i->vertices.at(1)));
//        Vertex v2 = boost::add_vertex(g_); g_[v2] = wmCopy(cloud.at(i->vertices.at(2)));
//        Vertex v3 = boost::add_vertex(g_); g_[v3] = wmCopy(cloud.at(i->vertices.at(3)));
//        boost::add_edge(v0, v1, g_);
//        boost::add_edge(v1, v2, g_);
//        boost::add_edge(v2, v3, g_);
//        boost::add_edge(v3, v0, g_);
//      }
      else NUKLEI_THROW("Invalid polygon size " << i->vertices.size());
    }
#else
    NUKLEI_THROW("This function requires PCL >= 1.8.");
#endif
#else
    NUKLEI_THROW("This function requires PCL.");
#endif
  }
  
#ifdef NUKLEI_USE_CGAL
  IPoint cgalICopy(const Vector2& v)
  {
    return IPoint(v.X(), v.Y());
  }

  EPoint cgalECopy(const Vector2& v)
  {
    IK_to_EK to_exact;
    return to_exact(IPoint(v.X(), v.Y()));
  }

  Vector2 wmCopy(const EPoint& v)
  {
    EK_to_IK to_inexact;
    IPoint p(to_inexact(v));
    return Vector2(p.x(), p.y());
  }
#endif
  
  template<> Mesh<Vector2>
  Mesh<Vector3>::project(const Camera& cam,
                         const boost::optional<const kernel::se3&> t) const
  {
    Mesh<Vector2> pg(boost::num_vertices(g_), true);
    for (VertexIteratorPair vip = boost::vertices(g_);
         vip.first != vip.second; ++vip.first)
    {
      Vector3 v = g_[*vip.first];
      if (t) v = la::transform(t->loc_, t->ori_, v);
      pg.setVertex(*vip.first, cam.project(v));
    }
    for (EdgeIteratorPair eip = boost::edges(g_);
         eip.first != eip.second; ++eip.first)
      pg.addEdge(boost::source(*eip.first, g_), boost::target(*eip.first, g_));
    return pg;
  }

  template<> double
  Mesh<Vector2>::getAngle(const Vertex previous,
                          const Vertex current,
                          const Vertex next) const
  {
    // Remember that image coordinates have Z pointing away from the camera.
    // Positive rotation angles are clockwise from the cam's viewpoint.
    const Vector2 p(g_[previous]);
    const Vector2 c(g_[current]);
    const Vector2 n(g_[next]);
    const Vector2 v0 = p-c, v1 = n-c;
    if (c==p)
    {
      double angle = std::atan2(v1.Y(), v1.X());
      if (angle < 0) angle += 2*M_PI;
      NUKLEI_ASSERT(angle >= 0);
      return angle;
    }
    else
    {
      double angle0 = std::atan2(v0.Y(), v0.X());
      double angle1 = std::atan2(v1.Y(), v1.X());
      if (angle0 < 0) angle0 += 2*M_PI;
      if (angle1 < 0) angle1 += 2*M_PI;
      NUKLEI_ASSERT(angle0 >= 0 && angle1 >= 0 && angle0 <= 2*M_PI && angle1 <= 2*M_PI);
      if (angle1 > angle0) return angle1-angle0;
      else return 2*M_PI-(angle0-angle1);
    }
  }
  
  template<> void
  Mesh<Vector2>::draw(struct Image& img, const Vertex v,
                      const Vector3& rgb) const
  {
    img.drawPoint(g_[v], rgb);
  }

  template<> void
  Mesh<Vector2>::draw(struct Image& img, const Edge e,
                      const Vector3& rgb) const
  {
    Vertex v0 = boost::source(e, g_);
    Vertex v1 = boost::target(e, g_);
    img.drawLine(g_[v0], g_[v1], rgb);
    img.drawPoint(g_[v0], rgb);
    img.drawPoint(g_[v1], rgb);
  }
  
  template<> void
  Mesh<Vector2>::draw(struct Image& img,
                      const Vector3& rgb) const
  {
    std::cout << "drawing " << boost::num_edges(g_) << std::endl;
    for (EdgeIteratorPair eip = boost::edges(g_);
         eip.first != eip.second; ++eip.first)
      draw(img, *eip.first, rgb);
    for (VertexIteratorPair vip = boost::vertices(g_);
         vip.first != vip.second; ++vip.first)
      draw(img, *vip.first, rgb);
  }


  template<> Mesh<Vector2>::Vertex
  Mesh<Vector2>::buildNext(const Vertex& v0,
                           const Vertex& v1,
                           const Edge& e)
  {
#ifdef NUKLEI_USE_CGAL
    bool hasIntersect = false;
    double idsquare = 0;
    Vertex i0, i1;
    Edge ie;
    Vector2 il;
    
    for (EdgeIteratorPair eip = boost::edges(g_);
         eip.first != eip.second; ++eip.first)
    {
      Vertex w0 = boost::source(*eip.first, g_);
      Vertex w1 = boost::target(*eip.first, g_);
      
      ISegment isegv(cgalICopy(g_[v0]), cgalICopy(g_[v1]));
      ISegment isegw(cgalICopy(g_[w0]), cgalICopy(g_[w1]));
      
      bool doesIntersect = false;
#if CGAL_INTERSECTION_VERSION == 1
      CGAL::Object iresult = CGAL::intersection(isegv, isegw);
      if (CGAL::object_cast<IPoint>(&iresult))
      {
        doesIntersect = true;
      }
      else if (CGAL::object_cast<Segment>(&iresult))
      {
        doesIntersect = true;
      }
      else
      {
        doesIntersect = false;
      }
#else
      Intersector<IK>::result_type iresult = Intersector<IK>(isegv)(isegw);
      doesIntersect = bool(iresult);
#endif
      
      if (doesIntersect)
      {
        Segment segv(cgalECopy(g_[v0]), cgalECopy(g_[v1]));
        Segment segw(cgalECopy(g_[w0]), cgalECopy(g_[w1]));
        const Point* interPoint = NULL;
        const Segment* interSeg = NULL;
#if CGAL_INTERSECTION_VERSION == 1
        CGAL::Object result = CGAL::intersection(segv, segw);
        if ((interPoint = CGAL::object_cast<Point>(&result)))
        {
          doesIntersect = true;
        }
        else if ((interSeg = CGAL::object_cast<Segment>(&result)))
        {
          doesIntersect = true;
        }
        else
        {
          doesIntersect = false;
        }
#else
        Intersector<EK>::result_type result = Intersector<EK>(segv)(segw);
        doesIntersect = bool(result);
        interPoint = boost::get<Point>(&*result);
        interSeg = boost::get<Segment>(&*result);
#endif
        if (doesIntersect)
        {
          if (interPoint)
          {
            Vector2 is = wmCopy(*interPoint);
            if ((is-g_[v0]).SquaredLength() < FLOATTOL ||
                (is-g_[v1]).SquaredLength() < FLOATTOL)
              continue;
            if (hasIntersect == false || (is-g_[v0]).SquaredLength() < idsquare)
            {
              idsquare = (is-g_[v0]).SquaredLength();
              hasIntersect = true;
              ie = *eip.first;
              i0 = w0;
              i1 = w1;
              il = is;
            }
          }
          else if (interSeg)
          {
            //addEdge(w0, 0);
            //addEdge(w1, 0);
            //addEdge(v0, 0);
            //addEdge(v1, 0);
            //return v1;
            //NUKLEI_THROW("Not implemented");
          }
          else NUKLEI_THROW("Unexpected return value.");
        }
      }
    }
    if (hasIntersect)
    {
#ifdef NUKLEI_MESH_DEBUG
      std::cout << "Has intersect " << il <<  std::endl;
      std::cout << g_[v0] << " " << g_[v1] <<  std::endl;
      std::cout << g_[i0] << " " << g_[i1] <<  std::endl;
//      draw(img, e, Vector3::UNIT_Y);
//      draw(img, ie, Vector3::UNIT_Y);
#endif

      // fixme: figure out why this doesn't work with ie instead of i0,i1.
      boost::remove_edge(i0, i1, g_);
      boost::remove_edge(v0, v1, g_);
      Vertex vnew = boost::add_vertex(g_);
      g_[vnew] = il;
      addEdge(i0, vnew);
      addEdge(i1, vnew);
      addEdge(v0, vnew);
      addEdge(v1, vnew);
      return vnew;
    }
    return v1;
#else
    NUKLEI_THROW("This function requires CGAL.");
#endif
  }

  
  template<> Mesh<Vector2>
  Mesh<Vector2>::contour(const struct Image& img)
  {
#ifdef NUKLEI_MESH_TREE
    std::list<ISegment> segments;
    for (EdgeIteratorPair eip = boost::edges(g_);
         eip.first != eip.second; ++eip.first)
    {
      segments.push_back(ISegment(cgalICopy(g_[boost::source(*eip.first, g_)]),
                                  cgalICopy(g_[boost::target(*eip.first, g_)])));
    }
    Tree tree(segments.begin(),segments.end());
    tree.accelerate_distance_queries();
#endif
    Mesh<Vector2> contour;
    NUKLEI_ASSERT(rightmost_);
    Vertex current = *rightmost_;
    Vertex contourCurrent = contour.addVertex(g_[current]);
    Vertex contourRightmost = contourCurrent;
    Vertex previous = current;
    for (int kkk = 0; ; ++kkk)
    {
#ifdef NUKLEI_MESH_DEBUG
      Image image(img);
      //  image.setScale(16, true);
#endif
      double MAX_ANGLE = 16*M_PI;
      double angle = MAX_ANGLE;
      Vertex next;
      Edge nextEdge;
      for (OutEdgeIteratorPair eip = boost::out_edges(current, g_);
           eip.first != eip.second; ++eip.first)
      {
        Vertex tnext = boost::target(*eip.first, g_);
        //draw(image, *eip.first, Vector3::UNIT_X);
        double tangle = getAngle(previous, current, tnext);
        if (tangle < angle)
        {
          angle = tangle;
          next = tnext;
          nextEdge = *eip.first;
        }
      }
      NUKLEI_ASSERT(angle < MAX_ANGLE);
#ifdef NUKLEI_MESH_DEBUG
std::cout << "current: " << current << " next: " << next << std::endl;
#endif
      if (next == *rightmost_)
      {
        contour.addEdge(contourCurrent, contourRightmost);
        break;
      }
      else
      {
#ifdef NUKLEI_MESH_DEBUG
        Vertex oldnext = next;
        draw(image, Vector3(.5,.5,.5) );
#endif
        next = buildNext(current, next, nextEdge);
        Vertex contourNext = contour.addVertex(g_[next]);
        Edge e = contour.addEdge(contourCurrent, contourNext);
#ifdef NUKLEI_MESH_DEBUG
        contour.draw(image);
        if (next == oldnext)
          contour.draw(image, e, Vector3::UNIT_Y);
        else
          contour.draw(image, e, Vector3::UNIT_Z);
#endif
        previous = current;
        current = next;
        contourCurrent = contourNext;
#ifdef NUKLEI_MESH_DEBUG
        image.write("out/image-" + stringify(kkk) + ".jpg");
#endif
      }
    }
    return contour;
  }
  



#if defined(NUKLEI_USE_CGAL37) & defined(NUKLEI_USE_PCL)
  
  K::Point_3 kp3Copy(const pcl::PointXYZ& p)
  {
    return K::Point_3(p.x, p.y, p.z);
  }
  
  CMesh3 read(const std::string& filename)
  {
    pcl::PolygonMesh pclMesh;
    NUKLEI_ASSERT(pcl::PLYReader::read(filename, pclMesh) > 0);
    CMesh3 m;
    pcl::PointCloud<pcl::PointXYZ> cloud;
    pcl::fromPCLPointCloud2(pclMesh.cloud, cloud);
    for (std::vector<pcl::Vertices>::const_iterator i = pclMesh.polygons.begin();
         i != pclMesh.polygons.end(); ++i)
    {
      if (i->size() == 3u)
        m.add_face(m.add_vertex(kp3Copy(cloud.at(i->at(0)))),
                   m.add_vertex(kp3Copy(cloud.at(i->at(1)))),
                   m.add_vertex(kp3Copy(cloud.at(i->at(2)))));
      else if (i->size() == 4u)
        m.add_face(m.add_vertex(kp3Copy(cloud.at(i->at(0)))),
                   m.add_vertex(kp3Copy(cloud.at(i->at(1)))),
                   m.add_vertex(kp3Copy(cloud.at(i->at(2)))),
                   m.add_vertex(kp3Copy(cloud.at(i->at(3)))));
      else NUKLEI_THROW("Invalid polygon size " << i->size());
    }
  }
  
#endif

  
}
