// (C) Copyright Renaud Detry   2007-2015.
// Distributed under the GNU General Public License and under the
// BSD 3-Clause License (See accompanying file LICENSE.txt).

/** @file */

#include <nuklei/Image.h>

#ifdef NUKLEI_USE_OPENCV
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#endif


struct rgb_t { uint8_t r, g, b; };
struct bgrx_t { uint8_t b, g, r, x; };


namespace nuklei
{
  
  Image::Image()
#ifdef NUKLEI_USE_OPENCV
 : img_(new cv::Mat)
#endif
  {
    scale_ = 1;
  }
  
  Image::Image(const Image& img)
  {
#ifdef NUKLEI_USE_OPENCV
    img_ = boost::shared_ptr<cv::Mat>(new cv::Mat(img.img_->clone()));
    
    typedef std::vector< boost::shared_ptr<cv::Mat> > MatVec;
    std::vector<MatVec*> localDifferentials;
    localDifferentials.push_back(&dx_);
    localDifferentials.push_back(&dy_);
    localDifferentials.push_back(&gradIntensity_);
    localDifferentials.push_back(&gradDir_);
    std::vector<const MatVec*> remoteDifferentials;
    remoteDifferentials.push_back(&img.dx_);
    remoteDifferentials.push_back(&img.dy_);
    remoteDifferentials.push_back(&img.gradIntensity_);
    remoteDifferentials.push_back(&img.gradDir_);
    for (int i = 0; i < 4; ++i)
    {
      MatVec& l = *localDifferentials.at(i);
      const MatVec& r = *remoteDifferentials.at(i);
      for (MatVec::const_iterator i = r.begin();
           i != r.end(); ++i)
      {
        l.push_back(boost::shared_ptr<cv::Mat>(new cv::Mat((*i)->clone())));
      }
    }
#endif
    scale_ = img.scale_;
  }

  void Image::setScale(const double s, bool resize)
  {
#ifdef NUKLEI_USE_OPENCV
    if (resize)
      cv::resize(*img_, *img_, cv::Size(0,0), s, s);
    scale_ = s;
#else
    NUKLEI_THROW("This function requires OpenCV.");
#endif
  }
  
  void Image::computeGradients()
  {
#ifdef NUKLEI_USE_OPENCV
    using namespace cv;
    
    Mat gray;
    int delta = 0;
    int ksize = 3;
    int scale = 1;
    int ddepth = CV_32F;
    int py = 7;
    std::vector<Size> sizes;
    
    cvtColor(*img_, gray, cv::COLOR_BGR2GRAY);
    
    for (int i = 0; i < py; ++i)
    {
      if (i)
      {
        sizes.push_back( Size( gray.cols, gray.rows ) );
        pyrDown(gray, gray);
      }
      
      Mat blurred;
      GaussianBlur(gray, blurred, Size(3,3), 0, 0, BORDER_DEFAULT);
      
      Mat dx, dy;
      
      Sobel(blurred, dx, ddepth, 1, 0, ksize, scale, delta, BORDER_DEFAULT);
      Sobel(blurred, dy, ddepth, 0, 1, ksize, scale, delta, BORDER_DEFAULT);
      
      for (std::vector<Size>::reverse_iterator rit = sizes.rbegin();
           rit != sizes.rend(); ++rit)
      {
        pyrUp(dx, dx, *rit);
        pyrUp(dy, dy, *rit);
      }
      
      NUKLEI_ASSERT(img_->cols == dx.cols && img_->rows == dx.rows);
      NUKLEI_ASSERT(img_->cols == dy.cols && img_->rows == dy.rows);
      
      Mat gradIntensity = dx.clone(), gradDir = dx.clone();
      
      for (unsigned r = 0; r < dx.rows; ++r)
        for (unsigned c = 0; c < dx.cols; ++c)
        {
          float gx = dx.at<float>(r,c);
          float gy = dy.at<float>(r,c);
          float gi = std::sqrt(gx*gx + gy*gy);
          float gd = 0;
          if (gx < FLOATTOL && gy < FLOATTOL)
            gd = 0;
          else
            gd = std::fmod(std::atan2(gy, gx)+2*M_PI, M_PI);
          gradIntensity.at<float>(r,c) = gi;
          gradDir.at<float>(r,c) = gd;
        }
      
#if 0
      cv::normalize(gradIntensity, gradIntensity, 0, 255, NORM_MINMAX, -1, Mat());
      imwrite("/tmp/dx" + stringify(i) + ".png", dx);
      imwrite("/tmp/dy" + stringify(i) + ".png", dy);
      imwrite("/tmp/gi" + stringify(i) + ".png", gradIntensity);
      imwrite("/tmp/gd" + stringify(i) + ".png", gradDir);
#endif
      
      dx_.push_back(boost::shared_ptr<cv::Mat>(new cv::Mat(dx)));
      dy_.push_back(boost::shared_ptr<cv::Mat>(new cv::Mat(dy)));
      gradIntensity_.push_back(boost::shared_ptr<cv::Mat>(new cv::Mat(gradIntensity)));
      gradDir_.push_back(boost::shared_ptr<cv::Mat>(new cv::Mat(gradDir)));
    }
#else
    NUKLEI_THROW("This function requires OpenCV.");
#endif
  }
  
  std::vector<Vector2> Image::project(const std::vector<Vector3> worldPoints,
                                      const Vector3& color)
  {
    NUKLEI_THROW("Not implemented");
  }
  
  Vector2 Image::project(const Vector3& p,
                         const Vector3& rgb)
  {
#ifdef NUKLEI_USE_OPENCV
    Vector2 p2 = camera_.project(p);
    if (scale_ != 1) p2 *= scale_;
    cv::Point p0(p2.X(), p2.Y());
    cv::Scalar c(rgb.Z()*255, rgb.Y()*255, rgb.X()*255);
    int thickness = 1;
    int lineType = 8; // CV_AA
    cv::circle(*img_, p0, 3, c, thickness, lineType);
    
//    bgr_t bgr;
//    bgr.r = rgb.X()*255; bgr.g = rgb.Y()*255; bgr.b = rgb.Z()*255;
//    if (p2.X() >= 0 && p2.Y() >= 0 && p2.X() < img_->cols && p2.Y() < img_->rows)
//      img_->at<bgr_t>(p2.Y(), p2.X()) = bgr;
    return p2;
#else
    NUKLEI_THROW("This function requires OpenCV.");
#endif
  }
  
  void Image::read(const std::string& name)
  {
#ifdef NUKLEI_USE_OPENCV
    *img_ = cv::imread(name);
#else
    NUKLEI_THROW("This function requires OpenCV.");
#endif
  }
  
  void Image::write(const std::string& name) const
  {
#ifdef NUKLEI_USE_OPENCV
    cv::imwrite(name, *img_);
#else
    NUKLEI_THROW("This function requires OpenCV.");
#endif
  }
  
  
  void Image::readCamera(const std::string& opencvStereoCalibFile)
  {
    NUKLEI_TRACE_BEGIN();
    
    camera_.read(opencvStereoCalibFile);
    
    NUKLEI_TRACE_END();
  }
  
  void Image::drawLine(const Vector2& a, const Vector2& b,
                       const Vector3& rgb)
  {
#ifdef NUKLEI_USE_OPENCV
    cv::Point p0(a.X(), a.Y());
    cv::Point p1(b.X(), b.Y());
    if (scale_ != 1) p0 *= scale_;
    if (scale_ != 1) p1 *= scale_;
    cv::Scalar color(rgb.Z()*255, rgb.Y()*255, rgb.X()*255);
    int thickness = 2;
    int lineType = 8; // CV_AA
    cv::line(*img_, p0, p1, color, thickness, lineType);
#else
    NUKLEI_THROW("This function requires OpenCV.");
#endif
  }

  void Image::drawPoint(const Vector2& a,
                        const Vector3& rgb)
  {
#ifdef NUKLEI_USE_OPENCV
    cv::Point p0(a.X(), a.Y());
    if (scale_ != 1) p0 *= scale_;
    cv::Scalar color(rgb.Z()*255, rgb.Y()*255, rgb.X()*255);
    int thickness = 2;
    int lineType = 8; // CV_AA
    cv::circle(*img_, p0, 8, color, thickness, lineType);
#else
    NUKLEI_THROW("This function requires OpenCV.");
#endif
  }
  
}
