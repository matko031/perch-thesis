#!/bin/bash

conf_file=nuklei-$(uname | awk '{print tolower($0)}').conf
tmp=`mktemp -t nuklei-build-test-XXXXXXXXXXXX`
echo "Backing up conf file $conf_file to $tmp".
cp $conf_file $tmp

trap "{ cp $tmp $conf_file ; }" EXIT

set -ex

for opencv in yes no; do
    for yaml in yes no; do
        for cgal in yes no; do
            for pcl in yes no; do
                for ticpp in yes no; do
                    for partial_view in yes no; do
                        for openmp in yes no; do
                            ./scons.py -s -j 24 \
                                       use_opencv=$opencv \
                                       use_yamlcpp=$yaml \
                                       use_cgal=$cgal \
                                       use_pcl=$pcl \
                                       use_ticpp=$ticpp \
                                       partial_view=$partial_view \
                                       use_openmp=$openmp
                        done
                    done
                done
            done
        done
    done
done
