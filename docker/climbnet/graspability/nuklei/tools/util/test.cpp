// (C) Copyright Renaud Detry   2007-2015.
// Distributed under the GNU General Public License and under the
// BSD 3-Clause License (See accompanying file LICENSE.txt).

/** @file */

#include <string>
#include <sys/time.h>
#include <sys/resource.h>
#include <boost/tuple/tuple.hpp>

#include <tclap/CmdLine.h>
#include <nuklei/tclap-wrappers.h>
#include <nuklei/CoViS3DObservationIO.h>
#include <nuklei/SerializedKernelObservationIO.h>
#include <nuklei/Serial.h>
#include <nuklei/nullable.h>

using namespace nuklei;

int test(int argc, char ** argv)
{
  NUKLEI_TRACE_BEGIN();
  
  /* Parse command line arguments */
  
  TCLAP::CmdLine cmd(INFOSTRING + "Test App." );
  
  /* Standard arguments */
  
  TCLAP::ValueArg<int> niceArg
  ("", "nice",
   "Proccess priority.",
   false, NICEINC, "int", cmd);
  
  /* Custom arguments */
  
  TCLAP::UnlabeledValueArg<std::string> fileArg
  ("file",
   "Nuklei file.",
   false, "", "filename", cmd);
    
  cmd.parse( argc, argv );
  
  if (-20 <= niceArg.getValue() && niceArg.getValue() <= 20)
    NUKLEI_ASSERT(setpriority(PRIO_PROCESS, 0, niceArg.getValue()) == 0);
  
  while (true)
  {
    
  
  std::cout << "x: " << std::flush;
  double x = 0, y = 0, alpha = 0;
  std::cin >> x;
  std::cout << "y: " << std::flush;
  std::cin >> y;
  std::cout << "alpha: " << std::flush;
  std::cin >> alpha;
  
    kernel::se3 k;
    k.loc_.X() = 0;
    k.loc_.Y() = x;
    k.loc_.Z() = y;
    
    coord_t angle = alpha;
    Vector3 axis = Vector3::UNIT_X;
    Quaternion r;
    r.FromAxisAngle(axis, angle);
    r = la::normalized(r);
    k.ori_ = r;

    writeSingleObservation("2d", k, Observation::TXT);

    
  }
  
  
  return 0;
  
  NUKLEI_TRACE_END();
}

