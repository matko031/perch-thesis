// (C) Copyright Renaud Detry   2007-2015.
// Distributed under the GNU General Public License and under the
// BSD 3-Clause License (See accompanying file LICENSE.txt).

/** @file */

#include <string>
#include <sys/time.h>
#include <sys/resource.h>
#include <boost/tuple/tuple.hpp>

#include <tclap/CmdLine.h>
#include <nuklei/tclap-wrappers.h>
#include <nuklei/CoViS3DObservationIO.h>
#include <nuklei/SerializedKernelObservationIO.h>
#include <nuklei/Serial.h>
#include <nuklei/Stopwatch.h>
#include <nuklei/Image.h>
#include <nuklei/Mesh.h>

using namespace nuklei;

int im_proj(int argc, char ** argv)
{
  NUKLEI_TRACE_BEGIN();
  
  /* Parse command line arguments */
  
  TCLAP::CmdLine cmd(INFOSTRING + "Test App." );
  
  /* Standard arguments */
  
  TCLAP::ValueArg<int> niceArg
  ("", "nice",
   "Proccess priority.",
   false, NICEINC, "int", cmd);
  
  /* Custom arguments */
  
  TCLAP::UnlabeledValueArg<std::string> fileArg
  ("file",
   "Nuklei file.",
   false, "", "filename", cmd);
  
  TCLAP::ValueArg<std::string> calibArg
  ("c", "calib",
   "OpenCV calibration file.",
   false, "", "filename", cmd);
  
  TCLAP::ValueArg<std::string> pointsArg
  ("p", "points",
   "3D points.",
   false, "", "filename", cmd);
  
  TCLAP::ValueArg<std::string> meshArg
  ("m", "mesh",
   "3D points.",
   false, "", "filename", cmd);
  
  cmd.parse( argc, argv );
  
  if (-20 <= niceArg.getValue() && niceArg.getValue() <= 20)
    NUKLEI_ASSERT(setpriority(PRIO_PROCESS, 0, niceArg.getValue()) == 0);
  
  Image img;
  img.read(fileArg.getValue());
  img.readCamera(calibArg.getValue());
  
  KernelCollection kc;
  readObservations(pointsArg.getValue(), kc);
  
  if (!meshArg.getValue().empty())
  {
    Mesh3 m;
    m.read(meshArg.getValue());
    kernel::se3 t;
    Stopwatch sw("");
    Mesh2 mproj = m.project(img.getCamera(), t);
    sw.lap("Projection");
    if (0)
    {
      Image tmp(img);
      mproj.draw(tmp);
      tmp.write("projected_graph.png");
    }
    Mesh2 contour = mproj.contour(img);
    sw.lap("Contour");
    contour.draw(img);
    sw.lap("Draw Contour");
  }
  
  for (KernelCollection::const_iterator i = kc.begin();
       i != kc.end(); ++i)
  {
    img.project(i->getLoc(), Vector3(0, 255, 0));
  }
  
  img.write("/tmp/file.png");
  
  return 0;
  
  NUKLEI_TRACE_END();
}

int im_grad(int argc, char ** argv)
{
  NUKLEI_TRACE_BEGIN();
  
  /* Parse command line arguments */
  
  TCLAP::CmdLine cmd(INFOSTRING + "Test App." );
  
  /* Standard arguments */
  
  TCLAP::ValueArg<int> niceArg
  ("", "nice",
   "Proccess priority.",
   false, NICEINC, "int", cmd);
  
  /* Custom arguments */
  
  TCLAP::UnlabeledValueArg<std::string> fileArg
  ("file",
   "Image.",
   false, "", "filename", cmd);
  
  cmd.parse( argc, argv );
  
  if (-20 <= niceArg.getValue() && niceArg.getValue() <= 20)
    NUKLEI_ASSERT(setpriority(PRIO_PROCESS, 0, niceArg.getValue()) == 0);
  
  Image im;
  im.read(fileArg.getValue());
  im.computeGradients();
  
  return 0;
  
  NUKLEI_TRACE_END();
}

int pc_mask(int argc, char ** argv)
{
  NUKLEI_TRACE_BEGIN();
  
  /* Parse command line arguments */
  
  TCLAP::CmdLine cmd(INFOSTRING + "Mask app." );
  
  /* Standard arguments */
  
  TCLAP::ValueArg<int> niceArg
  ("", "nice",
   "Proccess priority.",
   false, NICEINC, "int", cmd);
  
  /* Custom arguments */
  
  TCLAP::UnlabeledValueArg<std::string> fileArg
  ("file",
   "Nuklei file.",
   false, "", "filename", cmd);
  
  TCLAP::ValueArg<std::string> calibArg
  ("i", "image",
   "OpenCV calibration file.",
   false, "", "filename", cmd);
  
  TCLAP::ValueArg<std::string> pointsArg
  ("o", "output",
   "3D points.",
   false, "", "filename", cmd);
  
  cmd.parse( argc, argv );
  
  if (-20 <= niceArg.getValue() && niceArg.getValue() <= 20)
    NUKLEI_ASSERT(setpriority(PRIO_PROCESS, 0, niceArg.getValue()) == 0);
  
  KernelCollection pc, mask;
  readObservationsWithSpecificFormat(fileArg.getValue(), pc, Observation::PCDGRID);
  readObservationsWithSpecificFormat(calibArg.getValue(), mask, Observation::PCDGRID);
  
  for (unsigned i = 0; i != pc.size(); ++i)
  {
    RGBColor& c = dynamic_cast<RGBColor&>(dynamic_cast<ColorDescriptor&>(mask.at(i).getDescriptor()).getColor());
    if (!(std::fabs(c.G()-1) < 100./255 && std::fabs(c.R()-0) < 100./255 && std::fabs(c.B()-0) < 100./255))
    {
      pc.at(i).setLoc(Vector3(std::numeric_limits<double>::quiet_NaN(),
                              std::numeric_limits<double>::quiet_NaN(),
                              std::numeric_limits<double>::quiet_NaN()));
      if (pc.at(i).hasDescriptor())
      {
        RGBColor& c = dynamic_cast<RGBColor&>(dynamic_cast<ColorDescriptor&>(pc.at(i).getDescriptor()).getColor());
        c.setRGB(Vector3::ZERO);
      }
    }
  }
  
  writeObservations(pointsArg.getValue(), pc, Observation::PCD);
  
  
  return 0;
  
  NUKLEI_TRACE_END();
}

