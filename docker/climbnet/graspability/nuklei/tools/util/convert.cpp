// (C) Copyright Renaud Detry   2007-2015.
// Distributed under the GNU General Public License and under the
// BSD 3-Clause License (See accompanying file LICENSE.txt).

/** @file */

#include <string>
#include <limits>
#include <sys/time.h>
#include <sys/resource.h>
#include <boost/tuple/tuple.hpp>

#include <tclap/CmdLine.h>
#include <nuklei/tclap-wrappers.h>
#include <nuklei/CoViS3DObservationIO.h>
#include <nuklei/SerializedKernelObservationIO.h>
#include <nuklei/Serial.h>
#include <nuklei/nullable.h>
#include <nuklei/ProgressIndicator.h>
#include <nuklei/ObservationIO.h>
#include <nuklei/Convert.h>

using namespace nuklei;



int invert_transfo(int argc, char ** argv)
{
  NUKLEI_TRACE_BEGIN();
  
  /* Parse command line arguments */
  
  TCLAP::CmdLine cmd(INFOSTRING + "Transfo Invert App." );
  
  /* Standard arguments */
  
  TCLAP::ValueArg<int> niceArg
  ("", "nice",
   "Proccess priority.",
   false, NICEINC, "int", cmd);
  
  /* Custom arguments */
  
  TCLAP::UnlabeledValueArg<std::string> inFileArg
  ("input",
   "Input.",
   true, "", "filename", cmd);
  
  TCLAP::UnlabeledValueArg<std::string> outFileArg
  ("output",
   "Output.",
   true, "", "filename", cmd);
  
  cmd.parse( argc, argv );
  
  if (-20 <= niceArg.getValue() && niceArg.getValue() <= 20)
    NUKLEI_ASSERT(setpriority(PRIO_PROCESS, 0, niceArg.getValue()) == 0);
  
  kernel::se3 t = kernel::se3(*readSingleObservation(inFileArg.getValue()));
  writeSingleObservation(outFileArg.getValue(), t.inverse());
  
  return 0;
  
  NUKLEI_TRACE_END();
}


int run(int argc, char ** argv)
{
  NUKLEI_TRACE_BEGIN();
  
  /* Parse command line arguments */
  
  TCLAP::CmdLine cmd(INFOSTRING + "Transfo Invert App." );
  
  /* Standard arguments */
  
  TCLAP::ValueArg<int> niceArg
  ("", "nice",
   "Proccess priority.",
   false, NICEINC, "int", cmd);
  
  /* Custom arguments */
  
  TCLAP::UnlabeledValueArg<std::string> inFileArg
  ("input",
   "Input.",
   true, "", "filename", cmd);
  
  cmd.parse( argc, argv );
  
  if (-20 <= niceArg.getValue() && niceArg.getValue() <= 20)
    NUKLEI_ASSERT(setpriority(PRIO_PROCESS, 0, niceArg.getValue()) == 0);
  
  Converter c;
  c.run(inFileArg.getValue());
  
  return 0;
  
  NUKLEI_TRACE_END();
}

