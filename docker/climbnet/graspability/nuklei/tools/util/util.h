// (C) Copyright Renaud Detry   2007-2015.
// Distributed under the GNU General Public License and under the
// BSD 3-Clause License (See accompanying file LICENSE.txt).

/** @file */

#ifndef NUKLEI_UTIL_H
#define NUKLEI_UTIL_H

int info(int argc, char ** argv);
int kde(int argc, char ** argv);
int importance_sampling(int argc, char ** argv);
int resample(int argc, char ** argv);
int homogeneous_subset(int argc, char ** argv);
int evaluate(int argc, char ** argv);
int test(int argc, char ** argv);
int invert_transfo(int argc, char ** argv);
int partial_view(int argc, char ** argv);
int create_mesh(int argc, char ** argv);
int convert_mesh(int argc, char ** argv);
int pe(int argc, char ** argv);
int paint_cloud_with_kde(int argc, char ** argv);
int paint_cloud_with_kde_contextual_grasping(int argc, char ** argv);
int im_proj(int argc, char ** argv);
int im_grad(int argc, char ** argv);
int pc_mask(int argc, char ** argv);
int run(int argc, char ** argv);

#endif
