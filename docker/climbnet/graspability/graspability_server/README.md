# Graspability Server

<img width="734" height="361" src="docs/graspability.png">

This repository contains servers for computing LEMUR microspine gripper graspability on the basis of gripper-scale geometry. 

## Graspability Server

Graspability server computes graspability as a function of spatial variance in a microspine gripper sized volume. Higher spatial variance is considered to lead to poorer (lower value) graspability. This assumes that micro-scale geometry smaller than the gripper size is always conducive to good grips (e.g. vesicular basalt). For example, a flat region of vesicular basalt will allow more cartridges to catch than a 90 degree corner. 

## Learned Graspability Server

The learned graspability server computes graspability on the basis of expert-annotated meshes from microspine gripper experts (e.g. Aaron Parness and Christine Fuller).

## Usage

To run the server:

    $ roslaunch graspability server.launch

To run the example client:

    $ rosrun graspability graspability_example_client





