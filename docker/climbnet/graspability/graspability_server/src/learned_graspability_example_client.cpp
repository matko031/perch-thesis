/**
 * Copyright (C) 2016, California Institute of Technology.
 * All Rights Reserved. U.S. Government Sponsorship Acknowledged.
 * Any commercial use must be negotiated with the Office of
 * Technology Transfer at the California Institute of Technology.
 *
 * This software may be subject to U.S. export control laws. By
 * accepting this software, the user agrees to comply with all
 * applicable U.S. export laws and regulations. User has the
 * responsibility to obtain export licenses, or other export authority
 * as may be required before exporting such information to foreign
 * countries or providing access to foreign persons.
 */

/**
 @file  graspability_client.cpp
 @brief Requests grasp candidates within a bounding sphere from the graspability
 server

 @date  05/12/2016

 @author Jeremy Nash (jeremy.nash@jpl.nasa.gov)
 Mobility and Robotic Systems (347), JPL
 */

#include <geometry_msgs/Vector3.h>
#include <graspability_server/GrabCandidates.h>
#include <graspability_server/InsertMesh.h>
#include <ros/package.h>
#include <ros/ros.h>
#include <cstdlib>

#include <dynamic_reconfigure/server.h>
#include <geometry_msgs/PointStamped.h>
#include <sensor_msgs/PointCloud2.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_listener.h>

#include <pcl/conversions.h>
#include <pcl/io/ply_io.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/search/kdtree.h>
#include <pcl/search/organized.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl_ros/transforms.h>

#include <graspability_server/GrabCandidates.h>
#include <graspability_server/GraspCandidate.h>
#include <graspability_server/InsertMesh.h>
#include <interactive_markers/interactive_marker_server.h>

int main(int argc, char *argv[]) {
  ros::init(argc, argv, "graspability_client");
  ros::NodeHandle nh("~");

  {
    ros::ServiceClient client =
        nh.serviceClient<graspability_server::InsertMesh>(
            "/insert_mesh");
    graspability_server::InsertMesh srv;

    {
      tf::Transform odom2baselink;
      odom2baselink.setOrigin(tf::Vector3(0, 0, 0));
      odom2baselink.setRotation(tf::Quaternion(0, 0, 0, 1));
      tf::StampedTransform tmp(odom2baselink, ros::Time::now(), "odom_combined",
                               "baselink");
      tf::transformStampedTFToMsg(tmp, srv.request.odom2baselink);
    }

    std::string cloud_name;
    if (!nh.getParam("cloud", cloud_name)) {
      ROS_ERROR_STREAM("cloud argument not provided");
    }

    {
      pcl::PolygonMesh mesh;
      if (pcl::io::loadPLYFile(cloud_name, mesh) == -1) {
        ROS_ERROR_STREAM("Cannot read " << cloud_name << ".");
      }

      pcl_conversions::fromPCL(mesh, srv.request.mesh);
    }

    if (!client.call(srv)) {
      ROS_WARN_STREAM("Service call failed.");
    }
  }

  {
    ros::ServiceClient client =
        nh.serviceClient<graspability_server::GrabCandidates>(
            "/grab_candidates");
    graspability_server::GrabCandidates srv;

    std::string cloud_name;
    if (!nh.getParam("cloud", cloud_name)) {
      ROS_ERROR_STREAM("cloud argument not provided");
    }

    srv.request.center.x = .44;//-0.137980;  // units are in meters in world frame
    srv.request.center.y = .41;//-0.565140;  // units are in meters in world frame
    srv.request.center.z = -.11;//0.765190;   // units are in meters in world frame
    srv.request.radius = 0.2;          // 0.2m radius
    srv.request.filter = true;
    srv.request.min_probability = .5;
    srv.request.max_n = 10;
    srv.request.min_position_distance_to_neighbor = .02;
    srv.request.min_orientation_distance_to_neighbor = M_PI / 8.;

    nh.getParam("radius", srv.request.radius);
    std::string tmpfilter = "false";
    if (nh.getParam("filter", tmpfilter)) {
      srv.request.filter = tmpfilter == "true";
    }
    nh.getParam("min_probability", srv.request.min_probability);
    nh.getParam("max_n", srv.request.max_n);
    nh.getParam("min_pos_dist", srv.request.min_position_distance_to_neighbor);
    nh.getParam("min_ori_dist",
                srv.request.min_orientation_distance_to_neighbor);

    pcl::PointCloud<pcl::PointXYZINormal>::Ptr points;
    points.reset(new pcl::PointCloud<pcl::PointXYZINormal>());

    if (client.call(srv)) {
      ROS_INFO_STREAM("Received " << srv.response.grasp_candidates.size()
                                  << " grasp candidates.");
      for (unsigned int i = 0; i < srv.response.grasp_candidates.size(); i++) {
        //        ROS_INFO_STREAM(i << ". " <<
        //                        "x: "  <<
        //                        srv.response.grasp_candidates[i].point.x  <<
        //                        ", " <<
        //                        "y: "  <<
        //                        srv.response.grasp_candidates[i].point.y  <<
        //                        ", " <<
        //                        "z: "  <<
        //                        srv.response.grasp_candidates[i].point.z  <<
        //                        ", " <<
        //                        "nx: " <<
        //                        srv.response.grasp_candidates[i].normal.x <<
        //                        ", " <<
        //                        "ny: " <<
        //                        srv.response.grasp_candidates[i].normal.y <<
        //                        ", " <<
        //                        "nz: " <<
        //                        srv.response.grasp_candidates[i].normal.z);
        pcl::PointXYZINormal pt;
        pt.x = srv.response.grasp_candidates[i].point.x;
        pt.y = srv.response.grasp_candidates[i].point.y;
        pt.z = srv.response.grasp_candidates[i].point.z;
        pt.normal_x = srv.response.grasp_candidates[i].normal.x;
        pt.normal_y = srv.response.grasp_candidates[i].normal.y;
        pt.normal_z = srv.response.grasp_candidates[i].normal.z;
        pt.curvature = srv.response.grasp_candidates[i].variance;
        pt.intensity =
            srv.response.grasp_candidates[i].probability_grasp_success;
        points->push_back(pt);
      }
    } else {
      ROS_WARN_STREAM("Service call failed.");
    }

    if (points->size() != 0)
      pcl::io::savePCDFileBinaryCompressed("/tmp/graspability.pcd", *points);
    else
      ROS_WARN_STREAM("Empty graspabilitiy map, not writing.");
  }
  return 0;
}
