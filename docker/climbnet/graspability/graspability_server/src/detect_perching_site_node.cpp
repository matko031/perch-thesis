#include "pcl_ros/point_cloud.h"
#include "tf/tf.h"
#include <actionlib/server/simple_action_server.h>
#include <mutex>

#include <pcl/filters/extract_indices.h>
#include <pcl/io/ply_io.h>
#include <pcl/surface/organized_fast_mesh.h>
#include <ros/ros.h>
#include <sensor_msgs/PointCloud2.h>
#include <thread>

#include "graspability_server/GrabCandidates.h"
#include "graspability_server/InsertMesh.h"
#include "std_srvs/Empty.h"
#include "visualization_msgs/Marker.h"
#include <graspability_server/DetectPerchSiteAction.h>

actionlib::SimpleActionServer<graspability_server::DetectPerchSiteAction>
    *as_detect_perch_site;

std::mutex mutex_cloud;
pcl::PointCloud<pcl::PointXYZ> current_cloud_;
bool got_first_cloud_;
pcl_msgs::PolygonMesh current_pcl_mesh_msg;
ros::ServiceClient srv_client_clear_map_;
ros::ServiceClient srv_client_insert_mesh_;
ros::ServiceClient srv_client_grab_candidates_;
ros::Publisher pub_perch_target_marker_;
ros::Subscriber sub_rgbd_;
std::thread detect_thread_;
std::string cam_frame_id_;

void rgbd_callback(const pcl::PointCloud<pcl::PointXYZ> &msg) {
  std::lock_guard<std::mutex> lk(mutex_cloud);
  got_first_cloud_ = true;
  current_cloud_ = msg;
}

void execute_cb_detect_perch_site(
    const graspability_server::DetectPerchSiteGoalConstPtr &goal) {
  std::lock_guard<std::mutex> lk(mutex_cloud);
  pcl::OrganizedFastMesh<pcl::PointXYZ> ofm;

  if (!got_first_cloud_) {
    ROS_WARN("Did not get a pointcloud yet... aborting.");
    as_detect_perch_site->setAborted();
  }

  int scale = 1;
  pcl::PointCloud<pcl::PointXYZ> down_sampled_cloud;
  down_sampled_cloud.width = current_cloud_.width / scale;
  down_sampled_cloud.height = current_cloud_.height / scale;

  for (int i = 0; i < current_cloud_.height; i += scale) {
    for (int j = 0; j < current_cloud_.width; j += scale) {
      down_sampled_cloud.push_back(current_cloud_.at(j, i));
    }
  }
  down_sampled_cloud.width = current_cloud_.width / scale;
  down_sampled_cloud.height = current_cloud_.height / scale;

  pcl::PointCloud<pcl::PointXYZ>::Ptr current_cloud_ptr(
      new pcl::PointCloud<pcl::PointXYZ>(down_sampled_cloud));

  ofm.setInputCloud(current_cloud_ptr);

  // ROS_INFO("Organized? %i", current_cloud_ptr->isOrganized());
  // ROS_INFO("Point cloud size %i", current_cloud_ptr->size());
  ofm.setMaxEdgeLength(2.0);
  ofm.setTrianglePixelSize(10);
  ofm.setTriangulationType(
      pcl::OrganizedFastMesh<pcl::PointXYZ>::TRIANGLE_ADAPTIVE_CUT);
  pcl::PolygonMesh triangles;
  ofm.reconstruct(triangles);

  // pcl::io::savePLYFileBinary("input.ply", triangles);

  pcl_msgs::PolygonMesh ros_polygon_mesh_msg;
  pcl_conversions::fromPCL(triangles, ros_polygon_mesh_msg);

  ROS_INFO("Number of mesh triangles %i", triangles.polygons.size());

  // Send the mesh to graspability server.
  graspability_server::InsertMesh srv_msg_insert_mesh;

  srv_msg_insert_mesh.request.mesh = ros_polygon_mesh_msg;
  geometry_msgs::TransformStamped tf_stamped_msg;

  tf::StampedTransform tf_identity_stamped;
  tf::Transform tf_identity;
  tf_identity.setIdentity();
  tf_identity_stamped.setData(tf_identity);
  tf_identity_stamped.stamp_ = ros::Time::now();
  tf_identity_stamped.frame_id_ = "odom";
  tf_identity_stamped.child_frame_id_ = "base_link";

  transformStampedTFToMsg(tf_identity_stamped,
                          srv_msg_insert_mesh.request.odom2baselink);

  srv_client_insert_mesh_.call(srv_msg_insert_mesh);

  // Then, grab candidates

  graspability_server::GrabCandidates srv_msg_grab_candidates;

  // TODO Need to set center correctly.
  srv_msg_grab_candidates.request.center.x = 0;
  srv_msg_grab_candidates.request.center.y = 0;
  srv_msg_grab_candidates.request.center.z = 0.5;

  // TODO Radius should be a param
  srv_msg_grab_candidates.request.radius = 0.3;
  srv_msg_grab_candidates.request.filter = true;

  srv_msg_grab_candidates.request.min_probability = .7;
  srv_msg_grab_candidates.request.max_n = 100;
  srv_msg_grab_candidates.request.min_position_distance_to_neighbor = .005;
  srv_msg_grab_candidates.request.min_orientation_distance_to_neighbor =
      M_PI / 8.;

  srv_client_grab_candidates_.call(srv_msg_grab_candidates);

  if (srv_msg_grab_candidates.response.grasp_candidates.size() == 0) {
    ROS_WARN("No perchable sites detected");
    as_detect_perch_site->setAborted();
    return;
  }

  // Get the grasp that has the highest priority along with it's neighbors.
  double max_filtered_prob = 0;
  int max_prob_candidate_idx = -1;
  for (int i = 0; i < srv_msg_grab_candidates.response.grasp_candidates.size();
       i++) {
    auto center_candidate =
        srv_msg_grab_candidates.response.grasp_candidates[i];
    int element_count = 1;
    double prob_sum = center_candidate.probability_grasp_success;
    for (int j = 0;
         j < srv_msg_grab_candidates.response.grasp_candidates.size(); j++) {
      if (i == j) {
        continue;
      }
      auto curr_candidate =
          srv_msg_grab_candidates.response.grasp_candidates[j];
      double diff_x = center_candidate.point.x - curr_candidate.point.x;
      double diff_y = center_candidate.point.y - curr_candidate.point.y;
      double diff_z = center_candidate.point.z - curr_candidate.point.z;

      if (std::sqrt((diff_x * diff_x) + (diff_y * diff_y) + (diff_z * diff_z)) <
          0.02) {
        element_count++;
        prob_sum += curr_candidate.probability_grasp_success;
      }
    }
    ROS_INFO("elem now %i", element_count);
    double prob_avg = prob_sum / (element_count + 0.0);
    if (prob_avg > max_filtered_prob) {
      max_filtered_prob = prob_avg;
      max_prob_candidate_idx = i;
      ROS_INFO("Max elem so far %i", element_count);
    }
  }

  double max_prob = max_filtered_prob;

  auto perch_target =
      srv_msg_grab_candidates.response.grasp_candidates[max_prob_candidate_idx];

  ROS_INFO("Max probability is %f, index %d", max_prob, max_prob_candidate_idx);

  graspability_server::DetectPerchSiteResult result;
  pcl_conversions::fromPCL(current_cloud_.header.stamp,
                           result.perch_site.header.stamp);
  result.perch_site.header.frame_id = cam_frame_id_;

  result.perch_site.pose.position.x = perch_target.point.x;
  result.perch_site.pose.position.y = perch_target.point.y;
  result.perch_site.pose.position.z = perch_target.point.z;
  result.perch_site.pose.orientation.x = 0;
  result.perch_site.pose.orientation.y = 0;
  result.perch_site.pose.orientation.z = 0;
  result.perch_site.pose.orientation.w = 1;
  ROS_INFO("Found perch site!");
  ROS_INFO("Sending WP: [%f, %f, %f]",
    perch_target.point.x,
    perch_target.point.y,
    perch_target.point.z);
  visualization_msgs::Marker marker_perch_target;
  // TODO Should change this.
  marker_perch_target.header.frame_id = cam_frame_id_;
  marker_perch_target.header.stamp = ros::Time();
  marker_perch_target.ns = "perch";
  marker_perch_target.id = 0;
  marker_perch_target.type = visualization_msgs::Marker::SPHERE;
  marker_perch_target.action = visualization_msgs::Marker::ADD;
  marker_perch_target.pose.position.x = perch_target.point.x; /// RIGHT
  marker_perch_target.pose.position.y = perch_target.point.y;
  marker_perch_target.pose.position.z = perch_target.point.z;
  marker_perch_target.pose.orientation.x = 0.0;
  marker_perch_target.pose.orientation.y = 0.0;
  marker_perch_target.pose.orientation.z = 0.0;
  marker_perch_target.pose.orientation.w = 1.0;
  marker_perch_target.scale.x = 0.1;
  marker_perch_target.scale.y = 0.1;
  marker_perch_target.scale.z = 0.1;
  marker_perch_target.color.a = 1.0; // Don't forget to set the alpha!
  marker_perch_target.color.r = 1.0;
  marker_perch_target.color.g = 0.0;
  marker_perch_target.color.b = 0.0;

  pub_perch_target_marker_.publish(marker_perch_target);

  as_detect_perch_site->setSucceeded(result);
}

int main(int argc, char **argv) {
  ros::init(argc, argv, "detect_perching_site_node");
  ros::NodeHandle node_handle;
  ros::NodeHandle pnh("~");

  got_first_cloud_ = false;

  // Get launch parameters
  std::string topic_pointcloud;
  pnh.param<std::string>("topic_pointcloud", topic_pointcloud, "/camera/depth_registered/points");

  pnh.param<std::string>("cam_frame_id", cam_frame_id_, "/camera");

  sub_rgbd_ = node_handle.subscribe(topic_pointcloud, 1,
                                    rgbd_callback);

  pub_perch_target_marker_ =
      node_handle.advertise<visualization_msgs::Marker>("perch_target", 1);

  srv_client_insert_mesh_ =
      node_handle.serviceClient<graspability_server::InsertMesh>(
          "insert_mesh");

  srv_client_grab_candidates_ =
      node_handle.serviceClient<graspability_server::GrabCandidates>(
          "grab_candidates");

  as_detect_perch_site = new actionlib::SimpleActionServer<
      graspability_server::DetectPerchSiteAction>(
      node_handle, "detect_perch_site", execute_cb_detect_perch_site, false);

  as_detect_perch_site->start();

  ros::Rate r(2);
  while (ros::ok()) {
    ros::spinOnce();
    r.sleep();
  }
}
