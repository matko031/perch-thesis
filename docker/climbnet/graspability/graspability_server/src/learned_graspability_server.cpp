#include <dynamic_reconfigure/server.h>
#include <geometry_msgs/PointStamped.h>
#include <ros/package.h>
#include <ros/ros.h>
#include <sensor_msgs/PointCloud2.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_listener.h>

#include <pcl/conversions.h>
#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>
#include <pcl/point_types.h>
#include <pcl/search/kdtree.h>
#include <pcl/search/organized.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl_ros/transforms.h>

#include <graspability_server/GrabCandidates.h>
#include <graspability_server/GraspCandidate.h>
#include <graspability_server/InsertMesh.h>
#include <interactive_markers/interactive_marker_server.h>

#include <lemg/BoWModel.hpp>
#include <lemg/Labeling.hpp>
#include <lemg/PruningFilter.hpp>

#include <boost/filesystem.hpp>
#include <boost/lexical_cast.hpp>
#include <ctime>
#include <visualization_msgs/MarkerArray.h>

bool first = true;
unsigned int prev_number_markers;
// put this into the launch file
unsigned int graspability_marker_offset = 100;

typedef enum { CNN = 0, KLR, HARDCODED, UNKNOWN } ClassifierVariant;

const std::string classifier_variant_str[] = {"cnn", "klr", "hardcoded",
                                              "unknown"};

visualization_msgs::MarkerArray GrabCandidatesRequestAndResponse2MarkerArray(
    graspability_server::GrabCandidates::Request &req,
    graspability_server::GrabCandidates::Response &res, std::string frame_id,
    ros::Time timestamp) {
  visualization_msgs::MarkerArray marker_array;
  visualization_msgs::Marker marker;
  marker.points.resize(2);
  geometry_msgs::Point point_tail, point_tip;
  double arrow_length = .03;
  ros::Time time_now = ros::Time::now();

  marker.header.frame_id = frame_id;
  marker.header.stamp = timestamp;
  marker.id = graspability_marker_offset;
  marker.type = visualization_msgs::Marker::SPHERE;
  marker.action = visualization_msgs::Marker::ADD;
  marker.pose.position.x = req.center.x;
  marker.pose.position.y = req.center.y;
  marker.pose.position.z = req.center.z;
  marker.pose.orientation.w = 1;
  marker.scale.x = 2 * req.radius;
  marker.scale.y = 2 * req.radius;
  marker.scale.z = 2 * req.radius;
  marker.color.a = 0.3;
  marker.color.r = 0.5;
  marker.color.g = 0.5;
  marker.color.b = 1.0;
  marker_array.markers.push_back(marker);

  marker.pose.position.x = 0;
  marker.pose.position.y = 0;
  marker.pose.position.z = 0;

  ROS_INFO_STREAM("Number of candidates: " << res.grasp_candidates.size());
  for (size_t i = 0; i < res.grasp_candidates.size(); ++i) {
    marker.header.frame_id = frame_id;
    marker.header.stamp = timestamp;
    marker.id = i + graspability_marker_offset + 1;
    marker.type = visualization_msgs::Marker::ARROW;
    marker.action = visualization_msgs::Marker::ADD;
    point_tail.x = res.grasp_candidates[i].point.x;
    point_tail.y = res.grasp_candidates[i].point.y;
    point_tail.z = res.grasp_candidates[i].point.z;
    point_tip.x = res.grasp_candidates[i].point.x +
                  arrow_length * res.grasp_candidates[i].normal.x;
    point_tip.y = res.grasp_candidates[i].point.y +
                  arrow_length * res.grasp_candidates[i].normal.y;
    point_tip.z = res.grasp_candidates[i].point.z +
                  arrow_length * res.grasp_candidates[i].normal.z;
    marker.points[0] = point_tail;
    marker.points[1] = point_tip;
    marker.scale.x = .002; // shaft dia
    marker.scale.y = .003; // head dia
    marker.scale.z = .01;  // head_len
    marker.color.a = 1.0;
    marker.color.r = 1.0;
    marker.color.g = 0.0;
    marker.color.b = 0.0;
    marker_array.markers.push_back(marker);
  }

  // Delete previous arrows
  if (first) {
    first = false;
  } else {
    if (prev_number_markers > res.grasp_candidates.size()) {
      for (size_t i = res.grasp_candidates.size(); i < prev_number_markers;
           i++) {
        marker.header.frame_id = "odom_combined";
        marker.header.stamp = time_now;
        marker.id = i + graspability_marker_offset;
        marker.type = visualization_msgs::Marker::ARROW;
        marker.action = visualization_msgs::Marker::DELETE;
        marker_array.markers.push_back(marker);
      }
    }
  }

  prev_number_markers = res.grasp_candidates.size();

  return marker_array;
}

std::string time_string() {
  std::time_t t = std::time(0);
  struct tm *now = std::localtime(&t);

  char buffer[512];
  std::strftime(buffer, 512, "%Y-%m-%d-%H-%M-%S", now);

  return buffer;
}

class Graspability {
public:
  Graspability()
      : viewpoint_(Eigen::Vector3d::Zero()),
        cloud_(new pcl::PointCloud<pcl::PointXYZ>()), nh_(), pnh_("~"),
        mesh_(new pcl::PolygonMesh()), log_index_(0), grasp_index_(0) {
    grab_service_ = nh_.advertiseService("grab_candidates",
                                         &Graspability::grabCandidates, this);
    insert_service_ =
        nh_.advertiseService("insert_mesh", &Graspability::insertMesh, this);
    cloud_pub_ =
        nh_.advertise<sensor_msgs::PointCloud2>("graspability_cloud", 1, true);
    mesh_pub_ =
        nh_.advertise<visualization_msgs::Marker>("graspability_mesh", 1, true);

    normal_arrow_pub_ = nh_.advertise<visualization_msgs::MarkerArray>(
        "footprint_normal_arrows", 1, true);

    ros::NodeHandle nh;

    if (!pnh_.getParam("classifier", classifier_filename_)) {
      classifier_filename_ = "";
    }

    if (!pnh_.getParam("log", log_base_)) {
      ROS_WARN_STREAM("No log directory provided. Using /tmp.");
      log_base_ = "/tmp";
    }

    std::string classifier_variant;
    if (!pnh_.getParam("classifier_variant", classifier_variant)) {
      // ROS_WARN_STREAM("Classifier variant not specified. Assuming CNN.");
      classifier_variant_ = CNN;
    } else {
      if (classifier_variant == "cnn")
        classifier_variant_ = CNN;
      else if (classifier_variant == "klr") {
        ROS_ERROR_STREAM("KLR currently not supported");
        classifier_variant_ = KLR;
        std::exit(EXIT_FAILURE);
      } else if (classifier_variant == "hardcoded") {
        classifier_variant_ = HARDCODED;
      } else {
        classifier_variant_ = UNKNOWN;
        ROS_ERROR_STREAM("classifier variant argument not provided");
        std::exit(EXIT_FAILURE);
      }
    }
    log_base_ += "/" + time_string();
  }

  ~Graspability() {}

  bool insertMesh(graspability_server::InsertMesh::Request &req,
                  graspability_server::InsertMesh::Response &res) {
    try {
      ROS_INFO_STREAM("** Received insertMesh service request.");

      {
        tf::StampedTransform tmp;
        tf::transformStampedMsgToTF(req.odom2baselink, tmp);
        odom2baselink_ = tmp;
        tf::Vector3 v = odom2baselink_.getOrigin();
        viewpoint_ = Eigen::Vector3d(v.getX(), v.getY(), v.getZ());
      }

      log_dir_ =
          log_base_ + "/" + boost::lexical_cast<std::string>(log_index_++);
      if (!boost::filesystem::create_directories(log_dir_)) {
        ROS_ERROR_STREAM("Cannot create log dir " << log_dir_);
        throw std::runtime_error("Cannot create log dir.");
      }

      pcl_conversions::toPCL(req.mesh, *mesh_);
      pcl::fromPCLPointCloud2(mesh_->cloud, *cloud_);
      if (pcl::io::savePLYFileBinary(log_dir_ + "/mesh.ply", *mesh_) < 0) {
        ROS_ERROR_STREAM("Cannot write PLY file to disk: " << log_dir_ +
                                                                  "/mesh.ply");
        throw std::runtime_error("Cannot write PLY file.");
      }

      {
        std::ofstream ofs((log_dir_ + "/viewpoint.txt").c_str());
        ofs << viewpoint_.x() << " " << viewpoint_.y() << " " << viewpoint_.z()
            << std::endl;
        if (ofs.fail()) {
          ROS_ERROR_STREAM("Cannot write viewpoint file to disk: "
                           << log_dir_ + "/viewpoint.txt");
          throw std::runtime_error("Cannot write viewpoint file.");
        }
      }

      grasp_index_ = 0;

      return true;

    } catch (std::exception &e) {
      ROS_ERROR_STREAM("Exception caught in insertMesh: " << e.what());
      {
        std::string filename =
            log_dir_ + "/failure-at-" + time_string() + ".txt";
        std::ofstream ofs(filename.c_str());
        ofs << "Exception caught in insertMesh:\n\n" << e.what() << std::endl;
        if (ofs.fail()) {
          ROS_ERROR_STREAM(
              "Cannot write failure message to disk: " << filename);
          std::exit(EXIT_FAILURE);
        }
      }
    } catch (...) {
      ROS_ERROR_STREAM("Caught unknown exception in insertMesh");
      {
        std::string filename =
            log_dir_ + "/failure-at-" + time_string() + ".txt";
        std::ofstream ofs(filename.c_str());
        ofs << "Unknown exception caught in insertMesh." << std::endl;
        if (ofs.fail()) {
          ROS_ERROR_STREAM(
              "Cannot write failure message to disk: " << filename);
          std::exit(EXIT_FAILURE);
        }
      }
    }
    return false;
  }

  bool grabCandidates(graspability_server::GrabCandidates::Request &req,
                      graspability_server::GrabCandidates::Response &res) {
    try {
      ROS_INFO_STREAM("** Received grabCandidates service request.");

      if (cloud_->size() == 0) {
        throw std::runtime_error("Empty point cloud.");
      }

      if (classifier_variant_ == UNKNOWN) {
        throw std::runtime_error("Unknown classifier variant.");
      }

      std::ofstream param_file(log_dir_ + "/" +
                               boost::lexical_cast<std::string>(grasp_index_) +
                               ".txt");

      pcl::PointXYZ searchPoint;
      searchPoint.x = req.center.x;
      searchPoint.y = req.center.y;
      searchPoint.z = req.center.z;
      float search_request_radius = req.radius;
      float search_request_radius_plus_foot_radius =
          (search_request_radius + lemg::RADIUS);
      int max_n_descriptors = req.max_n;

      param_file << "center: " << req.center.x << " " << req.center.y << " "
                 << req.center.z << std::endl;
      param_file << "radius: " << req.radius << std::endl;
      param_file << "search_request_radius_plus_foot_radius: "
                 << search_request_radius_plus_foot_radius << std::endl;
      param_file << "max_n_descriptors: " << max_n_descriptors << std::endl;
      param_file << "classifier_filename: " << classifier_filename_
                 << std::endl;
      param_file << "classifier_variant: "
                 << classifier_variant_str[classifier_variant_] << std::endl;

      pcl::PointCloud<pcl::PointNormal>::Ptr cloudRoi;
      pcl::PolygonMesh::Ptr meshRoi;
      {
        std::vector<int> pointIdxRadiusSearch;
        std::vector<float> doNotUseMeBecauseIdxArePostSorted;

        {
          pcl::search::KdTree<pcl::PointXYZ> tree;
          tree.setInputCloud(cloud_);
          bool nn = tree.radiusSearch(
              searchPoint, search_request_radius_plus_foot_radius,
              pointIdxRadiusSearch, doNotUseMeBecauseIdxArePostSorted);
          if (nn < 1) {
            throw std::runtime_error("Too few points in region of interest");
          }
          std::sort(pointIdxRadiusSearch.begin(), pointIdxRadiusSearch.end());

          cloudRoi.reset(new pcl::PointCloud<pcl::PointNormal>());
          pcl::copyPointCloud(*cloud_, pointIdxRadiusSearch, *cloudRoi);
          for (const auto pn : *cloudRoi) {
            // This test shouldn't be necessary - I'm fairly sure that
            // copyPointCloud or the point constructor initializes unexistent
            // fields to zero. But with PCL you're never too cautious.
            if (pn.normal_x != 0 || pn.normal_y != 0 || pn.normal_z != 0)
              throw std::runtime_error("Normals have not been inited to zero.");
          }

          meshRoi.reset(new pcl::PolygonMesh());
          std::vector<int> filterMap(cloud_->size(), -1);
          pcl::copyPointCloud(mesh_->cloud, pointIdxRadiusSearch,
                              meshRoi->cloud);
          for (size_t i = 0; i < pointIdxRadiusSearch.size(); ++i)
            filterMap.at(pointIdxRadiusSearch.at(i)) = i;
          for (const auto &triangle : mesh_->polygons) {
            if (triangle.vertices.size() != 3)
              throw std::runtime_error("lazily restricting to triangles");

            pcl::Vertices triangle_filtered;
            auto &fv = triangle_filtered.vertices;
            for (const auto &vertex : triangle.vertices) {
              if (filterMap.at(vertex) < 0)
                break;
              fv.push_back(filterMap.at(vertex));
            }

            if (fv.size() != 3)
              continue;

            if (fv.at(0) == fv.at(1) || fv.at(0) == fv.at(2) ||
                fv.at(1) == fv.at(2))
              continue;

            meshRoi->polygons.push_back(triangle_filtered);
          }
        }

        ROS_INFO_STREAM("Sphere of intererst contains "
                        << cloudRoi->size() << " points (out of "
                        << cloud_->size() << ")");
      }

      pcl::io::savePCDFileBinary("test.pcd", *cloudRoi);
      pcl::io::savePLYFileBinary("test.ply", *meshRoi);

      lemg::BoWModel m;
      // fixme: add argument in BoWModel to compute klr or cnn descriptors only
      if (classifier_variant_ == KLR) {
        m.readModel(classifier_filename_);
        m.setPointCloud(cloudRoi);
      } else {
        m.setClearImageDescriptors(false);
        m.setPointCloud(*meshRoi);
        m.setNaturalColorPointCloud(*meshRoi);
      }
      m.setMaxNDescriptors(max_n_descriptors);
      m.setViewpoint(viewpoint_);
      m.setRegionOfInterest(
          Eigen::Vector3d(searchPoint.x, searchPoint.y, searchPoint.z),
          search_request_radius);

      m.setGripperClocking(lemg::GRIPPER_CLOCK_X);

      ROS_INFO("Got here");

      m.computeDescriptors();

      std::vector<double> labels;
      if (classifier_variant_ == KLR) {
        labels = m.testClassifier();
      } else if (classifier_variant_ == HARDCODED) {
        labels = m.testImageModel("<hardcoded>");
      } else {
        ROS_INFO("Got hereOO");
        labels = m.testImageModel(classifier_filename_);
        ROS_INFO("Got hereNN");
      }

      {
        pcl::PointCloud<pcl::PointXYZRGB> colorCloud;
        ;
        pcl::fromPCLPointCloud2(meshRoi->cloud, colorCloud);
        for (auto &point : colorCloud) {
          point.r = point.g = point.b = 128;
        }
        lemg::paint(colorCloud, labels);
        pcl::toPCLPointCloud2(colorCloud, meshRoi->cloud);
      }

      ROS_INFO("Got hereX");

      m.writeModel(log_dir_ + "/" +
                   boost::lexical_cast<std::string>(grasp_index_) + ".model+");

      ROS_INFO("Got hereY");
      pcl::PointCloud<pcl::PointXYZINormal>::Ptr grasps =
          m.getEvaluatedGrasps();
      pcl::io::savePCDFileBinaryCompressed(
          log_dir_ + "/" + boost::lexical_cast<std::string>(grasp_index_) +
              ".pcd",
          *grasps);
      pcl::io::savePCDFileBinaryCompressed(
          log_dir_ + "/" + boost::lexical_cast<std::string>(grasp_index_) +
              "-roi.pcd",
          *cloudRoi);
      if (pcl::io::savePLYFileBinary(
              log_dir_ + "/" + boost::lexical_cast<std::string>(grasp_index_) +
                  "-roi.ply",
              *meshRoi) < 0) {
        ROS_ERROR_STREAM("Cannot write PLY file to disk: "
                         << log_dir_ + "/"
                         << boost::lexical_cast<std::string>(grasp_index_)
                         << "-roi.ply");
        throw std::runtime_error("Cannot write PLY file.");
      }

      {
        std::vector<lemg::histogram> histograms = m.getHistograms();
        std::ofstream histograms_stream(
            log_dir_ + "/" + boost::lexical_cast<std::string>(grasp_index_) +
            "-histograms.txt");
        std::copy(
            histograms.begin(), histograms.end(),
            std::ostream_iterator<lemg::histogram>(histograms_stream, "\n"));
      }

      ROS_INFO_STREAM("Computed " << grasps->size() << " grasps");

      param_file << "filter: " << (req.filter ? "true" : "false") << std::endl;

      if (req.filter) {
        param_file << "max_n: " << req.max_n << std::endl;
        param_file << "min_probability: " << req.min_probability << std::endl;
        param_file << "min_position_distance_to_neighbor: "
                   << req.min_position_distance_to_neighbor << std::endl;
        param_file << "min_orientation_distance_to_neighbor: "
                   << req.min_orientation_distance_to_neighbor << std::endl;

        grasps = lemg::filter(grasps, req.max_n, req.min_probability,
                              req.min_position_distance_to_neighbor,
                              req.min_orientation_distance_to_neighbor);
        ROS_INFO_STREAM("After filtering, " << grasps->size()
                                            << " grasps remain");
        pcl::io::savePCDFileBinaryCompressed(
            log_dir_ + "/" + boost::lexical_cast<std::string>(grasp_index_) +
                "-filtered.pcd",
            *grasps);
      } else
        ROS_INFO_STREAM("No filtering requested");

      for (auto i : *grasps) {
        graspability_server::GraspCandidate gc;
        gc.point.x = i.x;
        gc.point.y = i.y;
        gc.point.z = i.z;
        gc.normal.x = i.normal_x;
        gc.normal.y = i.normal_y;
        gc.normal.z = i.normal_z;
        gc.variance = i.curvature;
        gc.max_height_difference = -1;
        gc.probability_grasp_success = i.intensity;
        gc.predicted_max_normal_force = -1;
        gc.predicted_max_tangent_force = -1;
        res.grasp_candidates.push_back(gc);
      }

      // Publish graspability cloud

      {
        sensor_msgs::PointCloud2 ros_cloud;
        pcl::toROSMsg(*grasps, ros_cloud);
        ros_cloud.header.stamp = ros::Time::now();
        ros_cloud.header.frame_id = "odom_combined";
        cloud_pub_.publish(ros_cloud);
        publishMesh(*meshRoi);
      }
      // Display normals in PCL viewer
      /*
       pcl::PointCloud<pcl::PointNormal>::Ptr normals (new pcl::PointCloud<\
       pcl::PointNormal>);
       pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_subset (new pcl::PointCloud<\
       pcl::PointXYZ>);

       for (int i = 0; i < normal_cloud.points.size(); i++) {
       pcl::PointNormal pt;
       pt.normal[0] = normal_cloud.points[i].normal[0];
       pt.normal[1] = normal_cloud.points[i].normal[1];
       pt.normal[2] = normal_cloud.points[i].normal[2];
       normals->points.push_back(pt);

       pcl::PointXYZ pt_xyz;
       pt_xyz.x = normal_cloud.points[i].x;
       pt_xyz.y = normal_cloud.points[i].y;
       pt_xyz.z = normal_cloud.points[i].z;
       cloud_subset->points.push_back(pt_xyz);
       }

       pcl::visualization::PCLVisualizer viewer("PCL Viewer");
       viewer.setBackgroundColor (0.3, 0.0, 0.3);
       viewer.addPointCloudNormals<pcl::PointXYZ, pcl::PointNormal>
       (cloud_subset, normals, 5, 0.05, "normals");
       viewer.addCoordinateSystem (1.0);

       while (!viewer.wasStopped ()) {
       viewer.spinOnce ();
       }
       */

      visualization_msgs::MarkerArray marker_array =
          GrabCandidatesRequestAndResponse2MarkerArray(
              req, res, "odom_combined", ros::Time::now());
      normal_arrow_pub_.publish(marker_array);
      ROS_INFO("published marker array");

      grasp_index_++;

      ROS_INFO_STREAM("Published and responded with " << grasps->size()
                                                      << " grasps");
      return true;

    } catch (std::exception &e) {
      ROS_ERROR_STREAM("Exception caught in grabCandidates: " << e.what());
      {
        std::string filename =
            log_dir_ + "/failure-at-" + time_string() + ".txt";
        std::ofstream ofs(filename.c_str());
        ofs << "Exception caught in grabCandidates:\n\n"
            << e.what() << std::endl;
        if (ofs.fail()) {
          ROS_ERROR_STREAM(
              "Cannot write failure message to disk: " << filename);
          std::exit(EXIT_FAILURE);
        }
      }
    } catch (...) {
      ROS_ERROR_STREAM("Caught unknown exception in grabCandidates");
      {
        std::string filename =
            log_dir_ + "/failure-at-" + time_string() + ".txt";
        std::ofstream ofs(filename.c_str());
        ofs << "Unknown exception caught in grabCandidates." << std::endl;
        if (ofs.fail()) {
          ROS_ERROR_STREAM(
              "Cannot write failure message to disk: " << filename);
          std::exit(EXIT_FAILURE);
        }
      }
    }
    return false;
  }

  template <class PointT> inline Eigen::Vector3f getEigen(PointT pt) const {
    Eigen::Vector3f x;
    x << pt.x, pt.y, pt.z;
    return x;
  }

  void publishMesh(const pcl::PolygonMesh &mesh) {
    bool use_mesh_resource = false;
    // pcl_msgs::PolygonMesh meshRoiMsg;
    // pcl_conversions::fromPCL(*meshRoi, meshRoiMsg);

    // Publish the mesh
    pcl::PointCloud<pcl::PointXYZRGB> cloud;
    pcl::fromPCLPointCloud2(mesh.cloud, cloud);
    visualization_msgs::Marker marker;
    marker.header.frame_id = "odom_combined";
    marker.header.stamp = ros::Time::now();
    marker.ns = "triangle_list";
    marker.id = 0;

    if (use_mesh_resource) {
      marker.type = visualization_msgs::Marker::MESH_RESOURCE;
      // marker.mesh_resource =
      // "package://pr2_description/meshes/base_v0/base.dae";
      marker.mesh_resource = "file:///shared/tmp/mesh.dae";
    } else
      marker.type = visualization_msgs::Marker::TRIANGLE_LIST;
    marker.action = visualization_msgs::Marker::ADD;
    marker.pose.position.x = 0;
    marker.pose.position.y = 0;
    marker.pose.position.z = 0;
    marker.pose.orientation.x = 0.0;
    marker.pose.orientation.y = 0.0;
    marker.pose.orientation.z = 0.0;
    marker.pose.orientation.w = 1.0;
    if (!use_mesh_resource) {
      for (auto &triangle : mesh.polygons) {

        Eigen::Vector3f p0 = getEigen(cloud.points.at(triangle.vertices.at(0)));
        Eigen::Vector3f p1 = getEigen(cloud.points.at(triangle.vertices.at(1)));
        Eigen::Vector3f p2 = getEigen(cloud.points.at(triangle.vertices.at(2)));
        Eigen::Vector3f normal = (p1 - p0).cross(p2 - p1).normalized();
        Eigen::Vector3f viewpoint(viewpoint_.x(), viewpoint_.y(),
                                  viewpoint_.z());
        Eigen::Vector3f light = (p1 - viewpoint).normalized();
        float shade = .5 - light.dot(normal) * .5;
        shade = std::max(0.f, std::min(shade, 1.f));
        for (auto &vertex_index : triangle.vertices) {
          pcl::PointXYZRGB mpoint = cloud.points[vertex_index];
          geometry_msgs::Point point;
          point.x = mpoint.x;
          point.y = mpoint.y;
          point.z = mpoint.z;
          marker.points.push_back(point);
          std_msgs::ColorRGBA color;
          color.r = mpoint.r / 255. * shade;
          color.g = mpoint.g / 255. * shade;
          color.b = mpoint.b / 255. * shade;
          color.a = 1.0;
          marker.colors.push_back(color);
        }
      }
    }
    marker.scale.x = 1.0;
    marker.scale.y = 1.0;
    marker.scale.z = 1.0;
    marker.color.r = 1.0;
    marker.color.g = 1.0;
    marker.color.b = 1.0;
    marker.color.a = 1.0;
    ROS_INFO_STREAM("Publishing mesh marker...");
    mesh_pub_.publish(marker);
  }

private:
  // State
  tf::Transform odom2baselink_;
  Eigen::Vector3d viewpoint_;
  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_;
  pcl::PolygonMesh::Ptr mesh_;
  std::string classifier_filename_;
  std::string log_base_, log_dir_;
  int log_index_;
  int grasp_index_;
  ClassifierVariant classifier_variant_;

  // NodeHandles
  ros::NodeHandle nh_;
  ros::NodeHandle pnh_;

  // Services
  ros::ServiceServer grab_service_;
  ros::ServiceServer insert_service_;

  // Publishers
  ros::Publisher cloud_pub_;
  ros::Publisher mesh_pub_;
  ros::Publisher normal_arrow_pub_;
};

int main(int argc, char *argv[]) {
  try {
    ros::init(argc, argv, "learned_graspability_server");
    Graspability graspability;
    ros::spin();
    return 0;
  } catch (std::exception &e) {
    ROS_ERROR_STREAM("Exception caught: " << e.what());
    std::exit(EXIT_FAILURE);
  } catch (...) {
    ROS_ERROR_STREAM("Caught unknown exception");
    std::exit(EXIT_FAILURE);
  }
  return EXIT_FAILURE;
}
