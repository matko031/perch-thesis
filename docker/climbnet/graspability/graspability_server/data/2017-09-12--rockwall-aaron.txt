Model trained on an RPM reconstruction of the 107 rockwall, with Aaron's training data:

cd /work-data/lemur/2016-12-15--rockwall-tags/annot/rock_wall_mesh_no_limbs_AJP-flipped

file=$( basename $( pwd ) )
input_file=../$file

nq=16
nc=32
elenorm=
s=10000

lemg bcolor $input_file.ply -l $file.labels $file-nocolor.pcd
lemg btrain --nq $nq --nc $nc -s $s $elenorm -o $file.model  -l $file.labels $file-nocolor.pcd -C

the model file is in the new Cereal format.
