rosrun tf static_transform_publisher 0.0 0.0 0.0 0.0 0.0 0.0 odom_combined map 40 &

rosrun pcl_ros pcd_to_pointcloud examples/rockwall-light.pcd _frame_id:=/odom_combined 1 & 

rosrun graspability_server learned_graspability_server  _classifier:=data/2017-09-12--rockwall-aaron.model &


rosrun graspability_server learned_graspability_example_client _cloud:=examples/rockwall.pcd &

