#include <nuklei/Common.h>
#include <nuklei/Color.h>
#include <lemg/Labeling.hpp>
#include <lemg/Common.hpp>

namespace lemg
{
  using namespace nuklei;
  
  template<typename T>
  void rl(const std::string& file, std::vector<T>& v)
  {
    NUKLEI_ASSERT(v.size() == 0);
    std::ifstream ifs(file.c_str());
    if (!ifs.is_open())
      NUKLEI_THROW("Failed to open `" << file << "' for reading.");
    std::string line;
    while (std::getline(ifs, line))
    {
      v.push_back(nuklei::numify<T>(line));
    }
  }
  
  void readLabels(const std::string& file, std::vector<double>& v)
  {
    return rl<double>(file, v);
  }
  
  void readLabels(const std::string& file, std::vector<int>& v)
  {
    return rl<int>(file, v);
  }

  
  void paint(pcl::PointCloud<pcl::PointXYZRGB>& cloud,
             const std::vector<double>& labels,
             double originalColorWeight,
             double discretizationThreshold,
             bool normalize,
             bool negativeIsUndefined,
             bool flip)
  {
    NUKLEI_TRACE_BEGIN();
    
    NUKLEI_ASSERT(cloud.size() == labels.size());
    double maxc = -1e8, minc = 1e8;
    
    if (normalize)
    {
      for (auto i = labels.begin(); i != labels.end(); ++i)
      {
        if (!negativeIsUndefined || *i >= 0)
        {
          minc = std::min(minc, *i);
          maxc = std::max(maxc, *i);
        }
      }
    }
    
#pragma omp parallel for
    for (unsigned i = 0; i < cloud.size(); ++i)
    {
      if (negativeIsUndefined && labels.at(i) < 0) continue;
      
      float v = labels.at(i);
      
      if (normalize)
      {
        v = (v-minc)/(maxc-minc);
      }
      
      if (discretizationThreshold > 0)
      {
        if (v < discretizationThreshold) v = 0;
        else v = 1;
      }
      
      double ocw = originalColorWeight;
      
      pcl::PointXYZRGB& p = cloud.at(i);
      
      RGBColor o(p.r/255., p.g/255., p.b/255.);
      if (flip) v = 1-v;
      RGBColor m(HSVColor(v*M_PI/3*2, 1, 1));
      RGBColor c;
      c.setVector(ocw*o.getVector()+(1-ocw)*m.getVector());
      GVector wsum = c.getVector()*255;
      Eigen::Vector3f sum(wsum[0], wsum[1], wsum[2]);
      
      
      //Eigen::Vector3f ic(p.r, p.g, p.b);
      //Eigen::Vector3f green(255,(1-v)*255,(1-v)*255);
      //Eigen::Vector3f sum = .3*ic+.7*green;
      if (std::isnan(p.z)) sum = Eigen::Vector3f(0, 0, 0);
      p.r = sum[0];
      p.g = sum[1];
      p.b = sum[2];
    }
    NUKLEI_TRACE_END();
  }

  template<typename PointT>
  void writeLabel(PointT& p, const int label)
  {
    if (label == 0)
    {
      p.r = 255; p.g = 0; p.b = 0;
    }
    else if (label == 1)
    {
      p.r = 0; p.g = 255; p.b = 0;
    }
    else if (label == 2)
    {
      p.r = 0; p.g = 0; p.b = 255;
    }
    else NUKLEI_THROW("Unknown label " << label);
  }
  
  template
  void writeLabel<pcl::PointXYZRGB>(pcl::PointXYZRGB& p, const int label);
  template
  void writeLabel<pcl::PointXYZRGBA>(pcl::PointXYZRGBA& p, const int label);
}
