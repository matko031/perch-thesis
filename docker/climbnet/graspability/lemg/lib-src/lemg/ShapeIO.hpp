#ifndef LEMG_SHAPEIO_HPP
#define LEMG_SHAPEIO_HPP

#include <lemg/Common.hpp>

namespace lemg {
  
  struct ShapeIO
  {
  private:
    template <class PointT>
    inline Eigen::Vector3f getEigen(PointT pt) const
    {
      Eigen::Vector3f x;
      x << pt.x, pt.y, pt.z;
      return x;
    }
    
  public:
    void read(const std::string& file);
    void read(const pcl::PolygonMesh& mesh);
    void write(const std::string& file) const;

    pcl::PCLPointCloud2& pc() { return mesh_.cloud; }
    const pcl::PCLPointCloud2& pc() const { return mesh_.cloud; }
    pcl::PolygonMesh& mesh() { return mesh_; }
    const pcl::PolygonMesh& mesh() const { return mesh_; }
    
    bool pc2HasNormals() const
    {
      bool has_normals = false;
      for (const auto& field : pc().fields)
      {
        if (field.name == "normal_x")
          has_normals = true;
      }
      return has_normals;
    }

    bool pc2HasField(const std::string& field_name) const
    {
      bool has_field = false;
      for (const auto& field : pc().fields)
      {
        if (field.name == field_name)
          has_field = true;
      }
      return has_field;
    }

    template<typename PointT> pcl::PointCloud<PointT> getWithPolygonNormals() const;

    template<typename PointT> pcl::PointCloud<PointT> get() const;
    
    template<typename PointT> void set(const pcl::PointCloud<PointT>& cloud)
    {
      pcl::toPCLPointCloud2(cloud, mesh_.cloud);
    }
    
    // Concatenate fields
    template<typename PointT> void cat(const pcl::PointCloud<PointT>& cloud)
    {
      pcl::PCLPointCloud2 in1, in2;
      pcl::toPCLPointCloud2(cloud, in1);
      in2 = mesh_.cloud;
      // in2 in1 order matters: in2 overwritten by in1.
      pcl::concatenateFields(in2, in1, mesh_.cloud);
    }
    
    void simplify(const double resolution = 0.);

    std::string getFileFormat() const { return type_; }
    void setFileFormat(const std::string& type) { type_ = type; }
    
  private:
    pcl::PolygonMesh mesh_;
    std::string type_;
  };
  
}

#endif
