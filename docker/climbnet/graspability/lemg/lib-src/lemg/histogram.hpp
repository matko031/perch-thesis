#ifndef LEMG_HISTOGRAM_HPP
#define LEMG_HISTOGRAM_HPP

#include <lemg/cereal/archives/binary.hpp>
#include <lemg/Common.hpp>

#include <pcl/conversions.h>
#include <pcl/point_types.h>

namespace lemg {
  
  struct histogram
  {
    typedef Eigen::VectorXd V;
    
    histogram() : pointId_(-1) {}
    histogram(int n) : bins_(V::Zero(n)), pointId_(-1) {}
    
    unsigned size() const { return bins_.size(); }
    
    void inc(int i)
    {
      bins_(i) += 1;
    }
    
    void set(int i, double value)
    {
      bins_(i) = value;
    }
    
    double get(int i)
    {
      return bins_(i);
    }
        
    double kernel(const histogram& h) const;
    double chi(const histogram& h) const;
    
    void setPointId(const int id) { pointId_ = id; }
    int getPointId() const { return pointId_; }

    friend std::istream& operator>>(std::istream &in, histogram &h);
    friend std::ostream& operator<<(std::ostream &out, const histogram &h);
  private:
    friend class cereal::access;
    template<class Archive>
    void serialize(Archive &ar, std::uint32_t const version)
    {
      ar(CEREAL_NVP(bins_),
         CEREAL_NVP(pointId_));
    }

    V bins_;
    int pointId_;
  };
  
  inline std::istream& operator>>(std::istream &in, histogram &h)
  {
    double hSize = -1;
    LEMG_ASSERT(in >> hSize);
    h = histogram(hSize);
    for (unsigned i = 0; i < h.size(); ++i)
      LEMG_ASSERT(in >> h.bins_[i]);
    return in;
  }

  inline std::ostream& operator<<(std::ostream &out, const histogram &h)
  {
    out << h.size() << " ";
    for (unsigned i = 0; i < h.size(); ++i)
      out << h.bins_[i] << " ";
    return out;
  }

  
  inline double histogram::kernel(const histogram& h) const
  {
#ifdef LEMG_CIRCULAR_DESCRIPTOR
    std::vector<double> differences;
    int nd = bins_.size()/QUADRANT_DESCRIPTOR_SIZE;
    V binsCat(bins_.size()*2);
    binsCat.segment(0, bins_.size()) = bins_;
    binsCat.segment(bins_.size(), bins_.size()) = bins_;
    for (unsigned i = 0; i < nd; ++i)
    {
      double d = 0;
      //for (unsigned j = 0; j < bins_.size(); ++j)
      //  d += std::abs(bins_(j)-h.bins_( (i*QUADRANT_DESCRIPTOR_SIZE+j)%bins_.size() ));
      d += (h.bins_-binsCat.segment(i*QUADRANT_DESCRIPTOR_SIZE,
                                    bins_.size())).lpNorm<2>();
      differences.push_back(d);
    }
    return - *std::min_element(differences.begin(), differences.end());
#else
    const V& h1 = bins_;
    const V& h2 = h.bins_;
    
    double h1m = h1.sum()/h1.size();
    double h2m = h2.sum()/h2.size();
    
    double num = (( h1.array()-h1m ) * ( h2.array()-h2m )).sum();
    // On Ubuntu+GCC, denom_slow is 5x longer to compute than denom
    // On Ubuntu+clang, or on OSX, it makes no difference.
    //double denom_slow = std::sqrt( ( h1.array()-h1m ).pow(2).sum() * ( h2.array()-h2m ).pow(2).sum() );
    double denom = std::sqrt( ( h1.array()-h1m ).matrix().squaredNorm() * ( h2.array()-h2m ).matrix().squaredNorm() );
    return num/denom;
#endif
  }
  
  inline double histogram::chi(const histogram& h) const
  {
    const V& h1 = bins_;
    const V& h2 = h.bins_;
    
    V difference = h1-h2;
    V sum = h1+h2;
    
    double ret = 0;
    for (unsigned i = 0; i < h1.size(); ++i)
    {
      if (sum(i) != 0)
        ret += difference(i)*difference(i)/sum(i);
    }
    
    return ret;
  }

}

CEREAL_CLASS_VERSION(lemg::histogram, 0);

#endif
