// NormalEstimation.hpp
// Written Ian Rankin - July 2018
//
// This is the header to estimate normals in a robust way.
// This is written because PCL's normal estimation written in
// version 1.8.1_3 is numerically unstable for large number of points
// used in the estimation.
//
// This is due to their use of a single pass method for computing the covariance
// matrix which is numerically unstable, versus a 2 pass method.
// Note: there may be a stable single pass method, but the 2 pass method is
// not particularly slower with worst case O(2n)=O(n) instead of O(n)
// Actual number of operations is less than twice however.

#ifndef __NORMAL_ESTIMATION_JPL_IMPL__
#define __NORMAL_ESTIMATION_JPL_IMPL__

#include "NormalEstimation.h"
#include "pcl/features/normal_3d.h"

// computePointNormal
// This function computes the point normal of the neighborhood of points defined
// by cloudIndicies. This performs PCA, by first computing the centroid of the
// points, then constructing the covariance matrix from that centroid.
// the generic solvePlaneParameters from pcl is called to perform the Eigenvalue
// decomposition to find the normal vector.
//
// @param cloud - the given point cloud.
// @param cloudIndicies - the indicies of cloud to estimate the normal over.
//    These in practice should be returned from a nearest neighbor search.
// @param planeParameters - returns the plane parameters for the point.
// @param curvature - returns the estimated curvature of the point.
//    computed using the size of eigenvalues.
//
// @return - true if it finished successfully, false if fails.
template <class PointT>
bool jpl::computePointNormal(const pcl::PointCloud<PointT> &cloud,
         const std::vector<int> &cloudIndicies,
         Eigen::Vector4f &planeParameters,
         float &curvature)
{
   EIGEN_ALIGN16 Eigen::Matrix3f covariance;
   Eigen::Vector4f centroid;

   if (cloudIndicies.size() < 3 ||
      !jpl::findCovAndMeanF(cloud, cloudIndicies, centroid, covariance))
   {
      planeParameters.setConstant(std::numeric_limits<float>::quiet_NaN());
      curvature = std::numeric_limits<float>::quiet_NaN();
      return false;
   } // end if for bad number of points.

   pcl::solvePlaneParameters(covariance, centroid, planeParameters, curvature);
   return true;
} // end computePointNormal


// computePointNormal
// This function computes the point normal of the neighborhood of points defined
// by cloudIndicies. This performs PCA, by first computing the centroid of the
// points, then constructing the covariance matrix from that centroid.
// the generic solvePlaneParameters from pcl is called to perform the Eigenvalue
// decomposition to find the normal vector.
//
// @param cloud - the given point cloud.
// @param cloudIndicies - the indicies of cloud to estimate the normal over.
//    These in practice should be returned from a nearest neighbor search.
// @param nx - the x direction of the normal
// @param ny - the y direction of the normal
// @param nz - the z direction of the normal
// @param curvature - returns the estimated curvature of the point.
//    computed using the size of eigenvalues.
//
// @return - true if it finished successfully, false if fails.
template <typename PointT>
bool jpl::computePointNormal(const pcl::PointCloud<PointT> &cloud,
         const std::vector<int> &cloudIndicies,
         float &nx, float &ny, float &nz,
         float &curvature)
{
   EIGEN_ALIGN16 Eigen::Matrix3f covariance;
   Eigen::Vector4f centroid;

   if (cloudIndicies.size() < 3 ||
      !jpl::findCovAndMeanF(cloud, cloudIndicies, centroid, covariance))
   {
      nx = std::numeric_limits<float>::quiet_NaN();
      ny = std::numeric_limits<float>::quiet_NaN();
      nz = std::numeric_limits<float>::quiet_NaN();
      curvature = std::numeric_limits<float>::quiet_NaN();
      return false;
   } // end if for bad number of points.

   pcl::solvePlaneParameters(covariance, nx, ny, nz, curvature);
   return true;
} // end computePointNormal






// findCovAndMeanF
// This is an two-pass method to sample covariance.
// By doing this I hope to remove the numerical instabilty
// of finding the covariance matrix.
// This method is outligned on wikipedia here:
// https://en.wikipedia.org/wiki/Algorithms_for_calculating_variance
// this covariance and mean is computed with all floats for intermediate values.
//
// @param indcies - the indcies of the cloud to compute.
// @param centroid - the output centroid of the area.
// @param covarianceMat - the output covariance matrix.
template <typename PointT>
int jpl::findCovAndMeanF(const pcl::PointCloud<PointT> &cloud,
                  const std::vector<int> &indcies,
                  Eigen::Vector4f &centroid,
                  Eigen::Matrix3f &covarianceMat)
{
   float N = (float)indcies.size();
   // first compute the means of points.
   float meanX, meanY, meanZ;
   meanX = meanY = meanZ = 0.0;
   for (int i = 0; i < indcies.size(); i++) {
      PointT pt = cloud.points[indcies[i]];
      meanX += pt.x;
      meanY += pt.y;
      meanZ += pt.z;
   }
   meanX = meanX / N;
   meanY = meanY / N;
   meanZ = meanZ / N;


   float covXX, covXY, covXZ, covYY, covYZ, covZZ;
   covXX = covXY = covXZ = covYY = covYZ = covZZ = 0;

   float resX, resY, resZ; // the residuals of each axes.
   for (int i = 0; i < indcies.size(); i++) {
      PointT pt = cloud.points[indcies[i]];
      resX = pt.x - meanX;
      resY = pt.y - meanY;
      resZ = pt.z - meanZ;

      covXX += (resX * resX / N);
      covXY += (resX * resY / N);
      covXZ += (resX * resZ / N);
      covYY += (resY * resY / N);
      covYZ += (resY * resZ / N);
      covZZ += (resZ * resZ / N);
   } // ebd for loop computing covariances

   // setup final outputs.
   centroid[0] = meanX;
   centroid[1] = meanY;
   centroid[2] = meanZ;

   covarianceMat.coeffRef(0,0) = covXX;
   covarianceMat.coeffRef(0,1) = covXY;
   covarianceMat.coeffRef(0,2) = covXZ;
   covarianceMat.coeffRef(1,1) = covYY;
   covarianceMat.coeffRef(1,2) = covYZ;
   covarianceMat.coeffRef(2,2) = covZZ;
   covarianceMat.coeffRef(1,0) = covarianceMat.coeff(0,1);
   covarianceMat.coeffRef(2,0) = covarianceMat.coeff(0,2);
   covarianceMat.coeffRef(2,1) = covarianceMat.coeff(1,2);

   return indcies.size();
} // end findCovAndMeanF


// findCovAndMeanD
// This is an two-pass method to sample covariance.
// By doing this I hope to remove the numerical instabilty
// of finding the covariance matrix.
// This method is outligned on wikipedia here:
// https://en.wikipedia.org/wiki/Algorithms_for_calculating_variance
// this covariance and mean is computed with all doubles for intermediate values.
//
// @param indcies - the indcies of the cloud to compute.
// @param centroid - the output centroid of the area.
// @param covarianceMat - the output covariance matrix.
template <typename PointT>
int jpl::findCovAndMeanD(const pcl::PointCloud<PointT> &cloud,
                  const std::vector<int> &indcies,
                  Eigen::Vector4f &centroid,
                  Eigen::Matrix3f &covarianceMat)
{
   double N = (double)indcies.size();
   // first compute the means of points.
   double meanX, meanY, meanZ;
   meanX = meanY = meanZ = 0.0;
   for (int i = 0; i < indcies.size(); i++) {
      PointT pt = cloud.points[indcies[i]];
      meanX += pt.x;
      meanY += pt.y;
      meanZ += pt.z;
   }
   meanX = meanX / N;
   meanY = meanY / N;
   meanZ = meanZ / N;


   double covXX, covXY, covXZ, covYY, covYZ, covZZ;
   covXX = covXY = covXZ = covYY = covYZ = covZZ = 0;

   double resX, resY, resZ; // the residuals of each axes.
   for (int i = 0; i < indcies.size(); i++) {
      PointT pt = cloud.points[indcies[i]];
      resX = pt.x - meanX;
      resY = pt.y - meanY;
      resZ = pt.z - meanZ;

      covXX += (resX * resX / N);
      covXY += (resX * resY / N);
      covXZ += (resX * resZ / N);
      covYY += (resY * resY / N);
      covYZ += (resY * resZ / N);
      covZZ += (resZ * resZ / N);
   } // ebd for loop computing covariances

   // setup final outputs.
   centroid[0] = meanX;
   centroid[1] = meanY;
   centroid[2] = meanZ;

   covarianceMat.coeffRef(0,0) = covXX;
   covarianceMat.coeffRef(0,1) = covXY;
   covarianceMat.coeffRef(0,2) = covXZ;
   covarianceMat.coeffRef(1,1) = covYY;
   covarianceMat.coeffRef(1,2) = covYZ;
   covarianceMat.coeffRef(2,2) = covZZ;
   covarianceMat.coeffRef(1,0) = covarianceMat.coeff(0,1);
   covarianceMat.coeffRef(2,0) = covarianceMat.coeff(0,2);
   covarianceMat.coeffRef(2,1) = covarianceMat.coeff(1,2);

   return indcies.size();
} // end findCovAndMeanD


#endif
