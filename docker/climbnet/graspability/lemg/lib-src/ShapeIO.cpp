#include <lemg/PruningFilter.hpp>
#include <nuklei/Common.h>
#include <nuklei/Color.h>
#include <lemg/Common.hpp>
#include <lemg/ShapeIO.hpp>
#include <boost/algorithm/string/predicate.hpp>

#include <numeric>

namespace lemg
{
  using namespace nuklei;
  
  void ShapeIO::read(const std::string& file)
  {
    try
    {
      // I don't think PCL readers throw exceptions. If they do in the future,
      // we'll catch them here.
      
      if (!boost::ends_with(file, ".ply") && !boost::ends_with(file, ".PLY"))
        // If PLY read fails, stupid PCL writes an error report on cerr.
        // We want to avoid that.
        throw std::runtime_error("Filename does not end in .ply");
      
      NUKLEI_ASSERT(pcl::io::loadPLYFile(file, mesh_) >= 0);
      type_ = "ply";
    }
    catch (std::exception& e)
    {
      try
      {
        NUKLEI_ASSERT(pcl::io::loadPCDFile(file, mesh_.cloud) >= 0);
        type_ = "pcd";
      }
      catch (std::exception& f)
      {
        throw std::runtime_error(std::string() +
                                 "Both ply and pcd read attempts of " +
                                 file + " failed.");
      }
    }
  }
  
  void ShapeIO::read(const pcl::PolygonMesh& mesh)
  {
    mesh_ = mesh;
    type_ = "ply";
  }
  
  void ShapeIO::write(const std::string& file) const
  {
    if (type_ == "ply")
    {
      NUKLEI_ASSERT(pcl::io::savePLYFileBinary(file, mesh_) >= 0);
    }
    else if (type_ == "pcd")
    {
      // PCL 1.8: no pcl::io::saveBinaryCompressed for PCLPointCloud2
      NUKLEI_ASSERT(pcl::PCDWriter().writeBinaryCompressed(file, mesh_.cloud) >= 0);
    }
    else
      NUKLEI_THROW("Unknown type '" << type_ << "'");
  }
  
  void ShapeIO::simplify(const double resolution)
  {
    mesh_ = lemg::pruning_filter(mesh_, resolution);
  }
  
  template<typename PointT> pcl::PointCloud<PointT> ShapeIO::get() const
  {
    pcl::PointCloud<PointT> cloud;
    pcl::fromPCLPointCloud2(mesh_.cloud, cloud);
    std::string fields;
    for (const auto& field : pc().fields)
    {
      fields += field.name + " ";
    }
    //NUKLEI_LOG("PointCloud2 [" << fields << "] -> " << typeid(PointT).name());
    return cloud;
  }
  
  template pcl::PointCloud<pcl::PointXYZ> ShapeIO::get<pcl::PointXYZ>() const;
  template pcl::PointCloud<pcl::PointNormal> ShapeIO::get<pcl::PointNormal>() const;
  template pcl::PointCloud<pcl::PointXYZRGB> ShapeIO::get<pcl::PointXYZRGB>() const;
  template pcl::PointCloud<pcl::PointXYZRGBA> ShapeIO::get<pcl::PointXYZRGBA>() const;
  template pcl::PointCloud<pcl::PointXYZINormal> ShapeIO::get<pcl::PointXYZINormal>() const;
  
  
  template<typename PointT> pcl::PointCloud<PointT> ShapeIO::getWithPolygonNormals() const
  {
    LEMG_ASSERT(type_ == "ply" && mesh_.polygons.size() > 0);
    
    pcl::PointCloud<PointT> cloud = get<PointT>();
    
    bool input_has_normals = pc2HasNormals();
    int input_undefined_normals_n = 0;
    
    for (const auto& point : cloud)
    {
      if (point.normal_x == 0 &&
          point.normal_y == 0 &&
          point.normal_z == 0)
        {
          input_undefined_normals_n++;
        }
    }
    
    if (!input_has_normals)
    {
      NUKLEI_ASSERT(input_undefined_normals_n == cloud.size());
    }
    else
    {
      NUKLEI_ASSERT(input_undefined_normals_n < cloud.size());

      if (input_undefined_normals_n >= cloud.size()/10.)
      {
        NUKLEI_WARN("Using input-defined normals. " <<
                    input_undefined_normals_n << " normals are undefined.");
      }
      else if (input_undefined_normals_n > 0)
      {
        NUKLEI_LOG("Using input-defined normals. " <<
                   input_undefined_normals_n << " normals are undefined.");
      }
    }
    
    {
      size_t i = 0;
      size_t numFilled = 0;
      std::vector<int> disagree(cloud.size(), 0);
      const double normalDisagrementThresholdInDegrees = 90;
      
      while (i < mesh_.polygons.size())
      {
        LEMG_ASSERT(mesh_.polygons.at(i).vertices.size() >= 3);
        
        Eigen::Vector3f p0 = getEigen(cloud.points.at(mesh_.polygons.at(i).vertices.at(0)));
        Eigen::Vector3f p1 = getEigen(cloud.points.at(mesh_.polygons.at(i).vertices.at(1)));
        Eigen::Vector3f p2 = getEigen(cloud.points.at(mesh_.polygons.at(i).vertices.at(2)));
        
        Eigen::Vector3f norm = (p1 - p0).cross(p2 - p1).normalized();
        
        for (int j = 0; j < mesh_.polygons.at(i).vertices.size(); j++)
        {
          uint32_t idx = mesh_.polygons.at(i).vertices.at(j);
          
          if (input_has_normals)
          {
            Eigen::Vector3f cloudnorm(cloud.at(idx).normal_x,
                                      cloud.at(idx).normal_y,
                                      cloud.at(idx).normal_z);
            if (cloudnorm.dot(norm) < std::cos(normalDisagrementThresholdInDegrees/180.*M_PI))
              disagree.at(idx) = 1;
          }
          else
          {
            if (cloud.at(idx).normal_x == 0 &&
                cloud.at(idx).normal_y == 0 &&
                cloud.at(idx).normal_z == 0)
            {
              cloud.at(idx).normal_x = norm(0);
              cloud.at(idx).normal_y = norm(1);
              cloud.at(idx).normal_z = norm(2);
              numFilled++;
            }
            else
            {
              Eigen::Vector3f cloudnorm(cloud.at(idx).normal_x,
                                        cloud.at(idx).normal_y,
                                        cloud.at(idx).normal_z);
              if (cloudnorm.dot(norm) < std::cos(normalDisagrementThresholdInDegrees/180.*M_PI))
                disagree.at(idx) = 1;
            }
          }
        }
        i++;
      }
      if (!input_has_normals && numFilled != cloud.size())
      {
        int pc = (cloud.size()-numFilled)*100./cloud.size();
        if (pc > 0)
        {
          NUKLEI_WARN("Input does not contain normals. Normals could not be "
                      "computed for " << cloud.size()-numFilled << " " <<
                      "out of " << cloud.size() << " vertices (" <<
                      pc << "%).");
        }
      }
      int numDisagree = std::accumulate(disagree.begin(), disagree.end(), 0);
      if (numDisagree > 0)
      {
        if (input_has_normals)
        {
          int pc = (numDisagree)*100./cloud.size();
          if (pc > 0)
          {
            NUKLEI_WARN("Input contains vertex normals. The normals of " <<
                        numDisagree << " vertices out of " << cloud.size() <<
                        " (" << pc << "%) are more than " <<
                        normalDisagrementThresholdInDegrees <<
                        "° far from triangle-computed normals. "
                        "Setting those normals to 0,0,0. These "
                        "points will be ignored by lemg.");
          }
        }
        else
        {
          int pc = (numDisagree)*100./cloud.size();
          if (pc > 0)
          {
            NUKLEI_WARN("Input does not contain normals. The normals of " <<
                        numDisagree << " vertices out of " << cloud.size() << " ("
                        << pc << "%) differs by more than " <<
                        normalDisagrementThresholdInDegrees <<
                        "° depending on the triangle that is "
                        "used to compute it. Setting those normals to 0,0,0. These "
                        "points will be ignored by lemg.");
          }
        }
        for (int point_index = 0; point_index < cloud.size(); ++point_index)
        {
          if (disagree.at(point_index))
          {
            cloud.at(point_index).normal_x = 0;
            cloud.at(point_index).normal_y = 0;
            cloud.at(point_index).normal_z = 0;
          }
        }
        
      }
      
#ifdef LEMG_OUTPUT_PLY_SHOWING_WHERE_NORMALS_DISAGREE
      pcl::PointCloud<pcl::PointXYZRGBNormal> colorcloud = get<pcl::PointXYZRGBNormal>();
      
      for (int point_index = 0;  < cloud.size(); ++)
      {
        if (disagree.at())
          colorcloud.at().r = 255;
      }
      
      ShapeIO dd = *this;
      
      dd.set(colorcloud);
      dd.write("/tmp/a.ply");
#endif
    }
    
    return cloud;
  }
  
  template pcl::PointCloud<pcl::PointNormal> ShapeIO::getWithPolygonNormals<pcl::PointNormal>() const;
  
}
