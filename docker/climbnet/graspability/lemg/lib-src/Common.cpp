
#include <nuklei/Common.h>
#include <nuklei/Color.h>
#include <lemg/Common.hpp>
#include <boost/algorithm/string/predicate.hpp>

//#define LEMG_WRITE_DEBUG_DATA

namespace lemg
{
  using namespace nuklei;
  
  void loadCloud(const std::string &filename, pcl::PCLPointCloud2 &cloud)
  {
    NUKLEI_ASSERT(pcl::io::loadPCDFile(filename, cloud) >= 0);
  }
  
  const std::string INFOSTRING =
  std::string("LEMG built on " __DATE__ " at " __TIME__) + "\n"
  "Git branch: " + LEMG_XSTR(LEMG_GIT_BRANCH) + "\n"
  "Git commit hash: " + LEMG_XSTR(LEMG_GIT_COMMIT_HASH) + "\n"
  "Nuklei build info:\n" + nuklei::INFOSTRING;
}
