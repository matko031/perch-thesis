#include <lemg/PruningFilter.hpp>
#include <lemg/Common.hpp>
#include <kdtree++/kdtree.hpp>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/search/kdtree.h>
#include <set>

namespace lemg
{
  
  struct comparator
  {
    bool operator()(const pcl::PointXYZ& p1, const pcl::PointXYZ& p2) const
    {
      if ((p1.z < p2.z)) return true;
      if ((p1.z == p2.z) && (p1.y < p2.y)) return true;
      if ((p1.z == p2.z) && (p1.y == p2.y) && (p1.x < p2.x)) return true;
      return false;
    }
  };
  
  struct tree_point
  {
    typedef double value_type;
    
    tree_point() : idx_(-1) { vec_[0]= vec_[1] = vec_[2] = 0; }
    tree_point (value_type x, value_type y, value_type z) : idx_(-1)
    { vec_[0]=x; vec_[1]=y; vec_[2]=z;  }
    tree_point (value_type x, value_type y, value_type z, int idx) : idx_(idx)
    { vec_[0]=x; vec_[1]=y; vec_[2]=z;  }
    
    inline value_type operator[](const size_t i) const { return vec_[i]; }
    
    inline value_type x() const { return vec_[ 0 ]; }
    inline value_type y() const { return vec_[ 1 ]; }
    inline value_type z() const { return vec_[ 2 ]; }
    
    inline value_type& x() { return vec_[ 0 ]; }
    inline value_type& y() { return vec_[ 1 ]; }
    inline value_type& z() { return vec_[ 2 ]; }
    
    inline const value_type* vec() const { return vec_; }
    inline value_type* vec() { return vec_; }
    
    inline bool operator==(const tree_point& p) const
    {
      return (x() == p.x()) && (y() == p.y()) && (z() == p.z()) && (idx() == p.idx());
    }
    
    inline bool operator!=(const tree_point& p) const
    { return ! (*this == p); }
    
    inline int idx() const { return idx_; }
    inline int& idx() { return idx_; }
    
    struct accessor
    {
      typedef value_type result_type;
      
      inline result_type
      operator()(const tree_point& p, const size_t i) const throw ()
      { return p[i]; }
    };
    
    struct distance
    {
      value_type transformed_distance(const tree_point& p1,
                                      const tree_point& p2) const
      {
        value_type distx= p1.x()-p2.x();
        value_type disty= p1.y()-p2.y();
        value_type distz= p1.z()-p2.z();
        return distx*distx+disty*disty+distz*distz;
      }
      
      value_type new_distance(value_type& dist, value_type old_off, value_type new_off,
                              int /* cutting_dimension */)  const
      {
        return dist + new_off*new_off - old_off*old_off;
      }
      
      value_type transformed_distance(value_type d) const { return d*d; }
      
      value_type inverse_of_transformed_distance(value_type d) { return std::sqrt(d); }
      
    };
    
  private:
    value_type vec_[3];
    int idx_;
  };
  
  template<class T>
  inline T const& as_const(T const& t)
  { return t; }
  
  // VoxelGrid: resolution parametrizes cell size.
  // Kd-tree: 0.5*resolution parametrizes exclusion radius.
  // sdt::set: resolution ignored.
  pcl::PointCloud<pcl::PointXYZ>::Ptr
  pruning_filter(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud, double resolution)
  {
    // remove duplicate points, lower resolution.
    pcl::PointCloud<pcl::PointXYZ>::Ptr filtered(new pcl::PointCloud<pcl::PointXYZ>());
    
#define LEMUR_REMOVE_DUPLICATES_LIBKDTREEPP
#if defined(LEMUR_REMOVE_DUPLICATES_PCLVOXGRID)
    pcl::VoxelGrid< pcl::PointXYZ > sor;
    sor.setInputCloud(cloud);
    sor.setLeafSize(resolution, resolution, resolution);
    sor.filter(*filtered);
#elif defined(LEMUR_REMOVE_DUPLICATES_STDSET)
    std::set<pcl::PointXYZ, comparator> filter_set;
    for (pcl::PointCloud<pcl::PointXYZ>::const_iterator i = cloud->begin();
         i != cloud->end(); ++i)
      filter_set.insert(*i);
    
    for (std::set<pcl::PointXYZ>::iterator i = filter_set.begin();
         i != filter_set.end(); ++i)
      filtered->push_back(*i);
#elif defined(LEMUR_REMOVE_DUPLICATES_LIBKDTREEPP)
    const double radius = resolution/2.;
    typedef KDTree::KDTree< 3, tree_point, tree_point::accessor > Tree;
    boost::shared_ptr<Tree> tree(new Tree);
    for (pcl::PointCloud<pcl::PointXYZ>::const_iterator i = cloud->begin();
         i != cloud->end(); ++i)
    {
      tree_point s(i->x, i->y, i->z,
                   std::distance(as_const(*cloud).begin(), i));
      std::vector<tree_point> in_range;
      as_const(*tree).find_within_range(s, radius,
                                        std::back_inserter(in_range));
      bool has_neighbor = false;
      for (std::vector<tree_point>::const_iterator n_i = in_range.begin();
           n_i != in_range.end(); n_i++)
      {
        double distance = tree_point::distance().transformed_distance(*n_i, s);
        if (distance < radius*radius)
        {
          has_neighbor = true;
          break;
        }
      }
      
      if (!has_neighbor)
      {
        filtered->push_back(*i);
        tree->insert(s);
        //tree->optimise();
      }
    }
    
#else
#error Filterning scheme undefined.
#endif
    
    return filtered;
  }
  
  pcl::PointCloud<pcl::PointXYZ>
  pruning_filter(const pcl::PointCloud<pcl::PointXYZ>& cloud, double resolution)
  {
    pcl::PointCloud<pcl::PointXYZ>::Ptr filtered
    (new pcl::PointCloud<pcl::PointXYZ>(cloud));
    return *pruning_filter(filtered, resolution);
  }
  
  // If resolution==0, only vertex duplicates are discarded.
  pcl::PolygonMesh
  pruning_filter(const pcl::PolygonMesh& mesh, double resolution)
  {
    typedef pcl::PointXYZ point_t;
    
    pcl::PCLPointCloud2::Ptr cloud(new pcl::PCLPointCloud2(mesh.cloud));
    pcl::PointCloud<point_t>::Ptr pc(new pcl::PointCloud<point_t>());
    pcl::fromPCLPointCloud2(*cloud, *pc);
    
    pcl::PCLPointCloud2::Ptr cloud_filtered(new pcl::PCLPointCloud2());
    
    std::vector<int> filter_map;
    
    {
      pcl::PointCloud<point_t> tmp;
      pcl::IndicesPtr indices_in(new std::vector<int>);
      
#ifdef LEMG_OBSOLETE_FIND_WITHIN_RANGE_SOLUTION
      double radius = 1e-6;
      if (resolution > 0) radius = resolution/2.;
#endif
      typedef KDTree::KDTree< 3, tree_point, tree_point::accessor > Tree;
      boost::shared_ptr<Tree> tree(new Tree);
      for (pcl::PointCloud<pcl::PointXYZ>::const_iterator i = pc->begin();
           i != pc->end(); ++i)
      {
        size_t index = std::distance(as_const(*pc).begin(), i);
        tree_point s(i->x, i->y, i->z, index);
        int found = -1;
#ifdef LEMG_OBSOLETE_FIND_WITHIN_RANGE_SOLUTION
        std::vector<tree_point> in_range;
        as_const(*tree).find_within_range(s, radius,
                                          std::back_inserter(in_range));
        for (std::vector<tree_point>::const_iterator n_i = in_range.begin();
             n_i != in_range.end(); n_i++)
        {
          const auto& n = *n_i;
          if (resolution > 0)
          {
            auto tpd = tree_point::distance();
            double distance = tpd.transformed_distance(n, s);
            if (distance < resolution*resolution)
            {
              found = n.idx();
              break;
            }
          }
          else
          {
            if (n.x() == s.x() && n.y() == s.y() && n.z() == s.z())
            {
              found = n.idx();
              break;
            }
          }
        }
#else
        std::pair<Tree::const_iterator, Tree::distance_type>
        nearest = as_const(*tree).find_nearest(s);
        if (nearest.first != as_const(*tree).end())
        {
          const auto& n = *nearest.first;
          if (resolution > 0)
          {
            auto tpd = tree_point::distance();
            double distance = tpd.transformed_distance(n, s);
            if (distance < resolution*resolution)
              found = n.idx();
          }
          else
          {
            if (n.x() == s.x() && n.y() == s.y() && n.z() == s.z())
              found = n.idx();
          }
        }
#endif
        if (found == -1)
        {
          tree->insert(s);
          filter_map.push_back(indices_in->size());
          indices_in->push_back(index);
        }
        else
        {
          filter_map.push_back(filter_map.at(found));
        }
      }
      
      pcl::ExtractIndices<pcl::PCLPointCloud2> filter;
      filter.setInputCloud(cloud);
      filter.setIndices(indices_in);
      filter.filter(*cloud_filtered);
    }
    
    pcl::PolygonMesh mesh_filtered;
    mesh_filtered.cloud = *cloud_filtered;
    for (const auto& triangle : mesh.polygons)
    {
      pcl::Vertices triangle_filtered;
      auto& fv = triangle_filtered.vertices;
      for (const auto& vertex : triangle.vertices)
      {
        fv.push_back(filter_map.at(vertex));
      }
      
      if (fv.size() != 3)
        throw std::runtime_error("lazily restricting to triangles");
      
      if (fv.at(0) == fv.at(1) ||
          fv.at(0) == fv.at(2) ||
          fv.at(1) == fv.at(2))
        continue;
      
      mesh_filtered.polygons.push_back(triangle_filtered);
    }
    return mesh_filtered;
  }
  

}
