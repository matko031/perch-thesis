// (C) Copyright Renaud Detry   2007-2015.
// Distributed under the GNU General Public License and under the
// BSD 3-Clause License (See accompanying file LICENSE.txt).

/** @file */

#ifndef LEMG_UTIL_H
#define LEMG_UTIL_H

int compute_normals(int argc, char ** argv);
int don(int argc, char ** argv);
//int concatenate_fields(int argc, char ** argv);
int paint(int argc, char ** argv);
int bow_test(int argc, char ** argv);
int bow_train_codebook(int argc, char ** argv);
int bow_train_classifier(int argc, char ** argv);
int labels_from_color(int argc, char ** argv);
int bow_evaluate_classifier(int argc, char ** argv);
int bow_compare(int argc, char ** argv);
int bow_filter(int argc, char ** argv);
int bow_view_sim(int argc, char ** argv);
int bow_compute_descriptors(int argc, char ** argv);
int mesh_copy_color(int argc, char ** argv);
int mesh_make_color_channels_nonzero(int argc, char ** argv);
int view(int argc, char ** argv);
int hole_filler(int argc, char ** argv);
int simplify_mesh(int argc, char ** argv);

#endif

