// (C) Copyright Renaud Detry   2007-2015.
// Distributed under the GNU General Public License and under the
// BSD 3-Clause License (See accompanying file LICENSE.txt).

/** @file */

#include <string>
#include <pcl/search/organized.h>
#include <pcl/search/kdtree.h>
#include <pcl/io/ply_io.h>
#include <pcl/io/pcd_io.h>
#include <pcl/conversions.h>
#include <pcl/point_types.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <boost/thread/thread.hpp>
#include <boost/algorithm/string/replace.hpp>
#include <tclap/CmdLine.h>
#include <nuklei/KernelCollection.h>
#include <nuklei/Stopwatch.h>
#include <lemg/BoWModel.hpp>
#include <lemg/ShapeIO.hpp>
#include <lemg/Labeling.hpp>

using namespace nuklei;
using namespace lemg;


int paint(int argc, char ** argv)
{
  NUKLEI_TRACE_BEGIN();
  
  /* Parse command line arguments */
  
  TCLAP::CmdLine cmd(lemg::INFOSTRING);
  
  /* Custom arguments */
  
  TCLAP::UnlabeledValueArg<std::string> inFileArg
  ("input",
   "Input.",
   true, "", "filename", cmd);
  
  TCLAP::UnlabeledValueArg<std::string> outFileArg
  ("output",
   "Output.",
   true, "", "filename", cmd);
  
  TCLAP::ValueArg<std::string> labelsArg
  ("l", "labels",
   "Labels.",
   false, "", "filename", cmd);
  
  TCLAP::ValueArg<std::string> outputImageFileArg
  ("p", "output_png",
   "File to which a painted PNG file will be written.",
   false, "", "string", cmd);
  
  TCLAP::ValueArg<double> highpassThresholdArg
  ("", "highpass_threshold",
   "Set all values below this threshold to 0.",
   false, 100./1000, "double", cmd);
  
  TCLAP::ValueArg<double> thresholdArg
  ("t", "threshold",
   "Binary output, based on the provided threshold.",
   false, -1, "double", cmd);
  
  TCLAP::ValueArg<double> originalColorWeightArg
  ("", "original",
   "Weight of original color.",
   false, 0, "double", cmd);
  
  cmd.parse( argc, argv );
  
  std::string inputFile = inFileArg.getValue();
  
  ShapeIO shio1;
  shio1.read(inputFile);
  
  pcl::PointCloud<pcl::PointXYZRGB>::Ptr colorCloud(new pcl::PointCloud<pcl::PointXYZRGB>());
  *colorCloud = shio1.get<pcl::PointXYZRGB>();
  
  pcl::PointCloud<pcl::PointXYZINormal>::Ptr intensityCloud(new pcl::PointCloud<pcl::PointXYZINormal>());
  *intensityCloud = shio1.get<pcl::PointXYZINormal>();
  
  std::vector<double> labels;
  if (!labelsArg.getValue().empty())
  {
    readLabels(labelsArg.getValue(), labels);
  }
  else
  {
    for (auto& i : *intensityCloud)
      labels.push_back(i.intensity);
  }
  NUKLEI_ASSERT(labels.size() == colorCloud->size());
  
  paint(*colorCloud,
        labels,
        originalColorWeightArg.getValue(),
        thresholdArg.getValue(),
        false, true, false);
  
  if (!outFileArg.getValue().empty())
  {
    shio1.set(*colorCloud);
    shio1.write(outFileArg.getValue());
  }
  
  if (!outputImageFileArg.getValue().empty())
  {
    NUKLEI_ASSERT(false);
    if (colorCloud->height == 1)
      NUKLEI_THROW("Unorganized point cloud; cannot write image.");
    //io::PointCloudImageExtractorFromCurvatureField<Normal> pcie_normal;
    //pcie_normal.setScalingMethod(pcie_normal.SCALING_FULL_RANGE);
    //pcl::io::PointCloudImageExtractorFromRGBField<pcl::PointXYZRGB> pcie_rgb;
    //pcl::PCLImage image;
    //pcie_rgb.extract(*colorCloud, image);
    //pcl::io::savePNGFile(outputImageFileArg.getValue(), image);
  }
  
  return 0;
  
  NUKLEI_TRACE_END();
}





BoWModel* mp = NULL;
BoWModel* fp = NULL;
int v1(0);
int v1b(0);
int v2(0);
pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_foot(new pcl::PointCloud<pcl::PointXYZRGB>());
pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_foot_xyz(new pcl::PointCloud<pcl::PointXYZ>());
pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_wall(new pcl::PointCloud<pcl::PointXYZRGB>());
pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_model(new pcl::PointCloud<pcl::PointXYZRGB>());
pcl::search::KdTree<pcl::PointXYZ> search_foot;
pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGB> foot_rgb(cloud_foot);
pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGB> wall_rgb(cloud_wall);
pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGB> model_rgb(cloud_model);

void pp_callback(const pcl::visualization::PointPickingEvent& event, void* viewer_void)
{
  NUKLEI_TRACE_BEGIN();
  if(event.getPointIndex()!=-1)
  {
    pcl::visualization::PCLVisualizer& viewer = *(pcl::visualization::PCLVisualizer*)viewer_void;
    
    int id = event.getPointIndex();
    
    if (id == -1)
      return;
    
    
    
    // Return the correct index in the cloud instead of the index on the screen
    std::vector<int> indices (1);
    std::vector<float> distances (1);
    
    // Because VTK/OpenGL stores data without NaN, we lose the 1-1 correspondence, so we must search for the real point
    pcl::PointXYZ picked_pt;
    event.getPoint (picked_pt.x, picked_pt.y, picked_pt.z);
    search_foot.setInputCloud (cloud_foot_xyz);
    search_foot.nearestKSearch (picked_pt, 1, indices, distances);
    id = indices.front();
    
    std::cout << "Selected " << id << " instead of " << event.getPointIndex() << std::endl;
    
    //    mp->setRegionOfInterest(Eigen::Vector3d(cloud_foot->at(id).x,
    //                                            cloud_foot->at(id).y,
    //                                            cloud_foot->at(id).z), .5);
    try {
      const Disc& dis = fp->getDisc(picked_pt.x, picked_pt.y, picked_pt.z);
      id = dis.getPointId();
      std::cout << "have disc " << id << std::endl;
      std::vector<double> s = mp->computeSimilarities(dis.getHistogramC());
      std::cout << "have histograms " << id << std::endl;
      paint(*cloud_model,
            s,
            0, 0, true, false);
      
      std::cout << "have paint " << id << std::endl;
      
      std::cout << cloud_foot->at(id).x << " " <<  cloud_foot->at(id).y<< " " << cloud_foot->at(id).z << std::endl;
      
      
      Eigen::Vector3d n = fp->getDisc(id).getNormal()/33;
      Eigen::Vector3d cloud_vector(cloud_foot->at(id).x, cloud_foot->at(id).y, cloud_foot->at(id).z);
      Eigen::Vector3d cloud_vector2 = cloud_vector+n;
      NUKLEI_ASSERT((fp->getDisc(id).getPoint()-cloud_vector).norm() < 1e-6);
      pcl::ModelCoefficients cylinder_coeff;
      cylinder_coeff.values.resize (7);    // We need 7 values
      cylinder_coeff.values[0] = cloud_foot->at(id).x;
      cylinder_coeff.values[1] = cloud_foot->at(id).y;
      cylinder_coeff.values[2] = cloud_foot->at(id).z;
      cylinder_coeff.values[3] = n.x ();
      cylinder_coeff.values[4] = n.y ();
      cylinder_coeff.values[5] = n.z ();
      cylinder_coeff.values[6] = RADIUS;
      viewer.removeShape("cylinder");
      viewer.addCylinder (cylinder_coeff, "cylinder", v1);
      
      viewer.removeShape("arrow");
      viewer.addLine(pcl::PointXYZ(cloud_vector.x(),
                                   cloud_vector.y(),
                                   cloud_vector.z()),
                     pcl::PointXYZ(cloud_vector2.x(),
                                   cloud_vector2.y(),
                                   cloud_vector2.z()),
                     cloud_foot->at(id).r/255.,
                     cloud_foot->at(id).g/255.,
                     cloud_foot->at(id).b/255.,
                     "arrow", v1);
      
      viewer.updatePointCloud(cloud_model, model_rgb, "model_rgb");
    } catch (nuklei::Error& e)
    {
      std::cout << e.what() << std::endl;
    }
  }
  NUKLEI_TRACE_END();
}

void pp_callback2(const pcl::visualization::PointPickingEvent& event, void* viewer_void)
{
  NUKLEI_TRACE_BEGIN();
  if(event.getPointIndex()!=-1)
  {
    pcl::visualization::PCLVisualizer& viewer = *(pcl::visualization::PCLVisualizer*)viewer_void;
    
    // Return the correct index in the cloud instead of the index on the screen
    std::vector<int> indices (1);
    std::vector<float> distances (1);
    
    // Because VTK/OpenGL stores data without NaN, we lose the 1-1 correspondence, so we must search for the real point
    pcl::PointXYZ picked_pt;
    event.getPoint (picked_pt.x, picked_pt.y, picked_pt.z);
    search_foot.setInputCloud (cloud_foot_xyz);
    search_foot.nearestKSearch (picked_pt, 1, indices, distances);
    int id = indices.front();
    
    std::cout << "Selected " << id << " instead of " << event.getPointIndex() << std::endl;
    
    //    mp->setRegionOfInterest(Eigen::Vector3d(cloud_foot->at(id).x,
    //                                            cloud_foot->at(id).y,
    //                                            cloud_foot->at(id).z), .5);
    try {
      const Disc& dis = fp->getDisc(picked_pt.x, picked_pt.y, picked_pt.z);
      std::cout << id << " == " << dis.getPointId() << std::endl;
      std::cout << "have disc " << id << std::endl;
      
      //Disc dd = mp->getDisc(id);
      std::vector<std::pair<int, double>> in = mp->getKLR().influentialTrainingData(dis.getHistogramC());
      std::cout << dis.getHistogramC() << std::endl;
      std::vector<double> s(cloud_model->size(), 0);
      for (unsigned k = 0; k < in.size(); ++k)
        s.at(in.at(k).first) = in.at(k).second;
      
      paint(*cloud_model,
            s,
            0, 0, true, false);
      viewer.updatePointCloud(cloud_model, model_rgb, "model_rgb");

      
      {
        Eigen::Vector3d n = mp->getDisc(id).getNormal();
        Eigen::Vector3d cloud_vector(cloud_foot->at(id).x, cloud_foot->at(id).y, cloud_foot->at(id).z);
        Eigen::Vector3d cloud_vector2 = cloud_vector+n/10;
        NUKLEI_ASSERT((mp->getDisc(id).getPoint()-cloud_vector).norm() < 1e-6);
        pcl::ModelCoefficients cylinder_coeff;
        cylinder_coeff.values.resize (7);    // We need 7 values
        cylinder_coeff.values[0] = cloud_foot->at(id).x;
        cylinder_coeff.values[1] = cloud_foot->at(id).y;
        cylinder_coeff.values[2] = cloud_foot->at(id).z;
        cylinder_coeff.values[3] = n.x ()/33;
        cylinder_coeff.values[4] = n.y ()/33;
        cylinder_coeff.values[5] = n.z ()/33;
        cylinder_coeff.values[6] = RADIUS;
        viewer.removeShape("cylinder");
        viewer.addCylinder (cylinder_coeff, "cylinder", v1);
        viewer.removeShape("arrow");
        viewer.addLine(pcl::PointXYZ(cloud_vector.x(),
                                     cloud_vector.y(),
                                     cloud_vector.z()),
                       pcl::PointXYZ(cloud_vector2.x(),
                                     cloud_vector2.y(),
                                     cloud_vector2.z()),
                       cloud_foot->at(id).r/255.,
                       cloud_foot->at(id).g/255.,
                       cloud_foot->at(id).b/255.,
                       "arrow", v1);
      }
      
      {
        int friend_id = in.front().first;
        std::cout << "Front " << friend_id << " " << in.front().second << std::endl;
        Eigen::Vector3d n = mp->getDisc(friend_id).getNormal();
        Eigen::Vector3d cloud_vector(cloud_foot->at(friend_id).x, cloud_foot->at(friend_id).y, cloud_foot->at(friend_id).z);
        Eigen::Vector3d cloud_vector2 = cloud_vector+n/10;
        NUKLEI_ASSERT((mp->getDisc(friend_id).getPoint()-cloud_vector).norm() < 1e-6);
        pcl::ModelCoefficients cylinder_coeff;
        cylinder_coeff.values.resize (7);    // We need 7 values
        cylinder_coeff.values[0] = cloud_foot->at(friend_id).x;
        cylinder_coeff.values[1] = cloud_foot->at(friend_id).y;
        cylinder_coeff.values[2] = cloud_foot->at(friend_id).z;
        cylinder_coeff.values[3] = n.x ()/33;
        cylinder_coeff.values[4] = n.y ()/33;
        cylinder_coeff.values[5] = n.z ()/33;
        cylinder_coeff.values[6] = RADIUS;
        viewer.removeShape("cylinder2");
        viewer.addCylinder(cylinder_coeff, "cylinder2", v2);
        viewer.removeShape("arrow2");
        viewer.addLine(pcl::PointXYZ(cloud_vector.x(),
                                     cloud_vector.y(),
                                     cloud_vector.z()),
                       pcl::PointXYZ(cloud_vector2.x(),
                                     cloud_vector2.y(),
                                     cloud_vector2.z()),
                       cloud_foot->at(friend_id).r/255.,
                       cloud_foot->at(friend_id).g/255.,
                       cloud_foot->at(friend_id).b/255.,
                       "arrow2", v2);
      }
      
    } catch (nuklei::Error& e)
    {
      std::cout << e.what() << std::endl;
    }
    
    
    viewer.updatePointCloud(cloud_model, model_rgb, "model_rgb");
  }
  NUKLEI_TRACE_END();
}



int bow_view_sim(int argc, char ** argv)
{
  NUKLEI_TRACE_BEGIN();
  
  /* Parse command line arguments */
  
  TCLAP::CmdLine cmd(lemg::INFOSTRING);
  
  /* Custom arguments */
  
  TCLAP::ValueArg<std::string> modelArg
  ("t", "training_model",
   "Model file.",
   false, "", "filename", cmd);
  
  TCLAP::ValueArg<std::string> modelPCArg
  ("u", "training_pc",
   "Model pointcloud file.",
   false, "", "filename", cmd);
  
  TCLAP::ValueArg<std::string> footArg
  ("f", "foot_model",
   "Model file.",
   false, "", "filename", cmd);
  
  TCLAP::ValueArg<std::string> footPCArg
  ("g", "foot_pc",
   "Model pointcloud file.",
   false, "", "filename", cmd);
  
  TCLAP::ValueArg<std::string> wallPCArg
  ("w", "wall_pc",
   "Model pointcloud file.",
   false, "", "filename", cmd);
  
  TCLAP::SwitchArg distArg
  ("d", "distance",
   "Show distance to model points instead of nearest point in training set.", cmd);
  
  TCLAP::SwitchArg identityArg
  ("i", "identity",
   "Tie pose of left and right renderers.", cmd);

  cmd.parse( argc, argv );
  
  BoWModel m, f;
  mp = &m;
  fp = &f;
  if (!modelArg.getValue().empty())
    m.readModel(modelArg.getValue());
  m.setPointCloud(modelPCArg.getValue());
  
  if (!footArg.getValue().empty())
    f.readModel(footArg.getValue());
  f.setPointCloud(footPCArg.getValue());
  
  
  pcl::PCLPointCloud2::Ptr blob_wall(new pcl::PCLPointCloud2), blob_model(new pcl::PCLPointCloud2), blob_foot(new pcl::PCLPointCloud2);
  loadCloud(modelPCArg.getValue(), *blob_model);
  loadCloud(footPCArg.getValue(), *blob_foot);
  if (wallPCArg.getValue() != "")
    loadCloud(wallPCArg.getValue(), *blob_wall);
  
  fromPCLPointCloud2(*blob_wall, *cloud_wall);
  fromPCLPointCloud2(*blob_model, *cloud_model);
  fromPCLPointCloud2(*blob_foot, *cloud_foot);
  fromPCLPointCloud2(*blob_foot, *cloud_foot_xyz);
  for (auto i = cloud_model->begin(); i != cloud_model->end(); ++i)
  {
    i->r = i->g = i->b = 0;
  }
  for (auto i = cloud_wall->begin(); i != cloud_wall->end(); ++i)
  {
    i->r = i->g = i->b = 100;
  }
  
  pcl::PointCloud<pcl::PointXYZINormal>::Ptr intensityCloud(new pcl::PointCloud<pcl::PointXYZINormal>());
  fromPCLPointCloud2(*blob_foot, *intensityCloud);
  
  //std::vector<double> cp = f.testClassifier();
  //  std::vector<double> cp;
  //  for (auto& i : *intensityCloud)
  //    cp.push_back(i.intensity);
  //
  //  paint(*cloud_foot,
  //        cp,
  //        0, 0, false, true);
  
  foot_rgb = pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGB>(cloud_foot);
  
  boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer (new pcl::visualization::PCLVisualizer ("3D Viewer"));
  
  viewer->initCameraParameters ();
  
  viewer->createViewPort(0.0, 0.0, /*0.33*/ .5, 1.0, v1);
  viewer->setBackgroundColor (1, 1, 1, v1);
  viewer->addText("Graspability labels. Shift-click to select a grasp.", 10, 10, 0, 0, 0, "v1 text", v1);
  viewer->addPointCloud<pcl::PointXYZRGB> (cloud_foot, foot_rgb, "foot_rgb", v1);
  viewer->addPointCloud<pcl::PointXYZRGB> (cloud_wall, wall_rgb, "wall_rgb", v1);
  
//  viewer->createViewPort(0.33, 0.0, 0.66, 1.0, v1b);
//  viewer->setBackgroundColor (1, 1, 1, v1b);
//  viewer->addText("Graspability labels. Shift-click to select a grasp.", 10, 10, 0, 0, 0, "v1b text", v1b);
  
  viewer->createViewPort(/*0.66*/.5, 0.0, 1.0, 1.0, v2);
  viewer->setBackgroundColor (1, 1, 1, v2);
  viewer->addText("Color shows similarity to selected point.", 10, 10, 0, 0, 0, "v2 text", v2);
  viewer->addPointCloud<pcl::PointXYZRGB> (cloud_model, model_rgb, "model_rgb", v2);
  if (!identityArg.getValue())
    viewer->createViewPortCamera(v2);
  
  viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 3, "foot_rgb");
  viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 3, "wall_rgb");
  viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 3, "model_rgb");
  //viewer->addCoordinateSystem (1.0);
  
  if (distArg.getValue())
    viewer->registerPointPickingCallback(pp_callback, (void*)&*viewer);
  else
    viewer->registerPointPickingCallback(pp_callback2, (void*)&*viewer);
  
  viewer->setRepresentationToWireframeForAllActors();
  
  viewer->spin();
  //  while (!viewer->wasStopped ())
  //  {
  //    viewer->spinOnce (100);
  //    boost::this_thread::sleep (boost::posix_time::microseconds (100000));
  //  }
  
  return 0;
  
  NUKLEI_TRACE_END();
}




int view(int argc, char ** argv)
{
  /* Parse command line arguments */
  
  TCLAP::CmdLine cmd("");
  
  /* Custom arguments */
  
  TCLAP::UnlabeledValueArg<std::string> p1Arg
  ("input1",
   "Pointcloud 1.",
   true, "", "filename", cmd);
  
  TCLAP::UnlabeledValueArg<std::string> p2Arg
  ("input2",
   "Pointcloud 2.",
   true, "", "filename", cmd);
  
  cmd.parse( argc, argv );
  
  ShapeIO shio1, shio2;
  shio1.read(p1Arg.getValue());
  shio2.read(p2Arg.getValue());
  
  boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer(new pcl::visualization::PCLVisualizer ("3D Viewer"));
  viewer->initCameraParameters();
  
  int v1(0);
  viewer->createViewPort(0.0, 0.0, 0.5, 1.0, v1);
  viewer->setBackgroundColor (1, 1, 1, v1);
  viewer->addText("1", 10, 10, 0, 0, 0, "v1 text", v1);
  if (shio1.getFileFormat() == "ply")
  {
    viewer->addPolygonMesh(shio1.mesh(), "mesh1", v1);
  }
  else
  {
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud1(new pcl::PointCloud<pcl::PointXYZRGB>());
    *cloud1 = shio1.get<pcl::PointXYZRGB>();
    pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGB> rgb1(cloud1);
    viewer->addPointCloud<pcl::PointXYZRGB>(cloud1, rgb1, "sample cloud1", v1);
    viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 3, "sample cloud1");
  }
  
  int v2(1);
  viewer->createViewPort(0.5, 0.0, 1.0, 1.0, v2);
  viewer->setBackgroundColor (1, 1, 1, v2);
  viewer->addText("2", 10, 10, 0, 0, 0, "v2 text", v2);
  if (shio2.getFileFormat() == "ply")
  {
    viewer->addPolygonMesh(shio2.mesh(), "mesh2", v2);
  }
  else
  {
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud2(new pcl::PointCloud<pcl::PointXYZRGB>());
    *cloud2 = shio2.get<pcl::PointXYZRGB>();
    pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGB> rgb2(cloud2);
    viewer->addPointCloud<pcl::PointXYZRGB>(cloud2, rgb2, "sample cloud2", v2);
    viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 3, "sample cloud2");
  }
  
  
  viewer->spin();
  
  return 0;
}


