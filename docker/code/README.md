### JPL_DATA structure

`lemg bdesc`takes in annotated meshes and produces labeled images of the grasppoint

```bash

jpl_data/
	work-data/
		perch/
			2020-09-10--pisgah/
           		proc.sh # iphone/site[1|4] - iphone-annot/01.mk-desc.sh on (generate labeled images), 
           				# rs/orig data - ppu-process-all-raw-grasps.sh (create annotated meshes), rs/01-create-desc.sh (generate labeled images), 
           				# 05-train.sh in iphone-annot
            	tar.sh # archive proc.sh, iphone/*.ply, iphone-annot/*.sh, rs/*.sh, rs/orig
            	iphone/
					site[1|2|3|4]/ 	# iphone data from sites 1-4 + original meshes + annotated meshes in 1 and 4
									# site1 - ~15 good, ~15 bad; site4 - site4 - ~20 good, ~20 bad 
				iphone-annot/
					01-mk-desc.sh # generate labeled images from sfm1-2m-texture.ply in iphone/site1/annot, sfm1-[a|b]-1m-texture.ply in iphone/site4/annot
					05-train.sh # run climbnet-train on desc.nosync/all-lowres-s10000
					10-test.sh # run lemg paint on texture meshes in iphone/site[1|4]/
                    model-rs+ps.h5 # not sure, probably model trained on rs+ps data
					desc.nosync/ # labeled h5 files from iphone/site[1|4]
				rs/
					01-create-desc-1.sh # run lemg bdesc on input dir
					get_outputs.sh # collect all h5 outputs in desc.nosync in outputs dir
					desc.nosync/ # h5 output files with images and labels corresponding to the pointcloud data in orig (lemg bdesc)
					meshes.nosync # meshed correspondig to the pointcloud data in orig (ppu-process-all-raw-grasps)
					orig/ 	# original pointcloud data collected on the site
							# site1 - 8 grabs; sit2 - 8 grabs; site3 - 2 grabs; site4 - 5grabs
					outputs/ # same as desc.nosync, but without subdirectories
					get_outputs.sh
				rs-images/ # images from site1 and site4, one image per grab
				
			gitlab/
				perch-pointcloud-utils/
					# util scripts for preparing data
					# `ppu-process-all-raw-grasps.sh` creates annotated meshes from rs pointcloud data
			core # no idea what this is
	ztank/
		archive/
			perch/
				2019-10-29--tile-images_/
					images/ # images of empty tiles
					pcds/ # images and pcds of tiles with rocks on them
					sfm/
						tiles-mesh.jpg # some ugly jpg of the empty tiles mesh
						tiles-mesh.ply # mesh of empty tiles
						tiles.psx # game progress file?
						tiles.files/
							project.zip
								doc.xml # xml with some configs, but almost empty
							0/
								chunk.zip
									doc.xml # xml with some configs
								0/
									frame.zip # metadata of images in ../../../../images
									dense_cloud/
										dense_cloud.oc3 # not sure what this is
										dense_cloud.zip
											doc.xml # metadata about the oc3 file
									model/
										model.zip
											doc.xml # metadata of mesh.ply
											mesh.ply # tiles mesh
											texture.png # tiles texture
									point_cloud.1/
										point_cloud.zip # a bunch of .ply files, some of which cannot be opened + doc.xml with some metadata
									thumbnails/
										thumbnails.zip # thumbnails of the tiles images
										
								
```



```
Epoch 1/10
142/142 [==============================] - 3s 16ms/step - loss: 0.7940 - accuracy: 0.8309 - val_loss: 0.9519 - val_accuracy: 0.6547
Epoch 2/10
142/142 [==============================] - 2s 15ms/step - loss: 0.4093 - accuracy: 0.8709 - val_loss: 0.8874 - val_accuracy: 0.6438
Epoch 3/10
142/142 [==============================] - 2s 15ms/step - loss: 0.3835 - accuracy: 0.8772 - val_loss: 0.8728 - val_accuracy: 0.6498
Epoch 4/10
142/142 [==============================] - 2s 15ms/step - loss: 0.3711 - accuracy: 0.8784 - val_loss: 0.9020 - val_accuracy: 0.6540
Epoch 5/10
142/142 [==============================] - 2s 15ms/step - loss: 0.3620 - accuracy: 0.8819 - val_loss: 0.9400 - val_accuracy: 0.6577
Epoch 6/10
142/142 [==============================] - 2s 15ms/step - loss: 0.3535 - accuracy: 0.8847 - val_loss: 0.9348 - val_accuracy: 0.6676
Epoch 7/10
142/142 [==============================] - 2s 15ms/step - loss: 0.3525 - accuracy: 0.8823 - val_loss: 0.9594 - val_accuracy: 0.6478
Epoch 8/10
142/142 [==============================] - 2s 15ms/step - loss: 0.3492 - accuracy: 0.8839 - val_loss: 0.9708 - val_accuracy: 0.6447
Epoch 9/10
142/142 [==============================] - 2s 15ms/step - loss: 0.3393 - accuracy: 0.8884 - val_loss: 0.9275 - val_accuracy: 0.6608
Epoch 10/10
142/142 [==============================] - 2s 15ms/step - loss: 0.3311 - accuracy: 0.8915 - val_loss: 0.8805 - val_accuracy: 0.6697
Test loss (last/max): %f / %f (0.8804835081100464, 0.8804835081100464)
Test accuracy (last/max): %f / %f: (0.6696510314941406, 0.6696510314941406)
```
