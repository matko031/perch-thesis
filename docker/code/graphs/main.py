import copy
from matplotlib import font_manager, lines, pyplot as plt
import matplotlib
import logging
import numpy as np
from pathlib import Path
import pprint 
import climbnet
from climbnet import analyse
import pickle
import os
import math

pp = pprint.PrettyPrinter(indent=4)

plt.set_loglevel("info")
pil_logger = logging.getLogger('PIL')
pil_logger.setLevel(logging.INFO)

logging.getLogger('root').setLevel(logging.DEBUG)


data_dir = "/home/delimir/Documents/Faks/5/Thesis/condor/data"
default_valid_data_path = str(Path(data_dir, "res_1000-size_96", "jplrsSingle", "n1", "combined.h5"))

models_dir = "/home/delimir/Documents/Faks/5/Thesis/condor/models"
test_models_dir = "/home/delimir/Documents/Faks/5/Thesis/condor/test_models"
all_models_dir = "/home/delimir/Documents/Faks/5/Thesis/condor/all_models"
all_models_new_dir = "/home/delimir/Documents/Faks/5/Thesis/condor/all_models_new"

figures_dir = "/home/delimir/Documents/Faks/5/Thesis/latex/source/figures/results/"

global_seed=1
global_balance_classes=True




def print_table(tabl):
    column_width=20
    for row in tabl:             
        for el in row:         
            print(f"{el}\t\t", end="")
        print()

def model_name_to_plot_label(model_path, max_len=30):
    label = ""
    config = analyse.get_config_from_name(model_path)
    len_row = 0
    for k, v in config.items():
        new_len = len(str(k)) + len(str(v)) 
        if len_row + new_len > max_len:
            label += "\n"
            len_row = 0
        label += f"{str(k)}: {str(v)}, " 
        len_row += new_len
    return label[:-2]



def get_config(user_config=None):
    if user_config is None:
        user_config = {}

    default_config = {"i": "depth", "e": 50, "b": 128, "train": "expert280", "valid": "jplrsSingle1","res": 1000, "size": 96} 
    default_config.update(user_config)
    return default_config


def get_average_performance(model_path, valid_data_path, N):
    loss = 0
    acc = 0
    for seed in range(N):
        loss_i, acc_i = climbnet.evaluate(model_path, valid_data_path, balance_classes=True, seed=seed).values()
        loss += loss_i
        acc += acc_i

    loss = loss/N
    acc = acc/N

    return {"loss": loss, "accuracy": acc}


def get_most_wrong_predictions(correct, predicted, grasps):

    arg_max = np.argmax(correct, axis=1)
    err = 1 - predicted[ np.arange(predicted.shape[0]), arg_max ]
    err = sorted(list(enumerate(err)), key = lambda x: x[1] )

    final = []

    for i, w in err:
        final.append( (w, [list(predicted[i]), list(correct[i])], grasps[i].decode(), i) )

    return final


def get_most_uncertain_predictions(correct, predicted, grasps):

    p = list(enumerate( np.abs(predicted[:,0]-predicted[:,1]) ))
    p = sorted(p, key=lambda x: x[1])

    final = []

    for i, u in p:
        final.append( (u, [list(predicted[i]), list(correct[i])], grasps[i].decode(), i) )

    return final

def class_labels_to_int(labels):
        l = np.array(labels)
        return np.logical_and( l[:,0] == 0, l[:,1] == 1 ).astype(int)

def average_predictions_per_grasp(correct, predicted, grasps):
    p = np.round(predicted)

    results_per_grasp = {}

    res_correct = []
    res_predicted = []
    res_grasps = []

    for i, g in enumerate(grasps):
        if g not in results_per_grasp:
            results_per_grasp[g] = {"predictions": [], "correct": correct[i]} 
        results_per_grasp[g]["predictions"].append(p[i])

    for g, d in results_per_grasp.items():
        u, c = np.unique(d["predictions"], return_counts=True)
        prediction = u[np.argmax(c)]
        res_predicted.append(prediction)

        res_correct.append(d["correct"])
        res_grasps.append(g)



    return np.array(res_correct), np.array(res_predicted), np.array(res_grasps)


def get_predictions(model_path, data_path):
    res = climbnet.predict(model_path, data_path)
    correct = class_labels_to_int(res["correct"])
    predicted = res["predicted"][:,1]
    grasps = res["grasps"]

    return correct, predicted, grasps


def plot_predictions(correct, predicted):
    x = np.linspace(1, len(correct), len(correct))

    index_correct = predicted.round() == correct
    index_wrong = predicted.round() != correct

    x_correct = x[index_correct] 
    x_wrong = x[index_wrong] 

    y_correct = predicted[index_correct]
    y_wrong = predicted[index_wrong]

    plt.figure()
    plt.ylabel("Class predictions", fontsize=15)
    plt.xlabel("Validation samples", fontsize=15)
    plt.yticks(np.linspace(0,1,11))
    #plt.scatter(x, true, c="orange", s=30, alpha=0.5)
    plt.scatter(x_correct, y_correct, s=30, c="green", label="Correct")
    plt.scatter(x_wrong, y_wrong, s=30, c="red", label="Wrong")
    plt.legend(loc='upper left', fontsize=10)
    plt.grid(axis="both",which="both")
    plt.tight_layout()

def plot_training_acc_loss(model_path):
    fig = plt.figure()
    plt.subplot(1,2,1)
    analyse.plot_history(model_path, ['accuracy', 'val_accuracy']) 
    plt.ylim([0,1])
    plt.locator_params(nbins=11, axis='y')
    plt.legend(["Training accuracy", "Validation accuracy"])
    plt.ylabel("Accuracy")
    plt.xlabel("Epochs")

    plt.subplot(1,2,2)
    analyse.plot_history(model_path, ['loss', 'val_loss']) 
    plt.legend(["Training loss", "Validation loss"])
    plt.locator_params(nbins=11, axis='y')
    plt.ylabel("Loss")
    plt.xlabel("Epochs")
    plt.tight_layout()

    return fig


def plot_acc_loss(labels, accuracy, loss, label, horizontal=False, ticks_fontsize=10):

    fig = plt.figure()
    x_axis = np.arange(len(labels))
    
    bins = math.ceil(max(max(accuracy), max(loss))*10)
    if horizontal:
        plt.barh(y=x_axis, width=accuracy, height=0.4, label = 'Accuracy')
        plt.barh(y=x_axis + 0.4, width=loss, height=0.4, label = 'Loss')
        plt.yticks(x_axis, labels, fontsize=ticks_fontsize)
        plt.legend()
        plt.xlim([0, max(1.0, max(loss)) *1.01])
        #plt.locator_params(nbins=bins, axis='x')
        plt.locator_params(nbins=11, axis='x')
        plt.xlabel("Accuracy/Loss")
        plt.ylabel(label)
    else:
        plt.bar(x=x_axis, height=accuracy, width=0.4, label = 'Accuracy')
        plt.bar(x=x_axis+0.4, height=loss, width=0.4, label = 'Loss')
        plt.xticks(x_axis, labels, fontsize=ticks_fontsize)
        plt.legend()
        plt.ylim([0, max(1.0, max(loss)) *1.01])
        plt.locator_params(nbins=11, axis='y')
        plt.ylabel("Accuracy/Loss")
        plt.xlabel(label)

    plt.plot([0, len(labels)], [1,1], "k--", linewidth=1)
    plt.tight_layout()
    return fig




def scores_to_plot_data(scores):
    labels, loss, accuracy = [], [], []
    for name, score in scores.items():
        labels.append(name)
        loss.append(score["loss"])
        accuracy.append(score["accuracy"])

    return labels, loss, accuracy

    

def plot_graphs(graphs_to_plot):


    ##### Default model #####
    if "default_model" in graphs_to_plot:
        print("\n\n##### default_model #####\n\n")

        default_model = analyse.get_models(get_config(), models_dir, unique=True, required=True)
        plot_training_acc_loss(default_model)

        plt.savefig(figures_dir + "default_model_training.png", bbox_inches="tight")

        print("\n\n##### default_model - Done #####\n\n")



    ##### Epochs #####
    if "epochs" in graphs_to_plot:
        print("\n\n##### epochs - Done #####\n\n")

        models = {}
        models["expert 10\nepochs 10"] = analyse.get_models(get_config({"train": "expert280", "valid": "jplrsSingle1", "e": 10}), models_dir, unique=True, required=True)
        models["expert 10\nepochs 50"] = analyse.get_models(get_config({"train": "expert280", "valid": "jplrsSingle1", "e": 50}), models_dir, unique=True, required=True)
        models["expert 10\nepochs 100"] = analyse.get_models(get_config({"train": "expert280", "valid": "jplrsSingle1", "e": 100}), models_dir, unique=True, required=True)

        scores = {}
        for name, path in models.items():            
            e = name[-2:] if name[-2] != "0" else name[-3:]
            scores[name] = get_average_performance(path, default_valid_data_path, 5)
            plot_predictions(path, default_valid_data_path)
            plt.plot([0,40], [0.4, 0.4], "k--", linewidth=1)
            plt.plot([0,40], [0.6, 0.6], "k--", linewidth=1)
            plt.savefig(figures_dir + f"epochs{e}_predictions.png", dpi=300, bbox_inches="tight")

        pp.pprint(scores)
        labels, loss, acc = scores_to_plot_data(scores)
        plot_acc_loss(labels, acc, loss, "training configuration", horizontal=False)

        plt.savefig(figures_dir + "epochs.png", dpi=300, bbox_inches="tight")

        print("\n\n##### epochs - Done #####\n\n")



    ##### Uncertainty #####
    if "uncertainty" in graphs_to_plot:
        print("\n\n##### Uncertainty #####\n\n")

        models = {}
        models["depth, expert 10, epochs 50"] = analyse.get_models(get_config({"train": "expert280", "valid": "jplrsSingle1", "e": 50}), models_dir, unique=True, required=True)
        models["color, expert 10, epochs 50"] = analyse.get_models(get_config({"i": "color", "train": "expert280", "valid": "jplrsSingle1", "e": 50}), models_dir, unique=True, required=True)
        models["grey, expert 10, epochs 50"] = analyse.get_models(get_config({"i": "grey", "train": "expert280", "valid": "jplrsSingle1", "e": 50}), models_dir, unique=True, required=True)
        models["depth-color-stack, expert 10, epochs 50"] = analyse.get_models(get_config({"i": "depth-color-stack", "train": "expert280", "valid": "jplrsSingle1", "e": 50}), models_dir, unique=True, required=True)

        for name, path in models.items():            
            print("Model:", name)
            uncertainties = get_most_uncertain_predictions(path, default_valid_data_path)
            print_table(uncertainties)

        print("\n\n##### Uncertainty - Done #####\n\n")


    ##### Wrong #####
    if "wrong" in graphs_to_plot:

        print("\n\n##### Wrong #####\n\n")

        models = {}
        models["depth, expert 10, epochs 50"] = analyse.get_models(get_config({"train": "expert280", "valid": "jplrsSingle1", "e": 50}), models_dir, unique=True, required=True)
        models["color, expert 10, epochs 50"] = analyse.get_models(get_config({"i": "color", "train": "expert280", "valid": "jplrsSingle1", "e": 50}), models_dir, unique=True, required=True)
        models["grey, expert 10, epochs 50"] = analyse.get_models(get_config({"i": "grey", "train": "expert280", "valid": "jplrsSingle1", "e": 50}), models_dir, unique=True, required=True)
        models["depth-color-stack, expert 10, epochs 50"] = analyse.get_models(get_config({"i": "depth-color-stack", "train": "expert280", "valid": "jplrsSingle1", "e": 50}), models_dir, unique=True, required=True)

        for name, path in models.items():            
            print("Model:", name)
            wrongs = get_most_wrong_predictions(path, default_valid_data_path)
            print_table(wrongs)

        print("\n\n##### Wrong - Done #####\n\n")


    ##### Domain shift #####
    if "domain_shift" in graphs_to_plot: 
        print("\n\n##### Domain shift #####\n\n")
        valid_data = str(Path(data_dir, "res_1000-size_96", "jplSingle", "n1", "combined.h5"))

        expert28 = analyse.get_models(get_config({"train": "expert28", "valid": "jplSingle1"}), models_dir, unique=True, required=True)
        expert280 = analyse.get_models(get_config({"train": "expert280", "valid": "jplSingle1"}), models_dir, unique=True, required=True)
        expert2800 = analyse.get_models(get_config({"train": "expert2800", "valid": "jplSingle1"}), models_dir, unique=True, required=True)

        rs1 = analyse.get_models(get_config({"train": "rs1", "valid": "jplSingle1"}), models_dir, unique=True, required=True)
        rs10 = analyse.get_models(get_config({"train": "rs10", "valid": "jplSingle1"}), models_dir, unique=True, required=True)
        rs100 = analyse.get_models(get_config({"train": "rs100", "valid": "jplSingle1"}), models_dir, unique=True, required=True)

        scores = {}
        scores["Expert 1"] = get_average_performance(expert28, valid_data, 5)
        scores["Expert 10"] = get_average_performance(expert280, valid_data, 5)
        scores["Expert 100"] = get_average_performance(expert2800, valid_data, 5)

        scores["Pisgah 1"] = get_average_performance(rs1, valid_data, 5)
        scores["Pisgah 10"] = get_average_performance(rs10, valid_data, 5)
        scores["Pisgah 100"] = get_average_performance(rs100, valid_data, 5)

        pp.pprint(scores)
        labels, loss, acc = scores_to_plot_data(scores)
        plot_acc_loss(labels, acc, loss, "Training data")

        plt.savefig(figures_dir + "domain_shift.png", dpi=300, bbox_inches="tight")

        print("\n\n##### Domain shift - Done #####\n\n")


    ##### Best models #####
    if "best_models" in graphs_to_plot:
        print("\n\n##### Best models #####\n\n")

        desc_configs = {}
        desc_configs["valid_data1000_96"] = str(Path(data_dir, "res_1000-size_96", "jplrsSingle", "n1", "combined.h5"))
        desc_configs["valid_data2000_96"] = str(Path(data_dir, "res_2000-size_96", "jplrsSingle", "n1", "combined.h5"))
        desc_configs["valid_data1000_200"] = str(Path(data_dir, "res_1000-size_200", "jplrsSingle", "n1", "combined.h5"))
        desc_configs["valid_data2000_200"] = str(Path(data_dir, "res_2000-size_200", "jplrsSingle", "n1", "combined.h5"))

        
        for name, valid_data_path in desc_configs.items():
            print(f"\n\n{name}\n\n")

            scores_filename = f"scores_{name}.pickle"

            if os.path.isfile(scores_filename):
                scores = pickle.load(open(scores_filename, "rb"))
            else:
                scores = {}

            for model_path in list(Path(all_models_dir).iterdir()):
                try:
                    if model_path.name not in scores and "train_expert" in model_path.name:
                        print(model_path.name)
                        scores[model_path.name] = get_average_performance(model_path, valid_data_path, 5)

                        with open(scores_filename, "wb") as f:
                            pickle.dump(scores, f)
                    else:
                        if model_path.name in scores:
                            pass
                            #print("Already processed", model_path.name)
                        else:
                            pass
                            #print("Not trained on expert data", model_path.name)

                except Exception as e:
                    pass
                    #print("\n\n############### EXCEPTION ###########\n\n")
                    #print(e)

            sorted_models = sorted(list(scores.items()), reverse=True, key = lambda item: item[1]["accuracy"])
            best_models = dict(sorted_models[:10] )
            pp.pprint(best_models)


            labels, loss, acc = scores_to_plot_data(best_models)
            labels = list(map(lambda x: x.replace("-valid_rs1", ""), labels ))
            labels = list(map(model_name_to_plot_label, labels))
            plot_acc_loss(labels, acc, loss, label="Model configuration", horizontal=True, ticks_fontsize=8)

            print("Best accuracy:", max(acc))
            print("Worst accuracy:", min(acc))
            print("Average accuracy:", sum(acc)/len(acc))

            plt.savefig(figures_dir + f"best_models_{name}.png", dpi=300, bbox_inches="tight")
            
        print("\n\n##### Best models - Done #####\n\n")

    ##### Inputs #####
    if "inputs" in graphs_to_plot:
        print("\n\n##### Inputs #####\n\n")

        models = {}

        models["depth"] = analyse.get_models(get_config({"i": "depth"}), models_dir, unique=True, required=True)
        models["color"] = analyse.get_models(get_config({"i": "color"}), models_dir, unique=True, required=True)
        models["grey"] = analyse.get_models(get_config({"i": "grey"}), models_dir, unique=True, required=True)

        models["depth\ngrey"] = analyse.get_models(get_config({"i": "depth-grey"}), models_dir, unique=True, required=True)
        models["depth\ncolor"] = analyse.get_models(get_config({"i": "depth-color"}), models_dir, unique=True, required=True)

        models["depth\ngrey\nstack"] = analyse.get_models(get_config({"i": "depth-grey-stack"}), models_dir, unique=True, required=True)
        models["depth\ncolor\nstack"] = analyse.get_models(get_config({"i": "depth-color-stack"}), models_dir, unique=True, required=True)


        scores = {}
        for name, path in models.items():            
            scores[name] = get_average_performance(path, default_valid_data_path, 5)

        pp.pprint(scores)
        labels, loss, acc = scores_to_plot_data(scores)
        plot_acc_loss(labels, acc, loss, label="Model inputs")

        plt.savefig(figures_dir + "inputs.png", bbox_inches="tight")

        print("\n\n##### Inputs - Done #####\n\n")


    ##### Training descriptors #####
    if "training_descriptors" in graphs_to_plot:
        print("\n\n##### multiple_descriptors_validation #####\n\n")
        models = {}

        models["1000-96"] = {
                "model_path": analyse.get_models(get_config({"res": 1000, "size": 96, "train": "expert280", "valid": "jplrsSingle1"}), models_dir, unique=True, required=True), 
                "data_path": str(Path(data_dir, "res_1000-size_96", "jplrsSingle", "n1", "combined.h5"))
        }

        models["1000-200"] = {
                "model_path": analyse.get_models(get_config({"res": 1000, "size": 200, "train": "expert280", "valid": "jplrsSingle1"}), models_dir, unique=True, required=True), 
                "data_path": str(Path(data_dir, "res_1000-size_200", "jplrsSingle", "n1", "combined.h5"))
        }

        models["2000-96"] = {
                "model_path": analyse.get_models(get_config({"res": 2000, "size": 96, "train": "expert280", "valid": "jplrsSingle1"}), models_dir, unique=True, required=True), 
                "data_path": str(Path(data_dir, "res_2000-size_96", "jplrsSingle", "n1", "combined.h5"))
        }

        models["2000-200"] = { "model_path": analyse.get_models(get_config({"res": 2000, "size": 200, "train": "expert280", "valid": "jplrsSingle1"}), models_dir, unique=True, required=True), 
                "data_path": str(Path(data_dir, "res_2000-size_200", "jplrsSingle", "n1", "combined.h5"))
        }


        scores = {}
        for name, d in models.items():            
            scores[name] = get_average_performance(d["model_path"], d["data_path"], 5)

        pp.pprint(scores)
        labels, loss, acc = scores_to_plot_data(scores)
        plot_acc_loss(labels, acc, loss, label="Model inputs")

        plt.savefig(figures_dir + "training_descriptors.png", bbox_inches="tight")


        print("\n\n##### multiple_descriptors_validation - Done #####\n\n")


    ##### Validation descriptors #####
    if "validation_descriptors" in graphs_to_plot:
        print("\n\n##### validation descriptors #####\n\n")
        models = {}

        models["single1"] = {
                "model_path": analyse.get_models(get_config({"train": "expert280", "valid": "jplrsSingle1"}), models_dir, unique=True, required=True), 
                "data_path": str(Path(data_dir, "res_1000-size_96", "jplrsSingle", "n1", "combined.h5"))
        }

        models["single10"] = {
                "model_path": analyse.get_models(get_config({"train": "expert280", "valid": "jplrsSingle10"}), models_dir, unique=True, required=True), 
                "data_path": str(Path(data_dir, "res_1000-size_96", "jplrsSingle", "n10", "combined.h5"))
        }

        models["single100"] = {
                "model_path": analyse.get_models(get_config({"train": "expert280", "valid": "jplrsSingle100"}), models_dir, unique=True, required=True), 
                "data_path": str(Path(data_dir, "res_1000-size_96", "jplrsSingle", "n100", "combined.h5"))
        }

        models["1"] = {
                "model_path": analyse.get_models(get_config({"train": "expert280", "valid": "jplrs1"}), models_dir, unique=True, required=True), 
                "data_path": str(Path(data_dir, "res_1000-size_96", "jplrs", "n1", "combined.h5"))
        }

        models["10"] = {
                "model_path": analyse.get_models(get_config({"train": "expert280", "valid": "jplrs10"}), models_dir, unique=True, required=True), 
                "data_path": str(Path(data_dir, "res_1000-size_96", "jplrs", "n10", "combined.h5"))
        }

        models["100"] = {
                "model_path": analyse.get_models(get_config({"train": "expert280", "valid": "jplrs100"}), models_dir, unique=True, required=True), 
                "data_path": str(Path(data_dir, "res_1000-size_96", "jplrs", "n100", "combined.h5"))
        }

        plt.figure()
        accs = []
        accs_avg = []
        xlabels = []

        for name, d in models.items():            
            correct, predicted, grasps = get_predictions(d["model_path"], d["data_path"])
            correct_avg, predicted_avg, grasps_avg = average_predictions_per_grasp(correct, predicted, grasps)

            acc = np.sum(correct == np.round(predicted) ) / len(correct)
            acc_avg = np.sum(correct_avg == predicted_avg) / len(correct_avg)

            accs.append(acc)
            accs_avg.append(acc_avg)
            xlabels.append(name)

        x = np.arange(len(accs))

        plt.bar(x=x-0.2, height=accs, width=0.4, label = 'Accuracy')
        plt.bar(x=x+0.2, height=accs_avg, width=0.4, label = 'Accuracy averaged')

        plt.xticks(x, xlabels, fontsize=10)
        plt.legend()
        plt.ylim([0, 1])
        plt.locator_params(nbins=11, axis='y')
        plt.ylabel("Accuracy")
        plt.xlabel("Validation data")

        plt.savefig(figures_dir + "validation_descriptors.png", bbox_inches="tight")

        print("\n\n##### validation descriptors - Done #####\n\n")

    plt.show()





if __name__ == "__main__":
    graphs_to_plot = [
        #"default_model",
        #"epochs",
        #"uncertainty",
        #"wrong",
        #"domain_shift",
        "best_models",
        #"inputs",
        #"training_descriptors",
        #"validation_descriptors"
    ]

    plot_graphs(graphs_to_plot)
