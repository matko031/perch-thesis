"""
Title: Grad-CAM class activation visualization
Author: [fchollet](https://twitter.com/fchollet)
Date created: 2020/04/26
Last modified: 2021/03/07
Description: How to obtain a class activation heatmap for an image classification model.
"""
"""
Adapted from Deep Learning with Python (2017).
## Setup
"""

import numpy as np
import tensorflow as tf
from tensorflow import keras

import sys
# Display
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from PIL import Image
import h5py
import cv2
from pathlib import Path


def get_img_array_from_png(img_path, size):
    img = keras.preprocessing.image.load_img(img_path, target_size=size)
    array = keras.preprocessing.image.img_to_array(img)
    array = np.expand_dims(array, axis=0)

    return array


def get_img_array_from_h5(h5_path, inp):

    data_file = h5py.File(h5_path, "r")
   
    if inp == "depth":
        l = "X"
        s = 32

        img = np.array(cv2.resize(  
            data_file[l][0, :, :].astype(np.float32), (s, s),
            interpolation=cv2.INTER_LINEAR,  
        ))

        img = np.expand_dims(img, axis=2)

    if inp == "depth-color-stack":
        l = "cx"
        s = 66
        img1 = np.array(cv2.resize(  
            data_file["C"][0, :, :].astype(np.float32), (s, s),
            interpolation=cv2.INTER_LINEAR,  
        ))

        img2 = np.array(cv2.resize(  
            data_file["X"][0, :, :].astype(np.float32), (s, s),
            interpolation=cv2.INTER_LINEAR,  
        ))
        img2 = np.expand_dims(img2, axis=2)
        img = np.dstack((img1,img2))

    img = np.expand_dims(img, axis=0)

    return img

def make_gradcam_heatmap(img_array, model, last_conv_layer_name, pred_index=None):
    # First, we create a model that maps the input image to the activations
    # of the last conv layer as well as the output predictions
    grad_model = tf.keras.models.Model(
        [model.inputs], [model.get_layer(last_conv_layer_name).output, model.output]
    )

    # Then, we compute the gradient of the top predicted class for our input image
    # with respect to the activations of the last conv layer
    with tf.GradientTape() as tape:
        last_conv_layer_output, preds = grad_model(img_array)
        if pred_index is None:
            pred_index = tf.argmax(preds[0])
        class_channel = preds[:, pred_index]

    # This is the gradient of the output neuron (top predicted or chosen)
    # with regard to the output feature map of the last conv layer
    grads = tape.gradient(class_channel, last_conv_layer_output)

    # This is a vector where each entry is the mean intensity of the gradient
    # over a specific feature map channel
    pooled_grads = tf.reduce_mean(grads, axis=(0, 1, 2))

    # We multiply each channel in the feature map array
    # by "how important this channel is" with regard to the top predicted class
    # then sum all the channels to obtain the heatmap class activation
    last_conv_layer_output = last_conv_layer_output[0]
    heatmap = last_conv_layer_output @ pooled_grads[..., tf.newaxis]
    heatmap = tf.squeeze(heatmap)

    # For visualization purpose, we will also normalize the heatmap between 0 & 1
    heatmap = tf.maximum(heatmap, 0) / tf.math.reduce_max(heatmap)
    return heatmap.numpy()





def save_and_display_gradcam(img, heatmap, cam_path="cam.jpg", alpha=0.4):
    # Load the original image

    # Rescale heatmap to a range 0-255
    heatmap = np.uint8(255 * heatmap)

    # Use jet colormap to colorize heatmap
    jet = cm.get_cmap("jet")

    # Use RGB values of the colormap
    jet_colors = jet(np.arange(256))[:, :3]
    jet_heatmap = jet_colors[heatmap]

    # Create an image with RGB colorized heatmap
    jet_heatmap = keras.preprocessing.image.array_to_img(jet_heatmap)
    jet_heatmap = jet_heatmap.resize((img.shape[1], img.shape[0]))
    jet_heatmap = keras.preprocessing.image.img_to_array(jet_heatmap)

    # Superimpose the heatmap on original image
    if img.shape[2] == 4:
        img = img[:,:,:3]

    if img.shape[-1] == 1:
        img = np.stack((img[:,:,0],)*3, axis=-1)
    superimposed_img = jet_heatmap * alpha + img
    superimposed_img = keras.preprocessing.image.array_to_img(superimposed_img)

    # Save the superimposed image
    superimposed_img.save(cam_path)

    # Display Grad CAM
    Image.open(cam_path).show()



def runGradCAM(model_path, h5_path):

    img_array = get_img_array_from_h5(h5_path, "depth")

    model = keras.models.load_model(model_path)
    last_conv_layer_name = model.layers[-4].name

    model.layers[-1].activation = None

    preds = model.predict(img_array)
    print("Predicted:", preds)

    heatmap = make_gradcam_heatmap(img_array, model, last_conv_layer_name)

    plt.matshow(heatmap)

    save_and_display_gradcam(img_array[0], heatmap)


models_dir = "/home/delimir/Documents/Faks/5/Thesis/condor/models" 
img1 = "desc.h5"
model1 = str(Path(models_dir, "res_1000-size_96-train_expert280-valid_jplrsSingle1-i_depth-b_128-e_50"))

runGradCAM(model1, img1)

