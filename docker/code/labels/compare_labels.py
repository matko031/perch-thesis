from pathlib import Path
import math


def get_orig_label(filename):
    with open("orig/"+filename) as f:
        data = f.read().split()[0]
        if data == "success" or data == "sucess":
            label = 1
        elif data == "fail" or data == "failure":
            label = 0
        else: label = data
    return label


correct_ceil = 0
wrong_ceil = 0

correct_floor = 0
wrong_floor = 0

other_count = 0
other = []

skip_dirs = ["pisgah_site4_ng_bad1", "pisgah_site4_ng_good1"]
detry_dir = Path("detry")
for dir in detry_dir.iterdir():
    filename = dir.name
    if filename not in skip_dirs:
        with open(dir / "rq.txt") as f:
            data = f.read().split("\n")[:-1]
            shape, texture = [float(x.split(":")[1]) for x in data]
            label_floor = math.floor(shape*texture) # Threshold = 0.51
            label_ceil = math.ceil(shape*texture) # Threshold = 0.49
            label_orig = get_orig_label(filename+".txt")
            if label_orig in [0,1]:
                if label_floor == label_orig:
                    correct_floor += 1
                else:
                    wrong_floor += 1

                if label_ceil == label_ceil:
                    correct_ceil += 1
                else:
                    wrong_ceil += 1
            else:
                other_count += 1
                other.append(label_orig)

print(f"Correct floor: {correct_floor}")
print(f"Wrong floor: {wrong_floor}")
print(f"Correct ceil: {correct_ceil}")
print(f"Wrong ceil: {wrong_ceil}")
print(f"Other count: {other_count}")
print(f"Other: {other}")