#!/usr/bin/env 
from pathlib import Path
import pprint
import os

from climbnet import train_model


models_dir = "../../models"
data_dir = "../../data/" 

default = {
        "input": "depth",
        "epochs": 50,
        "batch_size": 128,
        "train_source": "expert",
        "train_nb": 280,
        "valid_source": "jplrsSingle",
        "valid_nb": 1,
        "res": 1000,
        "size": 96
        }

def get_data_path(source, nb, basedir = data_dir, res = default["res"], size = default["size"]):
    return str(Path(basedir, f"res_{res}-size_{size}", source, f"n{nb}", "combined.h5"  ))

def get_config(user_config=None):
    if user_config is None:
        user_config = {}

    config = {
            "input": default["input"],
            "epochs": default["epochs"],
            "batch_size": default["batch_size"]
        } 

    config.update(user_config) 
    return config


def get_model_path(
    basedir = models_dir, res = default["res"], size = default["size"], 
    source_train = default["train_source"], nb_train = default["train_nb"],
    source_valid = default["valid_source"], nb_valid = default["valid_nb"],
    inp = default["input"], batch_size = default["batch_size"], epochs = default["epochs"]
):

    return f"{basedir}/res_{res}-size_{size}-train_{source_train}{nb_train}-valid_{source_valid}{nb_valid}-i_{inp}-b_{batch_size}-e_{epochs}"



def train_models(models_to_train):


    ##### Default model #####
    if "default_model" in models_to_train:
        print("\n\n##### default_model #####\n\n")
        train_model(
            train_data_path = get_data_path(source=default["train_source"], nb=default["train_nb"]), val_data_path = get_data_path(source=default["valid_source"], nb=default["valid_nb"]),
            model_path = get_model_path(), user_config = get_config(), seed=1
        )
        print("\n\n##### Overfitting - Done #####\n\n")

    ##### Epochs #####
    if "epochs" in models_to_train:
        print("\n\n##### epochs #####\n\n")

        models = [
                {"model_path": get_model_path(source_train="expert", nb_train="280", epochs=100),   "config": {"epochs": 100}},
                {"model_path": get_model_path(source_train="expert", nb_train="280", epochs=50),    "config": {"epochs": 50}},
                {"model_path": get_model_path(source_train="expert", nb_train="280", epochs=10),    "config": {"epochs": 10}},
         ]

        for d in models:
            train_model(
                train_data_path = get_data_path(source="expert", nb=280), val_data_path = get_data_path(source="jplrsSingle", nb=1), 
                model_path = d["model_path"], user_config = get_config(d["config"]), seed=1
            )

        print("\n\n##### epochs - Done #####\n\n")


    if "uncertainty" in models_to_train:
        print("\n\n##### uncertainty #####\n\n")

        models = [
                {"model_path": get_model_path(inp="depth", source_train="expert", nb_train=280, source_valid="jplrsSingle", nb_valid=1, epochs=50), "config": {"epochs": 50, "input": "depth"}},
                {"model_path": get_model_path(inp="color", source_train="expert", nb_train=280, source_valid="jplrsSingle", nb_valid=1, epochs=50), "config": {"epochs": 50, "input": "color"}},
                {"model_path": get_model_path(inp="grey", source_train="expert", nb_train=280, source_valid="jplrsSingle", nb_valid=1, epochs=50), "config": {"epochs": 50, "input": "grey"}},
                {"model_path": get_model_path(inp="depth-color-stack", source_train="expert", nb_train=280, source_valid="jplrsSingle", nb_valid=1, epochs=50), "config": {"epochs": 50, "input": "depth-color-stack"}},
                {"model_path": get_model_path(inp="depth-grey-stack", source_train="expert", nb_train=280, source_valid="jplrsSingle", nb_valid=1, epochs=50), "config": {"epochs": 50, "input": "depth-grey-stack"}},
            ]

        for d in models:
            train_model(
                train_data_path = get_data_path(source="expert", nb=280), val_data_path = get_data_path(source="jplrsSingle", nb=1), 
                model_path = d["model_path"], user_config = get_config(d["config"]), seed=1
            )

        print("\n\n##### uncertainty - Done #####\n\n")




    ##### Domain shift #####
    if "domain_shift" in models_to_train:
        print("\n\n##### domain shift #####\n\n")

        valid_data = get_data_path(source="jplSingle", nb=1)

        models = [
             {"data_path": get_data_path(source="expert", nb=28), "model_path": get_model_path(source_train="expert", nb_train="28", source_valid="jplSingle", nb_valid=1)},
             {"data_path": get_data_path(source="expert", nb=280), "model_path": get_model_path(source_train="expert", nb_train="280", source_valid="jplSingle", nb_valid=1)},
             {"data_path": get_data_path(source="expert", nb=2800), "model_path": get_model_path(source_train="expert", nb_train="2800", source_valid="jplSingle", nb_valid=1)},
             {"data_path": get_data_path(source="rs", nb=1), "model_path": get_model_path(source_train="rs", nb_train="1", source_valid="jplSingle", nb_valid=1)},
             {"data_path": get_data_path(source="rs", nb=10), "model_path": get_model_path(source_train="rs", nb_train="10", source_valid="jplSingle", nb_valid=1)},
             {"data_path": get_data_path(source="rs", nb=100), "model_path": get_model_path(source_train="rs", nb_train="100", source_valid="jplSingle", nb_valid=1)},
         ]

        for d in models:
            train_model(
                train_data_path = d["data_path"], val_data_path = valid_data, 
                model_path = d["model_path"], user_config = get_config(), seed=1
            )

        print("\n\n##### domain shift - Done #####\n\n")


    if "inputs" in models_to_train:
        print("\n\n##### inputs #####\n\n")

        train_data = get_data_path(source="expert", nb=280)
        valid_data = get_data_path(source="jplrsSingle", nb=1)

        models = [
                {"model_path": get_model_path(inp="depth"), "config": {"input": "depth"}},
                {"model_path": get_model_path(inp="color"), "config": {"input": "color"}},
                {"model_path": get_model_path(inp="grey"), "config": {"input": "grey"}},

                {"model_path": get_model_path(inp="depth-grey"), "config": {"input": "depth-grey"}},
                {"model_path": get_model_path(inp="depth-color"), "config": {"input": "depth-color"}},

                {"model_path": get_model_path(inp="depth-grey-stack"), "config": {"input": "depth-grey-stack"}},
                {"model_path": get_model_path(inp="depth-color-stack"), "config": {"input": "depth-color-stack"}},
        ] 

        for d in models:
            train_model(
                train_data_path = train_data, val_data_path = valid_data, 
                model_path = d["model_path"], user_config = get_config(d["config"]), seed=1
            )

        print("\n\n##### inputs - Done #####\n\n")

    if "training_descriptors" in models_to_train:
        print("\n\n##### training descriptors #####\n\n")

        models = []
        for res in (1000, 2000):
            for size in (96, 200):
                model_path = get_model_path(res=res, size=size)
                train_data = get_data_path(res=res, size=size, source="expert", nb=280)
                valid_data = get_data_path(res=res, size=size, source="jplrsSingle", nb=1)

                train_model(
                    train_data_path = train_data, val_data_path = valid_data,
                    model_path = model_path, user_config = get_config(), seed=1
                )

        print("\n\n##### training descriptors - Done #####\n\n")
        

    if "validation_descriptors" in models_to_train:
        print("\n\n##### validation descriptors #####\n\n")

        train_data = get_data_path(source="expert", nb=280)
        models = [
            { "model_path": get_model_path(source_valid="jplrsSingle", nb_valid=1), "data_path": get_data_path(source="jplrsSingle", nb=1) },
            { "model_path": get_model_path(source_valid="jplrsSingle", nb_valid=10), "data_path": get_data_path(source="jplrsSingle", nb=10) },
            { "model_path": get_model_path(source_valid="jplrsSingle", nb_valid=100), "data_path": get_data_path(source="jplrsSingle", nb=100) },

            { "model_path": get_model_path(source_valid="jplrs", nb_valid=1), "data_path": get_data_path(source="jplrs", nb=1) },
            { "model_path": get_model_path(source_valid="jplrs", nb_valid=10), "data_path": get_data_path(source="jplrs", nb=10) },
            { "model_path": get_model_path(source_valid="jplrs", nb_valid=100), "data_path": get_data_path(source="jplrs", nb=100) },
        ]

        for d in models:
            train_model(
                train_data_path = train_data, val_data_path = d["data_path"],
                model_path = d["model_path"], user_config = get_config(), seed=1
            )

        print("\n\n##### validation descriptors - Done #####\n\n")








if __name__ == "__main__":
    models_to_train = [
        #"default_model",
        #"epochs",
        #"uncertainty",
        #"domain_shift",
        #"overfitting",
        #"best_models",
        #"inputs",
        "training_descriptors",
        "validation_descriptors",
        ]

    train_models(models_to_train)
