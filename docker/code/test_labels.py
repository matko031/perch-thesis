from pathlib import Path

detry_labels = Path("./detry-labels")
rs_labels = Path("rs_labels")
N = 0
correct = 0
for site_grab_dir in detry_labels.iterdir():
    f = open(site_grab_dir, "r")
    data = f.read().split("\n")
    f.close()
    shape, texture = [float(x.split(":")[1]) for x in data]
    if shape*texture > 0.49:
        res_detry = 1
    else:
        res_detry = 0

    dirname = site_grab_dir.name
    rs_dir = rs_labels / dirname

    f = open(rs_dir, "r")
    data = f.read().split("\n")
    f.close()
    # TODO get result
    res_rs = 1

    if res_detry == res_rs:
        correct += 1

    N += 1