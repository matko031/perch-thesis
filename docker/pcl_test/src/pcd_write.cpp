#include <cmath>
#include <iostream>
#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>
#include <pcl/point_types.h>
#include <pcl/surface/organized_fast_mesh.h>

pcl::PointCloud<pcl::PointXYZRGB>::Ptr get_cloud(int width, int height,
                                                 float spacing) {

  pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud(
      new pcl::PointCloud<pcl::PointXYZRGB>);
  cloud->width = (int)width / spacing;
  cloud->height = (int)height / spacing;
  cloud->is_dense = false;
  cloud->points.resize(cloud->width * cloud->height);

  int h = cloud->height / 2;
  int w = cloud->width / 2;

  for (int row = -h; row < h; row++) {
    for (int col = -w; col < w; col++) {
      pcl::PointXYZRGB point;

      float x = col * spacing;
      float y = row * spacing;
      float z = ((float)(rand() % 100)) / 10000;

      point.x = x;
      point.y = y;
      point.z = z;

      point.r = 50;
      point.g = 100;
      point.b = 150;

      cloud->at(row + h, col + w) = point;
    }
  }

  return cloud;
}

void colour_cloud(pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud, int red,
                  int green, int blue, float x_c, float y_c, float height,
                  float width) {

  int h = cloud->height / 2;
  int w = cloud->width / 2;

  for (int row = -h; row < h; row++) {
    for (int col = -w; col < w; col++) {
      pcl::PointXYZRGB *point = &cloud->at(row + h, col + w);

      float x = point->x;
      float y = point->y;

      float w = width / 2;
      float h = height / 2;

      if (x > x_c - w && x < x_c + w && y < y_c + h && y > y_c - h) {
        point->r = red;
        point->g = green;
        point->b = blue;
      }
    }
  }
}

void colour_cloud(pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud, int red,
                  int green, int blue, float x_c, float y_c, float r) {

  int h = cloud->height / 2;
  int w = cloud->width / 2;

  for (int row = -h; row < h; row++) {
    for (int col = -w; col < w; col++) {
      pcl::PointXYZRGB *point = &cloud->at(row + h, col + w);

      float d = std::pow(point->x - x_c, 2) + std::pow(point->y - y_c, 2);
      d = std::sqrt(d);

      if (d < r) {
        point->r = red;
        point->g = green;
        point->b = blue;
      }
    }
  }
}

pcl::PolygonMesh pcd2Mesh(pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud) {
  pcl::OrganizedFastMesh<pcl::PointXYZRGB> ofm;

  ofm.setInputCloud(cloud);
  ofm.setMaxEdgeLength(1.0);
  ofm.setTrianglePixelSize(0.1);
  ofm.setTriangulationType(
      pcl::OrganizedFastMesh<pcl::PointXYZRGB>::TRIANGLE_ADAPTIVE_CUT);
  pcl::PolygonMesh mesh;
  ofm.reconstruct(mesh);

  return mesh;
}

int main(int argc, char **argv) {

  if (argc != 2) {
    std::cout << "Usage: " << argv[0] << " <spacing between points>"
              << std::endl;
    return 1;
  }
  float spacing = std::atof(argv[1]);
  int cloud_width = 1;
  int cloud_height = 1;

  pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud;
  pcl::PolygonMesh mesh;

  cloud = get_cloud(cloud_width, cloud_height, spacing);

  colour_cloud(cloud, 0, 255, 0, 0, 0, 0.02); // center

  colour_cloud(cloud, 100, 100, 0, -0.25, 0, 0.05, 0.15); // left
  colour_cloud(cloud, 100, 0, 100, 0.25, 0, 0.05, 0.15);  // right
  mesh = pcd2Mesh(cloud);
  pcl::io::savePLYFileBinary("original.ply", mesh);

  colour_cloud(cloud, 0, 255, 0, -0.25, 0, 0.10, 0.03); // left
  colour_cloud(cloud, 0, 255, 0, 0.25, 0, 0.10, 0.03);  // right
  mesh = pcd2Mesh(cloud);
  pcl::io::savePLYFileBinary("annotated.ply", mesh);

  pcl::io::savePCDFileASCII("test_pcd.pcd", *cloud);
}
