1. Build the docker image and start the container:  
    `docker-compose up -d` (first time you run this, it will take a while to build the image)
1. Enter the container:
    `docker exec -it matko-ros bash`
1. Start the detect_perch_site_server:
    `roslaunch graspability_server detect_perch_site_server.launch`
    If realsense camera is not recognized, run `docker-compose down` on host and start from step 1 again.
1. Run the detect_perch_site_client:
    `rosrun graspability_server detect_perching_site_client`
1. You should see the grasp probability and coordinates in the terminal where `roslaunch` was run.

Optional:
#### Run rviz to see the realsense output:
1. Run `allow_docker_GUI.sh` script on host.
    It might be necessary to restart the container if it's already running
1. Start rviz with `rviz` command inside the container
1. Click Add button in the lower left corner (default location) to add new display
1. Select `By topic` -> `/camera/color/image_raw/Image` for colour image and `/camera/depth/image_rect_raw/Image` for depth
