packadd termdebug

iabbrev cout std::cout
iabbrev endl std::endl

function SemicolonToggle()
	let col = col('.')
	let line = getline('.')
	let char = matchstr(line, '.\ze\s*$')
	normal g_
 
	if char  ==? ';'
		normal x
	else
		normal a;
	endif

	call cursor(line('.'), col)
endfunction

nnoremap <buffer> <leader>; :call SemicolonToggle()<cr>



function LineCommentToggle()
	let colNum = col('.')
	let line = getline('.')
	let start = matchstr(line, '^\s*\zs...')
	normal ^
 
	if start[0:1] ==? '//'
		normal xx
		let colNum = colNum - 2
		if start[2] ==? " "
			normal x
			let colNum = colNum - 3
		endif
	else
		execute "normal i// "
		let colNum = colNum + 3
	endif
	
	call cursor(line('.'), colNum)
endfunction

nnoremap <buffer> <leader>c :call LineCommentToggle()<cr>


"let b:ale_fixers = ['astyle']
"let b:ale_linters = ['ccls']

