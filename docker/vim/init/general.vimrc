" General settings
filetype plugin indent on 
syntax on
set backspace=indent,eol,start
set shiftwidth=4 " Number of spaces used for >>, <<, etc 
set tabstop=4  "1 <Tab> = tabstop spaces
set softtabstop=4 
set expandtab 
set autoindent 
set smartindent 
set hidden
set noswapfile
set incsearch
set hlsearch
set autowrite
set autowriteall
set mouse=a
set wildmenu 
set wildmode=longest,full
set number relativenumber
set cursorline
set ignorecase
set smartcase 
set scrolloff=5
set clipboard=unnamedplus
set showmatch
set splitbelow
set splitright
set foldmethod=syntax
set nofoldenable
set shell=bash
set iskeyword-=-
set iskeyword-=:
set iskeyword-=_
set spelllang=en_gb

"Use 24-bit (true-color) mode in Vim/Neovim when outside tmux.
"If you're using tmux version 2.2 or later, you can remove the outermost $TMUX check and use tmux's 24-bit color support
"(see < http://sunaku.github.io/tmux-24bit-color.html#usage > for more information.)
"if (empty($TMUX))
"  if (has("nvim"))
"    "For Neovim 0.1.3 and 0.1.4 < https://github.com/neovim/neovim/pull/2198 >
"    let $NVIM_TUI_ENABLE_TRUE_COLOR=1
"  endif
"  "For Neovim > 0.1.5 and Vim > patch 7.4.1799 < https://github.com/vim/vim/commit/61be73bb0f965a895bfb064ea3e55476ac175162 >
"  "Based on Vim patch 7.4.1770 (`guicolors` option) < https://github.com/vim/vim/commit/8a633e3427b47286869aa4b96f2bfc1fe65b25cd >
"  " < https://github.com/neovim/neovim/wiki/Following-HEAD#20160511 >
"  if (has("termguicolors"))
"    set termguicolors
"  endif
"endif





set foldtext=MyFoldText()

let g:netrw_liststyle=3
let $RTP=split(&runtimepath, ',')[0] "$RTP=~/.vim 
let $RC="$HOME/.vim/vimrc"

let maplocalleader = " "
let mapleader = " "


"""""""""" STATUS LINE """"""""""
set statusline=%F         " Path to the file
set statusline+=\ %y
set statusline+=%=        " Switch to the right side
set statusline+=%l        " Current line
set statusline+=/         " Separator
set statusline+=%L        " Total lines
set laststatus=2


augroup numbertoggle
	autocmd!
	autocmd BufEnter,FocusGained,InsertLeave,WinEnter * if &nu && mode() != "i" | set rnu   | endif
	autocmd BufLeave,FocusLost,InsertEnter,WinLeave   * if &nu                  | set nornu | endif
augroup END


autocmd BufEnter * silent! lcd %:p:h



