" mapping alt+f to turn off search highlight
noremap f :nohlsearch<CR>

" capitalize current word
noremap <c-u> <esc>viwUi

" edit vimrc
nnoremap <leader>ve :vsplit $MYVIMRC<cr>
" source vimrc
nnoremap <leader>vs :source $MYVIMRC<cr>


" toggle netrw menu in left split
nnoremap <leader>e :call ToggleLex()<cr>

" show a list of open buffers
nnoremap <leader>b :buffers<cr>:b<space>


" gk/gj is used so that if a text streches over multiple visual lines, those
" are interpreted as separate lines
noremap j gj
noremap k gk


" Use Alt+Shift+K/Alt+Shift+L to jump 10 lines down/up
noremap <C-k> 10k
noremap <C-j> 10j
"noremap J 10j


" highlight last inserted text
nnoremap gV `[v`]

" jk is escape
inoremap jk <esc>


" Don't overwrite the default register when deleting with x
nnoremap x "_x


" Encircle current word in "
nnoremap <leader>" viw<esc>a"<esc>bi"<esc>lell

" Encircle current word in {}
nnoremap <leader>{ viw<esc>a}<esc>bi{<esc>lell
nnoremap <leader>} viw<esc>a}<esc>bi{<esc>lell

" Encircle current word in []
nnoremap <leader>[ viw<esc>a]<esc>bi[<esc>lell
nnoremap <leader>] viw<esc>a]<esc>bi[<esc>lell

" Encircle current word in <>
nnoremap <leader>< viw<esc>a><esc>bi<<esc>lell
nnoremap <leader>> viw<esc>a><esc>bi<<esc>lell


" Select text in next ()
onoremap in( :<c-u>normal! f(vi(<cr>
onoremap in) :<c-u>normal! f(vi(<cr>

" Select text in next [] 
onoremap in[ :<c-u>normal! f[vi[<cr>
onoremap in] :<c-u>normal! f[vi[<cr>

" Select text in next {}
onoremap in{ :<c-u>normal! f{vi{<cr>
onoremap in} :<c-u>normal! f{vi{<cr>


" Terminal
nnoremap <leader>t :term<cr>
nnoremap <leader>vt :vert term<cr>
