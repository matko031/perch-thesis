This directory contains all the code necessary to install and run graspability server through ROS.

It can either be done through Docker or locally.

### Docker install
1. Build the image with `docker build -t graspability_image .`
This will create a docker image named `graspability_image` with all necessary software preinstalled.

1. Start the container with `docker run -itd --name "graspability" graspability_image`
This will create a docker container named `graspability`.

1. Enter the container with `docker exec -it graspability bash`
This will give you shell access inside the container

1.  Follow the steps in the `Run server section below` 

* Before running these steps, choose ubuntu and ROS version in the Dockerfile.
Working has been tested on ubuntu 18.04 and 20.04 and ROS noetic and melodic. *

### Local install
1. Choose ros version in `install.sh`

1. Run install script with `./install.sh`

1.  Follow the steps in the `Run server section below` 



### Run server

1. Run the graspability server with `roslaunch graspability_server detect_perch_site_server.launch` 

1. Run the graspability client with `rosrun graspability_server detect_perching_site_client`. This will trigger the client specified in `catkin_ws/src/graspability_server/src/detect_perching_site_client.cpp` which will in turn call  `execute_cb_detect_perch_site` method in `catkin_ws/src/graspability_server/src/detect_perching_site_node.cpp`

* In case graspability_server package cannot be found by ros, source `catkin_ws/devel/setup.bash` file.*
