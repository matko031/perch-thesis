export DEBIAN_FRONTEND=noninteractive

#ros_version="melodic"
ros_version="noetic"

# Core dependencies
if ! command -v sudo &> /dev/null; then
    apt update && apt install sudo
else
    sudo apt update
fi

sudo apt install -y apt-utils

# General utils
sudo apt -y install bash-completion git vim tmux gdb curl lsb-core software-properties-common ranger tree 

sudo apt -y install gcc g++ make pkg-config python libgsl0-dev libblas-dev liblapack-dev libboost-all-dev libpcl-dev libcgal-dev libopencv-dev cmake 



# Ros
sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list' && \
	curl -s https://raw.githubusercontent.com/ros/rosdistro/master/ros.asc | apt-key add - && \
	apt update && \
	apt install -y ros-${ros_version}-desktop ros-${ros_version}-interactive-markers ros-${ros_version}-pcl-conversions ros-${ros_version}-pcl-ros ros-${ros_version}-tf-conversions ros-${ros_version}-eigen-conversions 

# Nuklei
cd catkin_ws/src/nuklei && \
    rm -rf scons.build && \
    sudo -E ./scons.py use_pcl=yes use_cgal=yes -j 4 install && \
cd -


source /opt/ros/${ros_version}/setup.bash && \
cd catkin_ws && \
    catkin_make && \
    source devel/setup.bash && \
    echo "source /opt/ros/${ros_version}/setup.bash" >> ~/.bashrc && \
    echo "source $(pwd)/devel/setup.bash" >> ~/.bashrc
cd ..


sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-key F6E65AC044F831AC80A06380C8B3A55A6F3EFCDE || \
    apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-key F6E65AC044F831AC80A06380C8B3A55A6F3EFCDE && \
    add-apt-repository "deb https://librealsense.intel.com/Debian/apt-repo $(lsb_release -cs) main" -u && \
    apt update && \
    apt install -y librealsense2-dkms librealsense2-utils librealsense2-dev librealsense2-dbg ros-${ros_version}-realsense2-camera ros-${ros_version}-rgbd-launch


#ENV LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:/usr/local/lib
