// (C) Copyright Renaud Detry   2007-2015.
// Distributed under the GNU General Public License and under the
// BSD 3-Clause License (See accompanying file LICENSE.txt).

/** @file */

#include <nuklei/PoseEstimator.h>
#include <nuklei/ProgressIndicator.h>
#include <nuklei/Stopwatch.h>
#include <tclap/CmdLine.h>

int pe(int argc, char ** argv)
{
  
  using namespace nuklei;
  using namespace TCLAP;
  
  CmdLine cmd("");
  
  UnlabeledValueArg<std::string> objectFileArg
  ("object_model",
   "Object file.",
   true, "", "filename", cmd);
  
  UnlabeledValueArg<std::string> sceneFileArg
  ("scene_model",
   "Scene file.",
   true, "", "filename", cmd);
  
  ValueArg<std::string> alignedObjectModelFileArg
  ("a", "aligned",
   "Transformed object model, matching object pose.",
   false, "", "filename", cmd);
  
  ValueArg<int> nArg
  ("n", "n_model_points",
   "Number of particle supporting the object model.",
   false, 0, "int", cmd);
  
  ValueArg<double> locHArg
  ("l", "loc_h",
   "Location kernel width.",
   false, 0, "float", cmd);
  
  ValueArg<double> oriHArg
  ("o", "ori_h",
   "Orientation kernel width (in radians).",
   false, 0.2, "float", cmd);
  
  ValueArg<int> nChainsArg
  ("c", "n_chains",
   "Number of MCMC chains.",
   false, 0, "int", cmd);
  
  ValueArg<int> chainLengthArg
  ("L", "chain_length",
   "Length of MCMC chains.",
   false, -1, "int", cmd);
  
  ValueArg<double> tempInflectionArg
  ("T", "temp_inflection",
   "Fraction of chain length at which the temperature stops decreasing.",
   false, .2, "double", cmd);
  
  ValueArg<double> indepProposalArg
  ("I", "independent_proposal_prob",
   "Probability of doing an independent proposal in MCMC PE. If zero, the independent proposal is never probed. If negative, skips MCMC and does gradient descent.",
   false, .75, "double", cmd);
  
  ValueArg<double> indepProposalPriorThresholdArg
  ("", "ip_threshold",
   "Bias scene sampling for independent proposal, starting at this threshold of the IP prior.",
   false, 0, "double", cmd);
  
  ValueArg<std::string> indepProposalPriorArg
  ("", "ip_prior",
   "Copy of scene model that included weights modeling a sampling prior for independent proposal.",
   false, "", "filename", cmd);

  ValueArg<std::string> bestTransfoArg
  ("t", "best_transfo",
   "File to write the most likely transformation to.",
   false, "", "filename", cmd);
  
  ValueArg<std::string> poseDistributionArg
  ("d", "distribution",
   "File to which weighted poses will be written.",
   false, "", "filename", cmd);
  
  ValueArg<std::string> gridArg
  ("g", "grid",
   "Full grid-format (organized point cloud) scene.",
   false, "", "filename", cmd);
  
  ValueArg<std::string> pdImageFileArg
  ("", "pd_image",
   "File to which the pose distribution will be written. Only with -d and -g.",
   false, "", "filename", cmd);
  
  SwitchArg visuArg
  ("V", "visualize",
   "Live visu of pose estimation.", cmd);
  
  SwitchArg computeNormalsArg
  ("", "normals",
   "OBSOLETE ARGUMENT. NORMALS ARE ALWAYS COMPUTED. "
   "Compute a normal vector for all input points. "
   "Makes pose estimation more robust.", cmd);
  
  SwitchArg accurateScoreArg
  ("s", "accurate_score",
   "OBSOLETE ARGUMENT. ACCURATE SCORE IS ALWAYS COMPUTED. "
   "Recompute the matching score using all input points "
   "(instead of using N points as given by -n N).", cmd);
  
  SwitchArg useWholeSceneCloudArg
  ("", "slow",
   "By default, only 10000 points of the scene point cloud are used, "
   "for speed. If --slow is specified, all input points are used.", cmd);
  
  SwitchArg timeArg
  ("", "time",
   "Print computation time.", cmd);
  
  SwitchArg progressArg
  ("p", "progress",
   "Print progress.", cmd);
  
  SwitchArg partialviewArg
  ("", "partial",
   "Match only the visible side of the model to the object.", cmd);
  
  ValueArg<std::string> meshFileArg
  ("", "mesh",
   "Mesh model of object evidence. Used only if --partial is enabled.",
   false, "", "filename", cmd);
  
  ValueArg<std::string> viewpointFileArg
  ("", "viewpoint",
   "File containing XYZ of the camera.",
   false, "", "filename", cmd);
  
  ValueArg<double> meshVisibilityArg
  ("", "point_to_mesh_visibility_dist",
   "Sets the distance to the mesh at which a point is considered to be visible.",
   false, 4., "float", cmd);
  
  cmd.parse( argc, argv );
  Stopwatch sw("");
  if (!timeArg.getValue())
    sw.setOutputType(Stopwatch::QUIET);
  
  // ------------- //
  // Read-in data: //
  // ------------- //
  
  PoseEstimator pe(locHArg.getValue(),
                   oriHArg.getValue(),
                   nChainsArg.getValue(),
                   nArg.getValue(),
                   boost::shared_ptr<CustomIntegrandFactor>(),
                   partialviewArg.getValue(),
                   progressArg.getValue());
  pe.setMeshToVisibilityTol(meshVisibilityArg.getValue());
  pe.setChainLength(chainLengthArg.getValue());
  pe.setTemperatureInflectionPoint(tempInflectionArg.getValue());
  pe.setIndependentProposalProbability(indepProposalArg.getValue());
  pe.setVisu(visuArg.getValue());
  pe.setIndependentProposalPriorThreshold(indepProposalPriorThresholdArg.getValue());
  if (!indepProposalPriorArg.getValue().empty())
  {
    KernelCollection p;
    readObservations(indepProposalPriorArg.getValue(), p);
    pe.setIndependentProposalPrior(p);
  }
  if (!gridArg.getValue().empty())
  {
    KernelCollection grid;
    readObservationsWithSpecificFormat(gridArg.getValue(), grid, Observation::PCDGRID);
    pe.setGrid(grid);
  }
  
  pe.load(objectFileArg.getValue(),
          sceneFileArg.getValue(),
          meshFileArg.getValue(),
          viewpointFileArg.getValue(),
          !useWholeSceneCloudArg.getValue(),
          true);
  
  
  sw.lap("data read");
  
  // ------------------------------- //
  // Prepare density for evaluation: //
  // ------------------------------- //
  
  bool pd = !poseDistributionArg.getValue().empty();
  KernelCollection
  poses = pe.modelToSceneTransformations(pd);
  
  sw.lap("alignment");
  
  boost::optional<kernel::se3> t;
  if (poses.size())
    t = kernel::se3(*poses.sortBegin(1));
  else
  {
    t = kernel::se3();
    t->setWeight(0);
  }
  
  sw.lap("sorting");
  
  std::cout << "Matching score: " << t->getWeight() << std::endl;
  
  if (!bestTransfoArg.getValue().empty())
  {
    writeSingleObservation(bestTransfoArg.getValue(), *t);
  }
  
  if (!alignedObjectModelFileArg.getValue().empty())
  {
    pe.writeAlignedModel(alignedObjectModelFileArg.getValue(), *t);
  }
  
  if (!poseDistributionArg.getValue().empty())
  {
    writeObservations(poseDistributionArg.getValue(), poses);
  }
  
  if (!pdImageFileArg.getValue().empty())
  {
    pe.writePdfImage(pdImageFileArg.getValue(), poses);
  }
  
  sw.lap("output");
  
  
  return 0;
}

