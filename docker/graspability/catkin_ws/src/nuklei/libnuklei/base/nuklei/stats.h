// (C) Copyright Renaud Detry   2007-2015.
// Distributed under the GNU General Public License and under the
// BSD 3-Clause License (See accompanying file LICENSE.txt).

/** @file */

#ifndef NUKLEI_STATS_H
#define NUKLEI_STATS_H

#include <fstream>
#include <cassert>
#include <algorithm>
#include <cmath>

#include <queue>
#include <set>
#include <list>

#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics/stats.hpp>
#include <boost/accumulators/statistics/min.hpp>
#include <boost/accumulators/statistics/median.hpp>
#include <boost/accumulators/statistics/mean.hpp>
#include <boost/accumulators/statistics/variance.hpp>
#include <boost/accumulators/statistics/max.hpp>


#include <nuklei/Common.h>
#include <nuklei/Definitions.h>

namespace nuklei {

  namespace ba = boost::accumulators;
  
  template<typename T = double>
  struct stats
  {
    stats(bool storeData = false) : storeData_(storeData) {}
    
    void push(T t)
    {
      acc_(t);
      if (storeData_) data_.push_back(t);
    }

    template<typename Iterator>
    void push(Iterator begin, Iterator end)
    {
      for (; begin != end; ++begin)
        push(*begin);
    }
    
    T min() const { return ba::min(acc_); }
    T mean() const { return ba::mean(acc_); }
    T stdev() const { return std::sqrt(ba::variance(acc_)); }
    T median() const { return ba::median(acc_); }
    T max() const { return ba::max(acc_); }
    
  private:
    ba::accumulator_set< T, ba::features< ba::tag::min, ba::tag::mean, ba::tag::variance, ba::tag::median, ba::tag::max > > acc_;
    bool storeData_;
    std::vector<T> data_;
  };

}

#endif
