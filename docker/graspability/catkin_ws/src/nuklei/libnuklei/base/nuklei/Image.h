// (C) Copyright Renaud Detry   2007-2015.
// Distributed under the GNU General Public License and under the
// BSD 3-Clause License (See accompanying file LICENSE.txt).

/** @file */

#ifndef NUKLEI_IMAGE_H
#define NUKLEI_IMAGE_H

#include <nuklei/LinearAlgebra.h>
#include <nuklei/Common.h>
#include <nuklei/Camera.h>

namespace cv
{
  class Mat;
}

namespace nuklei {

  struct rgb_t { uint8_t r, g, b; };
  struct bgr_t { uint8_t b, g, r; };

  struct Image
  {
    Image();
    Image(const Image& i);
    
    void computeGradients();
    
    std::vector<Vector2> project(const std::vector<Vector3> worldPoints,
                                 const Vector3& color);
    Vector2 project(const Vector3& p,
                    const Vector3& rgb = Vector3::ONE);
    
    void read(const std::string& name);
    void write(const std::string& name) const;
    
    void setScale(const double s, bool resize = false);
        
    void readCamera(const std::string& opencvStereoCalibFile);
    
    void drawLine(const Vector2& a, const Vector2& b,
                  const Vector3& rgb = Vector3::ONE);
    void drawPoint(const Vector2& a,
                   const Vector3& rgb = Vector3::ONE);
    
    const Camera& getCamera() const { return camera_; }
    
  private:
    boost::shared_ptr<cv::Mat> img_;
    std::vector< boost::shared_ptr<cv::Mat> > dx_, dy_, gradIntensity_, gradDir_;
    Camera camera_;
    double scale_;
  };
  

}

#endif
