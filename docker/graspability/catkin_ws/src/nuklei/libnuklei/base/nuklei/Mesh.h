// (C) Copyright Renaud Detry   2007-2015.
// Distributed under the GNU General Public License and under the
// BSD 3-Clause License (See accompanying file LICENSE.txt).

/** @file */

#ifndef NUKLEI_MESH_H
#define NUKLEI_MESH_H
#include <boost/graph/adjacency_list.hpp>
#include <boost/type_traits/is_same.hpp>
#include <boost/static_assert.hpp>
#include <nuklei/LinearAlgebra.h>
#include <nuklei/Common.h>
#include <nuklei/Camera.h>
#include <nuklei/Kernel.h>

namespace nuklei {

  template<typename Vec>
  struct Mesh
  {
    typedef boost::optional<double> Angle;
    typedef boost::adjacency_list<
    boost::vecS, boost::vecS, boost::undirectedS, Vec, Angle> Graph;
    typedef typename Graph::vertex_descriptor Vertex;
    typedef typename Graph::vertex_iterator VertexIterator;
    typedef std::pair<VertexIterator, VertexIterator> VertexIteratorPair;
    typedef typename Graph::edge_descriptor Edge;
    typedef typename Graph::edge_iterator EdgeIterator;
    typedef typename Graph::out_edge_iterator OutEdgeIterator;
    typedef std::pair<EdgeIterator, EdgeIterator> EdgeIteratorPair;
    typedef std::pair<OutEdgeIterator, OutEdgeIterator> OutEdgeIteratorPair;
    
    Mesh() : computeRightmost_(false) {};
    Mesh(const size_t n, const bool computeRightmost = false) :
    g_(n), computeRightmost_(computeRightmost) {};

    Vertex addVertex() { return boost::add_vertex(g_); }
    Vertex addVertex(const Vec& vec)
    {
      Vertex v = boost::add_vertex(g_);
      g_[v] = vec;
      return v;
    }
    
    void setVertex(const Vertex v, const Vec& vec);
    Edge addEdge(const Vertex v0, const Vertex v1);

    void read(const std::string& filename);
    Mesh<Vector2> project(const Camera& cam,
                          const boost::optional<const kernel::se3&> t = boost::none) const;
    Mesh<Vector2> contour(const struct Image& img);
    Vertex buildNext(const Vertex& v0,
                     const Vertex& v1,
                     const Edge& e
#ifdef NUKLEI_MESH_TREE
                     , struct Tree& tree
#endif
                     );

    double getAngle(const Vertex previous,
                    const Vertex current,
                    const Vertex next) const;
    void draw(struct Image& img, const Vertex v,
              const Vector3& rgb = Vector3::ONE) const;
    void draw(struct Image& img, const Edge e,
              const Vector3& rgb = Vector3::ONE) const;
    void draw(struct Image& img,
              const Vector3& rgb = Vector3::ONE) const;
    
  private:
    Graph g_;
    boost::optional<Vertex> rightmost_;
    bool computeRightmost_;

    BOOST_STATIC_ASSERT((boost::is_same<Vertex, size_t>::value));
  };
  
  typedef Mesh<Vector3> Mesh3;
  typedef Mesh<Vector2> Mesh2;
  
  template<typename Vec>
  void Mesh<Vec>::setVertex(const Vertex v, const Vec& vec)
  {
    g_[v] = vec;
    if (computeRightmost_)
    {
      if (!rightmost_) rightmost_ = v;
      else
      {
        if (vec.X() > g_[*rightmost_].X())
          rightmost_ = v;
      }
    }
  }

  template<typename Vec> typename Mesh<Vec>::Edge
  Mesh<Vec>::addEdge(const Vertex v0, const Vertex v1)
  {
    std::pair<Edge, bool> r = boost::add_edge(v0, v1, g_);
    NUKLEI_ASSERT(r.second);
    return r.first;
  }


}

#endif
