// (C) Copyright Renaud Detry   2007-2015.
// Distributed under the GNU General Public License and under the
// BSD 3-Clause License (See accompanying file LICENSE.txt).

/** @file */

#ifndef NUKLEI_CAMERA_H
#define NUKLEI_CAMERA_H

#include <nuklei/LinearAlgebra.h>
#include <nuklei/Common.h>


namespace nuklei {

  struct Camera
  {
    Camera();
    
    std::vector<Vector2> project(const std::vector<Vector3> worldPoints) const;
    Vector2 project(const Vector3& p) const;
    
    void read(const std::string& opencvStereoCalibFile);
    
  private:
    Matrix4 projection_;
  };
  

}

#endif
