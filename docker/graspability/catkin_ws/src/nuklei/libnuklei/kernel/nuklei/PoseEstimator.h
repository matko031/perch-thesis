// (C) Copyright Renaud Detry   2007-2015.
// Distributed under the GNU General Public License and under the
// BSD 3-Clause License (See accompanying file LICENSE.txt).

/** @file */

#ifndef NUKLEI_POSE_ESTIMATOR_H
#define NUKLEI_POSE_ESTIMATOR_H

#include <nuklei/KernelCollection.h>
#include <nuklei/ObservationIO.h>
#include <nuklei/Types.h>
#include <nuklei/ProgressIndicator.h>
#include <nuklei/parallelizer_decl.h>

#define NUKLEI_POSE_ESTIMATOR_POLYMORPHIC

namespace nuklei {
  
  const bool WEIGHTED_SUM_EVIDENCE_EVAL = false;
  const double WHITE_NOISE_POWER = 1e-4;
  
  /**
   * @brief Allows to use an external integrand factor, or test reachability
   *
   */
  struct CustomIntegrandFactor
  {
    virtual ~CustomIntegrandFactor() {}
    /**
     * @brief returns true if pose @p k is reachable by the robot */
    virtual bool test(const kernel::se3& k) const = 0;
    /**
     * @brief returns the evaluation at @p k of an additional integrand factor */
    virtual double factor(const kernel::se3& k) const = 0;
  };
  
  struct PoseEstimator
  {
    PoseEstimator(const double locH = 0,
                  const double oriH = .2,
                  const int nChains = -1,
                  const int n = -1,
                  boost::shared_ptr<CustomIntegrandFactor> cif = boost::shared_ptr<CustomIntegrandFactor>(),
                  const bool partialview = false,
                  const bool progress = true);
    
    void load(const std::string& objectFilename,
              const std::string& sceneFilename,
              const std::string& meshfile = "",
              const std::string& viewpointfile = "",
              const bool light = true,
              const bool computeNormals = true);
    
    void loadObject(const std::string& objectFilename,
                    const std::string& meshfile);
    void loadScene(const std::string& sceneFilename,
                   const std::string& viewpointfile,
                   const bool light);
    
    void load(const KernelCollection& objectModel,
              const KernelCollection& sceneModel,
              const std::string& meshfile = "",
              const Vector3& viewpoint = Vector3::ZERO,
              const bool light = true,
              const bool computeNormals = true);
    
    void usePartialViewEstimation(const Vector3& viewpoint)
    {
      viewpoint_ = viewpoint;
      partialview_ = true;
    }
    
    void setMeshToVisibilityTol(const double meshTol) { meshTol_ = meshTol; }

    void setGrid(const KernelCollection& grid) { grid_ = grid; grid_.computeColorImage(); }
    void setVisu(const bool v) { visu_ = v; }
    
    double getLocH() const;
    void setLocH(const double locH);
    
    double getOriH() const;
    void setOriH(const double oriH);

    void setChainLength(const int l) { chainLength_ = l; }
    int getChainLength() const { return chainLength_; }
    
    void setNChains(const int c) { nChains_ = c; }
    int getNChains() const { return nChains_; }

    void setN(const int n) { n_ = n; }
    int getN() const { return n_; }

    void setLight(const bool l) { light_ = l; }
    bool getLight() const { return light_; }

    void setComputeNormals(const bool n) { computeNormals_ = n; }
    bool getComputeNormals() const { return computeNormals_; }

    void setPartialView(const bool p);
    bool getPartialView() const { return partialview_; }

    void setBidirectionalMatchingScore(const bool p);
    bool getBidirectionalMatchingScore() const { return bidirectionalMatchingScore_; }

    void setViewpoint(const Vector3& v) { viewpoint_ = v; }
    const Vector3& getViewpoint() const { return viewpoint_; }

    void setProgress(const bool p) { progress_ = p; }
    bool getProgress() const { return progress_; }

    void enableSceneless() { sceneless_ = true; }
    
    void setTemperatureInflectionPoint(const double p) { tempInflection_ = p; }
    double getTemperatureInflectionPoint() const { return tempInflection_; }

    void setIndependentProposalProbability(const double p) { independentProposalProbability_ = p; }
    double getIndependentProposalProbability() const { return independentProposalProbability_; }

    void setIndependentProposalPriorThreshold(const double p) { independentProposalPriorThreshold_ = p; }
    double getIndependentProposalPriorThreshold() const { return independentProposalPriorThreshold_; }
    
    void setIndependentProposalPrior(const KernelCollection& d) { ipPrior_ = d; checkDisplanarity(); }

    void setParallelization(const parallelizer::Type t) { parallel_ = t; }
    parallelizer::Type getParallelization() const { return parallel_; }
    
    void setCustomIntegrandFactor(boost::shared_ptr<CustomIntegrandFactor> cif);
    boost::shared_ptr<CustomIntegrandFactor> getCustomIntegrandFactor() const;
    
    void setObjectModel(const KernelCollection& m) { objectModel_ = m; }
    const KernelCollection& getObjectModel() const { return objectModel_; }
    void setSceneModel(const KernelCollection& m) { sceneModel_ = m; }
    const KernelCollection& getSceneModel() const { return sceneModel_; }

    void setMeshFilename(const std::string& s) { meshFilename_ = s; }
    const std::string& getMeshFilename() const { return meshFilename_; }

    Vector3 getObjectCenter() const { return objectCenter_; }
    double getObjectStdev() const { return objectSize_; }

    kernel::se3 modelToSceneTransformation();
    
    KernelCollection modelToSceneTransformations
    (const bool returnSample = false);

    double findMatchingScore(const kernel::se3& pose, bool bidirectional = false) const;
    
    void writeAlignedModel(const std::string& filename,
                           const kernel::se3& t) const;
    void writePdfImage(const std::string& filename,
                       const KernelCollection& poseSample);
    
  private:
    
    Vector3 viewpointInFrame(const kernel::se3& frame) const;
    
    // Temperature function (cooling factor)
    static double Ti(const unsigned i, const unsigned F);
    
    /**
     * This function implements the algorithm of Fig. 2: Simulated annealing
     * algorithm of the ACCV paper.
     * - T_j is given by @c temperature
     * - u is given by Random::uniform()
     * - w_j is @c currentPose
     */
    bool
    metropolisHastings(kernel::se3& currentPose,
                       weight_t &currentWeight,
                       const weight_t temperature,
                       const bool firstRun,
                       const int n) const;
    
    kernel::se3
    mcmc(const int n, boost::optional<KernelCollection&> poseSample = boost::none);
    bool recomputeIndices(std::vector<int>& indices,
                          const kernel::se3& nextPose,
                          const int n) const;

    void checkDisplanarity();
    
    KernelCollection objectModel_;
    Vector3 objectCenter_;
    double objectSize_;
    KernelCollection sceneModel_;
    KernelCollection grid_;
    std::string meshFilename_;
    bool visu_;
    bool computeNormals_;
    bool light_;
    Vector3 viewpoint_;
    KernelCollection::EvaluationStrategy evaluationStrategy_;
    double loc_h_;
    double ori_h_;
    int nChains_;
    int chainLength_;
    int n_;
    double tempInflection_;
    double independentProposalProbability_;
    double independentProposalPriorThreshold_;
    KernelCollection ipPrior_;
    std::vector<int> independentProposalIndices_;
    boost::shared_ptr<CustomIntegrandFactor> cif_;
    bool partialview_;
    bool bidirectionalMatchingScore_;
    boost::shared_ptr<ProgressIndicator> pi_;
    bool progress_;
    parallelizer::Type parallel_;
    double meshTol_;
    bool sceneless_;
  };
  
}

#endif
