// (C) Copyright Renaud Detry   2007-2015.
// Distributed under the GNU General Public License and under the
// BSD 3-Clause License (See accompanying file LICENSE.txt).

/** @file */

#include "KernelCollectionTypes.h"

#ifdef NUKLEI_USE_CGAL
#if CGAL_VERSION_NR >= 1040800000
#ifndef CGAL_EIGEN3_ENABLED
#  define CGAL_EIGEN3_ENABLED 1
#endif
#elif CGAL_VERSION_NR >= 1040100000
#define CGAL_LAPACK_ENABLED
#endif
#include <CGAL/Monge_via_jet_fitting.h>
#include <CGAL/Cartesian.h>
#endif

#include <nuklei/KernelCollection.h>
#include <nuklei/ProgressIndicator.h>
#include <nuklei/PCLBridge.h>

#include <pcl/search/organized.h>
#include <pcl/search/kdtree.h>
#include <pcl/features/normal_3d_omp.h>
#include <pcl/features/normal_3d.h>

namespace nuklei
{
  
#ifdef NUKLEI_USE_CGAL
  namespace cgal_jet_fitting_types
  {
    typedef double                   DFT;
    typedef CGAL::Cartesian<DFT>     Data_Kernel;
    typedef Data_Kernel::Point_3     DPoint;
    typedef CGAL::Monge_via_jet_fitting<Data_Kernel> Monge_via_jet_fitting;
    typedef Monge_via_jet_fitting::Monge_form     Monge_form;
  }
#endif
  
  boost::tuple<Matrix3, Vector3, coord_t>
  KernelCollection::localLocationDifferential(const Vector3& loc) const
  {
    NUKLEI_TRACE_BEGIN();

#ifdef NUKLEI_USE_CGAL

    using namespace cgal_neighbor_search_types;
    using namespace cgal_jet_fitting_types;
    
    if (!deco_.has_key(NSTREE_KEY))
      NUKLEI_THROW("Undefined neighbor search tree. Call buildNeighborSearchTree() first.");

    boost::shared_ptr<Tree> tree(deco_.get< boost::shared_ptr<Tree> >(NSTREE_KEY));
      
    Point_d center(loc.X(), loc.Y(), loc.Z());

    std::vector<DPoint> in_points;
    K_neighbor_search search(*tree, center, 16+1);
    NUKLEI_ASSERT(search.begin() != search.end());
    for (K_neighbor_search::iterator i = search.begin(); i != search.end(); ++i)
    {
      in_points.push_back(DPoint(i->first.x(), i->first.y(), i->first.z()));
    }
    
    size_t d_fitting = 4;
    size_t d_monge = 4;

    if (in_points.size() != 16+1)
      return boost::tuple<Matrix3, Vector3, coord_t>();
    
    Monge_form monge_form;
    Monge_via_jet_fitting monge_fit;
    monge_form = monge_fit(in_points.begin(), in_points.end(), d_fitting, d_monge);

//    NUKLEI_LOG("vertex : " << in_points[0] << std::endl
//        << "number of points used : " << in_points.size() << std::endl
//        << monge_form << "condition_number : " << monge_fit.condition_number() << std::endl
//         << "pca_eigen_vals and associated pca_eigen_vecs :"  << std::endl <<
//            "first eval: " << monge_fit.pca_basis(0).first << "; first evec: " << monge_fit.pca_basis(0).second)

    
    Matrix3 eigenVectors;
    Vector3 eigenValues;
    coord_t conditionNumber;
    
    for (int i = 0; i < 3; i++)
    {
      Vector3 v(monge_fit.pca_basis(i).second[0],
                monge_fit.pca_basis(i).second[1],
                monge_fit.pca_basis(i).second[2]);
      eigenVectors.SetColumn(i, v);
      eigenValues[i] = monge_fit.pca_basis(i).first;
    }
        
    conditionNumber = monge_fit.condition_number();
    NUKLEI_ASSERT(conditionNumber != 0);
    
    return boost::make_tuple(eigenVectors, eigenValues, conditionNumber);
    
#else
    NUKLEI_THROW("This function requires CGAL. See http://nuklei.sourceforge.net/doxygen/group__install.html");
#endif
    
    NUKLEI_TRACE_END();
  }

  void KernelCollection::computeSurfaceNormals(const kernel::base::Type& type)
  {
#define NUKLEI_PCL_SURFACE_NORMAL_COMPUTATION
#if defined(NUKLEI_PCL_SURFACE_NORMAL_COMPUTATION) and defined(NUKLEI_USE_PCL)
    //fixme: should convert to organized pcl pointcloud if kc is organized
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>());
    pcl::PointCloud<pcl::Normal>::Ptr normals(new pcl::PointCloud<pcl::Normal>());
    for (const auto& i : *this)
    {
      cloud->push_back(pclCopy(i.getLoc()));
    }
    
    pcl::search::Search<pcl::PointXYZ>::Ptr tree;
    if (cloud->height == 1)
      tree.reset(new pcl::search::KdTree<pcl::PointXYZ>);
    else
      tree.reset(new pcl::search::OrganizedNeighbor<pcl::PointXYZ>);
    
    {
      pcl::NormalEstimationOMP<pcl::PointXYZ, pcl::Normal> ne;
      ne.setInputCloud(cloud);
      ne.setSearchMethod(tree);
      //ne.setViewPoint(0, 0, 0);
      //ne.setRadiusSearch(radius);
      ne.setKSearch(16+1);
      ne.compute(*normals);
    }

    KernelCollection kc2;
    
    ProgressIndicator pi(as_const(this)->size(), "Computing surface normals: ", Log::INFO);
    int skipped = 0;
    for (KernelCollection::const_iterator i = as_const(*this).begin();
         i != as_const(*this).end(); ++i)
    {
      const unsigned idx = std::distance(as_const(*this).begin(), i);
      Vector3 normal = wmNormalCopy(normals->at(idx));
      if (std::fabs(normal.SquaredLength()-1.) > 1e-3)
      {
        skipped++;
        continue;
      }
      normal = la::normalized(normal);
      if (type == kernel::base::R3XS2P)
      {
        kernel::r3xs2p k;
        k.loc_ = i->getLoc();
        k.dir_ = normal;
        kc2.add(k);
      }
      else if (type == kernel::base::R3XS2)
      {
        kernel::r3xs2 k;
        k.loc_ = i->getLoc();
        k.dir_ = normal;
        if (k.dir_.Z() < 0) k.dir_ = -k.dir_;
        kc2.add(k);
      }
      else NUKLEI_THROW("Unsupported kernel type.");
      kernel::base& k = kc2.back();
      k.setWeight(i->getWeight());
      if (i->hasDescriptor()) k.setDescriptor(i->getDescriptor());
      pi.inc();
    }
    if (skipped > 0)
      NUKLEI_WARN("Skipped " << skipped << " kernels for which "
                  "PCL couldn't compute local diff.");
    *this = kc2;
#else
    KernelCollection kc2;
    
    if (!deco_.has_key(NSTREE_KEY))
      NUKLEI_THROW("Undefined neighbor search tree. "
                   "Call buildNeighborSearchTree() first.");
    
    ProgressIndicator pi(as_const(this)->size(), "Computing surface normals: ", Log::INFO);
    int skipped = 0;
    for (KernelCollection::const_iterator i = as_const(*this).begin();
         i != as_const(*this).end(); ++i)
    {
      boost::tuple<Matrix3, Vector3, coord_t> dp =
      localLocationDifferential(i->getLoc());
      if (dp.get<2>() == 0)
      {
        skipped++;
        continue;
      }
      
      if (type == kernel::base::R3XS2P)
      {
        kernel::r3xs2p k;
        k.loc_ = i->getLoc();
        k.dir_ = dp.get<0>().GetColumn(2);
        kc2.add(k);
      }
      else if (type == kernel::base::R3XS2)
      {
        kernel::r3xs2 k;
        k.loc_ = i->getLoc();
        k.dir_ = dp.get<0>().GetColumn(2);
        if (k.dir_.Z() < 0) k.dir_ = -k.dir_;
        kc2.add(k);
      }
      else NUKLEI_THROW("Unsupported kernel type.");
      kernel::base& k = kc2.back();
      k.setWeight(i->getWeight());
      if (i->hasDescriptor()) k.setDescriptor(i->getDescriptor());
      pi.inc();
    }
    if (skipped > 0)
      NUKLEI_WARN("Skipped " << skipped << " kernels for which "
                  "CGAL couldn't compute local diff.");
    *this = kc2;
#endif
  }

  void KernelCollection::convertSurfaceNormals(const kernel::base::Type& type)
  {
    KernelCollection kc2;
    kernel::base::Type tt = kernelType();
    if (tt == type) return;
    
    for (KernelCollection::const_iterator i = as_const(*this).begin();
         i != as_const(*this).end(); ++i)
    {
      if (type == kernel::base::R3XS2P)
      {
        kernel::r3xs2p k;
        kernel::r3xs2 tk(*i);
        k.loc_ = tk.loc_;
        k.dir_ = tk.dir_;
        kc2.add(k);
      }
      else if (type == kernel::base::R3XS2)
      {
        kernel::r3xs2p k;
        kernel::r3xs2 tk(*i);
        k.loc_ = tk.loc_;
        k.dir_ = tk.dir_;
        if (k.dir_.Z() < 0) k.dir_ = -k.dir_;
        kc2.add(k);
      }
      else NUKLEI_THROW("Unsupported kernel type.");
      kernel::base& k = kc2.back();
      k.setWeight(i->getWeight());
      if (i->hasDescriptor()) k.setDescriptor(i->getDescriptor());
    }
    *this = kc2;
  }
  
  void KernelCollection::removeNormalsOrOrientations()
  {
    KernelCollection kc2;
    for (KernelCollection::const_iterator i = as_const(*this).begin();
         i != as_const(*this).end(); ++i)
    {
      kernel::r3 k;
      k.loc_ = i->getLoc();
      k.setWeight(i->getWeight());
      if (i->hasDescriptor()) k.setDescriptor(i->getDescriptor());
      kc2.add(k);
    }
    *this = kc2;
  }
}


