#include "wall_alignment/wall_alignment.h"

namespace wall_alignment
{

WallAlignmentNode::WallAlignmentNode(ros::NodeHandle &nh, dynamic_reconfigure::Server<Config> &dsrv)  :
  getWallAlignedPose_aserver_(nh, "get_wall_aligned_pose")
  , got_pointcloud_(false)
  , desired_wall_distance_(0.5)
  , got_tfs_(false)
{
  // Dynamic Reconfigure
  dynamic_reconfigure::Server<Config>::CallbackType dsrv_cb;
  dsrv_cb = boost::bind(&WallAlignmentNode::DynRecCallback, this, _1, _2);
  dsrv.setCallback(dsrv_cb);

  //init class attributes if necessary

  // Get launch parameters
  std::string topic_pointcloud;
  nh.param<std::string>("topic_pointcloud", topic_pointcloud, "pointcloud_sample");
  nh.param<std::string>("robot_base_link", this->robot_base_link_, "base_link");

  // [init publishers]
  
  // [init subscribers]
  this->pointcloud_subscriber_ = nh.subscribe(topic_pointcloud, 1, &WallAlignmentNode::pointcloud_callback, this);
  pthread_mutex_init(&this->pointcloud_mutex_,NULL);
  
  // [init services]
  
  // [init clients]
  
  // [init action servers]
  getWallAlignedPose_aserver_.registerStartCallback(boost::bind(&WallAlignmentNode::getWallAlignedPoseStartCallback, this, _1));
  getWallAlignedPose_aserver_.registerStopCallback(boost::bind(&WallAlignmentNode::getWallAlignedPoseStopCallback, this));
  getWallAlignedPose_aserver_.registerIsFinishedCallback(boost::bind(&WallAlignmentNode::getWallAlignedPoseIsFinishedCallback, this));
  getWallAlignedPose_aserver_.registerHasSucceedCallback(boost::bind(&WallAlignmentNode::getWallAlignedPoseHasSucceededCallback, this));
  getWallAlignedPose_aserver_.registerGetResultCallback(boost::bind(&WallAlignmentNode::getWallAlignedPoseGetResultCallback, this, _1));
  getWallAlignedPose_aserver_.registerGetFeedbackCallback(boost::bind(&WallAlignmentNode::getWallAlignedPoseGetFeedbackCallback, this, _1));
  getWallAlignedPose_aserver_.start();
  this->getWallAlignedPose_active=false;
  this->getWallAlignedPose_succeeded=false;
  this->getWallAlignedPose_finished=false;

  // [init action clients]
}

WallAlignmentNode::~WallAlignmentNode(void)
{
  // [free dynamic memory]
  pthread_mutex_destroy(&this->pointcloud_mutex_);
}

void WallAlignmentNode::mainNodeThread(void)
{
  if (!this->getWallAlignedPose_active)
  {
    if (!this->got_tfs_ && this->got_pointcloud_)
    {
      // Get tf between sensor and base_link
      while (!this->fcs_.getTfTransform(this->robot_base_link_,
        this->pointcloud_msg_->header.frame_id,
        this->pointcloud_msg_->header.stamp,
        ros::Duration(1.0),
        this->bl_T_sensor_)){}

      // ack
      this->got_tfs_ = true;
    }
    return;
  }

  // // TEMP
  // if (this->getWallAlignedPose_active)
  // {
  //   // FIXME: test
  //   this->aligned_pose_.header.stamp = ros::Time::now();
  //   this->aligned_pose_.header.frame_id = "uav1/base_link";
  //   this->aligned_pose_.pose.position.x = 0.5;
  //   this->aligned_pose_.pose.orientation.w = 1.0;

  //   // To finish the action server with success
  //   this->getWallAlignedPose_succeeded=true;
  //   this->getWallAlignedPose_finished=true;
  // }


  // [fill msg structures]
  
  // [fill srv structure and make request to the server]
  
  // [fill action structure and make request to the action server]
  // In the following we don't care if action is not active.
  if (this->getWallAlignedPose_active && !this->got_pointcloud_)
  {
    ROS_ERROR("[WallAlignmentNode]: Action was activated but no PointCloud was received. Finishing action.");
    // To finish the action server with failure
    this->getWallAlignedPose_succeeded=false;
    this->getWallAlignedPose_finished=true;
    this->getWallAlignedPose_active=false;
  }
  else if (this->getWallAlignedPose_active && this->got_pointcloud_)
  {
    this->pointcloud_mutex_enter();

    // Convert to PCL
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_in(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::fromROSMsg (*this->pointcloud_msg_, *cloud_in);
    this->pointcloud_mutex_exit();

    // Get min and max, assuming all point will belong to the wall
    Eigen::Vector4f min3D;
    Eigen::Vector4f max3D;
    pcl::getMinMax3D (*cloud_in, min3D, max3D);

    // Get yaw wrt to min and max
    double diff_z = min3D(2) - max3D(2);
    double yaw = 0.0;
    if (std::abs(diff_z) > 1e-3)
    {
      double diff_x = min3D(0) - max3D(0);
      double yaw_est = -atan(diff_z / diff_x);
      // Security check. We don't want to align more than 30 deg (approx)
      if (std::abs(yaw_est) < 0.5)
        yaw = yaw_est;
    }

    // Get depth
    double depth = max3D(2) - this->desired_wall_distance_;

    // Convert to pose
    std::vector<double> vec(6, 0.0);
    vec[2] = depth;
    vec[4] = yaw;
    HTransf sensor_T_wp_sensor = this->fcs_.toEigen(vec);
    HTransf sensor_T_wp_bl = this->bl_T_sensor_ * sensor_T_wp_sensor * this->bl_T_sensor_.inverse();
    this->fcs_.eigenToPoseStamped(sensor_T_wp_bl, this->aligned_pose_);
    this->aligned_pose_.header = this->pointcloud_msg_->header;
    this->aligned_pose_.header.frame_id = this->robot_base_link_;

    // To finish the action server with failure
    // this->getWallAlignedPose_succeeded=false;
    // this->getWallAlignedPose_finished=true;

    // To finish the action server with success
    this->getWallAlignedPose_succeeded=true;
    this->getWallAlignedPose_finished=true;

    // Set to inactive to avoid spin conflicts
    this->getWallAlignedPose_active = false;
  }

  // [publish messages]
}

/*  [subscriber callbacks] */
void WallAlignmentNode::pointcloud_callback(const sensor_msgs::PointCloud2::ConstPtr& msg)
{
  ROS_DEBUG("WallAlignmentNode::pointcloud_callback: New Message Received");

  //use appropiate mutex to shared variables if necessary
  this->pointcloud_mutex_enter();
  // TODO: Check this copy here
  this->pointcloud_msg_ = msg;
  this->got_pointcloud_ = true;
  this->pointcloud_mutex_exit();
}

void WallAlignmentNode::pointcloud_mutex_enter(void)
{
  pthread_mutex_lock(&this->pointcloud_mutex_);
}

void WallAlignmentNode::pointcloud_mutex_exit(void)
{
  pthread_mutex_unlock(&this->pointcloud_mutex_);
}


/*  [service callbacks] */

/*  [action callbacks] */
void WallAlignmentNode::getWallAlignedPoseStartCallback(const wall_alignment::getWallAlignedPoseGoalConstPtr& goal)
{
  lock();
  //check goal
  this->getWallAlignedPose_active=true;
  this->getWallAlignedPose_succeeded=false;
  this->getWallAlignedPose_finished=false;

  this->desired_wall_distance_ = goal->wall_distance;
  //execute goal
  unlock();
}

void WallAlignmentNode::getWallAlignedPoseStopCallback(void)
{
  lock();
  //stop action
  this->getWallAlignedPose_active=false;
  unlock();
}

bool WallAlignmentNode::getWallAlignedPoseIsFinishedCallback(void)
{
  bool ret = false;

  lock();
  //if action has finish for any reason
  ret = this->getWallAlignedPose_finished;
  unlock();

  return ret;
}

bool WallAlignmentNode::getWallAlignedPoseHasSucceededCallback(void)
{
  bool ret = false;

  lock();
  //if goal was accomplished
  ret = this->getWallAlignedPose_succeeded;
  this->getWallAlignedPose_active=false;
  unlock();

  return ret;
}

void WallAlignmentNode::getWallAlignedPoseGetResultCallback(wall_alignment::getWallAlignedPoseResultPtr& result)
{
  lock();
  //update result data to be sent to client
  result->aligned_pose = this->aligned_pose_;
  unlock();
}

void WallAlignmentNode::getWallAlignedPoseGetFeedbackCallback(wall_alignment::getWallAlignedPoseFeedbackPtr& feedback)
{
  lock();
  //update feedback data to be sent to client
  //ROS_INFO("feedback: %s", feedback->data.c_str());
  unlock();
}


/*  [action requests] */

void WallAlignmentNode::DynRecCallback(Config &config, uint32_t level)
{
  this->lock();
  this->config_=config;
  this->unlock();
}

} //end of namespace