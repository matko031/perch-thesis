#include "wall_alignment/wall_alignment_node.h"

/* main function */
int main(int argc,char *argv[])
{
  ros::init(argc, argv, "wall_alignment");
  ros::NodeHandle nh(ros::this_node::getName());
  ros::Rate loop_rate(50);

  dynamic_reconfigure::Server<wall_alignment::Config> dsrv;    

  wall_alignment::WallAlignmentNode node(nh, dsrv);

  while (ros::ok()) 
  {
    node.mainNodeThread();
    ros::spinOnce();
    loop_rate.sleep();
  }

  return 0;
}
