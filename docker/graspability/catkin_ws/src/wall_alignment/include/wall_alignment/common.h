// Copyright (C)
// All rights reserved.
//
// This file is part of a free software package: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
// 
// IMPORTANT NOTE: This code has been generated through a script. 
// Please do NOT delete any comments to guarantee the correctness
// of the scripts. ROS topics can be easly add by using those scripts. 

#ifndef _wall_alignment_common_h_
#define _wall_alignment_common_h_

#include <wall_alignment/WallAlignmentConfig.h>

#include <pthread.h>
#include <Eigen/Dense>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/common/common.h>
#include <geometry_msgs/PoseStamped.h>
#include <tf2_ros/transform_listener.h>
#include <geometry_msgs/TransformStamped.h>
#include <tf_conversions/tf_eigen.h>
#include <eigen_conversions/eigen_msg.h>

//include wall_alignment_alg main library

namespace wall_alignment
{

typedef Eigen::Transform<double, 3, Eigen::Affine> HTransf;

/**
 * \brief define config type
 *
 * Define a Config type with the WallAlignmentConfig. 
 * All implementations will then use the same variable type Config.
 */
typedef wall_alignment::WallAlignmentConfig Config;

/**
 * \brief Mutex class
 *
 */
class CMutex
{  
  protected:

    /**
     * \brief define config type
     *
     * Define a Config type with the WallAlignmentConfig. All driver implementations
     * will then use the same variable type Config.
     */
    pthread_mutex_t access_;
  
  public:

    CMutex()
    {
      pthread_mutex_init(&this->access_,NULL);
    };
    ~CMutex()
    {
      pthread_mutex_destroy(&this->access_);
    };
    /**
     * \brief Lock Algorithm
     *
     * Locks access to the Algorithm class
     */
    void lock(void) { pthread_mutex_lock(&this->access_); };
  
    /**
     * \brief Unlock Algorithm
     *
     * Unlocks access to the Algorithm class
     */
    void unlock(void) { pthread_mutex_unlock(&this->access_); };
  
    /**
     * \brief Tries Access to Algorithm
     *
     * Tries access to Algorithm
     * 
     * \return true if the lock was adquired, false otherwise
     */
    bool try_enter(void) 
    { 
      if(pthread_mutex_trylock(&this->access_)==0)
        return true;
      else
        return false;
    };  
};

class Functions
{
public:

  Functions() :
  tfListener_(tfBuffer_)
  {};

  // TF
  tf2_ros::Buffer tfBuffer_;
  tf2_ros::TransformListener tfListener_;

  HTransf toEigen(const std::vector<double>& _vec)
  {
    Eigen::Translation<double, 3> p(_vec[0],
                                    _vec[1],
                                    _vec[2]);
    HTransf eigenT;
    if (_vec.size() > 6)
    {
      Eigen::Quaterniond q(_vec[3],
                           _vec[4],
                           _vec[5],
                           _vec[6]);
      eigenT = p * q;
    }
    else
    {
      Eigen::Quaterniond q;
      q = Eigen::AngleAxisd(_vec[5], Eigen::Vector3d::UnitZ())
          * Eigen::AngleAxisd(_vec[4], Eigen::Vector3d::UnitY())
          * Eigen::AngleAxisd(_vec[3], Eigen::Vector3d::UnitX());
      eigenT = p * q;
    }
    return eigenT;
  }

  std::vector<double> fromEigen(const HTransf& _transf)
  {
    std::vector<double> vec(7);
    Eigen::Quaterniond q(_transf.rotation());
    vec[0] = _transf.translation().x();
    vec[1] = _transf.translation().y();
    vec[2] = _transf.translation().z();
    vec[3] = q.w();
    vec[4] = q.x();
    vec[5] = q.y();
    vec[6] = q.z();
    return vec;
  }

  void poseStampedToEigen(const geometry_msgs::PoseStamped& _pose, HTransf& _eigenT)
  {
    Eigen::Translation<double,3> p(_pose.pose.position.x,
                                   _pose.pose.position.y,
                                   _pose.pose.position.z);

    Eigen::Quaterniond q(_pose.pose.orientation.w,
                         _pose.pose.orientation.x,
                         _pose.pose.orientation.y,
                         _pose.pose.orientation.z);

    _eigenT = p*q;
  }

  void eigenToPoseStamped(const HTransf& _eigenT, geometry_msgs::PoseStamped& _pose)
  {
    _pose.pose.position.x = _eigenT.translation().x();
    _pose.pose.position.y = _eigenT.translation().y();
    _pose.pose.position.z = _eigenT.translation().z();
    Eigen::Quaterniond quat(_eigenT.rotation());
    _pose.pose.orientation.w = quat.w();
    _pose.pose.orientation.x = quat.x();
    _pose.pose.orientation.y = quat.y();
    _pose.pose.orientation.z = quat.z();
  }

  void transformStampedToEigen(const geometry_msgs::TransformStamped& _transform, HTransf& _eigenT)
  {
    Eigen::Translation<double,3> p(_transform.transform.translation.x,
                                   _transform.transform.translation.y,
                                   _transform.transform.translation.z);

    Eigen::Quaterniond q(_transform.transform.rotation.w,
                         _transform.transform.rotation.x,
                         _transform.transform.rotation.y,
                         _transform.transform.rotation.z);

    _eigenT = p*q;
  }

  void eigenToTransformStamped(const HTransf& _eigenT, geometry_msgs::TransformStamped& _transform)
  {
    geometry_msgs::Pose pose;
    tf::poseEigenToMsg(_eigenT, pose);

    _transform.transform.translation.x = pose.position.x;
    _transform.transform.translation.y = pose.position.y;
    _transform.transform.translation.z = pose.position.z;
    _transform.transform.rotation.w = pose.orientation.w;
    _transform.transform.rotation.x = pose.orientation.x;
    _transform.transform.rotation.y = pose.orientation.y;
    _transform.transform.rotation.z = pose.orientation.z;
  }

  /**
   * \brief Get TF transform
   */
  bool getTfTransform(const std::string& _parent_frame, const std::string& _child_frame, const ros::Time& _stamp, const ros::Duration& _timeout, HTransf& _eig_trans)
  {
    try
    {
      geometry_msgs::TransformStamped transformStamped = this->tfBuffer_.lookupTransform(_parent_frame, _child_frame, _stamp, _timeout);
      transformStampedToEigen(transformStamped, _eig_trans);
      return true;
    }
    catch (tf2::TransformException ex)
    {
      ROS_ERROR("Cannot get tf transform: %s", ex.what());
      return false;
    }
  }
};


} //end of namespace

#endif
