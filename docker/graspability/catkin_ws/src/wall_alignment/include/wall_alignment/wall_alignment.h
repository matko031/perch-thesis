// Copyright (C)
// All rights reserved.
//
// This file is part of a free software package: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
// 
// IMPORTANT NOTE: This code has been generated through a script. 
// Please do NOT delete any comments to guarantee the correctness
// of the scripts. ROS topics can be easly add by using those scripts. 

#ifndef _wall_alignment_h_
#define _wall_alignment_h_

#include "wall_alignment/common.h"

// ROS
#include <ros/ros.h>
#include <ros/package.h>

// boost
#include <boost/bind.hpp>

// dynamic reconfigure server include
#include <dynamic_reconfigure/server.h>

// [publisher subscriber headers]
#include <sensor_msgs/PointCloud2.h>

// [service client headers]

// [action server client headers]
#include <wall_alignment/action_server.h>
#include <wall_alignment/getWallAlignedPoseAction.h>

namespace wall_alignment
{

/**
 * \brief Algorithm WallAlignmentNode
 *
 */
class WallAlignmentNode
{
  private:

    Functions fcs_;

    bool got_pointcloud_;
    double desired_wall_distance_;

    bool got_tfs_;
    HTransf bl_T_sensor_;
    std::string robot_base_link_;

    // [publisher attributes]

    // [subscriber attributes]
    sensor_msgs::PointCloud2::ConstPtr pointcloud_msg_;
    ros::Subscriber pointcloud_subscriber_;
    void pointcloud_callback(const sensor_msgs::PointCloud2::ConstPtr& msg);
    pthread_mutex_t pointcloud_mutex_;
    void pointcloud_mutex_enter(void);
    void pointcloud_mutex_exit(void);

    // [service attributes]

    // [client attributes]

    // [action server attributes]
    geometry_msgs::PoseStamped aligned_pose_;
    ActionServer<wall_alignment::getWallAlignedPoseAction> getWallAlignedPose_aserver_;
    void getWallAlignedPoseStartCallback(const wall_alignment::getWallAlignedPoseGoalConstPtr& goal);
    void getWallAlignedPoseStopCallback(void);
    bool getWallAlignedPoseIsFinishedCallback(void);
    bool getWallAlignedPoseHasSucceededCallback(void);
    void getWallAlignedPoseGetResultCallback(wall_alignment::getWallAlignedPoseResultPtr& result);
    void getWallAlignedPoseGetFeedbackCallback(wall_alignment::getWallAlignedPoseFeedbackPtr& feedback);
    bool getWallAlignedPose_active;
    bool getWallAlignedPose_succeeded;
    bool getWallAlignedPose_finished;


    // [action client attributes]

    CMutex mutex_;
    void lock(void) { this->mutex_.lock(); };
    void unlock(void) { this->mutex_.unlock(); };
    bool try_enter(void) { return this->mutex_.try_enter(); }; 

   /**
    * \brief config variable
    *
    * This variable has all the driver parameters defined in the cfg config file.
    * Is updated everytime function config_update() is called.
    */
    Config config_;

  public:
   /**
    * \brief Constructor
    * 
    * This constructor initializes specific class attributes and all ROS
    * communications variables to enable message exchange.
    */
    WallAlignmentNode(ros::NodeHandle &nh, dynamic_reconfigure::Server<Config> &dsrv);

   /**
    * \brief Destructor
    * 
    * This destructor frees all necessary dynamic memory allocated within this
    * this class.
    */
    ~WallAlignmentNode(void);

   /**
    * \brief main node thread
    *
    * This is the main thread node function. Code written here will be executed
    * in every node loop while the algorithm is on running state. Loop frequency 
    * can be tuned by modifying loop rate attribute.
    */
    void mainNodeThread(void);

  protected:

   /**
    * \brief dynamic reconfigure server callback
    * 
    * This method is called whenever a new configuration is received through
    * the dynamic reconfigure. The derivated generic algorithm class must 
    * implement it.
    *
    * \param config an object with new configuration from all algorithm 
    *               parameters defined in the config file.
    * \param level  integer referring the level in which the configuration
    *               has been changed.
    */
    void DynRecCallback(Config &config, uint32_t level=0);

    // [diagnostic functions]
    
    // [test functions]
};

} //end of namespace

#endif
