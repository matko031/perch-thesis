# LEMG: LEMUR Graspability Lib

Author: Renaud Detry <detry@jpl.nasa.gov>

Repo: https://207.151.220.196:9010/climbingrobots/lemg

## Install:

To compile, you'll need

- Nuklei: https://207.151.220.196:9010/robotgrasping/nuklei

  The github-fn Nuklei repo holds a fork of http://nuklei.sf.net, with mods that support LEMG.

  Nuklei documentation is available here:
  
    http://nuklei.sourceforge.net/doxygen/group__install.html

  In short, you need the following packages:
  
      sudo apt-get install gcc g++ make pkg-config python libgsl0-dev libblas-dev liblapack-dev libboost-all-dev libcgal-dev libpcl-1.7-all-dev
  
  Use the jpl/lemur branch of Nuklei:
  
      git checkout jpl/lemur

  To support LEMG, you need to build Nuklei with CGAL and PCL support. Build with

      ./scons.py use_pcl=yes use_cgal=yes prefix=/usr/local
      sudo -E ./scons.py install

- PCL: http://pointclouds.org

## Usage

To use LEMG for graspability computation, look at `bow_evaluate_classifier()`. This function computes a `std::vector<double>` of graspability values for a given point cloud. The graspability values are in the same order as the input point cloud.
