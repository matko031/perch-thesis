#!/bin/bash

set -e

[ -e ~/bin/im_dyld ] && . im_dyld

center="$( cat $1.txt | grep 'center:' | awk '{ print $2 " " $3 " " $4 }')"
radius="$( cat $1.txt | grep 'radius:' | awk '{ print $2 }')"

output=$1-without-roi.pcd

nuklei conv --nsphere_roi "$center $radius" mesh.pcd $output
