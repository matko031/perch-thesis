// (C) Copyright Renaud Detry   2007-2015.
// Distributed under the GNU General Public License and under the
// BSD 3-Clause License (See accompanying file LICENSE.txt).

/** @file */

#include <lemg/BoWModel.hpp>
#include <lemg/Labeling.hpp>

#include <pcl/conversions.h>
#include <pcl/io/ply_io.h>
#include <pcl/point_types.h>
#include <pcl/search/kdtree.h>
#include <pcl/search/organized.h>
#include <string>

#include <tclap/CmdLine.h>

#include <nuklei/KernelCollection.h>
#include <nuklei/PCLBridge.h>
#include <nuklei/Stopwatch.h>

using namespace nuklei;
using namespace lemg;

int bow_compute_descriptors(int argc, char **argv) {
  NUKLEI_TRACE_BEGIN();

  /* Parse command line arguments */

  TCLAP::CmdLine cmd(lemg::INFOSTRING);

  /* Custom arguments */

  TCLAP::UnlabeledValueArg<std::string> inFileArg("input", "Input.", true, "",
                                                  "filename", cmd);

  TCLAP::ValueArg<std::string> labelsArg("l", "labels", "Labels.", false, "",
                                         "filename", cmd);

  TCLAP::ValueArg<std::string> colorMeshArg(
      "c", "color",
      "Mesh with same structure as input mesh, but with vertex color that "
      "encode "
      "natural appearance instead of class",
      false, "", "filename", cmd);

  TCLAP::ValueArg<int> nQuadrantsArg("", "nq", "Number of Quadrants.", false,
                                     -1, "int", cmd);

  TCLAP::ValueArg<int> nCodewordsArg("", "nc", "Number of Codewords.", false,
                                     -1, "int", cmd);

  TCLAP::SwitchArg discElevationNormalizationArg(
      "e", "disc_elevation_normalization", "Normalize disc elevation.", cmd);

  TCLAP::ValueArg<int> gripperClockingArg(
      "C", "gripper_clocking",
      "For symetric grippers, select 0 (none). For grippers that are not "
      "rotationally symetric: 1 sets the gripper's x axis in plane with X, 2 "
      "sets gripper's x axis to main axis of label cluster.",
      false, 0, "int", cmd);

  TCLAP::ValueArg<int> maxNDescriptorsArg("s", "max_n_descriptors",
                                          "Caps the number of descriptors.",
                                          false, -1, "int", cmd);

  TCLAP::ValueArg<std::string> inModelArg("i", "model_in", "Model Input.",
                                          false, "", "filename", cmd);

  TCLAP::ValueArg<std::string> inImageModelArg(
      "I", "image_model_in", "Model Input.", false, "", "filename", cmd);

  TCLAP::ValueArg<std::string> outModelArg("o", "model_out", "Model Output.",
                                           false, "", "filename", cmd);

  TCLAP::ValueArg<std::string> outputPCDArg(
      "", "grasps",
      "File to which grasp placement will be written, as a PCD file.", false,
      "", "filename", cmd);

  TCLAP::ValueArg<std::string> outputArg(
      "E", "estimates_out", "Output estimates.", false, "", "filename", cmd);

  TCLAP::ValueArg<std::string> outPortableRadialDescArg(
      "d", "portable_radial_desc",
      "Directory to which radial descriptors (depth/quadrants) are written, "
      "in a portable fashion (individual files and/or H5).",
      false, "", "filename", cmd);

  TCLAP::ValueArg<std::string> viewpointArg(
      "v", "viewpoint",
      "Viewpoint (to calculate front face, and center of ROI).", false, "",
      "x y z", cmd);

  TCLAP::ValueArg<std::string> roiArg(
      "r", "roi",
      "x y z r: center and radius of region of interest. Clip outliers.", false,
      "", "string", cmd);

  cmd.parse(argc, argv);

  NUKLEI_ASSERT(0 <= gripperClockingArg.getValue() &&
                gripperClockingArg.getValue() <= 2);
  BoWModel m;
  if (!inModelArg.getValue().empty())
    m.readModel(inModelArg.getValue());
  m.setPointCloud(inFileArg.getValue());
  if (!colorMeshArg.getValue().empty())
    m.setNaturalColorPointCloud(colorMeshArg.getValue());
  if (nQuadrantsArg.getValue() > 0)
    m.setNQuadrants(nQuadrantsArg.getValue());
  if (nCodewordsArg.getValue() > 0)
    m.setNCodewords(nCodewordsArg.getValue());

  m.setGripperClocking(
      static_cast<gripper_clocking_t>(gripperClockingArg.getValue()));
  m.setDiscElevationNormalization(discElevationNormalizationArg.getValue());
  if (maxNDescriptorsArg.getValue() > 0)
    m.setMaxNDescriptors(maxNDescriptorsArg.getValue());

  if (!labelsArg.getValue().empty())
    m.setGraspability(labelsArg.getValue());
  if (!viewpointArg.getValue().empty()) {
    Vector3 v = numify<Vector3>(viewpointArg.getValue());
    m.setViewpoint(eigenCopy<double>(v));
  }
  if (!roiArg.getValue().empty()) {
    Vector3 v = numify<Vector3>(roiArg.getValue(), false);
    std::istringstream iss(roiArg.getValue());
    double r = 0;
    NUKLEI_ASSERT(iss >> r >> r >> r >> r);
    if (r > 0)
      m.setRegionOfInterest(eigenCopy<double>(v), r);
  }
  m.setPortableRadialDescriptorOutput(outPortableRadialDescArg.getValue());
  m.setClearImageDescriptors(inImageModelArg.getValue().empty());
  m.computeDescriptors();
  if (!inImageModelArg.getValue().empty()) {
    std::vector<double> estimates =
        m.testImageModel(inImageModelArg.getValue());

    if (!outputArg.getValue().empty()) {
      writeLabels(outputArg.getValue(), estimates);
    }

    if (!outputPCDArg.getValue().empty()) {
      pcl::io::savePCDFileBinaryCompressed(outputPCDArg.getValue(),
                                           *m.getEvaluatedGrasps());
    }
  }
  if (!outModelArg.getValue().empty())
    m.writeModel(outModelArg.getValue());

  return 0;

  NUKLEI_TRACE_END();
}

int bow_train_classifier(int argc, char **argv) {
  NUKLEI_TRACE_BEGIN();

  /* Parse command line arguments */

  TCLAP::CmdLine cmd(lemg::INFOSTRING);

  /* Custom arguments */

  TCLAP::UnlabeledValueArg<std::string> inFileArg("input", "Input.", true, "",
                                                  "filename", cmd);

  TCLAP::ValueArg<std::string> labelsArg("l", "labels", "Labels.", true, "",
                                         "filename", cmd);

  TCLAP::ValueArg<int> nQuadrantsArg("", "nq", "Number of Quadrants.", false,
                                     -1, "int", cmd);

  TCLAP::ValueArg<int> nCodewordsArg("", "nc", "Number of Codewords.", false,
                                     -1, "int", cmd);

  TCLAP::SwitchArg discElevationNormalizationArg(
      "e", "disc_elevation_normalization", "Normalize disc elevation.", cmd);

  TCLAP::ValueArg<int> maxNDescriptorsArg("s", "max_n_descriptors",
                                          "Caps the number of descriptors.",
                                          false, -1, "int", cmd);

  TCLAP::ValueArg<std::string> inModelArg("i", "model_in", "Model Input.",
                                          false, "", "filename", cmd);

  TCLAP::ValueArg<std::string> outModelArg("o", "model_out", "Model Output.",
                                           false, "", "filename", cmd);

  TCLAP::SwitchArg forceCodebookArg("C", "force_train_codebook",
                                    "Re-train and overwrite codebook.", cmd);

  cmd.parse(argc, argv);

  BoWModel m;
  if (!inModelArg.getValue().empty())
    m.readModel(inModelArg.getValue());
  m.setPointCloud(inFileArg.getValue());
  if (nQuadrantsArg.getValue() > 0)
    m.setNQuadrants(nQuadrantsArg.getValue());
  if (nCodewordsArg.getValue() > 0)
    m.setNCodewords(nCodewordsArg.getValue());
  if (discElevationNormalizationArg.getValue() > 0)
    m.setDiscElevationNormalization(discElevationNormalizationArg.getValue());
  if (maxNDescriptorsArg.getValue() > 0)
    m.setMaxNDescriptors(maxNDescriptorsArg.getValue());
  m.setGraspability(labelsArg.getValue());

  if (forceCodebookArg.getValue()) {
    m.learnCodewords();
  }

  m.trainClassifier();

  if (!outModelArg.getValue().empty())
    m.writeModel(outModelArg.getValue());

  return 0;

  NUKLEI_TRACE_END();
}

int bow_evaluate_classifier(int argc, char **argv) {
  NUKLEI_TRACE_BEGIN();

  /* Parse command line arguments */

  TCLAP::CmdLine cmd(lemg::INFOSTRING);

  /* Custom arguments */

  TCLAP::UnlabeledValueArg<std::string> inFileArg("input", "Input.", true, "",
                                                  "filename", cmd);

  TCLAP::ValueArg<std::string> inModelArg("i", "model_in", "Model Input.",
                                          false, "", "filename", cmd);

  TCLAP::ValueArg<std::string> outModelArg("o", "model_out", "Model Output.",
                                           false, "", "filename", cmd);

  TCLAP::ValueArg<std::string> outputPCDArg(
      "", "grasps",
      "File to which grasp placement will be written, as a PCD file.", false,
      "", "filename", cmd);

  TCLAP::ValueArg<std::string> outputArg(
      "e", "estimates_out", "Output estimates.", false, "", "filename", cmd);

  TCLAP::ValueArg<std::string> labelsArg(
      "l", "labels",
      "Ground-truth labels (label values unused; allows to ignore unlabeled "
      "points)",
      false, "", "filename", cmd);

  cmd.parse(argc, argv);

  BoWModel m;
  NUKLEI_ASSERT(!inModelArg.getValue().empty())
  m.readModel(inModelArg.getValue());
  m.setPointCloud(inFileArg.getValue());
  if (!labelsArg.getValue().empty()) {
    // Discards previously-computed descriptors, limits eval to labeled points.
    m.setGraspability(labelsArg.getValue());
    m.computeDescriptors();
  }

  std::vector<double> estimates = m.testClassifier();

  if (!outModelArg.getValue().empty())
    m.writeModel(outModelArg.getValue());

  if (!outputArg.getValue().empty()) {
    writeLabels(outputArg.getValue(), estimates);
  }

  if (!outputPCDArg.getValue().empty()) {
    pcl::io::savePCDFileBinaryCompressed(outputPCDArg.getValue(),
                                         *m.getEvaluatedGrasps());
  }

  return 0;

  NUKLEI_TRACE_END();
}

int bow_compare(int argc, char **argv) {
  NUKLEI_TRACE_BEGIN();

  /* Parse command line arguments */

  TCLAP::CmdLine cmd(lemg::INFOSTRING);

  /* Custom arguments */

  TCLAP::UnlabeledValueArg<std::string> gtFileArg(
      "groundtruth", "Ground truth labels.", true, "", "filename", cmd);

  TCLAP::UnlabeledValueArg<std::string> cFileArg("result", "Computed labels.",
                                                 true, "", "filename", cmd);

  TCLAP::ValueArg<std::string> outputArg(
      "o", "output", "Distance between ground truth and computed labels.",
      false, "", "filename", cmd);

  TCLAP::ValueArg<double> thresholdArg("t", "threshold", "Decision threshold.",
                                       false, .5, "double", cmd);

  cmd.parse(argc, argv);

  std::vector<double> groundTruth, computed, difference;
  readLabels(gtFileArg.getValue(), groundTruth);
  readLabels(cFileArg.getValue(), computed);
  NUKLEI_ASSERT(groundTruth.size() == computed.size());
  difference = std::vector<double>(groundTruth.size(), -1);

  double threshold = thresholdArg.getValue();

  int success = 0, total = 0;
  double div = 0;
  for (unsigned i = 0; i < groundTruth.size(); ++i) {
    double g = groundTruth.at(i);
    double c = computed.at(i);
    if (g < 0 || c < 0)
      continue;
    difference.at(i) = 1 - std::fabs(g - c);
    div += std::fabs(g - c);
    if ((g - threshold) * (c - threshold) > 0)
      success++;
    total++;
  }

  std::cout << "Success rate: " << double(success) / total;
  std::cout << " (" << success << " out of " << total << ")\n";
  std::cout << "Average divergence: " << div / total << std::endl;

  if (!outputArg.getValue().empty()) {
    writeLabels(outputArg.getValue(), difference);
  }

  return 0;

  NUKLEI_TRACE_END();
}

int bow_filter(int argc, char **argv) {
  NUKLEI_TRACE_BEGIN();

  /* Parse command line arguments */

  TCLAP::CmdLine cmd(lemg::INFOSTRING);

  /* Custom arguments */

  TCLAP::UnlabeledValueArg<std::string> inFileArg("input", "Input.", true, "",
                                                  "filename", cmd);

  TCLAP::UnlabeledValueArg<std::string> outFileArg("output", "Output.", true,
                                                   "", "filename", cmd);

  TCLAP::ValueArg<double> probabilityThresholdArg(
      "t", "threshold", "Decision threshold.", false, .5, "double", cmd);

  TCLAP::ValueArg<int> maxNDescriptorsArg("s", "max_n_descriptors",
                                          "Caps the number of descriptors.",
                                          false, 0, "int", cmd);

  TCLAP::ValueArg<double> posThresholdArg("p", "position_threshold",
                                          "Position threshold.", false, .05,
                                          "double", cmd);

  TCLAP::ValueArg<double> oriThresholdArg("o", "orientation_threshold",
                                          "Orientation threshold.", false,
                                          M_PI / 8., "double", cmd);

  cmd.parse(argc, argv);

  std::string inputFile = inFileArg.getValue();
  pcl::PCLPointCloud2::Ptr blob(new pcl::PCLPointCloud2);
  loadCloud(inputFile, *blob);

  pcl::PointCloud<pcl::PointXYZINormal>::Ptr cloud(
      new pcl::PointCloud<pcl::PointXYZINormal>());

  fromPCLPointCloud2(*blob, *cloud);

  pcl::io::savePCDFileBinaryCompressed(
      outFileArg.getValue(),
      *filter(cloud, maxNDescriptorsArg.getValue(),
              probabilityThresholdArg.getValue(), posThresholdArg.getValue(),
              oriThresholdArg.getValue()));

  return 0;

  NUKLEI_TRACE_END();
}

int bow_test(int argc, char **argv) {
  NUKLEI_TRACE_BEGIN();

  /* Parse command line arguments */

  TCLAP::CmdLine cmd(lemg::INFOSTRING);

  /* Custom arguments */

  TCLAP::UnlabeledValueArg<std::string> inFileArg("input", "Input.", true, "",
                                                  "filename", cmd);

  TCLAP::ValueArg<int> nQuadrantsArg("", "nq", "Number of Quadrants.", false,
                                     16, "int", cmd);

  TCLAP::ValueArg<int> nCodewordsArg("", "nc", "Number of Codewords.", false,
                                     16, "int", cmd);

  TCLAP::SwitchArg discElevationNormalizationArg(
      "e", "disc_elevation_normalization", "Normalize disc elevation.", cmd);

  TCLAP::ValueArg<std::string> outputArg(
      "o", "output", "File to which the annotated surface is written.", false,
      "", "filename", cmd);

  TCLAP::ValueArg<std::string> labelsArg("l", "labels", "Labels.", false, "",
                                         "filename", cmd);

  cmd.parse(argc, argv);

  std::vector<double> labels;

  BoWModel m;
  m.setPointCloud(inFileArg.getValue());
  m.setNQuadrants(nQuadrantsArg.getValue());
  m.setNCodewords(nCodewordsArg.getValue());
  m.setDiscElevationNormalization(discElevationNormalizationArg.getValue());

  Stopwatch sw("");
  m.computeDescriptors();
  sw.lap();

  return 0;

  NUKLEI_TRACE_END();
}
