// (C) Copyright Renaud Detry   2007-2015.
// Distributed under the GNU General Public License and under the
// BSD 3-Clause License (See accompanying file LICENSE.txt).

/** @file */

#include <Eigen/Core>
#include <nuklei/Common.h>
#include <nuklei/Log.h>
#include "util.hpp"

int help(int argc, char ** argv);

struct CommandList
{
  const char* command;
  const char* help;
  int (*call) (int, char**);
};

const CommandList commands[] = {
  {
  command: "help",
  help: NULL,
  call: &help
  },
  {
  command: "compute_normals",
  help: "",
  call: &compute_normals
  },
  {
  command: "cn",
  help: "",
  call: &compute_normals
  },
//  {
//  command: "catf",
//  help: "Concatenate fields",
//  call: &concatenate_fields
//  },
  {
  command: "don",
  help: "Difference of normals",
  call: &don
  },
  {
  command: "paint",
  help: "",
  call: &paint
  },
  {
  command: "btest",
  help: "",
  call: &bow_test
  },
//  {
//  command: "bow_train_codebook",
//  help: "",
//  call: &bow_train_codebook
//  },
//  {
//  command: "bcode",
//  help: "",
//  call: &bow_train_codebook
//  },
  {
  command: "bow_train_classifier",
  help: "",
  call: &bow_train_classifier
  },
  {
  command: "btrain",
  help: "",
  call: &bow_train_classifier
  },
  {
  command: "bow_evaluate_classifier",
  help: "",
  call: &bow_evaluate_classifier
  },
  {
  command: "bclass",
  help: "",
  call: &bow_evaluate_classifier
  },
  {
  command: "labels_from_color",
  help: "",
  call: &labels_from_color
  },
  {
  command: "bcolor",
  help: "",
  call: &labels_from_color
  },
  {
  command: "lfc",
  help: "",
  call: &labels_from_color
  },
  {
  command: "bcomp",
  help: "",
  call: &bow_compare
  },
  {
  command: "bfilter",
  help: "",
  call: &bow_filter
  },
  {
  command: "bview",
  help: "",
  call: &bow_view_sim
  },
  {
  command: "bdesc",
  help: "",
  call: &bow_compute_descriptors
  },
  {
  command: "meshcc",
  help: "",
  call: &mesh_copy_color
  },
  {
  command: "meshnonzero",
  help: "",
  call: &mesh_make_color_channels_nonzero
  },
  {
  command: "view",
  help: "",
  call: &view
  },
  {
  command: "fill",
  help: "",
  call: &hole_filler
  },
  {
  command: "simplify_mesh",
  help: "",
  call: &simplify_mesh
  },
  {
  command: NULL,
  help: NULL,
  call: NULL
  }
};


int help(int argc, char ** argv)
{
  std::cout << "Usage: " << argv[0] << " FUNCTION [ARGS ...]" << std::endl;
  std::cout << "\nAvailable commands:\n";
  for (int i = 0; commands[i].command != NULL; i++)
  {
    std::cout << "  " << commands[i].command;
    if (commands[i].help)
    {
      std::cout << nuklei::Log::breakLines(std::string("  ") + commands[i].help);
    }
    std::cout << std::endl;
  }
  return EXIT_SUCCESS;
}


int main(int argc, char ** argv)
{
  try {
    if (argc < 2)
    {
      std::cerr << "Type '" << argv[0] << " help' for usage." << std::endl;
      return EXIT_FAILURE;
    }
    
    Eigen::initParallel();
    
    char* subargv[argc-1];
    for (int i = 1; i < argc; ++i)
      subargv[i-1] = argv[i];
    
    for (int i = 0; commands[i].command != NULL; i++)
    {
      if (std::string(argv[1]) == commands[i].command)
      {
        int r = commands[i].call(argc-1, subargv);
        //sleep(1000);
        return r;
      }
    }
    
    std::cerr << "Unknown command " << argv[1] << "." <<
    " Type '" << argv[0] << " help' for usage." << std::endl;
    
    return EXIT_FAILURE;
  }
  catch (std::exception &e) {
    std::cerr << "Exception caught: ";
    std::cerr << e.what() << std::endl;
    return EXIT_FAILURE;
  }
  catch (...) {
    std::cerr << "Caught unknown exception." << std::endl;
    return EXIT_FAILURE;
  }
  
}

