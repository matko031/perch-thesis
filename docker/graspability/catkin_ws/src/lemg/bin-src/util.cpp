// (C) Copyright Renaud Detry   2007-2015.
// Distributed under the GNU General Public License and under the
// BSD 3-Clause License (See accompanying file LICENSE.txt).

/** @file */

#include <lemg/Common.hpp>
#include <lemg/Labeling.hpp>
#include <lemg/ShapeIO.hpp>
#include <lemg/MeshPatching.hpp>
#include <pcl/PCLPointCloud2.h>
#include <pcl/conversions.h>
#include <pcl/io/pcd_io.h>
#include <pcl/console/print.h>
#include <pcl/console/parse.h>
#include <pcl/console/time.h>
#include <pcl/common/transforms.h>
#include <pcl/io/ply_io.h>
#include <pcl/search/organized.h>
#include <pcl/search/kdtree.h>
#include <nuklei/ObservationIO.h>
#include <nuklei/KernelCollection.h>
#include <nuklei/PCLBridge.h>
#include <cmath>

#include <tclap/CmdLine.h>

using namespace nuklei;
using namespace lemg;

int labels_from_color(int argc, char ** argv)
{
  NUKLEI_TRACE_BEGIN();
  
  /* Parse command line arguments */
  
  TCLAP::CmdLine cmd(lemg::INFOSTRING);
  
  /* Custom arguments */
  
  TCLAP::UnlabeledValueArg<std::string> inFileArg
  ("input",
   "Input PLY/PCD file, with red & green indicating success/failure.",
   true, "", "filename", cmd);
  
  TCLAP::UnlabeledValueArg<std::string> outFileArg
  ("output",
   "Output PCD file (PointXYZ).",
   false, "", "filename", cmd);
  
  TCLAP::ValueArg<std::string> labelsArg
  ("l", "labels",
   "Output labels.",
   false, "", "filename", cmd);
  
  cmd.parse( argc, argv );
  
  ShapeIO shio1;
  shio1.read(inFileArg.getValue());
  std::vector<int> labels = readLabels(shio1);
  
  if (!labelsArg.getValue().empty())
    writeLabels(labelsArg.getValue(), labels);
  
  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud
  (new pcl::PointCloud<pcl::PointXYZ>());
  *cloud = shio1.get<pcl::PointXYZ>();

  if (!outFileArg.getValue().empty())
    pcl::io::savePCDFileBinaryCompressed(outFileArg.getValue(), *cloud);
  
  return 0;
  
  NUKLEI_TRACE_END();
}

template<typename PointT>
void copyColor(ShapeIO& shio1, ShapeIO& shio2, const bool copyAllColorsArg,
               const bool originalColorWeightArg)
{
  
  typename pcl::PointCloud<PointT>::Ptr
  cloud1(new pcl::PointCloud<PointT>()),
  cloud2(new pcl::PointCloud<PointT>());
  *cloud1 = shio1.get<PointT>();
  *cloud2 = shio2.get<PointT>();
  
  typename pcl::search::Search<PointT>::Ptr tree2;
  
  {
    if (cloud2->height == 1)
      tree2.reset(new pcl::search::KdTree<PointT>);
    else
      tree2.reset(new pcl::search::OrganizedNeighbor<PointT>);
    tree2->setInputCloud(cloud2);
  }
  
  for (auto& p1 : *cloud1)
  {
    unsigned N = 2;
    if (copyAllColorsArg) N = 1;
    
    std::vector<int> idx;
    std::vector<float> d2;
    int nNeighbor = tree2->nearestKSearch(p1,
                                          N,
                                          idx,
                                          d2);
    NUKLEI_ASSERT(idx.size() == N && d2.size() == N && nNeighbor == N);
    
    if (copyAllColorsArg)
    {
      auto& p2 = cloud2->at(idx.front());
      double ocw = originalColorWeightArg;
      p1.r = ocw*p1.r + (1-ocw)*p2.r;
      p1.g = ocw*p1.g + (1-ocw)*p2.g;
      p1.b = ocw*p1.b + (1-ocw)*p2.b;
    }
    else
    {
      int n_blank = 0, n_tot = 0;
      std::vector<int> labeled_neighbors(N_CLASSES, 0);
      
      for (unsigned i = 0; i < N; ++i)
      {
        if (d2.at(i) > .03*.03)
          continue;
        n_tot++;
        
        const PointT& p2 = cloud2->at(idx.at(i));
        if (isLabeled(p2))
        {
          int label = labelFromColor(p2);
          NUKLEI_ASSERT(label >= 0 && label < N_CLASSES);
          labeled_neighbors.at(label)++;
        }
        else
        {
          n_blank++;
        }
      }
      
      if (n_tot == 0) continue;
      
      for (unsigned c = 0; c < labeled_neighbors.size(); c++)
      {
        if (double(labeled_neighbors.at(c)) / n_tot > .5)
        {
          writeLabel(p1, c);
        }
      }
    }
  }
  
  shio1.cat(*cloud1);
}

int mesh_copy_color(int argc, char ** argv)
{
  NUKLEI_TRACE_BEGIN();
  
  /* Parse command line arguments */
  
  TCLAP::CmdLine cmd(lemg::INFOSTRING);
  
  /* Custom arguments */
  
  TCLAP::UnlabeledValueArg<std::string> inFileArg
  ("input",
   "Input PLY/PCD file with color.",
   true, "", "filename", cmd);
  
  TCLAP::ValueArg<std::string> extraFileArg
  ("a", "add",
   "Add labels.",
   true, "", "filename", cmd);

  TCLAP::ValueArg<std::string> outFileArg
  ("o", "output",
   "Output file.",
   true, "", "filename", cmd);

  TCLAP::SwitchArg copyAllColorsArg
  ("c", "copy_all",
   "Copy all colors, instead of only red, green or blue.", cmd);
  
  TCLAP::ValueArg<double> originalColorWeightArg
  ("", "original",
   "Weight of original color.",
   false, 0, "double", cmd);
  
  cmd.parse( argc, argv );
  
  std::string inputFile = inFileArg.getValue();
  std::string extraFile = extraFileArg.getValue();

  ShapeIO shio1, shio2;
  
  shio1.read(inputFile);
  shio2.read(extraFile);
  
  if (shio1.pc2HasField("rgba"))
    copyColor<pcl::PointXYZRGBA>(shio1, shio2, copyAllColorsArg.getValue(),
                                 originalColorWeightArg.getValue());
  else
    copyColor<pcl::PointXYZRGB>(shio1, shio2, copyAllColorsArg.getValue(),
                                originalColorWeightArg.getValue());

  shio1.write(outFileArg.getValue());
  
  return 0;
  
  NUKLEI_TRACE_END();
}

int mesh_make_color_channels_nonzero(int argc, char ** argv)
{
  NUKLEI_TRACE_BEGIN();
  
  /* Parse command line arguments */
  
  TCLAP::CmdLine cmd(lemg::INFOSTRING);
  
  /* Custom arguments */
  
  TCLAP::UnlabeledValueArg<std::string> inFileArg
  ("input",
   "Input PLY/PCD file with color.",
   true, "", "filename", cmd);
  
  TCLAP::ValueArg<std::string> outFileArg
  ("o", "output",
   "Output file.",
   false, "", "filename", cmd);
  
  cmd.parse( argc, argv );
  
  std::string inputFile = inFileArg.getValue();
  
  ShapeIO shio1;
  
  shio1.read(inputFile);
  
  pcl::PointCloud<pcl::PointXYZRGB>::Ptr
  cloud1(new pcl::PointCloud<pcl::PointXYZRGB>());
  *cloud1 = shio1.get<pcl::PointXYZRGB>();
  
  int nLabeled = 0;

  for (auto& p1 : *cloud1)
  {
    if (isLabeled(p1))
    {
      nLabeled++;
    }
    
    if (p1.r == 0) p1.r = 1;
    if (p1.g == 0) p1.g = 1;
    if (p1.b == 0) p1.b = 1;
  }
  
  shio1.cat(*cloud1);
  
  std::cout << "Found " << nLabeled << " points that are labels." << std::endl;

  if (!outFileArg.getValue().empty())
    shio1.write(outFileArg.getValue());
  
  return 0;
  
  NUKLEI_TRACE_END();
}


int simplify_mesh(int argc, char ** argv)
{
  NUKLEI_TRACE_BEGIN();
  
  /* Parse command line arguments */
  
  TCLAP::CmdLine cmd(lemg::INFOSTRING);
  
  /* Custom arguments */
  
  TCLAP::UnlabeledValueArg<std::string> inFileArg
  ("input",
   "Input.",
   true, "", "filename", cmd);
  
  TCLAP::UnlabeledValueArg<std::string> outFileArg
  ("output",
   "Onput.",
   true, "", "filename", cmd);
  
  TCLAP::ValueArg<double> resolutionArg
  ("r", "resolution",
   "Simplification resolution. If zero or negative, only identical verticies are merged.",
   false, 0, "float", cmd);
  cmd.parse( argc, argv );
  
  ShapeIO shio1;
  shio1.read(inFileArg.getValue());
  shio1.simplify(resolutionArg.getValue());
  shio1.write(outFileArg.getValue());
  
  return 0;
  
  NUKLEI_TRACE_END();
}

int hole_filler(int argc, char ** argv)
{
  NUKLEI_TRACE_BEGIN();
  
  /* Parse command line arguments */
  
  TCLAP::CmdLine cmd(lemg::INFOSTRING);
  
  /* Custom arguments */
  
  TCLAP::UnlabeledValueArg<std::string> inFileArg
  ("input",
   "Input.",
   true, "", "filename", cmd);
  
  TCLAP::UnlabeledValueArg<std::string> outFileArg
  ("output",
   "Output.",
   true, "", "filename", cmd);
  
  TCLAP::ValueArg<std::string> footCenterArg
  ("c", "center",
   "Center of the foot.",
   true, "", "x y z", cmd);
  
  TCLAP::ValueArg<std::string> viewpointArg
  ("v", "viewpoint",
   "Viewpoint coordinates (RPM).",
   true, "", "x y z", cmd);
  cmd.parse( argc, argv );
  
  
  pcl::PolygonMesh mesh;
  Eigen::Vector3f footCenter, viewpoint;
  
  {
    // populate mesh and center variables:
    
    footCenter = eigenCopy<float>(numify<Vector3>(footCenterArg.getValue()));
    viewpoint = eigenCopy<float>(numify<Vector3>(viewpointArg.getValue()));
    
    if (pcl::io::loadPLYFile(inFileArg.getValue(), mesh) < 0)
      throw std::runtime_error("cannot read mesh");
  }
  
  lemg::patch(mesh, footCenter, viewpoint);
  
  if (pcl::io::savePLYFileBinary(outFileArg.getValue(), mesh) < 0)
    throw std::runtime_error("cannot write mesh");
  
  return 0;
  
  NUKLEI_TRACE_END();
}
