// (C) Copyright Renaud Detry   2007-2015.
// Distributed under the GNU General Public License and under the
// BSD 3-Clause License (See accompanying file LICENSE.txt).

/** @file */

#include <string>
#include <tclap/CmdLine.h>
#include <pcl/conversions.h>
#include <pcl/point_types.h>
#include <nuklei/KernelCollection.h>
#include <lemg/Common.hpp>
#include <lemg/Labeling.hpp>

using namespace nuklei;
using namespace lemg;

int paint(int argc, char ** argv)
{
  NUKLEI_TRACE_BEGIN();
  
  /* Parse command line arguments */
  
  TCLAP::CmdLine cmd(lemg::INFOSTRING);
  
  /* Custom arguments */
  
  TCLAP::UnlabeledValueArg<std::string> inFileArg
  ("input",
   "Input.",
   true, "", "filename", cmd);
  
  TCLAP::UnlabeledValueArg<std::string> outFileArg
  ("output",
   "Output.",
   true, "", "filename", cmd);

  TCLAP::ValueArg<std::string> labelsArg
  ("l", "labels",
   "Labels.",
   true, "", "filename", cmd);

  TCLAP::ValueArg<std::string> outputImageFileArg
  ("p", "output_png",
   "File to which a painted PNG file will be written.",
   false, "", "string", cmd);

  TCLAP::ValueArg<double> highpassThresholdArg
  ("", "highpass_threshold",
   "Set all values below this threshold to 0.",
   false, 100./1000, "double", cmd);

  TCLAP::ValueArg<double> thresholdArg
  ("t", "threshold",
   "Binary output, based on the provided threshold.",
   false, -1, "double", cmd);
  
  TCLAP::ValueArg<double> originalColorWeightArg
  ("", "original",
   "Weight of original color.",
   false, .3, "double", cmd);

  cmd.parse( argc, argv );

  NUKLEI_ASSERT(false); // ensuring this is not used anymore.
  
  std::string inputFile = inFileArg.getValue();
  pcl::PCLPointCloud2::Ptr blob(new pcl::PCLPointCloud2);
  loadCloud(inputFile, *blob);
  
  pcl::PointCloud<pcl::PointXYZRGB>::Ptr colorCloud(new pcl::PointCloud<pcl::PointXYZRGB>());
  fromPCLPointCloud2(*blob, *colorCloud);
  
  std::vector<double> labels;
  readLabels(labelsArg.getValue(), labels);
  
  NUKLEI_ASSERT(labels.size() == colorCloud->size());
  
  paint(*colorCloud,
        labels,
        originalColorWeightArg.getValue(),
        thresholdArg.getValue(),
        true, true);
  
  if (!outFileArg.getValue().empty())
    pcl::io::savePCDFileBinaryCompressed(outFileArg.getValue(), *colorCloud);
  
  if (!outputImageFileArg.getValue().empty())
  {
    NUKLEI_ASSERT(false);
    if (colorCloud->height == 1)
      NUKLEI_THROW("Unorganized point cloud; cannot write image.");
    //io::PointCloudImageExtractorFromCurvatureField<Normal> pcie_normal;
    //pcie_normal.setScalingMethod(pcie_normal.SCALING_FULL_RANGE);
    //pcl::io::PointCloudImageExtractorFromRGBField<pcl::PointXYZRGB> pcie_rgb;
    //pcl::PCLImage image;
    //pcie_rgb.extract(*colorCloud, image);
    //pcl::io::savePNGFile(outputImageFileArg.getValue(), image);
  }

  return 0;
  
  NUKLEI_TRACE_END();
}

