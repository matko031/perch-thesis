// (C) Copyright Renaud Detry   2007-2015.
// Distributed under the GNU General Public License and under the
// BSD 3-Clause License (See accompanying file LICENSE.txt).

/** @file */

#include <string>
#include <tclap/CmdLine.h>
#include <pcl/conversions.h>
#include <pcl/point_types.h>
#include <nuklei/KernelCollection.h>
#include <lemg/Common.hpp>

#include <pcl/search/organized.h>
#include <pcl/search/kdtree.h>
#include <pcl/surface/mls.h>
#include <pcl/features/normal_3d_omp.h>
#include <pcl/features/normal_3d.h>
#include <pcl/PCLPointCloud2.h>
#include <pcl/io/pcd_io.h>

using namespace nuklei;
using namespace lemg;

int compute_normals(int argc, char ** argv)
{
  NUKLEI_TRACE_BEGIN();
  
  /* Parse command line arguments */
  
  TCLAP::CmdLine cmd(lemg::INFOSTRING);
  
  /* Custom arguments */
  
  TCLAP::UnlabeledValueArg<std::string> inFileArg
  ("input",
   "Input.",
   true, "", "filename", cmd);
  
  TCLAP::UnlabeledValueArg<std::string> outFileArg
  ("output",
   "Output.",
   true, "", "filename", cmd);

  TCLAP::ValueArg<coord_t> radiusArg
  ("r", "radius",
   "Search radius.",
   true, 0, "float", cmd);

  TCLAP::ValueArg<std::string> outputTypeArg
  ("t", "type",
   "normal|pointnormal|originalnormal",
   false, "normal", "string", cmd);

  cmd.parse( argc, argv );

  std::string inputFile = inFileArg.getValue();
  pcl::PCLPointCloud2::Ptr blob(new pcl::PCLPointCloud2);
  loadCloud(inputFile, *blob);
  
  double radius = radiusArg.getValue();
  
  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>());
  pcl::PointCloud<pcl::Normal>::Ptr normals(new pcl::PointCloud<pcl::Normal>());

  fromPCLPointCloud2(*blob, *cloud);
  
  pcl::search::Search<pcl::PointXYZ>::Ptr tree;
  if (cloud->height == 1)
    tree.reset(new pcl::search::KdTree<pcl::PointXYZ>);
  else
    tree.reset(new pcl::search::OrganizedNeighbor<pcl::PointXYZ>);
  
  {
    pcl::NormalEstimation<pcl::PointXYZ, pcl::Normal> ne;
    ne.setInputCloud(cloud);
    ne.setSearchMethod(tree);
    ne.setViewPoint(0, 0, 0);
    ne.setRadiusSearch(radius);
    ne.compute(*normals);
  }
  
  if (outputTypeArg.getValue() == "normal")
  {
    pcl::io::savePCDFileBinaryCompressed(outFileArg.getValue(), *normals);
  }
  else if (outputTypeArg.getValue() == "pointnormal")
  {
    pcl::PointCloud<pcl::PointNormal>::Ptr pn(new pcl::PointCloud<pcl::PointNormal>());
    pcl::concatenateFields(*cloud, *normals, *pn);
    pcl::io::savePCDFileBinaryCompressed(outFileArg.getValue(), *pn);
  }
  else if (outputTypeArg.getValue() == "originalnormal")
  {
    pcl::PCLPointCloud2::Ptr outputBlob(new pcl::PCLPointCloud2);
    pcl::PCLPointCloud2 output_normals;
    pcl::toPCLPointCloud2(*normals, output_normals);
    pcl::concatenateFields(*blob, output_normals, *outputBlob);
    pcl::PCDWriter w;
    w.writeBinaryCompressed(outFileArg.getValue(), *outputBlob);
  }
  else NUKLEI_THROW("Invalid type '" << outputTypeArg.getValue() << "'");
  
  return 0;
  
  NUKLEI_TRACE_END();
}


#include <iostream>
#include <pcl/console/time.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>

Eigen::Vector4f    translation;
Eigen::Quaternionf orientation;

////////////////////////////////////////////////////////////////////////////////
/** \brief Parse command line arguments for file names.
 * Returns: a vector with file names indices.
 * \param argc
 * \param argv
 * \param extension
 */
std::vector<int>
parseFileExtensionArgument (int argc, char** argv, std::string extension)
{
  std::vector<int> indices;
  for (int i = 1; i < argc; ++i)
  {
    std::string fname = std::string (argv[i]);
    
    // Needs to be at least 4: .ext
    if (fname.size () <= 4)
      continue;
    
    // For being case insensitive
    std::transform (fname.begin (), fname.end (), fname.begin (), tolower);
    std::transform (extension.begin (), extension.end (), extension.begin (), tolower);
    
    // Check if found
    std::string::size_type it;
    if ((it = fname.find (extension)) != std::string::npos)
    {
      // Additional check: we want to be able to differentiate between .p and .png
      if ((extension.size () - (fname.size () - it)) == 0)
        indices.push_back (i);
    }
  }
  return (indices);
}

void
saveCloud (const std::string &filename, const pcl::PCLPointCloud2 &output)
{
  using namespace pcl::console;
  TicToc tt;
  tt.tic ();
  
  print_highlight ("Saving "); print_value ("%s ", filename.c_str ());
  
  pcl::PCDWriter w;
  w.writeBinaryCompressed (filename, output, translation, orientation);
  
  print_info ("[done, "); print_value ("%g", tt.toc ()); print_info (" ms : "); print_value ("%d", output.width * output.height); print_info (" points]\n");
}

/* ---[ */
int
concatenate_fields (int argc, char** argv)
{
  if (argc < 2)
  {
    std::cerr << "Syntax is: " << argv[0] << " file.pcd file.pcd ... output.pcd" << std::endl;
    return (-1);
  }
  
  std::vector<int> file_indices = parseFileExtensionArgument (argc, argv, ".pcd");
  
  //pcl::PointCloud<pcl::PointXYZ> cloud_all;
  pcl::PCLPointCloud2 cloud_all;
  for (size_t i = 0; i < file_indices.size () - 1; ++i)
  {
    // Load the Point Cloud
    pcl::PCLPointCloud2 cloud, tmp;
    loadCloud (argv[file_indices[i]], cloud);
    //pcl::PointCloud<pcl::PointXYZ> cloud;
    //pcl::io::loadPCDFile (argv[file_indices[i]], cloud);
    //cloud_all += cloud;
    if (i == 0) tmp = cloud;
    else
      pcl::concatenateFields(cloud_all, cloud, tmp);
    cloud_all = tmp;
    PCL_INFO ("Total number of points so far: %u. Total data size: %lu bytes.\n", cloud_all.width * cloud_all.height, cloud_all.data.size ());
  }
  
  saveCloud (argv[file_indices.back()], cloud_all);
  
  return (0);
}
