// (C) Copyright Renaud Detry   2007-2015.
// Distributed under the GNU General Public License and under the
// BSD 3-Clause License (See accompanying file LICENSE.txt).

/** @file */

#include <string>
#include <tclap/CmdLine.h>
#include <pcl/conversions.h>
#include <pcl/point_types.h>
#include <nuklei/KernelCollection.h>
#include <lemg/Common.hpp>
#include <lemg/Labeling.hpp>

#include <pcl/search/organized.h>
#include <pcl/search/kdtree.h>
#include <pcl/features/don.h>

using namespace nuklei;
using namespace lemg;

int don(int argc, char ** argv)
{
  NUKLEI_TRACE_BEGIN();
  
  /* Parse command line arguments */
  
  TCLAP::CmdLine cmd(lemg::INFOSTRING);
  
  /* Custom arguments */
  
  TCLAP::UnlabeledValueArg<std::string> inFileArg
  ("input",
   "Input.",
   true, "", "filename", cmd);
  
  TCLAP::ValueArg<std::string> normals1Arg
  ("n", "normals1",
   "Small-radius normals.",
   true, "", "filename", cmd);

  TCLAP::ValueArg<std::string> normals2Arg
  ("N", "normals2",
   "Large-radius normals.",
   true, "", "filename", cmd);

  TCLAP::ValueArg<std::string> outputArg
  ("o", "output",
   "Output Difference-of-normals labels.",
   true, "", "filename", cmd);

  cmd.parse( argc, argv );

  std::string inputFile = inFileArg.getValue();
  ShapeIO shio1;
  shio1.read(inputFile);
  
  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>());
  pcl::PointCloud<pcl::Normal>::Ptr normals1(new pcl::PointCloud<pcl::Normal>());
  pcl::PointCloud<pcl::Normal>::Ptr normals2(new pcl::PointCloud<pcl::Normal>());

  fromPCLPointCloud2(shio1.pc(), *cloud);
  
  pcl::search::Search<pcl::PointXYZ>::Ptr tree;
  if (cloud->height == 1)
    tree.reset(new pcl::search::KdTree<pcl::PointXYZ>);
  else
    tree.reset(new pcl::search::OrganizedNeighbor<pcl::PointXYZ>);

  NUKLEI_ASSERT(pcl::io::loadPCDFile<pcl::Normal>(normals1Arg.getValue(), *normals1) != -1);
  NUKLEI_ASSERT(pcl::io::loadPCDFile<pcl::Normal>(normals2Arg.getValue(), *normals2) != -1);

  std::cout << "Running DifferenceOfNormalsEstimation" << std::endl;
  //PointCloud<PointNormal>::Ptr doncloud(new pcl::PointCloud<PointNormal>);
  //copyPointCloud<PointXYZ, PointNormal>(*cloud, *doncloud);
  pcl::DifferenceOfNormalsEstimation<pcl::PointXYZ, pcl::Normal, pcl::Normal> don;
  don.setInputCloud(cloud);
  don.setNormalScaleLarge(normals2);
  don.setNormalScaleSmall(normals1);
  if (!don.initCompute())
  {
    std::cerr << "Error: Could not intialize DoN feature operator" << std::endl;
    exit(EXIT_FAILURE);
  }
  don.computeFeature(*normals1);

  float maxc = 0, minc = 1;
  for (pcl::PointCloud<pcl::Normal>::const_iterator i = normals1->begin(); i != normals1->end(); ++i)
  {
    minc = std::min(minc, i->curvature);
    maxc = std::max(maxc, i->curvature);
  }
  std::cout << "Min-max DON: " << minc << "-" << maxc << std::endl;
  
  writeLabels(outputArg.getValue(), *normals1);
    
  return 0;
  
  NUKLEI_TRACE_END();
}

