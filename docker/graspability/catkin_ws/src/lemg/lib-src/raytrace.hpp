#ifndef LEMG_RAYTRACE_HPP
#define LEMG_RAYTRACE_HPP

#include <string>
#include <nanort.h>

#include <lemg/Common.hpp>
#include <lemg/ShapeIO.hpp>


namespace lemg
{
  
  struct RTracer
  {
  public:
    void construct_scene(const ShapeIO& shio,
                         const pcl::PointCloud<pcl_point_t>::Ptr cloud,
                         const ShapeIO& naturalColorShio);
    std::pair<float, bgr_t> cast_ray(float viewpoint[3], float viewdir[3]) const;
    
  private:
    nanort::BVHAccel<float> bvh_;
    std::vector<float> vertices_;
    std::vector<unsigned int> faces_;
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr colorCloud_;
  };
  
}

#endif
