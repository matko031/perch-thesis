#ifndef LEMG_COMMON_HPP
#define LEMG_COMMON_HPP

#include <Eigen/Geometry>
#include <lemg/cereal/archives/binary.hpp>
#include <opencv2/core/core.hpp>
#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/search/search.h>
#include <stdexcept>

#define LEMG_XSTR(a) LEMG_STR(a)
#define LEMG_STR(a) #a

namespace lemg {

#define LEMG_FOOT_LEMUR 0
#define LEMG_FOOT_PERCH 1
#define LEMG_FOOT LEMG_FOOT_PERCH

#define LEMG_CIRCULAR_DESCRIPTOR 1
#define LEMG_DESCRIPTOR_IMPLEMENTATION 1

const unsigned QUADRANT_DESCRIPTOR_SIZE = 3;

#if LEMG_FOOT == LEMG_FOOT_LEMUR
const float IMAGE_RES = .125 * 1000.; // pixels per m
const int IMAGE_SIZE = 32;            // pixels
// Measured foot diameter: 10.5 inch
// The hooks don't go all the way to the fingertips
// -> ~25cm
const float RADIUS = 125. / 1000;
const float RADIUS_T_SQRT2 = RADIUS * 1.4142135624;
#elif LEMG_FOOT == LEMG_FOOT_PERCH
// foot: 163mm x 43mm
const float IMAGE_RES = 1000.; // pixels per m
// const int IMAGE_SIZE = 96; // pixels
const int IMAGE_SIZE = 200; // pixels
// 16cm gripper lenght, and by contrast to lemur, we include 2 extra cm of
// data on each side, for eg collision detection.
const float RADIUS = (float)IMAGE_SIZE / IMAGE_RES;
const float RADIUS_T_SQRT2 = RADIUS * 1.4142135624;
#else
#error Unknown foot
#endif

// static_assert(RADIUS <= IMAGE_SIZE/IMAGE_RES);

// square meters per pixel
const float SQ_METERS_PER_SQ_PX =
    1 / ((IMAGE_RES * IMAGE_RES) / (IMAGE_SIZE * IMAGE_SIZE));

#define LEMG_ASSERT(expression)                                                \
  {                                                                            \
    if (!(expression))                                                         \
      throw std::runtime_error("Failed to assert `" #expression "'");          \
  }

#define LEMG_FAST_ASSERT(expression)                                           \
  {                                                                            \
    if (!(expression)) {                                                       \
      std::cout << "Failed to assert `" #expression "'" << std::endl;          \
      std::terminate();                                                        \
    }                                                                          \
  }

typedef pcl::PointNormal pcl_point_t;

//
// \brief Pixel from meter
// Given position in meters return corresponding pixel
inline int pxfm(const double m) {
  int px = IMAGE_SIZE / 2 + m * IMAGE_RES;
  LEMG_FAST_ASSERT(px >= 0 && px < IMAGE_SIZE);
  return px;
}

//
// \brief Meter from pixel
// Given a pixel, get the position in meters where px is the center
inline double mfpx(const int px) {
  LEMG_FAST_ASSERT(px >= 0);
  double m = (px - IMAGE_SIZE / 2) / IMAGE_RES;
  return m;
}

inline cv::Point cvfm(const double x, const double y) {
  return cv::Point(pxfm(x), pxfm(-y));
}

inline std::pair<double, double> mfcv(const cv::Point &p) {
  return std::make_pair(mfpx(p.x), -mfpx(p.y));
}

//
// \brief Depth from meter
// Get bounded depth value in mm given height in m
inline int dfm(const double m) {
  LEMG_FAST_ASSERT(0 <= m && m <= 2 * RADIUS);
  // stretch between 0 and 250:
  int d = m / (2. * RADIUS) * 250.;
  return d;
}
//
// \brief Meter from depth
// Convert mm depth to m
inline double mfd(const int d) { return d * (2. * RADIUS) / 250.; }

typedef Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>
    GMatrix;
typedef Eigen::Matrix<double, 2, 1> Vector2;

struct rgb_t {
  uint8_t r, g, b;
};
struct bgr_t {
  uint8_t b, g, r;
};
struct bgrx_t {
  uint8_t b, g, r, x;
};

extern const std::string INFOSTRING;

void loadCloud(const std::string &filename, pcl::PCLPointCloud2 &cloud);

class BadNormalError : public std::runtime_error {
public:
  BadNormalError(const std::string &s) : std::runtime_error(s) {}
};

struct label_cluster_t {
  label_cluster_t() : axis(Eigen::Vector3f::Zero()), id(-1) {}
  std::vector<int> pts;
  Eigen::Vector3f axis;
  int id;
};

typedef std::shared_ptr<label_cluster_t> label_cluster_ptr_t;

enum gripper_clocking_t {
  GRIPPER_CLOCK_NONE = 0,
  GRIPPER_CLOCK_X = 1,
  GRIPPER_CLOCK_CLUSTER_AXIS = 2
};
} // namespace lemg

namespace cereal {

// Note: a singel serialize() function for Vector3d doesn't compile.
//       Suspecting that it's because VectorXd derives from the same type,
//       and is using split functions.

template <class Archive>
void save(Archive &ar, Eigen::Vector3d const &v, std::uint32_t const version) {
  ar(v.x(), v.y(), v.z());
}

template <class Archive>
void load(Archive &ar, Eigen::Vector3d &v, std::uint32_t const version) {
  ar(v.x(), v.y(), v.z());
}

template <class Archive>
void save(Archive &ar, Eigen::Vector2d const &v, std::uint32_t const version) {
  ar(v.x(), v.y());
}

template <class Archive>
void load(Archive &ar, Eigen::Vector2d &v, std::uint32_t const version) {
  ar(v.x(), v.y());
}

template <class Archive>
void save(Archive &ar, Eigen::VectorXd const &v, std::uint32_t const version) {
  size_t s = v.size();
  ar(s);
  for (size_t i = 0; i < s; ++i)
    ar(v(i));
}

template <class Archive>
void load(Archive &ar, Eigen::VectorXd &v, std::uint32_t const version) {
  size_t s = 0;
  ar(s);
  v = Eigen::VectorXd(s);
  for (size_t i = 0; i < s; ++i)
    ar(v(i));
}

template <class Archive>
void serialize(Archive &ar, Eigen::Quaterniond &q,
               std::uint32_t const version) {
  ar(q.w(), q.x(), q.y(), q.z());
}

template <class Archive>
void save(Archive &ar, lemg::GMatrix const &m, std::uint32_t const version) {
  int32_t rows = m.rows();
  int32_t cols = m.cols();
  ar(rows);
  ar(cols);
  ar(binary_data(m.data(), rows * cols * sizeof(double)));
}

template <class Archive>
void load(Archive &ar, lemg::GMatrix &m, std::uint32_t const version) {
  int32_t rows;
  int32_t cols;
  ar(rows);
  ar(cols);
  m.resize(rows, cols);
  ar(binary_data(m.data(),
                 static_cast<std::size_t>(rows * cols * sizeof(double))));
}

//  template <class Archive, class _Scalar, int _Rows, int _Cols, int _Options,
//  int _MaxRows, int _MaxCols> inline typename
//  std::enable_if<traits::is_output_serializable<BinaryData<_Scalar>,
//  Archive>::value, void>::type save(Archive & ar, Eigen::Matrix<_Scalar,
//  _Rows, _Cols, _Options, _MaxRows, _MaxCols> const & m)
//  {
//    int32_t rows = m.rows();
//    int32_t cols = m.cols();
//    ar(rows);
//    ar(cols);
//    ar(binary_data(m.data(), rows * cols * sizeof(_Scalar)));
//  }
//
//  template <class Archive, class _Scalar, int _Rows, int _Cols, int _Options,
//  int _MaxRows, int _MaxCols> inline typename
//  std::enable_if<traits::is_input_serializable<BinaryData<_Scalar>,
//  Archive>::value, void>::type load(Archive & ar, Eigen::Matrix<_Scalar,
//  _Rows, _Cols, _Options, _MaxRows, _MaxCols> & m)
//  {
//    int32_t rows;
//    int32_t cols;
//    ar(rows);
//    ar(cols);
//
//    m.resize(rows, cols);
//
//    ar(binary_data(m.data(), static_cast<std::size_t>(rows * cols *
//    sizeof(_Scalar))));
//  }
} // namespace cereal

#endif
