#ifndef LEMG_BOW_MODEL_HPP
#define LEMG_BOW_MODEL_HPP

#include <pcl/conversions.h>
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>

#include <lemg/cereal/archives/binary.hpp>

#include <lemg/RadialDescriptor.hpp>
#include <lemg/KernelLogisticRegressor.hpp>
#include <lemg/ShapeIO.hpp>

namespace lemg {
  
  struct Codebook
  {
    Codebook(int nCodewords) :
    nCodewords_(nCodewords), normalized_(false)
    {
      qdMean_.setZero();
      qdStdev_.setOnes();
    }
    
    QuadrantDescriptor normalized(const QuadrantDescriptor& qd) const;
    void setNormalization(const QuadrantDescriptor& qdMean,
                          const QuadrantDescriptor& qdStdev);
    
    std::vector<int> train(const std::vector<QuadrantDescriptor>& data);
    void annotate(std::vector<Disc>& discs) const;

    int computeClass(QuadrantDescriptor qd, const bool normalize = true) const;
    
    void saveAsPointcloud(const std::string& basename,
                          const std::vector<QuadrantDescriptor>& data,
                          const std::vector<int>& labels) const;
    void verify(const std::vector<QuadrantDescriptor>& data,
                const std::vector<int>& labels) const;

    void readCodewords(const std::string& file);
    void writeCodewords(const std::string& file) const;

    const std::vector<QuadrantDescriptor>& getCodewords() const;
    
  private:
    friend class cereal::access;
    template<class Archive>
    void serialize(Archive &ar, std::uint32_t const version)
    {
      size_t qdsize = QUADRANT_DESCRIPTOR_SIZE;
      ar(CEREAL_NVP(qdsize));
      LEMG_ASSERT(qdsize == QUADRANT_DESCRIPTOR_SIZE);
      ar(CEREAL_NVP(nCodewords_),
         CEREAL_NVP(qdMean_),
         CEREAL_NVP(qdStdev_),
         CEREAL_NVP(normalized_),
         CEREAL_NVP(codewords_));
    }

    int nCodewords_;
    QuadrantDescriptor qdMean_, qdStdev_;
    bool normalized_;
    std::vector<QuadrantDescriptor> codewords_;
  };
  
  struct BoWModel
  {
    BoWModel();
    void setPointCloud(const std::string& cloudName);
    void setPointCloud(pcl::PointCloud<pcl_point_t>::Ptr cloud);
    void setPointCloud(const pcl::PolygonMesh& mesh);
    
    void setNaturalColorPointCloud(const std::string& cloudName);
    void setNaturalColorPointCloud(const pcl::PolygonMesh& mesh);

    void setNQuadrants(const int n) { nQuadrants_ = n; }
    void setNCodewords(const int n) { nCodewords_ = n; codebook_ = Codebook(n); }
    void setDiscElevationNormalization(const bool n)
    { discPositionNormalization_ = n; }
    void setGripperClocking(const gripper_clocking_t c)
    { gripperClocking_ = c; }
    void setViewpoint(const Eigen::Vector3d& v) { viewpoint_ = v; hasViewpoint_ = true; }
    
    /**
     * @brief Cap the number of descriptors that are computed from the cloud.
     *
     * This parameter affects learnCodewords(), trainClassifier(), and
     * testClassifier(). By default, those three operations compute
     * one descriptor at each point of the input point cloud. If @p n
     * is smaller than the size of @p cloud, descriptors are computed
     * only at a fraction of the points of @p cloud. This only limits
     * the number of descriptors: each descriptor is still computed
     * using all its neighbors in @p cloud.
     **/
    void setMaxNDescriptors(const int n);
    
    /**
     * @brief Sets graspability labels.
     *
     * The length of @p g should be equal to the length of @p
     * cloud. Allowed values are @$ 0 @$ (negative example), @$ 1 @$
     * (positive example), or @$ -1 @$ (no label).
     **/
    void setGraspability(const std::vector<int>& g);
    /**
     * @brief Sets graspability labels.
     *
     * The length of @p g should be equal to the length of @p
     * cloud. Allowed values are @$ 0 @$ (negative example), @$ 1 @$
     * (positive example), or @$ -1 @$ (no label). This method casts 
     * @p g into a vector of integers.
     **/
    void setGraspability(const std::vector<double>& g);
    void setGraspability(const std::string& labelName);

    void setRegionOfInterest(const Eigen::Vector3d& center,
                             const double& radius)
    { useROI_ = true; roiCenter_ = center; roiRadius_ = radius; }

    void computeDescriptors();
    void setClearImageDescriptors(const bool b) { clearImageDescriptors_ = b; }

    void readModel(const std::string& file);
    void writeModel(const std::string& file);
    
    void learnCodewords();
    void readCodewords(const std::string& file);
    void writeCodewords(const std::string& file) const;
    
    void trainClassifier();
    std::vector<double> testClassifier();
    void readClassifier(const std::string& file);
    void writeClassifier(const std::string& file) const;
    
    std::vector<double> testImageModel(const std::string& keras_model);
    
    std::vector<double> computeSimilarities(const histogram& h);
    std::vector<double> computeSimilarities(const int pointId);
    
    const Disc& getDisc(const unsigned id) const;
    const Disc& getDisc(const double x,
                        const double y,
                        const double z) const;

    
    const std::vector<double>& getClassProbabilities() const
    { return classProbabilities_; }
    const std::vector<Eigen::Vector3d>& getNormals() const
    { return normalVectors_; }
    const std::vector<double>& getCurvatures() const
    { return curvatures_; }
    const std::vector<histogram>& getHistograms() const
    { return histograms_; }

    
    // By contrast to the other get methods, getEvaluatedGrasps only returns
    // points for which graspability was computed.
    pcl::PointCloud<pcl::PointXYZINormal>::Ptr getEvaluatedGrasps() const;
    
    void setPortableRadialDescriptorOutput(const std::string f)
    { portableRadialDescriptorOutput_ = f; }
    
    const lemg::KernelLogisticRegressor& getKLR() const { return *klr_; }
    
    void writeClusteredQuadrants() const;
  private:
    friend class cereal::access;
    template<class Archive>
    void serialize(Archive &ar, std::uint32_t const version)
    {
      ar(CEREAL_NVP(nQuadrants_),
         CEREAL_NVP(nCodewords_),
         CEREAL_NVP(discPositionNormalization_),
         CEREAL_NVP(viewpoint_),
         CEREAL_NVP(hasViewpoint_));
      
      ar(CEREAL_NVP(codebook_));
      
      bool hasKlr = bool(klr_);
      ar(hasKlr);
      if (hasKlr)
      {
        if (!klr_) klr_ = lemg::KernelLogisticRegressor();
        ar(CEREAL_NVP(*klr_));
      }
      else
        klr_ = boost::none;
      
      size_t nDiscs = discs_.size();
      ar(CEREAL_NVP(nDiscs));
      discs_.resize(nDiscs);
      for (size_t i = 0; i < nDiscs; ++i)
        ar(CEREAL_NVP(discs_.at(i)));
    }

    void computeRadialDescriptors();
    void clusterLabeledPoints();
    void drawDescriptorSubset();

    pcl::PointCloud<pcl_point_t>::Ptr cloud_;
    ShapeIO shio_;
    ShapeIO naturalColorShio_;
    pcl::search::Search<pcl_point_t>::Ptr tree_;
    std::vector<int> graspabilityLabels_;
    std::vector<label_cluster_ptr_t> labelClusters_;
    bool shapeIsMesh_;
    
    std::string cloudName_;
    std::string portableRadialDescriptorOutput_;
    bool clearImageDescriptors_;
    
    int nQuadrants_;
    int nCodewords_;
    bool discPositionNormalization_;
    gripper_clocking_t gripperClocking_;
    Eigen::Vector3d viewpoint_;
    bool hasViewpoint_;
    Codebook codebook_;
    std::vector<Disc> discs_;
    int maxNDescriptors_;
    std::vector<int> inputSubsetIndices_;
    boost::optional<lemg::KernelLogisticRegressor> klr_;
    
    bool useROI_;
    Eigen::Vector3d roiCenter_;
    double roiRadius_;
    
    std::vector<double> classProbabilities_;
    std::vector<Eigen::Vector3d> normalVectors_;
    std::vector<double> curvatures_;
    std::vector<histogram> histograms_;
  };

  
  pcl::PointCloud<pcl::PointXYZINormal>::Ptr
  filter(pcl::PointCloud<pcl::PointXYZINormal>::Ptr points,
         unsigned maxN = 0,
         double qualityThreshold = .0,
         double posDistThreshold = .05,
         double oriDistThreshold = M_PI/8.);

}

CEREAL_CLASS_VERSION(lemg::Codebook, 0);
CEREAL_CLASS_VERSION(lemg::BoWModel, 0);


#endif
