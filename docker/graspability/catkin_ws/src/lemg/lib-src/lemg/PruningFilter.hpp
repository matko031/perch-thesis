#ifndef LEMG_PRUNING_FILTER_H
#define LEMG_PRUNING_FILTER_H

#include <pcl/conversions.h>
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/PolygonMesh.h>

namespace lemg
{
  pcl::PointCloud<pcl::PointXYZ>::Ptr
  pruning_filter(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud, double resolution = 0.001);
  
  pcl::PointCloud<pcl::PointXYZ>
  pruning_filter(const pcl::PointCloud<pcl::PointXYZ>& cloud, double resolution = 0.001);
  
  pcl::PolygonMesh
  pruning_filter(const pcl::PolygonMesh& mesh, double resolution = 0);
}

#endif
