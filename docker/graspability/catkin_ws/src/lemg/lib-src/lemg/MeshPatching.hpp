#ifndef LEMG_MESH_PATCHING_HPP
#define LEMG_MESH_PATCHING_HPP

#include <pcl/search/organized.h>
#include <pcl/search/kdtree.h>
#include <pcl/io/ply_io.h>
#include <pcl/conversions.h>
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/features/normal_3d.h>

namespace lemg {

  
# define HOLE_FILLER_ASSERT(expression) \
{ \
if (!(expression)) \
throw std::runtime_error \
("Failed to assert `" #expression "'"); \
}

  void patch(pcl::PolygonMesh& mesh,
             const Eigen::Vector3f& footCenter,
             const Eigen::Vector3f& viewpoint,
             const float holeRadius = .17,
             const float planeFitRadiusRatio = 2.5,
             const float tiePointRadiusRatio = 1.3,
             const float tiePointZThreshold = .75,
             const int nTiePoints = 16)
  {
    // Assuming peripheral vertices will be within searchRadius of footCenter.
    float searchRadius = holeRadius*planeFitRadiusRatio;
    
    typedef pcl::PointXYZ point_t;
    
    pcl::PointCloud<point_t>::Ptr cloud(new pcl::PointCloud<point_t>());
    pcl::fromPCLPointCloud2(mesh.cloud, *cloud);
    pcl::search::KdTree<point_t> tree;
    tree.setInputCloud(cloud);
    
    std::vector<int> tiePointVector;
    
    {
      // find nTiePoints evenly-distributed peripheral points:
      
      std::map<int, int> tiePoints; // (quadrant_id, vertex_id)
      
      std::vector<int> pointIdxRadiusSearch;
      std::vector<float> pointRadiusSquaredDistance;
      
      point_t fc;
      fc.x = footCenter.x();
      fc.y = footCenter.y();
      fc.z = footCenter.z();
      int nNeighbor = tree.radiusSearch(fc,
                                        searchRadius,
                                        pointIdxRadiusSearch,
                                        pointRadiusSquaredDistance);
      
      if (nNeighbor <= 0)
        throw std::runtime_error("found zero peripheral points");
      
      
      float curvature = -1; // std::numeric_limits<float>::quiet_NaN();
      Eigen::Vector4f plane_parameters;
      pcl::computePointNormal(*cloud, pointIdxRadiusSearch,
                              plane_parameters,
                              curvature);
      
      Eigen::Vector3f vp = viewpoint-footCenter;
      Eigen::Vector3f normal = Eigen::Vector3f(plane_parameters[0],
                                               plane_parameters[1],
                                               plane_parameters[2]);
      float d = plane_parameters[3];
      if (vp.dot(normal) < 0)
      {
        normal = -normal;
        d = -d;
      }
      
      typedef Eigen::Hyperplane<float, 3> Plane;
      Plane plane(normal, d);
      
      Eigen::Vector3f projectedCenter = plane.projection(footCenter);
      auto q = Eigen::Quaternionf::FromTwoVectors(Eigen::Vector3f::UnitZ(),
                                                  normal);
      auto T = Eigen::Translation3f(projectedCenter) * q;

      for (const auto& neighborIdx : pointIdxRadiusSearch)
      {
        Eigen::Vector3f projected = T.inverse() * cloud->at(neighborIdx).getVector3fMap();
        
        if (Eigen::Vector2f(projected.x(), projected.y()).norm() >
            holeRadius * tiePointRadiusRatio)
          continue;
        
        if (std::fabs(projected.z()) > tiePointZThreshold)
          continue;
        
        float a = std::atan2(projected.y(), projected.x());
        float quadrantd = (a+2.*M_PI)/2./M_PI*nTiePoints;
        HOLE_FILLER_ASSERT(quadrantd > 0);
        int quadrant = int(quadrantd) % nTiePoints;
        if (tiePoints.find(quadrant) == tiePoints.end())
          tiePoints[quadrant] = neighborIdx;
        else
        {
          auto nearest = cloud->at(tiePoints[quadrant]).getVector3fMap();
          auto candidate = cloud->at(neighborIdx).getVector3fMap();
          if ((footCenter-candidate).squaredNorm() <
              (footCenter-nearest).squaredNorm())
            tiePoints[quadrant] = neighborIdx;
        }
      }
      
      for (auto it = tiePoints.begin(); it != tiePoints.end(); ++it)
        tiePointVector.push_back(it->second);
    }
    
    if (tiePointVector.size() < 3)
      // We can address this by:
      // - trying again with a large radius
      // - giving up and not filling this hole
      // Let's wait to see if it happens before thinking more about it.
      throw std::runtime_error("too few peripheral points");
    
    HOLE_FILLER_ASSERT(tiePointVector.size() >= 3);
    int origin = tiePointVector.front();
    for (std::vector<int>::const_iterator i = tiePointVector.begin()+1;
         i+1 != tiePointVector.end(); ++i)
    {
      pcl::Vertices triangle;
      triangle.vertices.push_back(origin);
      triangle.vertices.push_back(*i);
      triangle.vertices.push_back(*(i+1));
      mesh.polygons.push_back(triangle);
    }
  }
  
}

#endif
