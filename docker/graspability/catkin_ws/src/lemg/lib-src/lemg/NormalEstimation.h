// NormalEstimation.h
// Written Ian Rankin - July 2018
//
// This is the header to estimate normals in a robust way.
// This is written because PCL's normal estimation written in
// version 1.8.1_3 is numerically unstable for large number of points
// used in the estimation.
//
// This is due to their use of a single pass method for computing the covariance
// matrix which is numerically unstable, versus a 2 pass method.
// Note: there may be a stable single pass method, but the 2 pass method is
// not particularly slower with worst case O(2n)=O(n) instead of O(n)
// Actual number of operations is less than twice however.

#ifndef __NORMAL_ESTIMATION_JPL__
#define __NORMAL_ESTIMATION_JPL__


#include <pcl/point_types.h>
#include <pcl/point_cloud.h>

// for KdTreeFLANN, search tree.
#include <vector>

// For eigen matrix, and vector
#include <Eigen/StdVector>
#include <Eigen/Core>

namespace jpl {
   // computePointNormal
   // This function computes the point normal of the neighborhood of points defined
   // by cloudIndicies. This performs PCA, by first computing the centroid of the
   // points, then constructing the covariance matrix from that centroid.
   // the generic solvePlaneParameters from pcl is called to perform the Eigenvalue
   // decomposition to find the normal vector.
   //
   // @param cloud - the given point cloud.
   // @param cloudIndicies - the indicies of cloud to estimate the normal over.
   //    These in practice should be returned from a nearest neighbor search.
   // @param planeParameters - returns the plane parameters for the point.
   // @param curvature - returns the estimated curvature of the point.
   //    computed using the size of eigenvalues.
   //
   // @return - true if it finished successfully, false if fails.
   template <typename PointT>
   bool computePointNormal(const pcl::PointCloud<PointT> &cloud,
            const std::vector<int> &cloudIndicies,
            Eigen::Vector4f &planeParameters,
            float &curvature);

   // computePointNormal
   // This function computes the point normal of the neighborhood of points defined
   // by cloudIndicies. This performs PCA, by first computing the centroid of the
   // points, then constructing the covariance matrix from that centroid.
   // the generic solvePlaneParameters from pcl is called to perform the Eigenvalue
   // decomposition to find the normal vector.
   //
   // @param cloud - the given point cloud.
   // @param cloudIndicies - the indicies of cloud to estimate the normal over.
   //    These in practice should be returned from a nearest neighbor search.
   // @param nx - the x direction of the normal
   // @param ny - the y direction of the normal
   // @param nz - the z direction of the normal
   // @param curvature - returns the estimated curvature of the point.
   //    computed using the size of eigenvalues.
   //
   // @return - true if it finished successfully, false if fails.
   template <typename PointT>
   bool computePointNormal(const pcl::PointCloud<PointT> &cloud,
            const std::vector<int> &cloudIndicies,
            float &nx, float &ny, float &nz,
            float &curvature);



   // findCovAndMean
   // This is an two-pass method to sample covariance.
   // By doing this I hope to remove the numerical instabilty
   // of finding the covariance matrix.
   // This method is outligned on wikipedia here:
   // https://en.wikipedia.org/wiki/Algorithms_for_calculating_variance
   // this covariance and mean is computed with all doubles for intermediate values.
   //
   // @param indcies - the indcies of the cloud to compute.
   // @param centroid - the output centroid of the area.
   // @param covarianceMat - the output covariance matrix.
   template <typename PointT>
   int findCovAndMeanD(const pcl::PointCloud<PointT> &cloud,
                        const std::vector<int> &indcies,
                        Eigen::Vector4f &centroid,
                        Eigen::Matrix3f &covarianceMat);


   // findCovAndMean
   // This is an two-pass method to sample covariance.
   // By doing this I hope to remove the numerical instabilty
   // of finding the covariance matrix.
   // This method is outligned on wikipedia here:
   // https://en.wikipedia.org/wiki/Algorithms_for_calculating_variance
   // this covariance and mean is computed with all floats for intermediate values.
   //
   // @param indcies - the indcies of the cloud to compute.
   // @param centroid - the output centroid of the area.
   // @param covarianceMat - the output covariance matrix.
   template <typename PointT>
   int findCovAndMeanF(const pcl::PointCloud<PointT> &cloud,
                        const std::vector<int> &indcies,
                        Eigen::Vector4f &centroid,
                        Eigen::Matrix3f &covarianceMat);
}




// include implementation file
#include "NormalEstimation.hpp"

#endif
