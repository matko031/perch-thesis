// (C) Copyright Renaud Detry   2007-2015.
// Distributed under the GNU General Public License and under the
// BSD 3-Clause License (See accompanying file LICENSE.txt).

/** @file */


#ifndef LEMG_KERNEL_LOGISTIC_REGRESSOR_HPP
#define LEMG_KERNEL_LOGISTIC_REGRESSOR_HPP

#include <lemg/cereal/archives/binary.hpp>
#include <lemg/cereal/types/vector.hpp>
#include <boost/optional.hpp>
#include <lemg/histogram.hpp>

namespace cereal
{
//  template <class Archive, class _Scalar, int _Rows, int _Cols, int _Options, int _MaxRows, int _MaxCols> inline
//  typename std::enable_if<traits::is_output_serializable<BinaryData<_Scalar>, Archive>::value, void>::type
//  save(Archive & ar, Eigen::Matrix<_Scalar, _Rows, _Cols, _Options, _MaxRows, _MaxCols> const & m)
//  {
//    int32_t rows = m.rows();
//    int32_t cols = m.cols();
//    ar(rows);
//    ar(cols);
//    ar(binary_data(m.data(), rows * cols * sizeof(_Scalar)));
//  }
//  
//  template <class Archive, class _Scalar, int _Rows, int _Cols, int _Options, int _MaxRows, int _MaxCols> inline
//  typename std::enable_if<traits::is_input_serializable<BinaryData<_Scalar>, Archive>::value, void>::type
//  load(Archive & ar, Eigen::Matrix<_Scalar, _Rows, _Cols, _Options, _MaxRows, _MaxCols> & m)
//  {
//    int32_t rows;
//    int32_t cols;
//    ar(rows);
//    ar(cols);
//    
//    m.resize(rows, cols);
//    
//    ar(binary_data(m.data(), static_cast<std::size_t>(rows * cols * sizeof(_Scalar))));
//  }
}

namespace lemg
{
  
  /**
   * @ingroup kernels
   * @brief Implements kernel logistic regression.
   *
   * This class implements a two-class nonlinear classifier for @f$
   * SE(3) @f$ data. KLR is discussed in @ref kernels_klr.
   *
   * This class is based on @c libklr, which is provided with Makoto Yamada's <a
   * href="http://sugiyama-www.cs.titech.ac.jp/~yamada/iwklr.html">IWKLR</a>.
   */
  struct KernelLogisticRegressor
  {
    /** @brief */
    KernelLogisticRegressor() {}
    
    /**
     * @brief Imports input from @p data and @p labels, and computes the Gram
     * matrix of the data.
     *
     * The two structures @p data and @p labels should contain the same number
     * of elements. The @f$ i^{\mathrm{th}} @f$ element of @p labels indicates
     * the class of the @f$ i^{\mathrm{th}} @f$ element of @p data. The two
     * allowed values in @p labels are @c 1 and @c 2.
     *
     * This method also resets all the other member variables of the class.
     */
    void setData(const std::vector<histogram> &data,
                 const std::vector<int>& labels);
    
    /**
     * @brief Computes KLR weights.
     *
     * The implementation follows <a
     * href="http://dx.doi.org/10.1016/j.sigpro.2009.06.001">Semi-supervised
     * speaker identification under covariate shift</a>.
     */
    void train(const double delta = 0.0001, const unsigned itrNewton = 5);
    
    /**
     * @brief Returns @c true if the classifier has been trained.
     */
    bool isTrained()
    {
      return static_cast<bool>(vklr_);
    };
    
    /**
     * @brief Returns a pair of values which indicate the probability of classes
     * @c 1 and @c 2.
     */
    Vector2 test(const histogram &t) const;
    /**
     * @brief Returns the probabilities of classes @c 1 and @c 2 for all data
     * points in @p testSet.
     *
     * This method returns a @f$ 2 \times n @f$ matrix, where @f$ n @f$ is the
     * number of elements in @p testSet.
     */
    GMatrix test(const std::vector<histogram> &testSet) const;
    
    /**
     * @brief Returns KLR weights.
     *
     * The parameter @p delta is the regularization constant.
     */
    const GMatrix& vklr() const
    {
      LEMG_ASSERT(vklr_);
      return *vklr_;
    }
    
    std::vector<std::pair<int, double>> influentialTrainingData(const histogram& h) const;
    
    void read(const std::string& file);
    void write(const std::string& file) const;
    
  private:
    friend class cereal::access;
    template<class Archive>
    void serialize(Archive &ar, std::uint32_t const version)
    {
      ar(CEREAL_NVP(trainSet_),
         CEREAL_NVP(labels_));
      
      bool hasVklr = bool(vklr_);
      ar(hasVklr);
      if (hasVklr)
      {
        if (!vklr_) vklr_ = GMatrix();
        ar(CEREAL_NVP(*vklr_));
      }
      else
        vklr_ = boost::none;
    }

    void computeGramMatrix();
    
    std::vector<histogram> trainSet_;
    GMatrix gramMatrix_;
    std::vector<int> labels_;
    boost::optional<GMatrix> vklr_;
  };
}

CEREAL_CLASS_VERSION(lemg::KernelLogisticRegressor, 0);


#endif
