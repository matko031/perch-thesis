#ifndef LEMG_RADIAL_DESCRIPTOR_HPP
#define LEMG_RADIAL_DESCRIPTOR_HPP

#include <opencv2/core/core.hpp>

#include <lemg/cereal/archives/binary.hpp>

#include <lemg/Common.hpp>
#include <lemg/histogram.hpp>

namespace nuklei_wmf
{
  template<typename T> class Vector2;
  template<typename T> class Vector3;
  template<typename T> class Matrix3;
  template<typename T> class Quaternion;
}

namespace nuklei
{
  typedef nuklei_wmf::Vector2<double> Vector2;
  typedef nuklei_wmf::Vector3<double> Vector3;
  typedef nuklei_wmf::Quaternion<double> Quaternion;
  typedef nuklei_wmf::Matrix3<double> Matrix3;

  namespace kernel
  {
    class se3;
  }
}

namespace lemg {
  
  struct RTracer;
  
  struct XYPolar
  {
    XYPolar(const Eigen::Vector3d& p);
    
    bool operator<(const XYPolar& p) const
    {
      return r_ < p.r_;
    }
    
    double a_;
    double r_;
    Eigen::Vector3d p_;
  };
  
  inline double sig(const double a)
  {
    return 1./ ( 1. + std::exp(-a));
  }
  
  typedef Eigen::Matrix<double, QUADRANT_DESCRIPTOR_SIZE, 1> QuadrantDescriptor;
  
  struct Quadrant
  {
    Quadrant() : label_(-1) {}
    
    void setLabel(const int l) { label_ = l; }
    int getLabel() const { return label_; }
    
    void add(const XYPolar& point)
    {
      points_.push_back(point);
    }
    
    const QuadrantDescriptor& computeDescriptor(const std::string& file = "");
    const QuadrantDescriptor& getDescriptor() const { return *descriptor_; }
    void clearPoints() { points_.clear(); }

#ifdef LEMG_WRITE_DEBUG_DATA
    nuklei::KernelCollection kernelCollection() const;
    void addSlice(nuklei::KernelCollection& kc) const;
#endif
    
  private:
    std::vector<XYPolar> points_;
    boost::optional<QuadrantDescriptor> descriptor_;
    int label_;
  };
  
  struct Disc
  {
    Disc();
    Disc(const Eigen::Vector3d& originPoint,
         const Eigen::Vector3d& originNormal,
         int nQuadrants);

    void setNQuadrants(const int n) { nQuadrants_ = n; quadrants_.resize(n); }
    int getNQuadrants() const { return nQuadrants_; }
    void setNCodewords(const int n) { nCodewords_ = n; }

    void setPointId(const int id) { pointId_ = id; }
    int getPointId() const { return pointId_; }

    void setLabelCluster(const int id) { labelCluster_ = id; }
    int getLabelCluster() const { return labelCluster_; }

    void setCurvature(const double c) { curvature_ = c; }
    double getCurvature() const { return curvature_; }
    
    void setComputeColorImage(const bool c) { computeColorImage_ = c; }
    bool getComputeColorImage() const { return computeColorImage_; }
    
    void addPoint(std::shared_ptr<RTracer> rtrace,
                  const nuklei::Vector3& p);
    
    std::vector<Quadrant>& quadrants() { return quadrants_; }
    const std::vector<Quadrant>& quadrants() const { return quadrants_; }
    
    Quadrant& quadrant(const unsigned i) { return quadrants_.at(i); }
    const Quadrant& quadrant(const unsigned i) const { return quadrants_.at(i); }
    
    void setHistogram(const histogram& h) { histogram_ = h; }
    histogram getHistogramC() const;
    histogram getHistogram();
    const cv::Mat& getColorImage() const { return colorImage_; }
    const cv::Mat& getDepthImage() const { return depthImage_; }
    const double& getDepthImageRelativeSupportSurface() const { return depthImageRelativeSupportSurface_; }

    void computeImageDescriptors(std::shared_ptr<RTracer> rtrace, Eigen::Vector3f viewpos, Eigen::Vector3f viewdir);
    void writePortableRadialDescriptors(const std::string& s,
                                        struct DescriptorWriter& dw) const;
    void clearDepthImageDescriptors()
    {
      colorImage_ = cv::Mat();
      depthImage_ = cv::Mat();
      depthImageCount_ = cv::Mat();
    }
    
    void computeQuadrantDescriptors();
    
    void computeFrom(std::shared_ptr<RTracer> rtrace,
                     const pcl::PointCloud<pcl_point_t>& cloud,
                     const pcl::search::Search<pcl_point_t>& tree,
                     const Eigen::Vector3f& gripperClocking,
                     const Eigen::Vector3d& viewpoint,
                     const bool hasViewpoint,
                     bool discPositionNormalization);
    
    void setGraspable(const bool g) { graspable_ = g; }
    bool getGraspable() const { return graspable_; }
    
    void setPoint(const Eigen::Vector3d& point);
    const Eigen::Vector3d& getPoint() const { return point_; }
    void setOrientation(const Eigen::Vector3d& normal);
    const Eigen::Vector3d& getNormal() const { return normal_; }
    void setOrientation(const Eigen::Quaterniond& orientation,
                        const Eigen::Vector3d& normal = Eigen::Vector3d::Zero());
    const Eigen::Quaterniond& getOrientation() const { return orientation_; }

    void dump(const std::string& dir) const;
    
  private:
    friend class cereal::access;
    template<class Archive>
    void serialize(Archive &ar, std::uint32_t const version)
    {
      ar(CEREAL_NVP(nQuadrants_),
         CEREAL_NVP(nCodewords_),
         CEREAL_NVP(point_),
         CEREAL_NVP(normal_),
         CEREAL_NVP(curvature_),
         CEREAL_NVP(orientation_),
         CEREAL_NVP(graspable_),
         CEREAL_NVP(pointId_),
         CEREAL_NVP(histogram_));
    }

    int nQuadrants_;
    int nCodewords_;
    Eigen::Vector3d point_;
    Eigen::Vector3d normal_;
    double curvature_;
    Eigen::Quaterniond orientation_;
    std::vector<Quadrant> quadrants_;
    bool graspable_;
    int labelCluster_;
    int pointId_;
    histogram histogram_;
    bool computeQuadrants_;
    bool computeColorImage_;
    bool computeDepthImage_;
    cv::Mat colorImage_;
    cv::Mat depthImage_;
    cv::Mat depthImageCount_;
    double depthImageRelativeSupportSurface_;
  };
  
}

//namespace cereal
//{
//
//  template<class Archive>
//  void save(Archive & ar,
//            lemg::QuadrantDescriptor const & v,
//            std::uint32_t const version)
//  {
//    for (size_t i = 0; i < lemg::QUADRANT_DESCRIPTOR_SIZE; ++i)
//      ar(v(i));
//  }
//
//  template<class Archive>
//  void load(Archive & ar,
//            lemg::QuadrantDescriptor & v,
//            std::uint32_t const version)
//  {
//    for (size_t i = 0; i < lemg::QUADRANT_DESCRIPTOR_SIZE; ++i)
//      ar(v(i));
//  }
//
//}

CEREAL_CLASS_VERSION(lemg::QuadrantDescriptor, 0);
CEREAL_CLASS_VERSION(lemg::Disc, 0);

#endif
