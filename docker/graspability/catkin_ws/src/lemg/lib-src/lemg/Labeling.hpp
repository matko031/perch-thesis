#ifndef LEMG_LABELING_HPP
#define LEMG_LABELING_HPP

#include <lemg/Common.hpp>
#include <lemg/ShapeIO.hpp>
#include <pcl/PCLPointCloud2.h>
#include <pcl/conversions.h>
#include <pcl/point_cloud.h>
#include <stdexcept>

namespace lemg {

  template<typename T>
  bool color_eq(const T& p1, const T&p2)
  {
    return (p1.r == p2.r && p1.g == p2.g && p1.b == p2.b);
  }
  
  inline bool isLabel(const int c)
  {
    return c == 0 || (c > 250 && c < 256);
  }
  
  template<typename PointT>
  inline bool isLabeled(const PointT& p)
  {
    return (p.r == 0 || p.g == 0 || p.b == 0) && isLabel(p.r) && isLabel(p.g)  && isLabel(p.b) && !(p.r == 0 && p.g == 0 /*&& p.b == 0 fixme reenable when blue is used */) ;
  }
  
  template<typename PointT>
  void writeLabel(PointT& p, const int label);
  
  const int N_CLASSES = 3;
  
  template<typename PointT>
  inline int labelFromColor(const PointT& p)
  {
    if (!isLabeled(p)) return -1;
    
    bool r = p.r > 250 && p.g == 0  && p.b == 0;
    bool g = p.r == 0  && p.g > 250 && p.b == 0;
    bool b = p.r == 0  && p.g == 0  && p.b > 250;
    
    if (r) return 0;
    if (g) return 1;
    if (b) return -1; //fixme: change this to 2 when we switch to 2+ labels
    
    //NUKLEI_THROW("Unknown label [" << int(p.r) << "; " << int(p.g) << "; " << int(p.b) << "]");
    throw std::runtime_error("Unknown label");
  }

  inline std::vector<int> readLabels(ShapeIO& shio)
  {
    std::vector<int> labels;
    
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloudColor
    (new pcl::PointCloud<pcl::PointXYZRGB>());
    
    *cloudColor = shio.get<pcl::PointXYZRGB>();
    
    for (const auto& i : *cloudColor)
    {
      int l = labelFromColor(i);
      labels.push_back(l);
    }
    
    return labels;
  }
  
  void readLabels(const std::string& file, std::vector<double>& v);
  void readLabels(const std::string& file, std::vector<int>& v);

  template<typename Cloud>
  void readLabels(const std::string& file, Cloud& pc)
  {
    std::vector<double> v;
    readLabels(file, v);
    LEMG_ASSERT(pc.size() == v.size());
    for (unsigned i = 0; i != v.size(); ++i)
    {
      pc.at(i).curvature = v.at(i);
    }
  }
  
  template<typename T>
  void writeLabels(const std::string& file, const std::vector<T>& v)
  {
    std::ofstream ofs(file.c_str());
    for (auto i = v.begin(); i != v.end(); ++i)
      ofs << *i << "\n";
  }
  
  template<typename Cloud>
  void writeLabels(const std::string& file, const Cloud& pc)
  {
    std::vector<double> v;
    for (auto i = pc.begin(); i != pc.end(); ++i)
    {
      v.push_back(i->curvature);
    }
    writeLabels(file, v);
  }
  
  void paint(pcl::PointCloud<pcl::PointXYZRGB>& cloud,
             const std::vector<double>& labels,
             double originalColorWeight = 0,
             double discretizationThreshold = 0,
             bool normalize = false,
             bool negativeIsUndefined = true,
             bool flip = false);
  
  
}



#endif
