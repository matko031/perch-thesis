#ifndef LEMG_IO_HPP
#define LEMG_IO_HPP

#include <boost/iostreams/filtering_stream.hpp>
#include <boost/iostreams/filter/gzip.hpp>
#include <boost/iostreams/device/file.hpp>

#include <lemg/cereal/archives/binary.hpp>
#include <lemg/cereal/types/vector.hpp>
#include <lemg/cereal/types/string.hpp>

namespace lemg {

  template<typename T>
  void readObject(T &object, const std::string& filename)
  {
    boost::iostreams::filtering_istream gifs;
    gifs.push(boost::iostreams::gzip_decompressor());
    gifs.push(boost::iostreams::file_source(filename));
    LEMG_ASSERT(gifs.good());
    //std::ifstream gifs(filename.c_str());
    cereal::BinaryInputArchive iarchive(gifs);
    iarchive(object);
  }
  
  template<typename T>
  void writeObject(const T &object, const std::string& filename)
  {
    boost::iostreams::filtering_ostream gofs;
    gofs.push(boost::iostreams::gzip_compressor());
    gofs.push(boost::iostreams::file_sink(filename));
    LEMG_ASSERT(gofs.good());
    //std::ofstream gofs(filename.c_str());
    cereal::BinaryOutputArchive oarchive(gofs);
    oarchive(object);
  }
  
}

#endif
