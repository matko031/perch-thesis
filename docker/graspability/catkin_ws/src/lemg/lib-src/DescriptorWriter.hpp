#ifndef LEMG_DESCRIPTOR_WRITER_HPP
#define LEMG_DESCRIPTOR_WRITER_HPP

#include <opencv2/core/core.hpp>
#include <highfive/H5DataSet.hpp>
#include <highfive/H5Attribute.hpp>
#include <highfive/H5DataSpace.hpp>
#include <highfive/H5File.hpp>
#include <boost/filesystem.hpp>
#include <lemg/Common.hpp>

namespace lemg {
  
  struct DescriptorWriter
  {
    DescriptorWriter(const bool writeColorImage, const bool writeDepthImage) :
    count_(0),
    writeColorImage_(writeColorImage),
    writeDepthImage_(writeDepthImage)
    {}
    
    void create(const std::string& filename)
    {
      using namespace HighFive;
      
      if (boost::filesystem::exists(filename))
        boost::filesystem::remove(filename);
      
      file_.reset(new File(filename,
                           File::ReadWrite | File::Create | File::Truncate));
      // why does DataSpace::UNLIMITED require a cast to size_t?
      // (if no cast, the linker complains it cannot find the UNLIMITED symbol)
      c_dataspace_.reset(new DataSpace(color_s<std::size_t>(1),
                                       color_s<std::size_t>(std::size_t(DataSpace::UNLIMITED))));
      x_dataspace_.reset(new DataSpace(depth_s<std::size_t>(1),
                                       depth_s<std::size_t>(std::size_t(DataSpace::UNLIMITED))));
      y_dataspace_.reset(new DataSpace(vec2_s<std::size_t>(1),
                                       vec2_s<std::size_t>(std::size_t(DataSpace::UNLIMITED))));
      i_dataspace_.reset(new DataSpace(scalar_s<std::size_t>(1),
                                       scalar_s<std::size_t>(std::size_t(DataSpace::UNLIMITED))));
      p_dataspace_.reset(new DataSpace(vec3_s<std::size_t>(1),
                                       vec3_s<std::size_t>(std::size_t(DataSpace::UNLIMITED))));
      q_dataspace_.reset(new DataSpace(vec4_s<std::size_t>(1),
                                       vec4_s<std::size_t>(std::size_t(DataSpace::UNLIMITED))));
      s_dataspace_.reset(new DataSpace(scalar_s<std::size_t>(1),
                                       scalar_s<std::size_t>(std::size_t(DataSpace::UNLIMITED))));

      c_props_.reset(new HighFive::DataSetCreateProps);
      c_props_->add(Chunking(color_s<hsize_t>(1)));
      x_props_.reset(new HighFive::DataSetCreateProps);
      x_props_->add(Chunking(depth_s<hsize_t>(1)));
      y_props_.reset(new HighFive::DataSetCreateProps);
      y_props_->add(Chunking(vec2_s<hsize_t>(1)));
      i_props_.reset(new HighFive::DataSetCreateProps);
      i_props_->add(Chunking(scalar_s<hsize_t>(1)));
      p_props_.reset(new HighFive::DataSetCreateProps);
      p_props_->add(Chunking(vec3_s<hsize_t>(1)));
      q_props_.reset(new HighFive::DataSetCreateProps);
      q_props_->add(Chunking(vec4_s<hsize_t>(1)));
      s_props_.reset(new HighFive::DataSetCreateProps);
      s_props_->add(Chunking(scalar_s<hsize_t>(1)));

      if (writeColorImage_)
        c_dataset_.reset(new HighFive::DataSet(file_->createDataSet("C",
                                                                    *c_dataspace_,
                                                                    AtomicType<uint8_t>(),
                                                                    *c_props_)));
      if (writeDepthImage_)
        x_dataset_.reset(new HighFive::DataSet(file_->createDataSet("X",
                                                                    *x_dataspace_,
                                                                    AtomicType<uint8_t>(),
                                                                    *x_props_)));
      y_dataset_.reset(new HighFive::DataSet(file_->createDataSet("Y",
                                                                  *y_dataspace_,
                                                                  AtomicType<uint8_t>(),
                                                                  *y_props_)));
      i_dataset_.reset(new HighFive::DataSet(file_->createDataSet("I",
                                                                  *i_dataspace_,
                                                                  AtomicType<int32_t>(),
                                                                  *i_props_)));
      p_dataset_.reset(new HighFive::DataSet(file_->createDataSet("P",
                                                                  *p_dataspace_,
                                                                  AtomicType<float_t>(),
                                                                  *p_props_)));
      q_dataset_.reset(new HighFive::DataSet(file_->createDataSet("Q",
                                                                  *q_dataspace_,
                                                                  AtomicType<float_t>(),
                                                                  *q_props_)));
      s_dataset_.reset(new HighFive::DataSet(file_->createDataSet("S",
                                                                  *s_dataspace_,
                                                                  AtomicType<double>(),
                                                                  *s_props_)));
    }
    
    void writeNVertices(const int n)
    {
      HighFive::Attribute v = i_dataset_->createAttribute<int>("n_vertices_in_original_mesh",
                                                               HighFive::DataSpace::From(n));
      v.write(n);
    }
    
    void write(const cv::Mat& colorImage,
               const cv::Mat& depthImage,
               const int success = -1,
               const int& id = -1,
               const Eigen::Vector3d& v = Eigen::Vector3d::Zero(),
               const Eigen::Quaterniond& o = Eigen::Quaterniond::Identity(),
               const double& depthImageRelativeSupportSurface = -1)
    {
      using namespace HighFive;
      uint8_t c[IMAGE_SIZE][IMAGE_SIZE][3];
      for (std::size_t i = 0; i < colorImage.rows; ++i)
        for (std::size_t j = 0; j < colorImage.cols; ++j)
        {
          c[i][j][0] = colorImage.at<bgr_t>(i,j).r;
          c[i][j][1] = colorImage.at<bgr_t>(i,j).g;
          c[i][j][2] = colorImage.at<bgr_t>(i,j).b;
        }
      uint8_t x[IMAGE_SIZE][IMAGE_SIZE][1];
      for (std::size_t i = 0; i < depthImage.rows; ++i)
        for (std::size_t j = 0; j < depthImage.cols; ++j)
          x[i][j][0] = depthImage.at<uint8_t>(i,j);
      LEMG_ASSERT(success == 0 || success == 1);
      uint8_t y[2] = { uint8_t(1-success), uint8_t(success) };
      float_t p[3] = { float_t(v.x()), float_t(v.y()), float_t(v.z()) };
      float_t q[4] = { float_t(o.w()), float_t(o.x()), float_t(o.y()), float_t(o.z()) };
#ifdef _OPENMP
#pragma omp critical(lemg_descriptor_writer)
#endif
      {
        if (writeColorImage_)
          c_dataset_->resize(color_s(count_+1));
        if (writeDepthImage_)
          x_dataset_->resize(depth_s(count_+1));
        y_dataset_->resize(vec2_s(count_+1));
        i_dataset_->resize(scalar_s(count_+1));
        p_dataset_->resize(vec3_s(count_+1));
        q_dataset_->resize(vec4_s(count_+1));
        s_dataset_->resize(scalar_s(count_+1));

        if (writeColorImage_)
          c_dataset_->select(color_s(count_, false), color_s<std::size_t>(1)).write(c);
        if (writeDepthImage_)
          x_dataset_->select(depth_s(count_, false), depth_s<std::size_t>(1)).write(x);
        y_dataset_->select(vec2_s(count_, false), vec2_s<std::size_t>(1)).write(y);
        i_dataset_->select(scalar_s(count_), scalar_s<std::size_t>(1)).write(id);
        p_dataset_->select(vec3_s(count_, false), vec3_s<std::size_t>(1)).write(p);
        q_dataset_->select(vec4_s(count_, false), vec4_s<std::size_t>(1)).write(q);
        s_dataset_->select(scalar_s(count_), scalar_s<std::size_t>(1)).write(depthImageRelativeSupportSurface);
        if (count_ % 100 == 0) file_->flush();
        count_++;
      }
    }

    template<typename T>
    std::vector<T> color_s(const T& s, const bool otherAtEnd = true)
    {
      T i = (otherAtEnd ? IMAGE_SIZE:0);
      T j = (otherAtEnd ? 3:0);
      return {s, i, i, j};
    }

    template<typename T>
    std::vector<T> depth_s(const T& s, const bool otherAtEnd = true)
    {
      T i = (otherAtEnd ? IMAGE_SIZE:0);
      T j = (otherAtEnd ? 1:0);
      return {s, i, i, j};
    }

    template<typename T>
    std::vector<T> vec4_s(const T& s, const bool otherAtEnd = true)
    {
      return {s, T(otherAtEnd ? 4:0)};
    }

    template<typename T>
    std::vector<T> vec3_s(const T& s, const bool otherAtEnd = true)
    {
      return {s, T(otherAtEnd ? 3:0)};
    }

    template<typename T>
    std::vector<T> vec2_s(const T& s, const bool otherAtEnd = true)
    {
      return {s, T(otherAtEnd ? 2:0)};
    }
    
    template<typename T>
    std::vector<T> scalar_s(const T& s)
    {
      return {s};
    }

  private:
    boost::shared_ptr<HighFive::File> file_;
    boost::shared_ptr<HighFive::DataSpace> c_dataspace_, x_dataspace_, y_dataspace_, i_dataspace_, p_dataspace_, q_dataspace_, s_dataspace_;
    boost::shared_ptr<HighFive::DataSetCreateProps> c_props_, x_props_, y_props_, i_props_, p_props_, q_props_, s_props_;
    boost::shared_ptr<HighFive::DataSet> c_dataset_, x_dataset_, y_dataset_, i_dataset_, p_dataset_, q_dataset_, s_dataset_;
    std::size_t count_;
    bool writeColorImage_;
    bool writeDepthImage_;
  };
  
}



#endif
