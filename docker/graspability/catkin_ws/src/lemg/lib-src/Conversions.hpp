#ifndef LEMG_CONVERSIONS_HPP
#define LEMG_CONVERSIONS_HPP

#include <pcl/conversions.h>
#include <pcl/point_types.h>
#include <nuklei/KernelCollection.h>
#include <lemg/Common.hpp>


namespace nuklei
{
  inline kernel::r3xs2 copy(const pcl::PointNormal& p)
  {
    kernel::r3xs2 k;
    k.loc_ = Vector3(p.x, p.y, p.z);
    k.dir_ = Vector3(p.normal_x, p.normal_y, p.normal_z);
    return k;
  }
  
  inline pcl::PointNormal copy(const kernel::r3xs2& n)
  {
    pcl::PointNormal pn;
    pn.x = n.loc_.X();
    pn.y = n.loc_.Y();
    pn.z = n.loc_.Z();
    pn.normal_x = n.dir_.X();
    pn.normal_y = n.dir_.Y();
    pn.normal_z = n.dir_.Z();
    return pn;
  }
  
  inline kernel::r3xs2 copy(const pcl::PointXYZ& l, const pcl::Normal& d)
  {
    kernel::r3xs2 k;
    k.loc_ = Vector3(l.x, l.y, l.z);
    k.dir_ = Vector3(d.normal_x, d.normal_y, d.normal_z);
    return k;
  }
  
  inline pcl::PointNormal copy(const Vector3& l, const Vector3& d)
  {
    pcl::PointNormal pn;
    pn.x = l.X();
    pn.y = l.Y();
    pn.z = l.Z();
    pn.normal_x = d.X();
    pn.normal_y = d.Y();
    pn.normal_z = d.Z();
    return pn;
  }

}

#endif
