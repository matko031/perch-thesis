// (C) Copyright Renaud Detry   2007-2015.
// Distributed under the GNU General Public License and under the
// BSD 3-Clause License (See accompanying file LICENSE.txt).

/** @file */

#include <lemg/KernelLogisticRegressor.hpp>
#include <nuklei/ObservationIO.h>
#include <nuklei/ProgressIndicator.h>
#include <nuklei/Match.h>
#include "klr_train.h"

namespace lemg
{
  
  void KernelLogisticRegressor::setData(const std::vector<histogram> &data,
                                        const std::vector<int>& labels)
  {
    NUKLEI_ASSERT(data.size() == labels.size());
    // Shuffling the data has a large impact on training convergence.
    // I haven't experimented further to understand why, although intuitively
    // it makes sense.
    std::vector<unsigned> idx;
    for (unsigned i = 0; i < data.size(); ++i) idx.push_back(i);
    std::random_shuffle(idx.begin(), idx.end());
    trainSet_.clear();
    labels_.clear();
    for (auto i = idx.begin(); i != idx.end(); ++i)
    {
      trainSet_.push_back(data.at(*i));
      labels_.push_back(labels.at(*i));
    }
    computeGramMatrix();
  }
  
  void KernelLogisticRegressor::computeGramMatrix()
  {
    NUKLEI_TRACE_BEGIN();
    gramMatrix_ = GMatrix(trainSet_.size(), trainSet_.size());
    
    nuklei::ProgressIndicator piGram(trainSet_.size(), "Computing Gram: ");
#ifdef _OPENMP
#pragma omp parallel for
#endif
    for (unsigned i = 0; i < trainSet_.size(); ++i)
    {
      for (unsigned j = 0; j < trainSet_.size(); ++j)
      {
        gramMatrix_(i, j) = trainSet_.at(i).kernel(trainSet_.at(j));
      }
#ifdef _OPENMP
#pragma omp critical(lemg_gram_matrix)
#endif
      piGram.inc();
    }
    NUKLEI_TRACE_END();
  }
  
  void KernelLogisticRegressor::train(const double delta,
                                      const unsigned itrNewton)
  {
    NUKLEI_TRACE_BEGIN();
    
    NUKLEI_ASSERT(trainSet_.size() == labels_.size());
    NUKLEI_ASSERT(trainSet_.size() != 0);
    
    GMatrix vklr(trainSet_.size(), 2);
    
    klr_train(gramMatrix_.data(), gramMatrix_.rows(), gramMatrix_.cols(),
              &labels_.front(), labels_.size(),
              vklr.data(), vklr.rows(), vklr.cols(),
              delta, itrNewton);
    
    vklr_ = vklr.transpose();
    NUKLEI_TRACE_END();
  }
  
  struct absolute_value_comparator
  {
    //template<typename T>
    typedef std::pair<int, double> T;
    bool operator()(const T& a, const T& b)
    {
      return (std::abs(a.second) > std::abs(b.second));
    }
  };
  
  std::vector<std::pair<int, double>> KernelLogisticRegressor::influentialTrainingData(const histogram& h) const
  {
    NUKLEI_TRACE_BEGIN();
    NUKLEI_ASSERT(vklr_);
    std::vector<std::pair<int, double>> r;
    
    GMatrix Ktest(trainSet_.size(), 1);
    for (unsigned i = 0; i < trainSet_.size(); ++i)
    {
      Ktest(i, 0) = trainSet_.at(i).kernel(h);
    }
    
    GMatrix diff = vklr_->row(0) - vklr_->row(1);
    GMatrix coefs = diff.transpose().array() * Ktest.array();
    NUKLEI_ASSERT(coefs.rows() == trainSet_.size() &&
                  coefs.cols() == 1);
    
    for (size_t i = 0; i < coefs.size(); ++i)
    {
      r.push_back(std::make_pair(trainSet_.at(i).getPointId(), coefs(i)));
    }
    std::sort(r.begin(), r.end(), absolute_value_comparator());
    
    return r;
    NUKLEI_TRACE_END();
  }
  

  Vector2 KernelLogisticRegressor::test(const histogram &t) const
  {
    NUKLEI_TRACE_BEGIN();
    NUKLEI_ASSERT(vklr_);
    Vector2 pr;
    
    GMatrix Ktest(trainSet_.size(), 1);
    for (unsigned i = 0; i < trainSet_.size(); ++i)
    {
      Ktest(i, 0) = trainSet_.at(i).kernel(t);
    }
    
    GMatrix f = *vklr_ * Ktest;
    double m = std::max(f(0,0), f(1,0));
    
    double e1 = std::exp(f(0,0)-m), e2 = std::exp(f(1,0)-m);
    pr.x() = e1/(e1+e2);
    pr.y() = e2/(e1+e2);
    
    return pr;
    NUKLEI_TRACE_END();
  }
  
  
  GMatrix KernelLogisticRegressor::test(const std::vector<histogram> &testSet) const
  {
    NUKLEI_TRACE_BEGIN();
    
    GMatrix pr(2, testSet.size());
    
    for (std::vector<histogram>::const_iterator ti = testSet.begin();
         ti != testSet.end(); ++ti)
    {
      Vector2 p = test(*ti);
      pr(0,std::distance(testSet.begin(), ti)) = p.x();
      pr(1,std::distance(testSet.begin(), ti)) = p.y();
    }
    return pr;
    NUKLEI_TRACE_END();
  }
  
  std::ostream& operator<<(std::ostream &out, const GMatrix &m)
  {
    for (int i = 0; i < m.rows(); ++i)
    {
      for (int j = 0; j < m.cols(); ++j)
        NUKLEI_ASSERT(out << m(i,j) << " ");
      NUKLEI_ASSERT(out << "\n");
    }
    out << std::flush;
    return out;
  }

  
  void KernelLogisticRegressor::read(const std::string& file)
  {
    NUKLEI_TRACE_BEGIN();
    std::ifstream ifs(file.c_str());
    
    if (!ifs.good())
      NUKLEI_THROW("Failed to open file `" << file << "'");

    std::string versionString;
    ifs >> versionString;
    
    NUKLEI_ASSERT(versionString == "LEMG-KLR-Model-v1" ||
                  versionString == "LEMG-KLR-Model-v2");
    
    trainSet_.clear();
    int trainSize = -1, hSize = -1;
    NUKLEI_ASSERT(ifs >> trainSize >> hSize);
    for (int i = 0; i < trainSize; ++i)
    {
      histogram h(hSize);
      NUKLEI_ASSERT(ifs >> h);
      if (versionString == "LEMG-KLR-Model-v2")
      {
        int pointId = -1;
        NUKLEI_ASSERT(ifs >> pointId);
        h.setPointId(pointId);
      }
      trainSet_.push_back(h);
    }
    
    //int gmSize = -1;
    //NUKLEI_ASSERT(ifs >> gmSize);
    //gramMatrix_.SetSize(gmSize, gmSize);
    //for (int i = 0; i < gramMatrix_.rows(); ++i)
    //  for (int j = 0; j < gramMatrix_.cols(); ++j)
    //    NUKLEI_ASSERT(ifs >> gramMatrix_(i,j));
    
    labels_.clear();
    int labelSize = -1;
    NUKLEI_ASSERT(ifs >> labelSize);
    for (int i = 0; i < labelSize; ++i)
    {
      int l = -1;
      NUKLEI_ASSERT(ifs >> l);
      labels_.push_back(l);
    }
    
    int vklrRows = -1, vklrCols = -1;
    NUKLEI_ASSERT(ifs >> vklrRows >> vklrCols);
    vklr_ = GMatrix(vklrRows, vklrCols);
    for (int i = 0; i < vklr_->rows(); ++i)
      for (int j = 0; j < vklr_->cols(); ++j)
        NUKLEI_ASSERT(ifs >> (*vklr_)(i,j));
    NUKLEI_TRACE_END();
  }

  void KernelLogisticRegressor::write(const std::string& file) const
  {
    NUKLEI_TRACE_BEGIN();
    std::ofstream ofs(file.c_str());
    ofs.precision(16);
    
    ofs << "LEMG-KLR-Model-v2\n";

    NUKLEI_ASSERT(ofs << trainSet_.size() << " " << trainSet_.front().size() << "\n");
    for (std::vector<histogram>::const_iterator i = trainSet_.begin();
         i != trainSet_.end(); ++i)
      ofs << *i << " " << i->getPointId() << "\n";

    ofs << "\n";
    
    //NUKLEI_ASSERT(gramMatrix_.rows() == gramMatrix_.cols());
    //NUKLEI_ASSERT(ofs << gramMatrix_.rows() << "\n");
    //NUKLEI_ASSERT(nuklei::operator<<(ofs, gramMatrix_));

    NUKLEI_ASSERT(ofs << labels_.size() << "  ");
    std::copy(labels_.begin(), labels_.end(),
              std::ostream_iterator<int>(ofs, " "));

    ofs << "\n";

    NUKLEI_ASSERT(ofs << vklr_->rows() << " " << vklr_->cols() << "\n");
    NUKLEI_ASSERT(lemg::operator<<(ofs, *vklr_));
    
    ofs << std::endl;
    NUKLEI_TRACE_END();
  }


}
