#include <Eigen/Geometry>
#include <boost/filesystem.hpp>

#include <pcl/conversions.h>
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/features/normal_3d.h>
#include <pcl/search/organized.h>
#include <pcl/search/kdtree.h>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>


#include "Conversions.hpp"

#include <nuklei/KernelCollection.h>
#include <nuklei/ObservationIO.h>
#include <nuklei/ProgressIndicator.h>
#include <nuklei/stats.h>
#include <nuklei/PCLBridge.h>

#include <lemg/RadialDescriptor.hpp>
#include <lemg/histogram.hpp>

#include <lemg/NormalEstimation.h>

#include "raytrace.hpp"
#include "DescriptorWriter.hpp"


namespace lemg
{
  using namespace nuklei;
  
  XYPolar::XYPolar(const Eigen::Vector3d& p) : p_(p)
  {
#define LEMG_SQRT(x) std::sqrt(x)
#define LEMG_ATAN2(y,x) std::atan2(y,x)
//#define LEMG_SQRT(x) sqrt_approx(x)
//#define LEMG_ATAN2(y,x) atan2_approx2(y,x)
    
    r_ = LEMG_SQRT(p_.x()*p_.x() + p_.y()*p_.y());
    a_ = LEMG_ATAN2(p_.y(), p_.x());
  }

  const QuadrantDescriptor& Quadrant::computeDescriptor(const std::string& file)
  {
    KernelCollection kc;
#if LEMG_DESCRIPTOR_IMPLEMENTATION == 0
    // Implementation 0:
    // Project all points of the quadrant to a plane that contains r and z.
    // Fit a line.
    // Define feature vector based on the line parameters.
    NUKLEI_ASSERT(QUADRANT_DESCRIPTOR_SIZE == 2);
    Eigen::MatrixXd A(points_.size(), 2);
    Eigen::VectorXd b(points_.size());
    for (unsigned i = 0; i < points_.size(); ++i)
    {
      // z = p r + q
      // A x = b ; A = [ r 1 ; ... ] ; b = [ z; ... ]; x = [ p q ]'
      A(i, 0) = points_.at(i).r_;
      A(i, 1) = 1;
      b(i) = points_.at(i).p_.z();
      
      if (!file.empty())
      {
        kernel::r3 k;
        k.loc_ = Vector3(points_.at(i).r_, points_.at(i).p_.z(), 0);
        RGBColor rgb(1, 0, 0);
        ColorDescriptor cd;
        cd.setColor(rgb);
        k.setDescriptor(cd);
        kc.add(k);
      }
    }
    
    // https://eigen.tuxfamily.org/dox-devel/group__LeastSquares.html
    Eigen::VectorXd sol = A.colPivHouseholderQr().solve(b);
    //Eigen::VectorXd sol = (A.transpose() * A).ldlt().solve(A.transpose() * b);
    //Eigen::VectorXd sol(2);
    
    QuadrantDescriptor d;
#ifdef LEMG_INTUITIVE_LINE_FEATURE_NORMALIZATION
    // In this block, the vector d is set to the angle of the fitted line to the
    // x axis, and the y coordinate of the center of the fitted line segment.
    // (by contrast to the next block, that sets d to [p q])
    
    // Intuitively this transform makes sense, but in practice it doesn't
    // improve success rate.
    
    // first component is the angle of the fitted line to the x axis
    d(0) = std::atan(sol(0));
    // second component is the y value at half-radius, clamped
    d(1) = sol(0) * RADIUS/2 + sol(1);
    d(1) = std::max(std::min(d(1), .2), -.2);
#else
    // z = p r + q
    // Here, d(0) == p, d(1) == q
    
    d(0) = sol(0);
    d(1) = sol(1);
#endif
    descriptor_ = d;
    
    if (!file.empty())
    {
      for (double rad = 0; rad < RADIUS; rad += RADIUS/20)
      {
        kernel::r3 k;
        k.loc_ = Vector3(rad, (*descriptor_)(0) * rad + (*descriptor_)(1), 0);
        kc.add(k);
      }
      writeObservations(file, kc, Observation::SERIAL);
    }
#elif LEMG_DESCRIPTOR_IMPLEMENTATION == 1
    // Implementation 1:
    // discretize r (2 to 10 buckets), compute the z average in each bucket.
    // Set value to -10 when a bucket is empty. This only makes sense if the
    // following step is clustering. If we switch to a non-clustering
    // descriptor, -10 should be changed to a value that makes metric sense.
    QuadrantDescriptor sum, npoints;
    sum.setZero();
    npoints.setZero();
    for (auto i = points_.begin(); i != points_.end(); ++i)
    {
      if (i->r_ < .001) continue;
      //unsigned idx = std::floor(i->r_/RADIUS * QUADRANT_DESCRIPTOR_SIZE);
      int idx = -1;
      // divide quadrant into three pieces of equal surface:
      // r1^2 == r2^2-r1-2 == r3^2-r2^2
      // r3 = 1, r2 = sqrt(2)/sqrt(3) * r3, r1 = 1/sqrt(3) * r3
      if (i->r_/RADIUS < 0.5773502692) idx = 0;
      else if (i->r_/RADIUS < 0.8164965809) idx = 1;
      else idx = 2;
      NUKLEI_ASSERT(0 <= idx && idx < QUADRANT_DESCRIPTOR_SIZE);
      sum(idx) += i->p_.z();
      npoints(idx) += 1;
    }
    QuadrantDescriptor d;
    for (unsigned di = 0; di < QUADRANT_DESCRIPTOR_SIZE; ++di)
    {
      if (npoints(di) > 5)
        d(di) = sum(di)/npoints(di);
      else
        d(di) = -10;
    }
    descriptor_ = d;
#else
#error Unknown descriptor implementation
#endif
    
    return (*descriptor_);
  }
  
#ifdef LEMG_WRITE_DEBUG_DATA
  KernelCollection Quadrant::kernelCollection() const
  {
    KernelCollection kc;
    for (std::vector<XYPolar>::const_iterator i = points_.begin(); i < points_.end(); ++i)
    {
      kernel::r3 tmp;
      tmp.loc_ = i->p_;
      kc.add(tmp);
    }
    return kc;
  }

  void Quadrant::addSlice(KernelCollection& kc) const
  {
    for (unsigned i = 0; i < points_.size(); ++i)
    {
      kernel::r3 k;
      k.loc_ = Vector3(points_.at(i).r_, points_.at(i).p_.Z(), 0);
      //          RGBColor rgb(1, 0, 0);
      //          ColorDescriptor cd;
      //          cd.setColor(rgb);
      //          k.setDescriptor(cd);
      kc.add(k);
      
    }
  }
#endif
  
  Disc::Disc() :
  nQuadrants_(0),
  nCodewords_(0),
  point_(Eigen::Vector3d::Zero()),
  normal_(Eigen::Vector3d::Zero()),
  orientation_(Eigen::Quaterniond::Identity()),
  quadrants_(nQuadrants_),
  graspable_(false),
  labelCluster_(-1),
  pointId_(-1),
  computeQuadrants_(false),
  computeColorImage_(false),
  computeDepthImage_(true),
  colorImage_(cv::Mat(IMAGE_SIZE, IMAGE_SIZE, CV_8UC3, cv::Scalar(0, 0, 0))),
  depthImage_(cv::Mat(IMAGE_SIZE, IMAGE_SIZE, CV_32S, cv::Scalar(0))),
  depthImageCount_(cv::Mat(IMAGE_SIZE, IMAGE_SIZE, CV_32S, cv::Scalar(0))),
  depthImageRelativeSupportSurface_(-1)
  {
  }
  
  void Disc::addPoint(std::shared_ptr<RTracer> rtrace,
                      const nuklei::Vector3& p)
  {
    Vector3 v = la::project(wmCopy(point_), wmCopy(orientation_), p);
    LEMG_ASSERT(!(std::isnan)(v.Z()));
    LEMG_ASSERT(!(std::isinf)(v.Z()));
    double distance = RADIUS-v.Z();
    if (! (0 < distance && distance < 2*RADIUS) ) return;

    if (computeQuadrants_ &&
        ((v.X()*v.X() + v.Y()*v.Y()) < RADIUS*RADIUS))
    {
      XYPolar polar(eigenCopy<double>(v));
      double quadrantd = (polar.a_+2*M_PI)/2/M_PI*nQuadrants_;
      NUKLEI_ASSERT(quadrantd > 0);
      int quadrant = int(quadrantd) % nQuadrants_;
      quadrants_.at(quadrant).add(polar);
    }

    if (computeDepthImage_ && !rtrace &&
        (std::fabs(v.X()) < RADIUS && std::fabs(v.Y()) < RADIUS))
    {
#ifdef LEMG_IMAGE_SMOOTH
      const int lbound = -1, hbound = 2;
#else
      const int lbound = 0, hbound = 1;
#endif
      for (int i = lbound; i < hbound; i++)
      {
        for (int j = lbound; j < hbound; j++)
        {
          cv::Point pp = cvfm(v.X(), v.Y());
          pp.x += i;
          pp.y += j;
          
          if (pp.x < 0 || pp.y < 0 || pp.x >= IMAGE_SIZE || pp.y >= IMAGE_SIZE)
          {
            continue;
          }

          int& count = depthImageCount_.at<int>(pp);
          int& depth = depthImage_.at<int>(pp);

#ifdef LEMG_IMAGE_PC_AVG
          depth += dfm(distance);
          count++;
#else
          if (i == 0 && j == 0)
          {
            if (count < 0)
            {
              count = 0;
              depth = 0;
            }
            
            if (count == 0)
              depth = dfm(distance);
            else
              depth = std::min(depth, dfm(distance));
            count = 1;
          }
          else
          {
            if (count == 0)
            {
              depth = -dfm(distance);
              count = -1;
            }
            if (count <= 0)
            {
              depth = std::max(depth, -dfm(distance));
              count = -1;
            }
          }
#endif
          if (depth > std::numeric_limits<int>::max()/2)
            NUKLEI_WARN("Approaching int limit");
        }
      }
    }
  }
  
  void Disc::dump(const std::string& dir) const
  {
#ifdef LEMG_WRITE_DEBUG_DATA
    for (auto i = quadrants_.begin(); i != quadrants_.end(); ++i)
    {
      writeObservations(dir + "/" + stringify(std::distance(quadrants_.begin(), i)), i->kernelCollection());
      Quadrant qd = *i;
      qd.computeDescriptor(dir + "/" + stringify(std::distance(quadrants_.begin(), i)) + ".fit");
    }
#else
    NUKLEI_THROW("This method is only available in debug mode");
#endif
  }

  histogram Disc::getHistogramC() const
  {
    if (histogram_.size()) return histogram_;
    else NUKLEI_THROW("Histogram not available");
  }

  histogram Disc::getHistogram()
  {
    if (!computeQuadrants_) return histogram_;
    if (histogram_.size()) return histogram_;
#ifdef LEMG_CIRCULAR_DESCRIPTOR
    histogram h(nQuadrants_*QUADRANT_DESCRIPTOR_SIZE);
    h.setPointId(getPointId());
    for (unsigned i = 0; i != quadrants_.size(); ++i)
    {
      for (unsigned j = 0; j < QUADRANT_DESCRIPTOR_SIZE; ++j)
        h.set(i * QUADRANT_DESCRIPTOR_SIZE + j, quadrants_.at(i).getDescriptor()(j));
    }
    
    histogram_ = h;
    
    return h;
#else
    histogram h(nCodewords_);
    h.setPointId(getPointId());
    for (auto i = quadrants_.begin(); i != quadrants_.end(); ++i)
    {
      NUKLEI_ASSERT(i->getLabel() != -1);
      h.inc(i->getLabel());
    }
    
    histogram_ = h;
    
    return h;
#endif
  }
  
  void Disc::computeQuadrantDescriptors()
  {
    for (std::vector<Quadrant>::iterator i = quadrants().begin();
         i != quadrants().end(); ++i)
    {
      i->computeDescriptor();
#ifndef LEMG_WRITE_DEBUG_DATA
      i->clearPoints();
#endif
    }
  }

  void Disc::computeImageDescriptors(std::shared_ptr<RTracer> rtrace,
                                     Eigen::Vector3f viewpos,
                                     Eigen::Vector3f viewdir)
  {
    if (!rtrace)
    {
      for(int i=0; i<depthImage_.rows; i++)
      {
        for(int j=0; j<depthImage_.cols; j++)
        {
          if (depthImageCount_.at<int>(i,j) == 0)
            depthImage_.at<int>(i,j) = 255;
          else
          {
            depthImage_.at<int>(i,j) = depthImage_.at<int>(i,j) / depthImageCount_.at<int>(i,j);
          }
        }
      }
    }
    else
    {
      // Raytrace parallel rays via multiple viewpoints, not a single viewpoint
      //
      // We cannot simply translate the ray origin, as we need to move the distance
      // of one pixel along the tangent plane to the viewdir (normal vector)
      // we could compute the vector from point n to n+1, but this requires
      // solving an equation of 3 variables to move some distance d
      // in the plane
      //
      // Instead, we can compute our ray origins in a flat plane, then rotate
      // said plane to the viewdir which removes the need for solvers

      Eigen::Quaternionf q;
      q.w() = orientation_.w();
      q.x() = orientation_.x();
      q.y() = orientation_.y();
      q.z() = orientation_.z();
      cv::Point pp;
      for(pp.y=0; pp.y<depthImage_.rows; ++pp.y) {
        for(pp.x=0; pp.x<depthImage_.cols; ++pp.x) {
          // Pos of pixel wrt to the viewpos
          std::pair<double, double> offset = mfcv(pp);
          float x_pixel_offset = static_cast<float>(offset.first);
          float y_pixel_offset = static_cast<float>(offset.second);
          
          // Rotation matrices are numerically unstable as the angle of rotation
          // approaches 90 degrees, so use quaternions which are more accurate
          Eigen::Vector3f new_viewpos = q * (Eigen::Vector3f(x_pixel_offset, y_pixel_offset, 0)) + viewpos;
          // Subtract 0.125 so the origin viewpoint is at 0 distance
          std::pair<float, bgr_t> distance_bgr = rtrace->cast_ray(new_viewpos.data(), viewdir.data());
          LEMG_ASSERT(!(std::isnan)(distance_bgr.first));
          LEMG_ASSERT(!(std::isinf)(distance_bgr.first));
          if (0 < distance_bgr.first && distance_bgr.first < 2*RADIUS)
            depthImage_.at<int>(pp) = dfm(distance_bgr.first);
          else
            depthImage_.at<int>(pp) = 255;
          colorImage_.at<bgr_t>(pp) = distance_bgr.second;
        }
      }
      
//      float colorOffset = 0;
//      float colorMean = 0;
//      {
//        cv::Mat tmp;
//        colorImage_.convertTo(tmp, CV_32SC3);
//        auto chmean = cv::mean(tmp);
//        colorMean = (chmean[0] + chmean[1] + chmean[2])/3.;
//        colorOffset = 128. - colorMean;
//        tmp += colorOffset;
//        tmp.convertTo(colorImage_, CV_8UC3);
//      }
//      {
//        auto chmean = cv::mean(colorImage_);
//        float tmp = (chmean[0] + chmean[1] + chmean[2])/3.;
//        std::cout << tmp << " " << colorMean << std::endl;
//      }
    }
    
    // Assuming that the average size of holes in the mesh is larger than foot
    // Otherwise we need to check how homogeneous is the distribution of holes
    // in footprint (one side without support, vs sparse support throughout footprint)
    
    double ratio_of_depth_pixels_within_range = 0;
    {
      double tmp = (double(cv::countNonZero(depthImage_ != 255)) /
                    (depthImage_.rows*depthImage_.cols));
      ratio_of_depth_pixels_within_range = tmp;
    }
    
    depthImageRelativeSupportSurface_ = ratio_of_depth_pixels_within_range;
    
  }
  
  void Disc::computeFrom(std::shared_ptr<RTracer> rtrace,
                         const pcl::PointCloud<pcl_point_t>& cloud,
                         const pcl::search::Search<pcl_point_t>& tree,
                         const Eigen::Vector3f& gripperClocking,
                         const Eigen::Vector3d& viewpoint,
                         const bool hasViewpoint,
                         bool discPositionNormalization)
  {
    pcl::PointXYZ point = pclPoint(cloud.at(pointId_));

    std::vector<int> pointIdxRadiusSearch;
    std::vector<float> pointRadiusSquaredDistance;
    
    int nNeighbor = tree.radiusSearch(cloud.at(pointId_),
                                      RADIUS_T_SQRT2,
                                      pointIdxRadiusSearch,
                                      pointRadiusSquaredDistance);
    
    pcl::Normal normal;
    float curvature = -1; // std::numeric_limits<float>::quiet_NaN();
    normal.normal[0] = normal.normal[1] = normal.normal[2] = 0;
    
    if (nNeighbor > 0)
    {
      Eigen::Vector4f plane_parameters;
      jpl::computePointNormal(cloud, pointIdxRadiusSearch,
                              plane_parameters,
                              curvature);
      
      normal.normal_x = plane_parameters[0];
      normal.normal_y = plane_parameters[1];
      normal.normal_z = plane_parameters[2];
      
      if (hasViewpoint)
      {
        pcl::flipNormalTowardsViewpoint(point,
                                        viewpoint.x(),
                                        viewpoint.y(),
                                        viewpoint.z(),
                                        normal.normal[0],
                                        normal.normal[1],
                                        normal.normal[2]);
      }
      else
      {
        Vector3 inputNormal = wmNormalCopy(pclNormal(cloud.at(pointId_)));
        if (inputNormal.SquaredLength() > 0)
        {
          if (wmNormalCopy(normal).Dot(inputNormal) < 0)
          {
            normal.normal_x = -normal.normal_x;
            normal.normal_y = -normal.normal_y;
            normal.normal_z = -normal.normal_z;
          }
        }
        else
        {
          //NUKLEI_LOG(inputNormal);
          throw BadNormalError("");
          NUKLEI_THROW("If a viewpoint is not provided, the data must "
                          "provide front face info via polygon ordering.");
        }
      }
        
      if (discPositionNormalization)
      {
        typedef Eigen::Hyperplane<float, 3> P;
        typedef Eigen::ParametrizedLine<float, 3> L;
        typedef Eigen::Vector3f V;
        V n(plane_parameters[0], plane_parameters[1], plane_parameters[2]);
        double d = plane_parameters[3];
        P plane(n, d);
        L line(point.getVector3fMap(), n);
        point.getVector3fMap() = line.intersectionPoint(plane);
      }
    }
    
    {
      Vector3 v = wmNormalCopy(normal);
      double tmp = v.SquaredLength();
      if ( ! (tmp >= 0) || ! (std::fabs(tmp-1) < 1e-2) )
      {
        // If bad normal or normal is NaN
        throw BadNormalError(stringify(std::fabs(tmp-1)));
      }
    }

    setPoint(eigenCopy<double>(point));

    if (gripperClocking.squaredNorm() > 0)
    {
      Eigen::Matrix3d m;
      m.col(2) = eigenNormalCopy<double>(normal);
      Eigen::Vector3d c(gripperClocking(0), gripperClocking(1), gripperClocking(2));
            
      // There may be something wrong if the angle between a surface normal
      // and supposedly in-plane clocking is smaller than 22°.
      double dotp = m.col(2).dot(c);
      if (dotp > .92)
      {
        //NUKLEI_WARN("Label cluster axis too close to surface normal (" <<
        //            FastACos(dotp) * 180. / M_PI << " deg). Ignoring.");
        throw BadNormalError("Label cluster axis too close to surface normal");
      }
      m.col(1) = m.col(2).cross(c);
      m.col(0) = m.col(1).cross(m.col(2));
      setOrientation(Eigen::Quaterniond(m).normalized(), m.col(2));
    }
    else
      setOrientation(eigenNormalCopy<double>(normal));
    
    setCurvature(curvature);



    
    if (nNeighbor > 0 && (computeQuadrants_ || !rtrace))
    {
      for (size_t neighbor_i = 0; neighbor_i < pointIdxRadiusSearch.size();
           ++neighbor_i)
      {
        unsigned idx = pointIdxRadiusSearch.at(neighbor_i);
        addPoint(rtrace, wmCopy(pclPoint(cloud.at(idx))));
      }
    }
    
    if (computeQuadrants_)
    {
      computeQuadrantDescriptors();
    }
    if (computeDepthImage_)
    {
      if (!rtrace)
      {
        computeImageDescriptors(rtrace, Eigen::Vector3f(), Eigen::Vector3f());
      }
      else
      {
        // height shouldnt' matter because rays are parallel,
        // however depth is bounded [-125, 125] so use that as height
        float height = RADIUS;
        Eigen::Vector3f viewpos = eigenCopy<float>(point) + eigenNormalCopy<float>(normal) * height;
        Eigen::Vector3f viewdir = -eigenNormalCopy<float>(normal);
        // View position for raytracer
        // Need to fill out depth image by slightly varying ray direction
        computeImageDescriptors(rtrace, viewpos, viewdir);
      }
    }
  }

  void Disc::setPoint(const Eigen::Vector3d& point) { point_ = point; }
  
  void Disc::setOrientation(const Eigen::Vector3d& normal)
  {
    normal_ = normal;
    orientation_ = eigenCopy<double>(nuklei::la::so3FromS2(wmCopy(normal_), 2));
  }

  void Disc::setOrientation(const Eigen::Quaterniond& orientation,
                            const Eigen::Vector3d& normal)
  {
    orientation_ = orientation;
    if (normal.squaredNorm() > 0)
      normal_ = normal;
    else
    {
      Eigen::Matrix3d m(orientation);
      normal_ = m.col(2);
    }
  }

  
  void Disc::writePortableRadialDescriptors(const std::string& s,
                                            DescriptorWriter& dw) const
  {
    NUKLEI_ASSERT(!s.empty());
    std::string name = (s + "/" + stringify(getGraspable()) +
                        "/" +
                        stringify(getPointId(), -1, 16));
    if (computeDepthImage_ || computeColorImage_)
    {
      cv::Mat depth, color;
      if (computeDepthImage_)
        depthImage_.convertTo(depth, CV_8U);
      if (computeColorImage_)
        color = colorImage_;
      //NUKLEI_ASSERT(cv::imwrite(name + "-depth.png", depth));
      dw.write(color, depth, getGraspable(), getPointId(), getPoint(), getOrientation(),
               depthImageRelativeSupportSurface_);
    }
    if (computeQuadrants_)
    {
      histogram h = getHistogramC();
      if (false)
        {
          std::ofstream ofs((name + "-desc.txt").c_str());
          for (unsigned i = 0; i < h.size(); ++i)
            ofs << h.get(i) << "\n";
        }
//      cv::Mat tmp(h.size(), 1, CV_32FC1);
//      for (unsigned i = 0; i < h.size(); ++i)
//        tmp.at<float>(i) = h.get(i);
//      NUKLEI_ASSERTcv::imwrite(s + "-desc.png", tmp));
    }
    //std::ofstream ofs((s + "-label.txt").c_str());
    //ofs << getGraspable() << std::endl;
  }

  
}

