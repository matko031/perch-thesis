#include "raytrace.hpp"
#include <nuklei/Common.h>
#include <pcl/PolygonMesh.h>
#include <pcl/io/ply_io.h>

namespace lemg
{
  typedef nanort::real3<float> float3;
  inline float3 Lerp3(float3 v0, float3 v1, float3 v2, float u, float v) {
    return (1.0f - u - v) * v0 + u * v1 + v * v2;
  }

  //
  // \brief Given a path to a ply file, construct a bvh for raytracing
  //
  void RTracer::construct_scene(const ShapeIO& shio,
                                const pcl::PointCloud<pcl_point_t>::Ptr cloud,
                                const ShapeIO& naturalColorShio)
  {
    const pcl::PolygonMesh& mesh = shio.mesh();
    const pcl::PolygonMesh& colorMesh = naturalColorShio.mesh();
    
    bool useNaturalColor = colorMesh.polygons.size() > 0;
    
    colorCloud_.reset(new pcl::PointCloud<pcl::PointXYZRGB>());
    if (useNaturalColor)
    {
      *colorCloud_ = naturalColorShio.get<pcl::PointXYZRGB>();

      NUKLEI_ASSERT(mesh.polygons.size() == colorMesh.polygons.size())
      NUKLEI_ASSERT(cloud->size() == colorCloud_->size())
            
      // We currenty require both meshes to have the same geometric structure:
      for (int i = 0; i < cloud->size(); ++i)
      {
        NUKLEI_ASSERT(cloud->at(i).x == colorCloud_->at(i).x &&
                      cloud->at(i).y == colorCloud_->at(i).y &&
                      cloud->at(i).z == colorCloud_->at(i).z);
      }
      for (int i = 0; i < mesh.polygons.size(); ++i)
      {
        NUKLEI_ASSERT(mesh.polygons.at(i).vertices.at(0) == colorMesh.polygons.at(i).vertices.at(0) &&
                      mesh.polygons.at(i).vertices.at(1) == colorMesh.polygons.at(i).vertices.at(1) &&
                      mesh.polygons.at(i).vertices.at(2) == colorMesh.polygons.at(i).vertices.at(2));
      }
    }
    
    vertices_.resize(cloud->size() * 3);
    faces_.resize(mesh.polygons.size() * 3);
    
    for (int i = 0; i < cloud->size(); ++i)
    {
      vertices_.at(i * 3 + 0) = cloud->at(i).x;
      vertices_.at(i * 3 + 1) = cloud->at(i).y;
      vertices_.at(i * 3 + 2) = cloud->at(i).z;
     }
    
    for (int i = 0; i < mesh.polygons.size(); ++i)
    {
      faces_.at(i * 3 + 0) = mesh.polygons.at(i).vertices.at(0);
      faces_.at(i * 3 + 1) = mesh.polygons.at(i).vertices.at(1);
      faces_.at(i * 3 + 2) = mesh.polygons.at(i).vertices.at(2);
    }
    
    nanort::TriangleMesh<float> tmesh(
                                      vertices_.data(),
                                      faces_.data(),
                                      3 * sizeof(float));
    nanort::TriangleSAHPred<float> tpred(
                                         vertices_.data(),
                                         faces_.data(),
                                         3 * sizeof(float));
    
    nanort::BVHBuildOptions<float> opts;
    opts.cache_bbox = false;
    
    bvh_.Build(mesh.polygons.size(), tmesh, tpred, opts);
#if 0
    nanort::BVHBuildStatistics stats = bvh_.GetStatistics();
    NUKLEI_LOG("Raytracer BVH statistics:\n" <<
               "# of leaf   nodes  : " << stats.num_leaf_nodes << "\n" <<
               "# of branch nodes  : " << stats.num_branch_nodes << "\n" <<
               "Max tree depth   : " << stats.max_tree_depth);
#endif
  }
  
  //
  // \brief Cast a ray and return the distance to mesh collision
  // returns 0 in case of no collision
  //
  std::pair<float, bgr_t> RTracer::cast_ray(float viewpoint[3], float viewdir[3]) const
  {
    nanort::Ray<float> ray;
    // compiler converts float[3] in argument to float*
    // casting float* to float[3] afterwards violates some dumb ISO rule
    // workaround to disabling -fpermissive
    std::memcpy(ray.org, viewpoint, sizeof(float) * 3);
    std::memcpy(ray.dir, viewdir, sizeof(float) * 3);
    ray.min_t = 0.0f;
    ray.max_t = 2*RADIUS;
    nanort::TriangleIntersector<> intersector(vertices_.data(),
                                              faces_.data(),
                                              3 * sizeof(float));
    nanort::TriangleIntersection<> hit;
    bool did_hit = bvh_.Traverse(ray, intersector, &hit);

    bgr_t bgr;
    bgr.r = 0;
    bgr.g = 0;
    bgr.b = 0;
    
    if (did_hit)
    {
      unsigned int prim_id = hit.prim_id;
      
      if (colorCloud_->size() > 0)
      {
        unsigned int f0, f1, f2;
        f0 = faces_[3 * prim_id + 0];
        f1 = faces_[3 * prim_id + 1];
        f2 = faces_[3 * prim_id + 2];
        
        float3 c0, c1, c2;
        c0[0] = colorCloud_->at(f0).r;
        c0[1] = colorCloud_->at(f0).g;
        c0[2] = colorCloud_->at(f0).b;
        c1[0] = colorCloud_->at(f1).r;
        c1[1] = colorCloud_->at(f1).g;
        c1[2] = colorCloud_->at(f1).b;
        c2[0] = colorCloud_->at(f2).r;
        c2[1] = colorCloud_->at(f2).g;
        c2[2] = colorCloud_->at(f2).b;
        
        float3 vcol = Lerp3(c0, c1, c2, hit.u, hit.v);
        
        bgr.r = vcol[0];
        bgr.g = vcol[1];
        bgr.b = vcol[2];
      }

      return std::make_pair(hit.t, bgr);
    }
    
    return std::make_pair(0., bgr);
  }
  
}
