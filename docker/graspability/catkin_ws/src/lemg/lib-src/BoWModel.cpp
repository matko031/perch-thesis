#include <fdeep/fdeep.hpp>

#include <Eigen/Geometry>

#include <pcl/common/pca.h>
#include <pcl/search/kdtree.h>
#include <pcl/search/organized.h>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <nuklei/ObservationIO.h>
#include <nuklei/PCLBridge.h>
#include <nuklei/ProgressIndicator.h>
#include <nuklei/Stopwatch.h>
#include <nuklei/stats.h>

#include "Conversions.hpp"
#include "DescriptorWriter.hpp"
#include "raytrace.hpp"
#include <lemg/BoWModel.hpp>
#include <lemg/Labeling.hpp>
#include <lemg/io.hpp>

#ifndef NUKLEI_LEMUR_BRANCH
#warning This library requires the lemur branch of Nuklei
#endif

//#define LEMG_WRITE_DEBUG_DATA

namespace lemg {
using namespace nuklei;

extern boost::shared_ptr<HighFive::File> pfile;
extern boost::shared_ptr<HighFive::DataSpace> pdataspace;
extern boost::shared_ptr<HighFive::DataSetCreateProps> pprops;
extern boost::shared_ptr<HighFive::DataSet> pdataset;

static QuadrantDescriptor normalized(const QuadrantDescriptor &qd,
                                     const QuadrantDescriptor &qdMean,
                                     const QuadrantDescriptor &qdStdev) {
  QuadrantDescriptor n(qd - qdMean);
  for (unsigned di = 0; di < QUADRANT_DESCRIPTOR_SIZE; ++di) {
    n(di) /= qdStdev(di);
  }
  return n;
}

void Codebook::readCodewords(const std::string &file) {
  codewords_.clear();
  std::ifstream ifs(file.c_str());
  if (!ifs.good())
    NUKLEI_THROW("Failed to open file `" << file << "'");

  normalized_ = true;

  for (unsigned di = 0; di < QUADRANT_DESCRIPTOR_SIZE; ++di)
    NUKLEI_ASSERT(ifs >> qdMean_(di));
  for (unsigned di = 0; di < QUADRANT_DESCRIPTOR_SIZE; ++di)
    NUKLEI_ASSERT(ifs >> qdStdev_(di));

  for (int j = 0; j < nCodewords_; ++j) {
    QuadrantDescriptor d;
    for (unsigned di = 0; di < QUADRANT_DESCRIPTOR_SIZE; ++di)
      NUKLEI_ASSERT(ifs >> d(di));
    codewords_.push_back(d);
  }
}

void Codebook::writeCodewords(const std::string &file) const {
  // If removing this line, read/write the normalized_ variable from text file
  NUKLEI_ASSERT(normalized_);

  std::ofstream ofs(file.c_str());
  ofs.precision(12);

  for (unsigned di = 0; di < QUADRANT_DESCRIPTOR_SIZE; ++di)
    ofs << qdMean_(di) << " ";
  ofs << "\n";
  for (unsigned di = 0; di < QUADRANT_DESCRIPTOR_SIZE; ++di)
    ofs << qdStdev_(di) << " ";
  ofs << "\n";
  for (auto i = codewords_.begin(); i != codewords_.end(); ++i)
    for (unsigned di = 0; di < QUADRANT_DESCRIPTOR_SIZE; ++di)
      ofs << (*i)(di) << " ";
  ofs << "\n";

  // right now, the following lines are for logging purpuses - they
  // will be ignored by the read function.

  ofs << "radius: " << RADIUS << "\n";
  ofs << "quadrant_descriptor_size: " << QUADRANT_DESCRIPTOR_SIZE << "\n";
  ofs << "descriptor_implementation: " << LEMG_DESCRIPTOR_IMPLEMENTATION
      << "\n";
  ofs << "n_codewords: " << nCodewords_ << "\n";
}

QuadrantDescriptor Codebook::normalized(const QuadrantDescriptor &qd) const {
  NUKLEI_ASSERT(normalized_);
  return lemg::normalized(qd, qdMean_, qdStdev_);
}

void Codebook::setNormalization(const QuadrantDescriptor &qdMean,
                                const QuadrantDescriptor &qdStdev) {
  qdMean_ = qdMean;
  qdStdev_ = qdStdev;
  normalized_ = true;
}

std::vector<int> Codebook::train(const std::vector<QuadrantDescriptor> &data) {
  NUKLEI_INFO("Codebook::train() (cv::kmeans()) ...");

  cv::Mat cvData(data.size(), QUADRANT_DESCRIPTOR_SIZE, CV_32FC1);
  int K = nCodewords_;
  cv::Mat cvLabels;
  cv::TermCriteria crit = cv::TermCriteria(
      cv::TermCriteria::EPS + cv::TermCriteria::COUNT, 10, 1.0);
  int attempts = 3;
  int flags = cv::KMEANS_PP_CENTERS;
  cv::Mat centers;

  for (unsigned i = 0; i < data.size(); ++i) {
    const QuadrantDescriptor &d = data.at(i);
    for (unsigned di = 0; di < QUADRANT_DESCRIPTOR_SIZE; ++di) {
      cvData.at<float>(i, di) = d(di);
    }
  }

  cv::kmeans(cvData, K, cvLabels, crit, attempts, flags, centers);

  codewords_.resize(nCodewords_);
  for (int i = 0; i < K; i++) {
    for (unsigned di = 0; di < QUADRANT_DESCRIPTOR_SIZE; ++di)
      codewords_.at(i)(di) = centers.at<float>(i, di);
  }

  std::vector<int> labels;
  for (unsigned i = 0; i < data.size(); ++i)
    labels.push_back(cvLabels.at<int>(i));
  return labels;
}

void Codebook::annotate(std::vector<Disc> &discs) const {
  for (auto i = discs.begin(); i != discs.end(); ++i) {
    i->setNCodewords(nCodewords_);
    for (auto j = i->quadrants().begin(); j != i->quadrants().end(); ++j) {
      j->setLabel(computeClass(j->getDescriptor()));
    }
    i->getHistogram();
  }
}

void Codebook::saveAsPointcloud(const std::string &basename,
                                const std::vector<QuadrantDescriptor> &data,
                                const std::vector<int> &labels) const {
  KernelCollection centers;
  for (int i = 0; i < nCodewords_; ++i) {
    RGBColor rgb;
    rgb.makeRandom();
    ColorDescriptor cd;
    cd.setColor(rgb);
    kernel::r3 k;
    k.loc_ = Vector3(codewords_.at(i)(0), codewords_.at(i)(1),
                     QUADRANT_DESCRIPTOR_SIZE == 3 ? codewords_.at(i)(2) : 0);
    k.setDescriptor(cd);
    centers.add(k);
  }
  KernelCollection descriptors;
  for (unsigned i = 0; i < data.size(); ++i) {
    const QuadrantDescriptor &d = data.at(i);
    kernel::r3 k;
    k.loc_ = Vector3(d(0), d(1), QUADRANT_DESCRIPTOR_SIZE == 3 ? d(2) : 0);
    // int label = computeClass(d, false);
    int label = labels.at(i);
    k.setDescriptor(centers.at(label).getDescriptor());
    descriptors.add(k);
  }
  writeObservations(basename + "descriptors.pcd", descriptors,
                    Observation::PCD);
  writeObservations(basename + "centers.pcd", centers, Observation::PCD);
}

void Codebook::verify(const std::vector<QuadrantDescriptor> &data,
                      const std::vector<int> &labels) const {
  NUKLEI_INFO("Codebook::verify() ...");
  for (unsigned i = 0; i < data.size(); ++i) {
    const QuadrantDescriptor &d = data.at(i);
    if (labels.at(i) != computeClass(d, false)) {
      int cvl = labels.at(i);
      int myl = computeClass(d, false);
      double cvd = (d - codewords_.at(cvl)).norm();
      double myd = (d - codewords_.at(myl)).norm();
      if (cvd > FLOATTOL) {
        NUKLEI_ASSERT(myd < cvd);
        if (myd / cvd < .8)
          NUKLEI_WARN("Scary OpenCV Kmeans label: " << myd << " " << cvd);
      }
    }
  }
  NUKLEI_INFO("Codebook::verify() ... done.");
}

int Codebook::computeClass(QuadrantDescriptor qd, const bool normalize) const {
  if (normalize)
    qd = normalized(qd);
  int c = -1;
  double minSDist = 0;
  for (int i = 0; i < nCodewords_; ++i) {
    double sDist = (qd - codewords_.at(i)).squaredNorm();
    if (c == -1 || sDist < minSDist) {
      c = i;
      minSDist = sDist;
    }
  }
  return c;
}

const std::vector<QuadrantDescriptor> &Codebook::getCodewords() const {
  return codewords_;
}

BoWModel::BoWModel()
    : shapeIsMesh_(false), clearImageDescriptors_(true), nQuadrants_(0),
      nCodewords_(0), discPositionNormalization_(false),
      gripperClocking_(GRIPPER_CLOCK_NONE), viewpoint_(Eigen::Vector3d::Zero()),
      hasViewpoint_(false), codebook_(nCodewords_), maxNDescriptors_(-1),
      useROI_(false), roiCenter_(Eigen::Vector3d::Zero()), roiRadius_(0) {}

void BoWModel::setNaturalColorPointCloud(const std::string &cloudName) {
  naturalColorShio_.read(cloudName);
}

void BoWModel::setNaturalColorPointCloud(const pcl::PolygonMesh &mesh) {
  naturalColorShio_.read(mesh);
}

void BoWModel::setPointCloud(const std::string &cloudName) {
  cloudName_ = cloudName;
  shio_.read(cloudName_);
  pcl::PointCloud<pcl_point_t>::Ptr cloud(new pcl::PointCloud<pcl_point_t>());
  if (shio_.getFileFormat() == "pcd") {
    *cloud = shio_.get<pcl_point_t>();
    if (shio_.pc2HasNormals() == false) {
      NUKLEI_WARN("Input data does not define frontal faces");
    }
  } else {
    *cloud = shio_.getWithPolygonNormals<pcl_point_t>();
  }
  shapeIsMesh_ = shio_.getFileFormat() != "pcd";
  setPointCloud(cloud);
}

void BoWModel::setPointCloud(pcl::PointCloud<pcl_point_t>::Ptr cloud) {
  cloud_ = cloud;
  if (cloud_->height == 1)
    tree_.reset(new pcl::search::KdTree<pcl_point_t>);
  else
    tree_.reset(new pcl::search::OrganizedNeighbor<pcl_point_t>);
  tree_->setInputCloud(cloud_);
}

void BoWModel::setPointCloud(const pcl::PolygonMesh &mesh) {
  shio_.read(mesh);
  pcl::PointCloud<pcl_point_t>::Ptr cloud(new pcl::PointCloud<pcl_point_t>());
  *cloud = shio_.getWithPolygonNormals<pcl_point_t>();
  shapeIsMesh_ = true;
  setPointCloud(cloud);
}

void BoWModel::setMaxNDescriptors(const int n) { maxNDescriptors_ = n; }

void BoWModel::clusterLabeledPoints() {
  if (shio_.mesh().polygons.size() == 0 ||
      graspabilityLabels_.size() != cloud_->size()) {
    if (gripperClocking_ == GRIPPER_CLOCK_CLUSTER_AXIS) {
      NUKLEI_THROW("Need both labels and polygons to compute clocking from "
                   "label clusters");
    } else
      return;
  }
  labelClusters_.clear();

  for (int i = 0; i < graspabilityLabels_.size(); ++i) {
    if (graspabilityLabels_.at(i) == -1)
      labelClusters_.push_back(label_cluster_ptr_t());
    else {
      label_cluster_ptr_t lc(new label_cluster_t());
      lc->pts = std::vector<int>{i};
      labelClusters_.push_back(lc);
    }
  }
  for (const pcl::Vertices &triangle : shio_.mesh().polygons) {
    NUKLEI_ASSERT(triangle.vertices.size() == 3);
    for (unsigned int v1i = 0; v1i < 3; v1i++) {
      const unsigned int v2i = (v1i + 1) % 3;
      const unsigned int v1 = triangle.vertices.at(v1i);
      const unsigned int v2 = triangle.vertices.at(v2i);
      if (graspabilityLabels_.at(v1) == -1)
        continue;
      if (graspabilityLabels_.at(v1) != graspabilityLabels_.at(v2))
        continue;
      label_cluster_ptr_t v1c = labelClusters_.at(v1);
      label_cluster_ptr_t v2c = labelClusters_.at(v2);
      if (v1c == v2c)
        continue;
      for (const int &v : v2c->pts)
        labelClusters_.at(v) = v1c;
      // We've overwritten all instances of v2c that were held by labelClusters_
      NUKLEI_ASSERT(v2c.unique());
      std::copy(v2c->pts.begin(), v2c->pts.end(), std::back_inserter(v1c->pts));
    }
  }

  std::unordered_set<label_cluster_ptr_t> s(labelClusters_.begin(),
                                            labelClusters_.end());

  int id = 0;
  for (auto i : s) {
    if (!i)
      continue;

    i->id = id++;

    if (gripperClocking_ == GRIPPER_CLOCK_CLUSTER_AXIS) {
      if (i->pts.size() < 5) {
        // NUKLEI_WARN("Label cluster has only " << i->pts.size() << " elements.
        // "
        //            "Skipping PCA.");
      } else {
        pcl::PCA<pcl_point_t> pca;
        pca.setInputCloud(cloud_);
        // Cannot belive that PCL doesn't have a setIndices method that takes a
        // reference.
        boost::shared_ptr<std::vector<int>> tmp(new std::vector<int>(i->pts));
        pca.setIndices(tmp);

        auto eigenvalues = pca.getEigenValues();
        auto eigenvectors = pca.getEigenVectors();
        NUKLEI_ASSERT(eigenvalues(0) > eigenvalues(1) &&
                      eigenvalues(0) > eigenvalues(2));
        if (eigenvalues(0) < 1.5 * eigenvalues(1)) {
          NUKLEI_WARN("Label cluster's main axis is not obvious.");
        } else {
          i->axis = eigenvectors.col(0);
        }
      }
    }
  }
}

void BoWModel::drawDescriptorSubset() {
  if (graspabilityLabels_.size() != 0)
    NUKLEI_ASSERT(graspabilityLabels_.size() == cloud_->size());
  inputSubsetIndices_.clear();
  std::vector<int> pai, nai;
  for (unsigned i = 0; i < cloud_->size(); ++i) {
    // fixme: use a search structure for identifying points within ROI

    if (useROI_) {
      Eigen::Vector3d diff =
          roiCenter_ - eigenCopy<double>(pclPoint(cloud_->at(i)));
      double d = diff.squaredNorm();
      if (d > roiRadius_ * roiRadius_)
        continue;
    }

    if (graspabilityLabels_.size() == 0)
      inputSubsetIndices_.push_back(i);
    else {
      if (graspabilityLabels_.at(i) == 1)
        pai.push_back(i);
      else if (graspabilityLabels_.at(i) == 0)
        nai.push_back(i);
    }
  }
  std::random_shuffle(pai.begin(), pai.end());
  std::random_shuffle(nai.begin(), nai.end());
  if (graspabilityLabels_.size() != 0) {
#if 0
      NUKLEI_INFO("Found " << pai.size() << " positive data, " << nai.size() << " negative data."
                  "\nTrimming to " << std::min(pai.size(), nai.size()) << ".");
      pai.resize(std::min(pai.size(), nai.size()));
      nai.resize(std::min(pai.size(), nai.size()));
#else
    NUKLEI_INFO("Found " << pai.size() << " positive data, " << nai.size()
                         << " negative data."
                            "\nNOT trimming.");
#endif
    inputSubsetIndices_.insert(inputSubsetIndices_.end(), pai.begin(),
                               pai.end());
    inputSubsetIndices_.insert(inputSubsetIndices_.end(), nai.begin(),
                               nai.end());
  }
  std::random_shuffle(inputSubsetIndices_.begin(), inputSubsetIndices_.end());
  if (maxNDescriptors_ != -1 &&
      int(inputSubsetIndices_.size()) > maxNDescriptors_) {
    std::random_shuffle(inputSubsetIndices_.begin(), inputSubsetIndices_.end());
    inputSubsetIndices_.resize(maxNDescriptors_);
  }
}

void BoWModel::computeDescriptors() {
  computeRadialDescriptors();
#ifndef LEMG_CIRCULAR_DESCRIPTOR
  codebook_.annotate(discs_);
#endif
}

void BoWModel::computeRadialDescriptors() {
  discs_.clear();
  inputSubsetIndices_.clear();

  bool computeColorImage = naturalColorShio_.mesh().polygons.size() > 0;
  DescriptorWriter dw(computeColorImage, shapeIsMesh_);
  if (!portableRadialDescriptorOutput_.empty()) {
    dw.create(portableRadialDescriptorOutput_);
    dw.writeNVertices(cloud_->size());
  }

  drawDescriptorSubset();
  clusterLabeledPoints();

  std::shared_ptr<RTracer> rtrace;
  if (shapeIsMesh_) {
    rtrace = std::make_shared<RTracer>();
    rtrace->construct_scene(shio_, cloud_, naturalColorShio_);
  }

  int skipped = 0;
  ProgressIndicator piDesc(inputSubsetIndices_.size(),
                           "Computing descriptors: ");
  // piDesc.setBackspace(false);
  Stopwatch sw;
  sw.setOutputType(Stopwatch::QUIET);
  // sw.disable();
  double searchTime = 0, normalTime = 0, descriptorTime = 0;

#ifdef _OPENMP
#pragma omp parallel for
#endif
  for (unsigned ci = 0; ci < inputSubsetIndices_.size(); ci++) {
    sw.reset();

    unsigned point_idx = inputSubsetIndices_.at(ci);

    NUKLEI_ASSERT(graspabilityLabels_.size() == 0 ||
                  graspabilityLabels_.at(point_idx) >= 0);

    Disc disc;
    disc.setNQuadrants(nQuadrants_);
    disc.setPointId(point_idx);
    disc.setComputeColorImage(computeColorImage);

    Eigen::Vector3f gripperClockingAxis = Eigen::Vector3f::Zero();
    if (gripperClocking_ == GRIPPER_CLOCK_CLUSTER_AXIS) {
      gripperClockingAxis = labelClusters_.at(point_idx)->axis;
      if (gripperClockingAxis.squaredNorm() == 0) {
        skipped++;
        continue;
      }
    } else if (gripperClocking_ == GRIPPER_CLOCK_X) {
      gripperClockingAxis = Eigen::Vector3f::UnitX();
    } else {
      NUKLEI_ASSERT(gripperClocking_ == GRIPPER_CLOCK_NONE);
    }

    if (labelClusters_.size() > 0 && labelClusters_.at(point_idx)) {
      disc.setLabelCluster(labelClusters_.at(point_idx)->id);
    }

    try {
      disc.computeFrom(rtrace, *cloud_, *tree_, gripperClockingAxis, viewpoint_,
                       hasViewpoint_, discPositionNormalization_);
    } catch (const BadNormalError &e) {
      skipped++;
      continue;
    }

    if (graspabilityLabels_.size() >
        0) // fixme: casting an int to a bool. -1 is cast to graspable.
      disc.setGraspable(graspabilityLabels_.at(point_idx));

#ifdef LEMG_CIRCULAR_DESCRIPTOR
    disc.getHistogram();
    if (!portableRadialDescriptorOutput_.empty())
      disc.writePortableRadialDescriptors(portableRadialDescriptorOutput_, dw);
    if (clearImageDescriptors_)
      disc.clearDepthImageDescriptors();
#endif

#ifdef _OPENMP
#pragma omp critical(lemg_compute_descriptor)
#endif
    {
      discs_.push_back(disc);
      piDesc.inc();
    }

    descriptorTime += sw.lap().first;
    sw.reset();
  }
  piDesc.forceEnd();

  if (sw.isEnabled())
    NUKLEI_DEBUG(NUKLEI_NVP(searchTime) << "\n"
                                        << NUKLEI_NVP(normalTime) << "\n"
                                        << NUKLEI_NVP(descriptorTime));

  NUKLEI_DEBUG("Skipped " << skipped
                          << " bad normals or bad cluster axes, out of "
                          << inputSubsetIndices_.size() << " point.");

#ifdef LEMG_OUTPUT_DISC_POSES_TO_TMP
  std::ofstream ofs("/tmp/disc_poses.txt");
  for (auto i : discs_) {
    auto l = i.getPoint();
    auto o = i.getOrientation();
    ofs << l.x() << " " << l.y() << " " << l.z() << "   ";
    ofs << o.w() << " " << o.x() << " " << o.y() << " " << o.z() << "\n";
  }
#endif
}

void BoWModel::setGraspability(const std::vector<int> &g) {
  for (const auto &i : g) {
    if (i < 0)
      continue;
    NUKLEI_ASSERT(0 <= i && i <= 1);
  }
  graspabilityLabels_ = g;
}

void BoWModel::setGraspability(const std::vector<double> &g) {
  graspabilityLabels_.clear();
  for (const double &i : g) {
    if (i < 0)
      graspabilityLabels_.push_back(-1);
    else if (i < .5)
      graspabilityLabels_.push_back(0);
    else
      graspabilityLabels_.push_back(1);
    NUKLEI_ASSERT(i <= 1);
  }
}

void BoWModel::setGraspability(const std::string &labelName) {
  if (labelName == "-") {
    if (!shio_.getFileFormat().empty())
      setGraspability(readLabels(shio_));
    else
      NUKLEI_THROW(
          "Reading labels from datafile requires to load the data first.");
  } else {
    std::vector<int> labels;
    readLabels(labelName, labels);
    setGraspability(labels);
  }
}

void BoWModel::learnCodewords() {
  computeRadialDescriptors();
#ifndef LEMG_CIRCULAR_DESCRIPTOR
  std::vector<QuadrantDescriptor> data, normalizedData;
  std::vector<nuklei::stats<double>> stats(QUADRANT_DESCRIPTOR_SIZE);
  ProgressIndicator pi(discs_.size(), "Extracting descriptors ");
  for (auto disc_i = discs_.begin(); disc_i != discs_.end(); ++disc_i) {
    for (std::vector<Quadrant>::iterator i = disc_i->quadrants().begin();
         i != disc_i->quadrants().end(); ++i) {
      QuadrantDescriptor qd = i->getDescriptor();
      data.push_back(qd);
      for (unsigned di = 0; di < QUADRANT_DESCRIPTOR_SIZE; ++di)
        stats.at(di).push(qd(di));
    }
    pi.inc();
  }

  QuadrantDescriptor qdMean, qdStdev;
  for (unsigned di = 0; di < QUADRANT_DESCRIPTOR_SIZE; ++di) {
    qdMean(di) = stats.at(di).mean();
    qdStdev(di) = stats.at(di).stdev();
  }
  codebook_.setNormalization(qdMean, qdStdev);

  normalizedData.resize(data.size());
  for (unsigned j = 0; j < data.size(); ++j)
    normalizedData.at(j) = normalized(data.at(j), qdMean, qdStdev);

  std::vector<int> labels = codebook_.train(normalizedData);

  codebook_.annotate(discs_);
#ifdef LEMG_WRITE_DEBUG_DATA
  codebook_.saveAsPointcloud("lemg-debug-clustering-normalized-",
                             normalizedData, labels);
  codebook_.saveAsPointcloud("lemg-debug-clustering-not-normalized-", data,
                             labels);
  writeClusteredQuadrants();
#endif
#endif
}

void BoWModel::readCodewords(const std::string &file) {
  codebook_.readCodewords(file);
}

void BoWModel::writeCodewords(const std::string &file) const {
  codebook_.writeCodewords(file);
}

void BoWModel::readModel(const std::string &file) {
  NUKLEI_TRACE_BEGIN();
  readObject(*this, file);
  NUKLEI_TRACE_END();
}

void BoWModel::writeModel(const std::string &file) { writeObject(*this, file); }

void BoWModel::trainClassifier() {
  Stopwatch sw("Training ");
  sw.setMinLogLevel(Log::INFO);

  sw.lap("Descriptors");

  std::vector<histogram> histograms;
  std::vector<int> graspabilityLabels;

  int positive = 0, negative = 0;

  for (auto disc_i = discs_.begin(); disc_i != discs_.end(); ++disc_i) {
    histograms.push_back(disc_i->getHistogram());
    graspabilityLabels.push_back(disc_i->getGraspable());
    if (disc_i->getGraspable() == 1)
      positive++;
    else if (disc_i->getGraspable() == 0)
      negative++;
    else
      NUKLEI_THROW("Unsupported class number");
  }

  NUKLEI_INFO("Training on "
              << discs_.size() << " discs, with "
              << double(positive) / discs_.size() * 100 << "% positives, "
              << double(negative) / discs_.size() * 100 << "% negatives.");

  klr_ = lemg::KernelLogisticRegressor();
  klr_->setData(histograms, graspabilityLabels);
  sw.lap("Gram Matrix");
  klr_->train();
  sw.lap("klr_->train()");

  int success = 0;
  ProgressIndicator pi(histograms.size(), "Self-validation: ");
#ifdef _OPENMP
#pragma omp parallel for
#endif
  for (unsigned i = 0; i < histograms.size(); ++i) {
    Vector2 c = klr_->test(histograms.at(i));
    NUKLEI_ASSERT(std::abs(c.x() + c.y() - 1) < FLOATTOL);
    int d = 0;
    if (c.y() > .5)
      d = 1;
    int l = graspabilityLabels.at(i);
#ifdef _OPENMP
#pragma omp critical(lemg_test_classifier)
#endif
    {
      if (l == d)
        success++;
      pi.inc();
    }
  }
  NUKLEI_INFO("Success rate: " << double(success) / histograms.size());
}

std::vector<double> BoWModel::testClassifier() {
  Stopwatch sw("Testing ");
  sw.setMinLogLevel(Log::INFO);

  sw.lap("Descriptors");

  ProgressIndicator pi(discs_.size(), "Classifying ");
  classProbabilities_ = std::vector<double>(cloud_->size(), -1);
  normalVectors_ =
      std::vector<Eigen::Vector3d>(cloud_->size(), Eigen::Vector3d::Zero());
  curvatures_ = std::vector<double>(cloud_->size(), -1);
  histograms_ = std::vector<histogram>(cloud_->size());
#ifdef _OPENMP
#pragma omp parallel for
#endif
  for (unsigned disc_i = 0; disc_i < discs_.size(); ++disc_i) {
    Vector2 c = klr_->test(discs_.at(disc_i).getHistogram());
#ifdef _OPENMP
#pragma omp critical(lemg_test_classifier)
#endif
    {
      classProbabilities_.at(discs_.at(disc_i).getPointId()) = c.y();
      normalVectors_.at(discs_.at(disc_i).getPointId()) =
          discs_.at(disc_i).getNormal();
      curvatures_.at(discs_.at(disc_i).getPointId()) =
          discs_.at(disc_i).getCurvature();
      histograms_.at(discs_.at(disc_i).getPointId()) =
          discs_.at(disc_i).getHistogram();
      pi.inc();
    }
  }
  sw.lap("Classification");
  sw.split("testing");

  return classProbabilities_;
}

void BoWModel::readClassifier(const std::string &file) {
  klr_ = KernelLogisticRegressor();
  klr_->read(file);
}

void BoWModel::writeClassifier(const std::string &file) const {
  klr_->write(file);
  // right now, the following lines are for logging purpuses - they
  // will be ignored by the read function.
  std::ofstream ofs(file, std::ios_base::app);
  ofs << "radius: " << RADIUS << "\n";
  ofs << "quadrant_descriptor_size: " << QUADRANT_DESCRIPTOR_SIZE << "\n";
  ofs << "descriptor_implementation: " << LEMG_DESCRIPTOR_IMPLEMENTATION
      << "\n";
  ofs << "n_quadrants: " << nQuadrants_ << "\n";
  ofs << "n_codewords: " << nCodewords_ << "\n";
  ofs << "disc_position_normalization: " << discPositionNormalization_ << "\n";
  ofs << "max_n_descriptors: " << maxNDescriptors_ << "\n";
}

void BoWModel::writeClusteredQuadrants() const {
#ifdef LEMG_WRITE_DEBUG_DATA
  std::vector<KernelCollection> clusters(nCodewords_);
  ProgressIndicator piWrite(discs_.size() * discs_.front().getNQuadrants(),
                            "Writing clustered quadrants");
  for (std::vector<Disc>::const_iterator i = discs_.begin(); i != discs_.end();
       ++i) {
    const std::vector<Quadrant> &quadrants = i->quadrants();
    for (std::vector<Quadrant>::const_iterator j = quadrants.begin();
         j != quadrants.end(); ++j) {
      j->addSlice(clusters.at(j->getLabel()));
      piWrite.inc();
    }
  }
  for (unsigned i = 0; i < clusters.size(); ++i) {
    if (clusters.at(i).size() == 0) {
      NUKLEI_DEBUG("Quadrant cluster " << i << " is empty");
      continue;
    }
    std::string filename = "lemg-debug-cluster-" + stringify(i);
    writeObservations(filename + ".pcd", clusters.at(i), Observation::PCD);
    std::ofstream ofs((filename + ".txt").c_str());
    ofs << codebook_.getCodewords().at(i)(0) << "(";
    ofs << codebook_.getCodewords().at(i)(0) / M_PI * 180 << "deg) "
        << codebook_.getCodewords().at(i)(1) << std::endl;
  }
#else
  NUKLEI_THROW("This method is only available in debug mode");
#endif
}

pcl::PointCloud<pcl::PointXYZINormal>::Ptr
filter(pcl::PointCloud<pcl::PointXYZINormal>::Ptr points, unsigned maxN,
       double qualityThreshold, double posDistThreshold,
       double oriDistThreshold) {
  pcl::PointCloud<pcl::PointXYZINormal>::Ptr selected;
  selected.reset(new pcl::PointCloud<pcl::PointXYZINormal>());

  std::vector<std::pair<double, unsigned>> sorted;

  for (unsigned i = 0; i < points->size(); ++i) {
    sorted.push_back(std::make_pair(points->at(i).intensity, i));
  }
  std::sort(sorted.begin(), sorted.end(),
            std::greater<std::pair<double, unsigned>>());

  for (const auto &i : sorted) {
    using namespace nuklei;
    bool overlapping = false;
    const pcl::PointXYZINormal &p = points->at(i.second);
    if (qualityThreshold > 0 && p.intensity < qualityThreshold)
      continue;
    for (const auto &j : *selected) {
      Vector3 pd = (Vector3(p.x, p.y, p.z) - Vector3(j.x, j.y, j.z));

      Vector3 p_normal = Vector3(p.normal_x, p.normal_y, p.normal_z);
      Vector3 j_normal = Vector3(j.normal_x, j.normal_y, j.normal_z);

      {
        // This block verifies that normals are within TOL of unit lenght.
        // (because normals are normalized to unit length below)
        // float32 epsilon is 5e-8, + we're comparing squared lengths.
        // TOL = 1e-5 seems like a sane value.
        double TOL = 1e-5;
        double p_normal_sn = p_normal.SquaredLength();
        double j_normal_sn = j_normal.SquaredLength();
        if (std::fabs(p_normal_sn - 1) > TOL) {
          NUKLEI_THROW("Normal isn't unit length. Error is "
                       << std::fabs(p_normal_sn - 1));
        }
        if (std::fabs(j_normal_sn - 1) > TOL) {
          NUKLEI_THROW("Normal isn't unit length. Error is "
                       << std::fabs(j_normal_sn - 1));
        }

        double dot = p_normal.Dot(j_normal);
        if (!(-1 - TOL < dot && dot < 1 + TOL)) {
          NUKLEI_THROW("Dot product out of bounds: " << dot);
        }
      }

      // Nuklei's tolerance is 1e-12, which is smaller than float32 epsilon.
      // Need to normalize.
      double nd = dist<groupS::s2>::d(la::normalized(p_normal),
                                      la::normalized(j_normal));

      if (pd.SquaredLength() < posDistThreshold * posDistThreshold &&
          nd < oriDistThreshold) {
        overlapping = true;
        break;
      }
    }
    if (overlapping)
      continue;

    selected->push_back(p);
    if (maxN != 0 && selected->size() == maxN)
      break;
  }
  return selected;
}

pcl::PointCloud<pcl::PointXYZINormal>::Ptr
BoWModel::getEvaluatedGrasps() const {
  pcl::PointCloud<pcl::PointXYZINormal>::Ptr points;
  points.reset(new pcl::PointCloud<pcl::PointXYZINormal>());
  for (unsigned disc_i = 0; disc_i < discs_.size(); ++disc_i) {
    unsigned i = discs_.at(disc_i).getPointId();

    pcl::PointXYZINormal pt;
    pt.x = cloud_->at(i).x;
    pt.y = cloud_->at(i).y;
    pt.z = cloud_->at(i).z;
    pt.normal_x = normalVectors_.at(i).x();
    pt.normal_y = normalVectors_.at(i).y();
    pt.normal_z = normalVectors_.at(i).z();
    pt.curvature = curvatures_.at(i);
    pt.intensity = classProbabilities_.at(i);
    points->push_back(pt);
  }
  return points;
}

std::vector<double> BoWModel::computeSimilarities(const histogram &h) {
  NUKLEI_TRACE_BEGIN();

  {
    Stopwatch sw("Computing descriptors ");
    sw.setMinLogLevel(Log::INFO);
  }

  Stopwatch sw("Computing similarities ");
  sw.setMinLogLevel(Log::INFO);

  std::vector<double> hSims(cloud_->size(), 0);
  ProgressIndicator pi(discs_.size(), "Computing similarities ");
#ifdef _OPENMP
#pragma omp parallel for
#endif
  for (unsigned disc_i = 0; disc_i < discs_.size(); ++disc_i) {
    if (useROI_) {
      Eigen::Vector3d diff = roiCenter_ - discs_.at(disc_i).getPoint();
      double d = diff.squaredNorm();
      if (d > roiRadius_ * roiRadius_)
        continue;
    }

    double s = discs_.at(disc_i).getHistogram().kernel(h);
#ifdef _OPENMP
#pragma omp critical(lemg_compute_similarities)
#endif
    {
      hSims.at(discs_.at(disc_i).getPointId()) = s;
      pi.inc();
    }
  }
  sw.lap("Computing similarities");
  sw.split("Computing similarities");

  writeLabels("/tmp/x", hSims);

  return hSims;

  NUKLEI_TRACE_END();
}

std::vector<double> BoWModel::computeSimilarities(const int pointId) {
  NUKLEI_TRACE_BEGIN();

  {
    Stopwatch sw("Computing descriptors ");
    sw.setMinLogLevel(Log::INFO);
  }

  histogram h;
  for (unsigned disc_i = 0; disc_i < discs_.size(); ++disc_i) {
    if (discs_.at(disc_i).getPointId() == pointId) {
      h = discs_.at(disc_i).getHistogram();
      break;
    }
  }
  if (h.size() == 0) {
    NUKLEI_LOG("Could not find point");
    return std::vector<double>(cloud_->size(), -1);
  } else
    return computeSimilarities(h);

  NUKLEI_TRACE_END();
}

const Disc &BoWModel::getDisc(const unsigned id) const {
  for (const auto &i : discs_) {
    if (i.getPointId() == id)
      return i;
  }
  NUKLEI_THROW("Disc " << id << " not found.");
}

const Disc &BoWModel::getDisc(const double x, const double y,
                              const double z) const {
  Eigen::Vector3d p(x, y, z), idv;
  int id = -1;

  for (const auto &i : discs_) {
    if (id < 0) {
      id = i.getPointId();
      idv = i.getPoint();
    } else {
      if ((i.getPoint() - p).norm() < (idv - p).norm()) {
        id = i.getPointId();
        idv = i.getPoint();
      }
    }
  }
  return getDisc(id);
}

inline void nuklei_logger(const std::string &str) {
  // NUKLEI_LOG(str);
}

double support_indicator(const double &depthImageRelativeSupportSurface) {
  const double a = .65, b = .85;
  return std::min(
      1., std::max(0., (depthImageRelativeSupportSurface - a) / (b - a)));
}

double flatness_indicator(const cv::Mat &depthImage) {
  double score = 0;
  int count = 0;

  double minVal;
  double maxVal;
  cv::Point minLoc;
  cv::Point maxLoc;
  // maxVal is not usable, bc it will often be equal to 255
  cv::minMaxLoc(depthImage, &minVal, &maxVal, &minLoc, &maxLoc);

  for (auto i = depthImage.begin<int32_t>(); i != depthImage.end<int32_t>();
       ++i) {
    if (*i == 255)
      continue;
    count++;
    score += (*i - minVal);
  }

  score /= count;

  const double a = 50, b = 0; // mstt: 15
  return std::min(1., std::max(0., (score - a) / (b - a)));
}

std::vector<double> BoWModel::testImageModel(const std::string &keras_model) {
  Stopwatch sw("Testing ");
  sw.setMinLogLevel(Log::INFO);

  sw.lap("Descriptors");

  ProgressIndicator pi(discs_.size(), "Classifying ");
  classProbabilities_ = std::vector<double>(cloud_->size(), -1);
  normalVectors_ =
      std::vector<Eigen::Vector3d>(cloud_->size(), Eigen::Vector3d::Zero());
  curvatures_ = std::vector<double>(cloud_->size(), -1);
  histograms_ = std::vector<histogram>(cloud_->size());

  if (keras_model != "<hardcoded>") {
    const auto model = fdeep::load_model(keras_model, true, nuklei_logger);

    const int DEPTH_IMAGE_SIZE = 32;
#ifdef _OPENMP
#pragma omp parallel for
#endif
    for (unsigned disc_i = 0; disc_i < discs_.size(); ++disc_i) {
      cv::Mat depth = discs_.at(disc_i).getDepthImage();
      {
        cv::Mat tmp;
        depth.convertTo(tmp, CV_32F, 1. / 128., -1.);
        depth = tmp;
      }
      {
        cv::Mat tmp;
        cv::resize(depth, tmp, cv::Size(DEPTH_IMAGE_SIZE, DEPTH_IMAGE_SIZE), 0,
                   0, cv::INTER_LINEAR);
        depth = tmp;
      }

      cv::Mat color = discs_.at(disc_i).getColorImage();
      {
        cv::Mat tmp;
        cv::cvtColor(color, tmp, cv::COLOR_BGR2RGB);
        color = tmp;
      }
      {
        cv::Mat tmp;
        color.convertTo(tmp, CV_32F, 1. / 128., -1.);
        color = tmp;
      }

      std::vector<float_t> depth_array(depth.begin<float_t>(),
                                       depth.end<float_t>());
      auto depth_input = fdeep::tensor(
          fdeep::tensor_shape(DEPTH_IMAGE_SIZE, DEPTH_IMAGE_SIZE, 1),
          std::move(depth_array));
      // typedef cv::Vec<float, 3> float_rgb_t;
      // std::vector<float_t> color_array(color.begin<float_t>(),
      // color.end<float_t>()); auto color_input =
      // fdeep::tensor(fdeep::tensor_shape(IMAGE_SIZE, IMAGE_SIZE, 3),
      //                                  std::move(color_array));
      // Sticking to depth-only until color is shown to help substantially:
      const auto outputs = nuklei::as_const(model).predict({depth_input});
      NUKLEI_ASSERT(outputs.size() == 1);
      const auto output_shape = outputs.front().shape();
      NUKLEI_ASSERT(output_shape.volume() == 2);
      double p = outputs.front().get(0, 0, 0, 0, 0);
      p = 1. - p;
      NUKLEI_ASSERT(0 <= p && p <= 1);
#ifdef _OPENMP
#pragma omp critical(lemg_test_classifier)
#endif
      {
        classProbabilities_.at(discs_.at(disc_i).getPointId()) = p;
        normalVectors_.at(discs_.at(disc_i).getPointId()) =
            discs_.at(disc_i).getNormal();
        curvatures_.at(discs_.at(disc_i).getPointId()) =
            discs_.at(disc_i).getCurvature();
        // histograms_.at(discs_.at(disc_i).getPointId()) =
        // discs_.at(disc_i).getHistogram();
        pi.inc();
      }
    }
  } else {
#ifdef _OPENMP
#pragma omp parallel for
#endif
    for (unsigned disc_i = 0; disc_i < discs_.size(); ++disc_i) {
      double p = (flatness_indicator(discs_.at(disc_i).getDepthImage()) *
                  support_indicator(
                      discs_.at(disc_i).getDepthImageRelativeSupportSurface()));
      NUKLEI_ASSERT(0 <= p && p <= 1);
#ifdef _OPENMP
#pragma omp critical(lemg_test_classifier)
#endif
      {
        classProbabilities_.at(discs_.at(disc_i).getPointId()) = p;
        normalVectors_.at(discs_.at(disc_i).getPointId()) =
            discs_.at(disc_i).getNormal();
        curvatures_.at(discs_.at(disc_i).getPointId()) =
            discs_.at(disc_i).getCurvature();
        // histograms_.at(discs_.at(disc_i).getPointId()) =
        // discs_.at(disc_i).getHistogram();
        pi.inc();
      }
    }
  }
  sw.lap("Classification");
  sw.split("testing");

  return classProbabilities_;
}

} // namespace lemg
