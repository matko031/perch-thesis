Model trained on an RPM reconstruction of the 107 rockwall, with Aaron's training data:

cd /work-data/lemur/2016-12-15--rockwall-tags/annot/rock_wall_mesh_no_limbs_AJP-flipped

file=$( basename $( pwd ) )
input_file=../$file

nq=16
nc=32
elenorm=
s=10000

lemg bcolor $input_file.ply -l $file.labels $file-nocolor.pcd

lemg bcode --nq $nq --nc $nc -s $s $elenorm -c $file.codebook $file-nocolor.pcd
lemg btrain --nq $nq --nc $nc -s $s $elenorm -o $file.classifier -c $file.codebook -l $file.labels $file-nocolor.pcd
lemg bclass --nq $nq --nc $nc  $elenorm -o $file.estimates -c $file.codebook --classifier $file.classifier $file-nocolor.pcd --grasps $file.grasps.pcd
