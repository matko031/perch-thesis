/**
 * Copyright (C) 2016, California Institute of Technology.
 * All Rights Reserved. U.S. Government Sponsorship Acknowledged.
 * Any commercial use must be negotiated with the Office of
 * Technology Transfer at the California Institute of Technology.
 *
 * This software may be subject to U.S. export control laws. By
 * accepting this software, the user agrees to comply with all
 * applicable U.S. export laws and regulations. User has the
 * responsibility to obtain export licenses, or other export authority
 * as may be required before exporting such information to foreign
 * countries or providing access to foreign persons.
 */

/**
   @file  graspability_client.cpp
   @brief Requests grasp candidates within a bounding sphere from the
   graspability server

   @date  05/12/2016

   @author Jeremy Nash (jeremy.nash@jpl.nasa.gov)
   Mobility and Robotic Systems (347), JPL
*/

#include <geometry_msgs/Vector3.h>
#include <graspability_server/GrabCandidates.h>
#include <ros/ros.h>
#include <cstdlib>

int main(int argc, char *argv[]) {
  ros::init(argc, argv, "graspability_client");
  ros::NodeHandle nh;
  ros::ServiceClient client =
      nh.serviceClient<graspability_server::GrabCandidates>(
          "/graspability_server/grab_candidates");
  graspability_server::GrabCandidates srv;

  srv.request.center.x = 0.5;  // units are in meters in world frame
  srv.request.center.y = 0.5;  // units are in meters in world frame
  srv.request.center.z = 0.2;  // units are in meters in world frame
  srv.request.radius = 0.5;    // 0.5m radius

  if (client.call(srv)) {
    ROS_INFO_STREAM("Received " << srv.response.grasp_candidates.size()
                                << " grasp candidates.");
    for (unsigned int i = 0; i < srv.response.grasp_candidates.size(); i++) {
      ROS_INFO_STREAM(
          i << ". "
            << "x: " << srv.response.grasp_candidates[i].point.x << ", "
            << "y: " << srv.response.grasp_candidates[i].point.y << ", "
            << "z: " << srv.response.grasp_candidates[i].point.z << ", "
            << "nx: " << srv.response.grasp_candidates[i].normal.x << ", "
            << "ny: " << srv.response.grasp_candidates[i].normal.y << ", "
            << "nz: " << srv.response.grasp_candidates[i].normal.z);
    }
  } else {
    ROS_WARN_STREAM("Service call failed.");
  }

  return 0;
}
