#include <iostream>
#include <ros/ros.h>
#include <actionlib/client/simple_action_client.h>
#include <graspability_server/DetectPerchSiteAction.h>

int main(int argc, char** argv){

    ros::init(argc, argv, "detect_perching_site_client");

    actionlib::SimpleActionClient<graspability_server::DetectPerchSiteAction> client("detect_perch_site", true);

	std::cout << "Waiting for server" << std::endl;
	client.waitForServer();
	std::cout << "Server up" << std::endl;

	graspability_server::DetectPerchSiteGoal goal;
	client.sendGoal(goal);
	std::cout << "Sent goal" << std::endl;

	bool finished = client.waitForResult(ros::Duration(30.0));

	if (finished){
		const auto& res = client.getResult();
		std::cout << "Result: " << res << std::endl;
	} else{

		std::cout << "Did not finish in time" << std::endl;
	}

	return 0;
}
