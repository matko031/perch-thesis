/**
 * Copyright (C) 2016, California Institute of Technology.
 * All Rights Reserved. U.S. Government Sponsorship Acknowledged.
 * Any commercial use must be negotiated with the Office of
 * Technology Transfer at the California Institute of Technology.
 *
 * This software may be subject to U.S. export control laws. By
 * accepting this software, the user agrees to comply with all
 * applicable U.S. export laws and regulations. User has the
 * responsibility to obtain export licenses, or other export authority
 * as may be required before exporting such information to foreign
 * countries or providing access to foreign persons.
 */

/**
   @file  graspability_server.cpp
   @brief Determines grasp candidates from the sensor point cloud data

   @date  05/12/2016

   @author Jeremy Nash (jeremy.nash@jpl.nasa.gov)
   Mobility and Robotic Systems (347), JPL
*/

#include <dynamic_reconfigure/server.h>
#include <geometry_msgs/PointStamped.h>
#include <graspability_server/GrabCandidates.h>
#include <graspability_server/GraspCandidate.h>
#include <graspability_server/InsertScans.h>
#include <interactive_markers/interactive_marker_server.h>
#include <pcl/conversions.h>
#include <pcl/point_types.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl_ros/transforms.h>
#include <sensor_msgs/PointCloud2.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_listener.h>
#include "ros/ros.h"

//#include <pcl/visualization/cloud_viewer.h>

// STL headers
#include <algorithm>
#include <iostream>
#include <limits>
#include <vector>

// Eigen
#include <Eigen/Dense>
#include <Eigen/Eigenvalues>

// Fast voxel map
#include <fast_voxel_map/fast_voxel_map.h>

// Markers
#include <visualization_msgs/MarkerArray.h>

#include <std_srvs/Empty.h>

using namespace std;
using namespace Eigen;

bool first = true;
unsigned int prev_number_markers;
// put this into the launch file
unsigned int graspability_marker_offset = 100;

visualization_msgs::MarkerArray GrabCandidatesRequestAndResponse2MarkerArray(
    graspability_server::GrabCandidates::Request &req,
    graspability_server::GrabCandidates::Response &res, std::string frame_id,
    ros::Time timestamp) {
  visualization_msgs::MarkerArray marker_array;
  visualization_msgs::Marker marker;
  marker.points.resize(2);
  geometry_msgs::Point point_tail, point_tip;
  double arrow_length = .03;
  ros::Time time_now = ros::Time::now();

  marker.header.frame_id = frame_id;
  marker.header.stamp = timestamp;
  marker.id = graspability_marker_offset;
  marker.type = visualization_msgs::Marker::SPHERE;
  marker.action = visualization_msgs::Marker::ADD;
  marker.pose.position.x = req.center.x;
  marker.pose.position.y = req.center.y;
  marker.pose.position.z = req.center.z;
  marker.pose.orientation.w = 1;
  marker.scale.x = 2 * req.radius;
  marker.scale.y = 2 * req.radius;
  marker.scale.z = 2 * req.radius;
  marker.color.a = 0.3;
  marker.color.r = 0.5;
  marker.color.g = 0.5;
  marker.color.b = 1.0;
  marker_array.markers.push_back(marker);

  marker.pose.position.x = 0;
  marker.pose.position.y = 0;
  marker.pose.position.z = 0;

  ROS_INFO_STREAM("Number of candidates: " << res.grasp_candidates.size());
  for (size_t i = 0; i < res.grasp_candidates.size(); ++i) {
    marker.header.frame_id = frame_id;
    marker.header.stamp = timestamp;
    marker.id = i + graspability_marker_offset + 1;
    marker.type = visualization_msgs::Marker::ARROW;
    marker.action = visualization_msgs::Marker::ADD;
    point_tail.x = res.grasp_candidates[i].point.x;
    point_tail.y = res.grasp_candidates[i].point.y;
    point_tail.z = res.grasp_candidates[i].point.z;
    point_tip.x = res.grasp_candidates[i].point.x +
                  arrow_length * res.grasp_candidates[i].normal.x;
    point_tip.y = res.grasp_candidates[i].point.y +
                  arrow_length * res.grasp_candidates[i].normal.y;
    point_tip.z = res.grasp_candidates[i].point.z +
                  arrow_length * res.grasp_candidates[i].normal.z;
    marker.points[0] = point_tail;
    marker.points[1] = point_tip;
    marker.scale.x = .002;  // shaft dia
    marker.scale.y = .003;  // head dia
    marker.scale.z = .01;   // head_len
    marker.color.a = 1.0;
    marker.color.r = 1.0;
    marker.color.g = 0.0;
    marker.color.b = 0.0;
    marker_array.markers.push_back(marker);
  }

  // Delete previous arrows
  if (first) {
    first = false;
  } else {
    if (prev_number_markers > res.grasp_candidates.size()) {
      for (size_t i = res.grasp_candidates.size(); i < prev_number_markers;
           i++) {
        marker.header.frame_id = "odom_combined";
        marker.header.stamp = time_now;
        marker.id = i + graspability_marker_offset;
        marker.type = visualization_msgs::Marker::ARROW;
        marker.action = visualization_msgs::Marker::DELETE;
        marker_array.markers.push_back(marker);
      }
    }
  }

  prev_number_markers = res.grasp_candidates.size();

  return marker_array;
}

class Graspability {
 public:
  Graspability()
      : voxel_map_(0.02, 100000),
        pnh_("~")  // 2cm resolution, 20,000 voxels?
  {
    grab_service_ = pnh_.advertiseService("grab_candidates",
                                          &Graspability::grabCandidates, this);
    insert_service_ =
        pnh_.advertiseService("insert_scans", &Graspability::insertScans, this);
    cloud_pub_ =
        pnh_.advertise<sensor_msgs::PointCloud2>("graspability_cloud", 1, true);
    normal_arrow_pub_ = pnh_.advertise<visualization_msgs::MarkerArray>(
        "footprint_normal_arrows", 1, true);
    reset_service_ =
        pnh_.advertiseService("reset", &Graspability::resetSrv, this);

    ros::NodeHandle nh_;
  }
  ~Graspability() {}

  bool grabCandidates(graspability_server::GrabCandidates::Request &req,
                      graspability_server::GrabCandidates::Response &res) {
    ROS_INFO_STREAM("Received service request.");

    // Find all points within the radius around center_x,y,z
    int center_x = voxel_map_.FloatToIntIndex(req.center.x);
    int center_y = voxel_map_.FloatToIntIndex(req.center.y);
    int center_z = voxel_map_.FloatToIntIndex(req.center.z);
    int steps = voxel_map_.FloatToIntIndex(req.radius) +
                1;  // +1 voxel for extra margin

    graspability_server::GraspCandidate gc;

    int num_valid_voxels = 0;

    for (int i = -steps; i < (steps + 1); i++) {
      for (int j = -steps; j < (steps + 1); j++) {
        for (int k = -steps; k < (steps + 1); k++) {
          VoxelPtr voxel =
              voxel_map_.AccessVoxel(center_x + i, center_y + j, center_z + k);
          if (voxel) {
            num_valid_voxels++;

            gc.point.x = voxel_map_.IntIndexToFloat(voxel->x);
            gc.point.y = voxel_map_.IntIndexToFloat(voxel->y);
            gc.point.z = voxel_map_.IntIndexToFloat(voxel->z);

            // compute distance to center, and reject if exceeds radius
            if (sqrt(pow(gc.point.x - req.center.x, 2) +
                     pow(gc.point.y - req.center.y, 2) +
                     pow(gc.point.z - req.center.z, 2)) > req.radius) {
              continue;
            }

            gc.normal.x = voxel->normal[0];
            gc.normal.y = voxel->normal[1];
            gc.normal.z = voxel->normal[2];

            gc.variance = voxel->variance;

            gc.max_height_difference = -1;
            gc.probability_grasp_success = -1;
            gc.predicted_max_normal_force = -1;
            gc.predicted_max_tangent_force = -1;
            res.grasp_candidates.push_back(gc);
          }
        }
      }
    }

    visualization_msgs::MarkerArray marker_array =
        GrabCandidatesRequestAndResponse2MarkerArray(req, res, "odom_combined",
                                                     ros::Time::now());

    normal_arrow_pub_.publish(marker_array);
    ROS_INFO("published marker array");

    ROS_INFO_STREAM("Discovered "
                    << num_valid_voxels << " valid voxels within a radius of "
                    << req.radius << "m radius around the point "
                    << req.center.x << ", " << req.center.y << ", "
                    << req.center.z);

    return true;
  }

  bool resetSrv(std_srvs::Empty::Request &req,
                std_srvs::Empty::Response &resp) {
    ROS_INFO_STREAM("Graspability received a reset service call");
    voxel_map_.Reset();
    return true;
  }

  bool insertScans(graspability_server::InsertScans::Request &req,
                   graspability_server::InsertScans::Response &res) {
    tf::StampedTransform odom2baselink_stamped;
    tf::transformStampedMsgToTF(req.odom2baselink, odom2baselink_stamped);
    tf::Transform odom2baselink = odom2baselink_stamped;

    float max_range = 2.0;
    for (unsigned int i = 0; i < req.laser_scans.size(); i++) {
      tf::StampedTransform body2hokuyo_stamped;
      tf::transformStampedMsgToTF(req.body2hokuyo[i], body2hokuyo_stamped);
      tf::Transform body2hokuyo = body2hokuyo_stamped;

      // Transform and convert point cloud
      pcl::PointCloud<pcl::PointXYZ> pc_input;
      pcl::PointCloud<pcl::PointXYZ> cloud;
      pcl::fromROSMsg(req.laser_scans[i], pc_input);
      pcl_ros::transformPointCloud(pc_input, cloud,
                                   odom2baselink * body2hokuyo);

      // note: this range check assumes robot is at the origin; we need to
      // update this logic to reference body frame for when robot starts moving
      for (unsigned int i = 0; i < cloud.size(); i++) {
        // Check range before transforming by odom2baselink
        float range = sqrt(pc_input.points[i].x * pc_input.points[i].x +
                           pc_input.points[i].y * pc_input.points[i].y +
                           pc_input.points[i].z * pc_input.points[i].z);
        if (range < max_range) {
          voxel_map_.AddValue(cloud.points[i].x, cloud.points[i].y,
                              cloud.points[i].z);
        }
      }
    }

    ROS_INFO_STREAM("Number of voxels: " << voxel_map_.NumVoxels());
    ROS_INFO_STREAM("Calculating statistics...");

    VoxelPtr center_voxel;
    pcl::PointCloud<pcl::PointXYZINormal> normal_cloud;
    voxel_map_.Rewind();
    while (voxel_map_.NextVoxel(center_voxel)) {
      if (center_voxel->num_points < 2) {
        continue;
      }

      int num_points = 0;
      Vector3f sum_X;
      Matrix3f sum_XXT;
      sum_X.setZero();
      sum_XXT.setZero();
      // With 2cm voxels, this searches a cube w/ side length 0.02m + (6 * 2 *
      // 0.02m) = 0.26m, which is in between the lemur foot diameter (.23m) and
      // and the mesh foot diameter (.29m)
      for (int i = -6; i < 7; i++) {
        for (int j = -6; j < 7; j++) {
          for (int k = -6; k < 7; k++) {
            VoxelPtr voxel = voxel_map_.AccessVoxel(
                center_voxel->x + i, center_voxel->y + j, center_voxel->z + k);
            if (voxel) {
              if (voxel->num_points > 1) {
                num_points += voxel->num_points;
                sum_X += voxel->sum_X;
                sum_XXT += voxel->sum_XXT;
              }
            }
          }
        }
      }

      // If we have at least 5 points in a lemur foot sized area, compute the
      // statistics
      if (num_points > 4) {
        // Vector3f local_mu = center_voxel->sum_X /
        // static_cast<float>(center_voxel->num_points);

        // Compute the covariance
        Vector3f mu = sum_X / static_cast<float>(num_points);
        // Matrix3f covariance = (sum_XXT / static_cast<float>(num_points)) -
        // (mu * mu.transpose());

        Matrix3f covariance =
            (sum_XXT - 2 * sum_X * mu.transpose() +
             static_cast<float>(num_points) * (mu * mu.transpose())) /
            static_cast<float>(num_points);

        // Compute the smallest eigenvector and eigenvalue for surface normal
        // and variance
        SelfAdjointEigenSolver<Matrix3f> eigensolver;
        eigensolver.compute(covariance);
        // Eigenvalues are sorted in increasing order, so [0] gives us the
        // lowest eigenvalue
        float lambda = eigensolver.eigenvalues()[0];
        Vector3f eigenvector = eigensolver.eigenvectors().col(0);
        // Reset sum_x, sum_xxt?

        // Flip normal so that it points towards the center of the robot
        // Assuming that the robot frame is centered at 0,0,0 in this point
        // cloud
        if (eigenvector.dot(-1 * mu) < 0) {
          eigenvector *= -1;
        }

        // Add point to XYZINormal point cloud
        pcl::PointXYZINormal pt;
        pt.x = voxel_map_.IntIndexToFloat(center_voxel->x);
        pt.y = voxel_map_.IntIndexToFloat(center_voxel->y);
        pt.z = voxel_map_.IntIndexToFloat(center_voxel->z);
        pt.normal[0] = eigenvector[0];
        pt.normal[1] = eigenvector[1];
        pt.normal[2] = eigenvector[2];
        if (lambda < 0) {
          ROS_WARN_STREAM(
              "Computed a negative smallest eigenvalue during gripper-scale "
              "covariance estimation: "
              << lambda);
          lambda = fabs(lambda);
        }

        pt.intensity = sqrt(lambda);
        normal_cloud.points.push_back(pt);

        // Add normal to center voxel (so the interactive marker can look it up)
        center_voxel->normal[0] = eigenvector[0];
        center_voxel->normal[1] = eigenvector[1];
        center_voxel->normal[2] = eigenvector[2];
        center_voxel->variance = lambda;
      }
    }

    // Publish graspability cloud
    sensor_msgs::PointCloud2 ros_cloud;
    pcl::toROSMsg(normal_cloud, ros_cloud);
    ros_cloud.header.stamp = ros::Time::now();
    ros_cloud.header.frame_id = "odom_combined";
    cloud_pub_.publish(ros_cloud);
    return true;
  }

  void spin() { ros::spin(); }

 private:
  FastVoxelMap voxel_map_;
  ros::ServiceServer grab_service_;
  ros::ServiceServer insert_service_;
  ros::NodeHandle pnh_;
  ros::Publisher cloud_pub_;
  ros::Publisher normal_arrow_pub_;
  ros::ServiceServer reset_service_;
};

int main(int argc, char *argv[]) {
  ros::init(argc, argv, "graspability_server");
  Graspability graspability;
  graspability.spin();
  return 0;
}
