## Location 1:

1. Start the GUI and save all grabs in `grabs/s1/` dir by pressing `w`:
`python main_RGBD.py -o grabs/s1/`

1. After several grabs on same location press `q` to stop.

1. Respond to questions in terminal to create a `notes.txt` file

1. Convert the `.ply` files to `pcd` format:
`python convertToPCD.py -d grabs/s1/`

## Location 2:

#### Repeat the same steps

## Location 3:
...




