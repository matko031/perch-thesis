import cv2
import pyrealsense2 as rs
import argparse
from pathlib import Path
import numpy as np
import pickle
import os

def parse_arguments():

    parser = argparse.ArgumentParser(description='Graphical tool to facilitate\
            data collection using read sense camera')
    parser.add_argument('-o', '--out', required=True, type=str, metavar='',
            help='Output location of pcd files.' )

    return parser.parse_args()

def get_grab_metadata(out_dir):
    while True:
        success = input("Was grab successful? ").lower()
        rating = input("How would you rate grasp from 1 to 5 (1 total failure, 5 total success)? ")
        comment = input("Write any additional comments here ")

        if success not in ("y", "n", "yes", "no") :
            print("Success has to be y/n/yes/no ")
        elif not rating.isdigit() or int(rating) < 1 or int(rating)>5:
            print("Rating has to be an integer, minimum 1 and maximum 5")
        else:
            break

    f = open(Path(out_dir) / "notes.txt", "w")
    if success in ("y", "yes"):
        f.write("success\n")
    else:
        f.write("fail\n")
    f.write(rating + "\n")
    if comment != "":
        f.write(comment + "\n")


def main(out_dir):

    if not os.path.isdir(out_dir):
        os.makedirs(out_dir)

    # Configure depth and color streams
    pipeline = rs.pipeline()
    config = rs.config()
    align = rs.align(rs.stream.color) 
    pc = rs.pointcloud()

    align_to = rs.stream.color
    align = rs.align(align_to)

    # Get device product line for setting a supporting resolution
    pipeline_wrapper = rs.pipeline_wrapper(pipeline)
    pipeline_profile = config.resolve(pipeline_wrapper)
    device = pipeline_profile.get_device()
    device_product_line = str(device.get_info(rs.camera_info.product_line))

    found_rgb = False
    for s in device.sensors:
        if s.get_info(rs.camera_info.name) == 'RGB Camera':
            found_rgb = True
            break
    if not found_rgb:
        print("The demo requires Depth camera with Color sensor")
        exit(0)

    config.enable_stream(rs.stream.depth, 640, 480, rs.format.z16, 30)

    if device_product_line == 'L500':
        config.enable_stream(rs.stream.color, 960, 540, rs.format.bgr8, 30)
    else:
        config.enable_stream(rs.stream.color, 640, 480, rs.format.bgr8, 30)

    # Start streaming
    pipeline.start(config)

    counter = 0

    colorizer = rs.colorizer()

    try:
        while True:

            # Wait for a coherent pair of frames: depth and color
            frames = pipeline.wait_for_frames()
            depth_frame = frames.get_depth_frame()
            color_frame = frames.get_color_frame()
            if not depth_frame or not color_frame:
                continue

            # Convert images to numpy arrays
            depth_image = np.asanyarray(depth_frame.get_data())
            color_image = np.asanyarray(color_frame.get_data())

            # Apply colormap on depth image (image must be converted to 8-bit per pixel first)
            # depth_colormap = cv2.applyColorMap(cv2.convertScaleAbs(depth_image, alpha=0.03), cv2.COLORMAP_JET)

            # depth_colormap_dim = depth_colormap.shape
            color_colormap_dim = color_image.shape

            # If depth and color resolutions are different, resize color image to match depth image for display
            #if depth_colormap_dim != color_colormap_dim:
            #    resized_color_image = cv2.resize(color_image, dsize=(depth_colormap_dim[1], depth_colormap_dim[0]), interpolation=cv2.INTER_AREA)
            #    frame = np.hstack((resized_color_image, depth_colormap))
            #else:
            #    frame = np.hstack((depth_colormap, color_image))

            frame = color_image
            x_center = depth_image.shape[1] // 2
            y_center = depth_image.shape[0] // 2
            mean_depth_value = round(depth_image[y_center-20:y_center+20, x_center-20:x_center+20].mean(), 2)
            cv2.putText(frame, "Average depth value: " + str(mean_depth_value), 
                    (0,20), cv2.FONT_HERSHEY_TRIPLEX, 0.5, (0,0,255), 1)

            cv2.line(frame, (x_center, y_center-20), (x_center, y_center+20), (255,0,255), thickness=3)  
            cv2.line(frame, (x_center-20, y_center), (x_center+20, y_center), (255,0,255), thickness=3)  


            # Show images
            cv2.namedWindow('RealSense', cv2.WINDOW_AUTOSIZE)
            cv2.imshow('RealSense', frame)

            key = cv2.waitKey(1)


            if key == 113: # q
                cv2.destroyAllWindows()
                get_grab_metadata(out_dir) 
                break


            elif key == 119: # w

                frame[:,:,:] = 255
                cv2.waitKey(200)
                cv2.imshow("RealSense", frame)
                cv2.waitKey(200)

                filename = f"{out_dir}/g{counter}.ply"

                # Get aligned frames
                aligned_depth_frame = frames.get_depth_frame()  
                color_frame = frames.get_color_frame()
                color_image = np.array(color_frame.get_data())

                frames = pipeline.wait_for_frames()
                aligned_frames = align.process(frames)
                aligned_depth_frame = aligned_frames.get_depth_frame()
                color_frame = aligned_frames.get_color_frame()
                pc.map_to(color_frame)
                points = pc.calculate(aligned_depth_frame)

                print("Saving to ply...")
                points.export_to_ply(filename, color_frame)
                print("Done")

                counter += 1

    finally:
        # Stop streaming
        pipeline.stop()

if __name__ == "__main__":
    args=parse_arguments()
    main(args.out)

