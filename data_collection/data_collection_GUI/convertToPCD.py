import open3d as o3d
import os
import argparse

def parse_arguments():

    parser = argparse.ArgumentParser(description='Convert ply files to pcd')
    parser.add_argument('-d', '--dir', required=True, type=str, metavar='',
            help='Location of ply files.' )

    return parser.parse_args()


if __name__ == "__main__":
    args=parse_arguments()
    
    for file in os.listdir(args.dir):
        if file.endswith(".ply"):
            pcd = o3d.io.read_point_cloud(args.dir + "/" + file)
            o3d.io.write_point_cloud(args.dir + "/" + file[:-4]+".pcd", pcd)

