### Data needed
Need for more *real* data from actual grabs. Currently available data of the sort is located under `jpl_data/work-data/perch/2020-09-10--pisgah/rs/orig` in the shared `perch.tgz`.  Here is the ideal file structure we need:

```
test_data/
	grab_site/ #not necessary if all grabs are done at the same site
		grab{location_index}/
			grab{grab_index}.pcd
			notes.txt
```

The idea is that there is a sequence of grabs executed at different locations of the test wall. For each location, drone/gripper should execute each grab from same position with minor disturbances in radius of 5mm. For each grab a separate pcd file is created. This is needed in order to see the influence of minor movements that will happen during the operation in practice.  Each pcd *snapshot* should be taken at 40cm away from the wall.  In the file structure, `location_index` refers to the index of the location on the wall (i.e., 1st location, 2nd location, etc.) and `grab_index` refers to the index of the grab at each location (1st grab, 2nd grab, etc.).

`notes.txt` file should contain rating for each grab executed at the location. Each rating is a number between 1 and 4 (1 and 4 included) and should be put in new line. As an example, `notes.txt` file at location where 5 grabs were executed could look like this:

```
1
1
2
1
2
```

The ratings are to be interpreted as follows:

- 1: grab failed completely
- 2: grab failed, but managed to latch partially
- 3: grab successful, but not completely stable
- 4: grab succeeded completely



We would like to have 5 *good* locations, 10 *bad* locations and 15 *in between* locations. At each location execute 5 grabs. 

Total grabs: 150 Total time: 150 grabs * 3 min/grab = 450min



In the list below you can find examples of good, bad and edge cases grabs. The files these lists refer to are sent together with this document. 



**Good grabs:**

- site1_g5
  - slightly convex with some irregularities which the gripper spines can latch onto

- site2_g3 
  - lots of tiny irregularities the gripper spines can latch onto

- site2_g7
  - slightly convex with some irregularities which the gripper spines can latch onto




**Bad grabs:**

- site4_g5 
  - too smooth structure which lacks features the spines can latch onto despite its convexity

- at the moment we only have real data from *bad* grab just in this one case. When you take new data, we would also like to have several *bad grabs* on completely flat vertical surface.



**Edge cases:**

- site1_g7
  - not completely smooth irregularities, but no good support either

- site1_g8
  - not completely smooth irregularities, but no good support either

- site3_g1
  - not completely smooth irregularities, but no good support either

